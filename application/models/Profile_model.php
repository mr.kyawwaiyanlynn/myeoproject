<?php
    /**
    * 
    */
    class Profile_model extends CI_Model
    {
        
        function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        function update($name,$location,$education,$sentence,$image,$cv, $letter, $video)
        {
             //var_dump($name,'ji',$location,$education,$sentence,$image,$cv, $letter, $video);die();
            $array  = array('location' => $location,'education'=>$education,'sentence'=>$sentence,'picture'=>$image,
                             'cv'=>$cv,'cover_letter'=>$letter,'intro_video'=>$video );
            //var_dump($array);die();
            $this->db->where('name',$name);
            $res = $this->db->update('user',$array);
            return $res ;
        }

        function check($id,$oppcheck,$skillcheck)
        {
            // var_dump($id,$oppcheck,$skillcheck);die();
            if(!empty($oppcheck))
            {
                foreach ($oppcheck as  $value)
                {
                    $this->db->insert('user_opp',array('user_id'=>$id,'opp_id'=>$value));
                }
            }
            if(!empty($skillcheck))
            {
                foreach ($skillcheck as  $value)
                {
                    $this->db->insert('user_skill',array('user_id'=>$id,'skill_id'=>$value));
                }
            }
             return true;
            
        }

            
        function getall()
        {
            $all = $this->db->select('user');
            return $all;
        }

        function get($decrypt_id)
        {
            // var_dump($decrypt_id);die();
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user_id',$decrypt_id);
            $res=$this->db->get();
            // var_dump($res->row(),'lllll');die();
            return $res->row();
        }

        function cget($user_id)
        {
            // var_dump($user_id);die();
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user_id',$user_id);
            $res=$this->db->get();
            // var_dump($res->result(),'lllll');die();
            return $res->row();
        }
        
        function edit($decrypt_id,$name,$location,$education,$sentence,$image,$cv, $letter, $video)
        {
            // var_dump($cv,$letter,$video);die();
            $array  = array('name'=>$name,'location' => $location,'education'=>$education,'sentence'=>$sentence,'picture'=>$image,'cv'=>$cv,'cover_letter'=>$letter,'intro_video'=>$video );    

                
            $this->db->where('user_id',$decrypt_id);
            $edit = $this->db->update('user',$array);
            return $edit;
        }


        function skill_chk($decrypt_id)
        {
            $this->db->select('*');
            $this->db->from('user_skill');
            $this->db->join('skill','user_skill.skill_id=skill.skill_id');
            $this->db->where('user_id',$decrypt_id);
            $res = $this->db->get();
            // var_dump($res->result());
            return $res->result();
        }

        function skill_chk2($user_id)
        {
            $this->db->select('*');
            $this->db->from('user_skill');
            $this->db->join('skill','user_skill.skill_id=skill.skill_id');
            $this->db->where('user_id',$user_id);
            $res = $this->db->get();
            // var_dump($res->result());
            return $res->result();
        }

        function skill_de($user_id)
        {
            $this->db->where('user_id',$user_id);
            $this->db->delete('user_skill');
        }

        function opp_chk($user_id)
        {
            $this->db->select('*');
            $this->db->from('user_opp');
            // $this->db->join('opportunity','user_opp.opp_id=opportunity.opp_id');
            $this->db->where('user_id',$user_id);
            $res = $this->db->get();
            // var_dump($res->result(),'.............');
            return $res->result();
        }

        function opp_chk2($user_id)
        {
            $this->db->select('*');
            $this->db->from('user_opp');
            // $this->db->join('opportunity','user_opp.opp_id=opportunity.opp_id');
            $this->db->where('user_id',$user_id);
            $res = $this->db->get();
            // var_dump($res->result(),'.............');
            return $res->result();
        }

        function opp_de($user_id)
        {
            $this->db->where('user_id',$user_id);
            $this->db->delete('user_opp');
        }

        function changepwd($user_id,$pwd)
        {
            // var_dump($user_id,$pwd);die();
            $this->db->where('user_id',$user_id);
            $this->db->update('password',$pwd);
            $res=$this->db->get();
            return $res;
        }

        function sample($oppcheck)
        {
           $this->db->select('*');
            $this->db->from('user_opp');
            $this->db->where('opp_id', $oppcheck);
            $query = $this->db->get();
    // var_dump($query->result());die();
           if($query->num_rows()>0)
           {
            // echo $this->db->last_query()."<br>";
            return $query->result();  // return an array of objects
           }
           else
           {
            return null;
           } 
        }

        function chkedit($id,$oppcheck,$skillcheck)
        {
            if($oppcheck)
            {
                foreach ($oppcheck as  $value)
                {
                    $this->db->delete('user_opp',array('user_id' =>$id));
                }
            }
            if($skillcheck)
            {
                foreach ($skillcheck as  $value)
                {
                    $this->db->delete('user_skill',array('user_id' =>$id));
                }
            }
             return true;
            
        }

        function chkin($id,$oppcheck,$skillcheck)
        {
            // var_dump($id,$oppcheck,$skillcheck);die();
            if($oppcheck)
            {
                foreach ($oppcheck as  $value)
                {
                    $this->db->insert('user_opp',array('user_id'=>$id,'opp_id'=>$value));
                }
            }
            if($skillcheck)
            {
                foreach ($skillcheck as  $value)
                {
                    $this->db->insert('user_skill',array('user_id'=>$id,'skill_id'=>$value));
                }
            }
             return true;
            
        }

        function pic($user_id)
        {
            $this->db->select('picture');
            $this->db->from('user');
            $this->db->where('user_id',$user_id);
            $res=$this->db->get();
            // var_dump($res->row());die();
            return $res->row();
        }
        function cv($user_id)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user_id',$user_id);
            $res=$this->db->get();
            return $res->row();
        }
        function letter($user_id)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user_id',$user_id);
            $res=$this->db->get();
            return $res->row();
        }
        function vd($user_id)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user_id',$user_id);
            $res=$this->db->get();
            return $res->row();
        }

    }
    ?>