<?php
/**
* 
*/
class Home_model extends CI_Model
{
  function get($id)
  {
     $this->db->select('*');
     $this->db->from('home');
     $this->db->join('opp_detail','home.opp_id=opp_detail.opp_id');
     $this->db->join('skill_detail','home.skill_id=skill_detail.skill_id');
    
     $this->db->where('home.user_id',$id);//
     $data=$this->db->get();
     return $data->result();
  }
}