<?php
class Uploadcsv_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insertCSV($data)
            {
                $this->db->insert('post', $data);
                return TRUE;
            }



    public function view_data(){
        $query=$this->db->query("SELECT *
                                 FROM post 
                                 ORDER BY id DESC
                                 ");
        return $query->result_array();
    }

}