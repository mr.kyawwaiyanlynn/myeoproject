<?php
	/**
	* 
	*/
	class Api_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		function getpost($data)
		{
			if($data=="custom")
		   {
			$this->db->select('*');
			$this->db->from('post');
			$this->db->order_by('id','desc');		
			$this->db->where('deleted',0);
			$this->db->where('save',0);			
			$data=$this->db->get();
		   }else if($data=="opp")
		   {
		   	$this->db->select('*');
			$this->db->from('post');
			$this->db->order_by('id','desc');		
			$this->db->where('deleted',0);
			$this->db->where('save',0);
			$this->db->where('opp',1);
			$data=$this->db->get();
		   }
		   else
		   {
            $this->db->select('*');
			$this->db->from('post');
			$this->db->order_by('id','desc');		
			$this->db->where('deleted',0);
			$this->db->where('save',0);
			$this->db->where('skill',1);
			$data=$this->db->get();
		   }
            // $this->db->distinct();;
			return $data->result_array();
		}
		function opptype($count,$array)
		{  
            
		  for($i=0;$i<$count;$i++)
           {   
           	   
				$this->db->select('type');
				$this->db->from('opportunity');
				$this->db->join('post_opp','post_opp.opp_id=opportunity.opp_id');
				$this->db->join('post','post.id=post_opp.post_id');
				$this->db->where('post.id',$array[$i]);	
				$this->db->where('post.deleted',0);
				$this->db->where('post.save',0);
				$this->db->where('post.skill',0);			
				$opp=$this->db->get();
				$data[$i]=$opp->result();
				 // var_dump($data);die();
				
		   }
		   
           return $data;
		}
		function skilltype($count,$array)
		{  
           
		  for($i=0;$i<$count;$i++)
           {   
           	   
				$this->db->select('type');
				$this->db->from('skill');
				$this->db->join('post_skill','post_skill.skill_id=skill.skill_id');
				$this->db->join('post','post.id=post_skill.post_id');
				$this->db->where('post.id',$array[$i]);
				$this->db->where('post.deleted',0);
				$this->db->where('post.save',0);
				$this->db->where('post.opp',0);

				$skill=$this->db->get();
				$data[$i]=$skill->result();
				
		   }
		  
           return $data;
		}
	}