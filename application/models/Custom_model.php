<?php
/**
* 
*/
class Custom_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database(); 
	}
  
	function oppdetail($opp_id,$limit=false,$offset=false,$order_by=false)
	{
		$this->db->select('*');
		$this->db->from('opportunity');
		$this->db->join('opp_detail','opportunity.opp_id=opp_detail.opp_id');
		$this->db->where('opportunity.opp_id',$opp_id);
		$opp_detail = $this->db->get();
		return $opp_detail->result();
	}

	function skilldetail($skill_id,$limit=false,$offset=false,$order_by=false)
	{
		$this->db->select('*');
		$this->db->from('skill');
		$this->db->join('skill_detail','skill.skill_id=skill_detail.skill_id');
		$this->db->where('skill.skill_id',$skill_id);
		$skill_detail = $this->db->get();
		return $skill_detail->result();
	}

	function getall($id)
	{
		$this->db->select('*');
		$this->db->from('post');		
		$this->db->where('post.id',$id);
		// $this->db->where('save',0);
		$result=$this->db->get();
		// var_dump($result->result());die();
		return $result->row();
	}
	function get_opptype($id)
	{
		$this->db->select('opportunity.type');
		$this->db->from('opportunity');		
		$this->db->join('post_opp','opportunity.opp_id=post_opp.opp_id');
		$this->db->join('post','post.id=post_opp.post_id');
		$this->db->where('post.id',$id);
		$result=$this->db->get();
		return $result->result();
	}  
	function get_skilltype($id)
	{
		$this->db->select('skill.type');
		$this->db->from('skill');		
		$this->db->join('post_skill','skill.skill_id=post_skill.skill_id');
		$this->db->join('post','post.id=post_skill.skill_id');
		$this->db->where('post.id',$id);
		$result=$this->db->get();
		return $result->result();
	} 
    function check($id)
    {
    	$this->db->select('skill');
    	$this->db->from('post');
    	$this->db->where('id',$id);
    	$result=$this->db->get();
    	return $result->row();
    }
	
//===========TKH=========== 
	function count($limit=false,$offset=false,$order_by=false)
	{

		$this->db->from('post');
		$this->db->where('save',0);
		$this->db->where('deleted',0);
		// var_dump($this->db->count_all_results());die();
		return $this->db->count_all_results();
	}
//================TKH=============for custom view to show all posts   
	function custom($limit=false,$offset=false,$order_by=false)
	{
		// var_dump($id);
		$this->db->select('*');
		$this->db->from('post');		
		$this->db->where('post.deleted','0');
		$this->db->where('post.save','0');
		// $this->db->where('post.deleted','0');
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('id',$order_by);
	    $all = $this->db->get();
        // var_dump($all->result());die();
		return $all->result_array();
		

	} 
	

    function searchdata($search,$limit=false,$offset=false,$order_by=false)
    {
    	$this->db->select('*');
    	$this->db->from('post');
    	$this->db->like('title',$search);
    	// $this->db->like('tag',serialize($search));
    	$this->db->limit($limit);
		$this->db->offset($offset);
    	$data=$this->db->get();
        // var_dump($data->result());die();
        return $data->result_array();
    }

  



	function searchcount($search)
	{
        $this->db->from('post');
        $this->db->like('title',$search);
		$this->db->where('save',0);
		$this->db->where('deleted',0);
		
		return $this->db->count_all_results();
	}

	// function video_pop($id)
	// {
	// 	$this->db->select('id,vdlink');
	// 	$this->db->from('post');
	// 	$this->db->where('id',$id);
	// 	$res = $this->db->get();
	// 	// var_dump($res->result());die();
	// 	return $res->result();
	// }

	function tag($search)
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->like('description',$search);
		$this->db->where('save',0);
		$this->db->where('deleted',0);
		$tag = $this->db->get();
		return $tag->result_array();
		// return $this->db->
	}

	   function tagdata($tags,$limit=false,$offset=false,$order_by=false)
    {
    	// var_dump($tags);die();
    	$this->db->select('*');
    	$this->db->from('post');
    	$this->db->like('tag',$tags);
    	// $this->db->like('tag',serialize($search));
    	$this->db->limit($limit);
		$this->db->offset($offset);
    	$data=$this->db->get();
        // var_dump($data->result());die();
        return $data->result_array();
    }

   



	

}
?>