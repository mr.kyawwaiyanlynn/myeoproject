<?php
/**
* 
*/
class Opportunity_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getall($id)
	{
		// var_dump($id);die();
		$this->db->select('*');
		$this->db->from('post');		
		$this->db->where('post.id',$id);
		// $this->db->where('save',0);
		$result=$this->db->get();
		// var_dump($result->result());die();
		return $result->row();
	}
	function get_type($id)
	{
		$this->db->select('opportunity.type');
		$this->db->from('opportunity');		
		$this->db->join('post_opp','opportunity.opp_id=post_opp.opp_id');
		$this->db->join('post','post.id=post_opp.post_id');
		$this->db->where('post.id',$id);
		$result=$this->db->get();
		return $result->result_array();
	}
	function left_all()
	{
		$this->db->select('*');
		$this->db->from('opportunity');
		$result=$this->db->get();
		// var_dump($result->result());die();
		return $result->result();
	}
	//============TKH=================for detail
	function oppdetail($type,$limit=false,$offset=false,$order_by=false)
	{
		
		// var_dump($type);die();
		$this->db->select('*,post.id as id');
		$this->db->from('post');
		$this->db->join('post_opp','post_opp.post_id=post.id');
		$this->db->join('opportunity','opportunity.opp_id=post_opp.opp_id');
		$this->db->where('opportunity.type',$type);
		$this->db->where('post.save',0);
		$this->db->where('post.deleted',0);
		$this->db->where('post.opp',1);
		$this->db->limit($limit);
		$this->db->offset($offset);		
		$opp_detail = $this->db->get();
		// var_dump($opp_detail->result());die();
		return $opp_detail->result();
	}
	
	function check($search)
  	{
	  	$this->db->select('*');
	  	$this->db->from('opp_detail');
	  	$this->db->like('opp_title',$search);
	  	$data=$this->db->get();
	  	return $data->result();
 	 }
 	function get_opp($limit=false,$offset=false,$order_by=false) 
	{       
		    $this->db->select('*');
			$this->db->from('opp_detail');
			$this->db->join('opportunity','opportunity.opp_id=opp_detail.oppdetail_id');	
			$this->db->limit($limit);
			$this->db->offset($offset);
			$data=$this->db->get();
			return $data->result();
	}
//============TKH=================
    function count($id,$limit=false,$offset=false,$order_by=false)
	{
        $this->db->select('post.id');
		$this->db->from('post');
		$this->db->distinct('post.id');
		$this->db->where('post.skill',0);
		$this->db->where('post.deleted',0);
		$this->db->where('post.save',0);
		// var_dump($this->db->count_all_results());die();
		return $this->db->count_all_results();
	}
//============TKH=================
    function getpost_id($id)	   
    {
    	$this->db->select('post_opp.post_id');
		$this->db->from('post_opp');		
		$this->db->join('user_opp','user_opp.opp_id=post_opp.opp_id');
		$this->db->where('user_opp.user_id',$id);		
		
		$this->db->distinct();
		$all = $this->db->get();
        // var_dump($all->result());die();
		return $all->result_array();

    }
	function opp($id,$limit=false,$offset=false,$order_by=false)
	{
		$this->db->select('post.id  id,post.vdlink,post.image,post.title,post.description,
			post.save,post.deleted,post.apply_link,post.deadline,post.opp,post.skill,post.tag,
			post.location');
		$this->db->from('post');		
		$this->db->where('post.skill',0);
		$this->db->where('post.deleted',0);
		$this->db->where('post.save',0);
		$this->db->order_by('id','desc');
		$this->db->distinct();
		$this->db->limit($limit);
		$this->db->offset($offset);
		$all = $this->db->get();
		return $all->result();


	} 
	function search($search,$limit=false,$offset=false,$order_by=false)
	{
         $this->db->select('*');
         $this->db->from('post');
		$this->db->like('title',$search);
		$this->db->where('deleted',0);
		$this->db->where('save',0);
        $this->db->where('opp',1);
		// $this->db->like('title',$search);
        $this->db->limit($limit);
		$this->db->offset($offset);
		$result=$this->db->get();
		return $result->result();

	}
	function searchcount($search)
	{
        $this->db->from('post');
        $this->db->like('title',$search);
		$this->db->where('save',0);
		$this->db->where('deleted',0);
		$this->db->where('opp',1);
		return $this->db->count_all_results();
	}
	function countloc($value,$limit=false,$offset=false,$order_by=false)
	{
		$this->db->from('post');
        $this->db->like('location',$value);
		$this->db->where('save',0);
		$this->db->where('deleted',0);
		// $this->db->where('skill_id',0);
		// $this->db->where('skill',0);
		
		$this->db->where('opp',1);

		// var_dump($this->db->count_all_results());die();
		return $this->db->count_all_results();
	}

	function location($value,$limit=false,$offset=false,$order_by=false)
	{
         $this->db->select('*');
         $this->db->from('post');
		$this->db->like('location',$value);
		$this->db->where('save',0);
		$this->db->where('deleted',0);
		// $this->db->where('skill_id',0);
		$this->db->where('skill',0);
		$this->db->where('opp',1);
        $this->db->limit($limit);
		$this->db->offset($offset);
		$result=$this->db->get();
		// var_dump($result->result());die();
		return $result->result();

	}
}
?>