<?php
/**
* 
*/
class Feed_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function getFeedArticles()
	{
		$this->db->select('*');
		$this->db->from('post');	
		$this->db->limit(5);
		$this->db->order_by('post.id','desc');
		$this->db->where('save',0);
		$result=$this->db->get();
		// var_dump($result->result());die();
		return $result->result();
	}

	function getFeedArticles_opp()
	{
		$this->db->select('*');
		$this->db->from('post');	
		$this->db->where('save',0);
		$this->db->where('post.opp',1);
		$this->db->limit(5);
		$this->db->order_by('post.id','desc');
		$result=$this->db->get();
		// var_dump($result->result());die();
		return $result->result();
	}
}
?>