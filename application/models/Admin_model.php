<?php
	/**
	* 
	*/
	class Admin_model extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
			$this->load->library('google_url_api');
		}

		function opportunity_upload($data){
			$result=$this->db->insert('opp_detail',$data);
			return $result;
		}

		function skill_upload($data)
		{
			$result=$this->db->insert('skill_detail',$data);
			return $result;
		}

		function oppdetail_getall(){

			$this->db->select('*,opportunity.type as opptype');
			$this->db->from('post');
			$this->db->join('opportunity','opportunity.opp_id=post.opp_id','left');
			$this->db->order_by('post.id','desc');
			$this->db->where('post.deleted','0');
			$this->db->where('post.save','0');
			$this->db->where('opp',1);
			$query=$this->db->get();
			return $query->result_array();
 
		}

		function opportunity(){
			$all_opp=$this->db->get('opportunity');
			return $all_opp->result();
		}
		function get_opp($decrypt_id)//HSU OPP GET 
		{
			$this->db->select('*');
			$this->db->from('post');
			$this->db->where('id',$decrypt_id);
			$result = $this->db->get();
			return $result->row();
		}
		function opp_de($decrypt_id)
		{
			$this->db->where('id',$decrypt_id);
        	$this->db->delete('post');
		}
		function opportunity_edit($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id,$official){

			foreach ($opp_id as $value)
			{
			$result=$this->db->insert('post',array('opp_id'=>$value,'image'=>$opp_image,'description'=>$description,'deadline'=>$deadline,'tag'=>$tag,'apply_link'=>$apply,'title'=>$title)); //to check with values alreay added
			}
			return $result;
		}
		function opportunity_edit2($opp_id,$tag,$apply,$deadline,$video,$description,$title,$decrypt_id){
			foreach ($opp_id as $value)
			{
			$result=$this->db->insert('post',array('opp_id'=>$value,'deadline'=>$deadline,'tag'=>$tag,'apply_link'=>$apply,'vdlink'=>$video,'description'=>$description,'title'=>$title)); //to check with values alreay added
			}
			return $result;
		}
		function opportunity_delete($id){
			$data=array('deleted'=>'1');
		    $this->db->where('id',$id);
		    $result=$this->db->update('post',$data);
		    return $result;
		}
		function skilldetail_getall(){
			$this->db->select('*,skill.type as skilltype');
			$this->db->from('post');
			$this->db->join('skill','skill.skill_id=post.skill_id','left');
			$this->db->order_by('post.id','desc');
			$this->db->where('post.deleted','0');
			$this->db->where('post.save','0');
			$this->db->where('skill',1);
			$query=$this->db->get();
			return $query->result_array();
			
		}
		function skilldetail_get($sdetail_id){
			$query=$this->db->get_where('post',array('id'=>$sdetail_id));
			return $query->row();
		}
		function skill(){
			$all_skill=$this->db->get('skill');
			return $all_skill->result();
		}
		function skill_de($decrypt_id)
		{
			$this->db->where('id',$decrypt_id);
        	$this->db->delete('post');
		}
		function skill_edit($skill_id,$apply,$description,$image,$deadline,$tag,$title,$decrypt_id){
			foreach($skill_id as $value)
			{	
			// $this->db->where('id',$decrypt_id);
			$result=$this->db->insert('post',array('skill_id'=>$value,'deadline'=>$deadline,'tag'=>$tag,'apply_link'=>$apply,'image'=>$image,'description'=>$description,'title'=>$title)); //to check with values alreay added
			}
			return $result;
		}
		function skill_edit2($skill_id,$video,$tag,$apply,$deadline,$description,$title,$decrypt_id){
			foreach ($skill_id as $value)
			{	
			// $this->db->where('id',$decrypt_id);
			$result=$this->db->insert('post',array('skill_id'=>$value,'deadline'=>$deadline,'tag'=>$tag,'apply_link'=>$apply,'vdlink'=>$video,'description'=>$description,'title'=>$title)); //to check with values alreay added
			}
			return $result;
		}
		function skill_delete($sdetail_id){
			// var_dump($sdetail_id);die();
			$data=array('deleted'=>'1');
		    $this->db->where('id',$sdetail_id);
		    $result=$this->db->update('post',$data);
		    return $result;
			
		}
		function del_post($id){
			$res=$this->db->delete('post',array('id'=>$id));
			return $res;

		}
		//ztd retrieve userlist//
		function getall_users(){
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where('user.user_id!=',1);
            $result=$this->db->get();
            return $result->result_array();
		}
	    function count(){
	    	$this->db->where('user_id!=',1);
            $count=$this->db->count_all_results('user');

            return $count;
        }
		function getall_opp(){
			$result=$this->db->get('opportunity');
			return $result->result_array();
		}
		function getall_skill(){
			$result=$this->db->get('skill');
			return $result->result_array();
		}
 	    function search($opp,$skill){
 	    	$this->db->select('user.*');
		    $this->db->from('user');
		    $this->db->join('user_opp','user_opp.user_id=user.user_id');
		    $this->db->join('user_skill','user_skill.user_id=user.user_id');
		    $this->db->where('user_opp.opp_id',$opp); 
		    $this->db->where('user_skill.skill_id',$skill); 
			$result=$this->db->get();
			$rowcount = $result->num_rows();
			
			return $result->result_array(); 

		}
		function getuser_profile($decrypt_id){
			$this->db->where('user_id',$decrypt_id);
			$result=$this->db->get('user');
			
			
			return $result->row();
		}
		function getuser_opp($decrypt_id){
			$this->db->select('opportunity.type');
			$this->db->from('opportunity');
			$this->db->join('user_opp','user_opp.opp_id=opportunity.opp_id');
			$this->db->where('user_opp.user_id',$decrypt_id);
			$result=$this->db->get();
			return $result->result_array();


		}
		function getuser_skill($decrypt_id){
			$this->db->select('skill.type');
			$this->db->from('skill');
			$this->db->join('user_skill','user_skill.skill_id=skill.skill_id');
			$this->db->where('user_skill.user_id',$decrypt_id);
			$result=$this->db->get();
			return $result->result_array();


		}
		function return_opp($oppdata){
			return $oppdata->result();
		}
		function return_skill($skilldata){
			return $skilldata->result();
		}								

      
		

//Save & Savedraft
//TKH	

	function upload($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck)//save & savedraft(video)
		{
		  $array=array('title'=>$title,'tag'=>$tag,'deadline'=>$deadline,'description'=>$description,'apply_link'=>$apply_link,'official_link'=>$official,'time'=>$time,'image'=>$image,'vdlink'=>$video,'c_location'=>$c_location,'location'=>$location,'opp'=>$opp,'skill'=>$skill);
		  $this->db->trans_start();
	          $this->db->insert('post',$array);

	          $id     = $this->db->insert_id();
	                                   
               $encrypt_ids=$this->encrypt->encode($id);
               $modify_encrypt=str_replace("/","~",$encrypt_ids);
                
	          // $url = 'http://localhost/myeo/custom/alldetail/'.$modify_encrypt;
	          // // var_dump($url);die();
	          // $short_url = $this->google_url_api->shorten($url);
           //     // var_dump($short_url->id);die();
	          // $this->db->where('id',$id);
	          // $this->db->update('post',array('url'=>$short_url->id));
	           if($oppcheck!=Null)
	           {
		           foreach ($oppcheck as  $value) 
		           { 

		           	 $this->db->insert('post_opp',array('post_id'=>$id,'opp_id'=>$value));
		           }
			   }
			   if($skillcheck!=Null)
			   {
					foreach ($skillcheck as  $value) 
		           { 

		           	 $this->db->insert('post_skill',array('post_id'=>$id,'skill_id'=>$value));
		           }		
	           }

           $this->db->trans_complete();
           if($this->db->trans_status()===FALSE)
           {
           	 $this->db->trans_rollback();
           }
           else
           {
           	 $this->db->trans_commit();
           	 return true;
           }
		}
	function upload_draft($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck,$save)//save & savedraft(video)
		{
			 // var_dump($image);die();
		  $array=array('title'=>$title,'tag'=>$tag,'deadline'=>$deadline,'description'=>$description,'apply_link'=>$apply_link,'official_link'=>$official,'image'=>$image,'vdlink'=>$video,'c_location'=>$c_location,'location'=>$location,'time'=>$time,'opp'=>$opp,'skill'=>$skill,'save'=>$save);
		  $this->db->trans_start();
	          $this->db->insert('post',$array);
	          $id     = $this->db->insert_id();
	           if($oppcheck!=Null)
	           {
		           foreach ($oppcheck as  $value) 
		           { 

		           	 $this->db->insert('post_opp',array('post_id'=>$id,'opp_id'=>$value));
		           }
			   }
			   if($skillcheck!=Null)
			   {
					foreach ($skillcheck as  $value) 
		           { 

		           	 $this->db->insert('post_skill',array('post_id'=>$id,'skill_id'=>$value));
		           }		
	           }

           $this->db->trans_complete();
           if($this->db->trans_status()===FALSE)
           {
           	 $this->db->trans_rollback();
           }
           else
           {
           	 $this->db->trans_commit();
           	 return true;
           }
		}	


		function add_feature($id)
		{
		
			$res=$this->db->update('post',array('feature'=>1),array('id'=>$id));
			return $res;
		}
		
		function remove_feature($id)
		{
			$res=$this->db->update('post',array('feature'=>0),array('id'=>$id));
		    return $res;
		}
		//========TKH========get post list
		function getpost()
		{
			$this->db->select('*,opportunity.type as opptype,skill.type as skilltype');
			$this->db->from('post');
			$this->db->join('opportunity','opportunity.opp_id=post.opp_id','left');
			$this->db->join('skill','skill.skill_id=post.skill_id','left');

			$this->db->order_by('post.id','desc');
			$this->db->where('post.deleted','0');
			$this->db->where('post.save','0');
			// $this->db->where('post.feature',0);
			$data=$this->db->get();
           $this->db->distinct();;
			return $data->result_array();
		}
		function opptype($count,$array)
		{  
            
		  for($i=0;$i<$count;$i++)
           {   
           	   
				$this->db->select('type');
				$this->db->from('opportunity');
				$this->db->join('post_opp','post_opp.opp_id=opportunity.opp_id');
				$this->db->join('post','post.id=post_opp.post_id');
				$this->db->where('post.id',$array[$i]);	
				$this->db->where('post.deleted',0);
				$this->db->where('post.save',0);
				$this->db->where('post.skill',0);			
				$opp=$this->db->get();
				$data[$i]=$opp->result();
				 // var_dump($data);die();
				
		   }
		   // echo $data;
		   // var_dump($data);die();
           return $data;
		}
		function skilltype($count,$array)
		{  
           
		  for($i=0;$i<$count;$i++)
           {   
           	   
				$this->db->select('type');
				$this->db->from('skill');
				$this->db->join('post_skill','post_skill.skill_id=skill.skill_id');
				$this->db->join('post','post.id=post_skill.post_id');
				$this->db->where('post.id',$array[$i]);
				$this->db->where('post.deleted',0);
				$this->db->where('post.save',0);
				$this->db->where('post.opp',0);

				$skill=$this->db->get();
				$data[$i]=$skill->result();
				 // var_dump($data);die();
				
		   }
		   // echo $data;
		   // var_dump($data);die();
           return $data;
		}
		function saveopptype($count,$array)
		{  
            
		  for($i=0;$i<$count;$i++)
           {   
           	   
				$this->db->select('type');
				$this->db->from('opportunity');
				$this->db->join('post_opp','post_opp.opp_id=opportunity.opp_id');
				$this->db->join('post','post.id=post_opp.post_id');
				$this->db->where('post.id',$array[$i]);	
				$this->db->where('post.deleted',0);
				$this->db->where('post.save',1);
				$this->db->where('post.skill',0);			
				$opp=$this->db->get();
				$data[$i]=$opp->result();
				 // var_dump($data);die();
				
		   }
		   // echo $data;
		   // var_dump($data);die();
           return $data;
		}
		function saveskilltype($count,$array)
		{  
           
		  for($i=0;$i<$count;$i++)
           {   
           	   
				$this->db->select('type');
				$this->db->from('skill');
				$this->db->join('post_skill','post_skill.skill_id=skill.skill_id');
				$this->db->join('post','post.id=post_skill.post_id');
				$this->db->where('post.id',$array[$i]);
				$this->db->where('post.deleted',0);
				$this->db->where('post.save',1);
				$this->db->where('post.opp',0);

				$skill=$this->db->get();
				$data[$i]=$skill->result();
				 // var_dump($data);die();
				
		   }
		   // echo $data;
		   // var_dump($data);die();
           return $data;
		}
		//======TKH=======get edit post
		function post_edit($id)
		{
			// var_dump($id);	
			$this->db->select('*,opportunity.type as opptype,skill.type as skilltype');
			$this->db->from('post');
			$this->db->join('opportunity','opportunity.opp_id=post.opp_id','left');
			$this->db->join('skill','skill.skill_id=post.skill_id','left');
			// $this->db->join('post_opp','post.id=post_opp.post_id');
			$this->db->where('post.id',$id);
			$this->db->where('post.deleted','0');
			$this->db->where('post.save','0');
			$data=$this->db->get();
			// var_dump($data->row());die();
			return $data->row();
		}
		function draft_edit($id)
		{
			// var_dump($id);	
			$this->db->select('*,opportunity.type as opptype,skill.type as skilltype');
			$this->db->from('post');
			$this->db->join('opportunity','opportunity.opp_id=post.opp_id','left');
			$this->db->join('skill','skill.skill_id=post.skill_id','left');
			// $this->db->join('post_opp','post.id=post_opp.post_id');
			$this->db->where('post.id',$id);
			$this->db->where('post.deleted','0');
			$this->db->where('post.save','1');
			$data=$this->db->get();
			// var_dump($data->row());die();
			return $data->row();
		}
		function feature_edit($id)
		{
			// var_dump($id);	
			$this->db->select('*,opportunity.type as opptype,skill.type as skilltype');
			$this->db->from('post');
			$this->db->join('opportunity','opportunity.opp_id=post.opp_id','left');
			$this->db->join('skill','skill.skill_id=post.skill_id','left');
			// $this->db->join('post_opp','post.id=post_opp.post_id');
			$this->db->where('post.id',$id);
			$this->db->where('post.deleted','0');
			$this->db->where('post.save','0');
			$this->db->where('post.feature','1');
			$data=$this->db->get();
			// var_dump($data->row());die();
			return $data->row();
		}
        //======TKH=====editpost
        function delete_opp($decrypt_id)
        {
        	$this->db->where('post_id',$decrypt_id);
        	$this->db->delete('post_opp');
        	return true;
        }
        function editpost_opp($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck)
          
        {
        	// var_dump($apply_link);die();
           
		  $array=array('title'=>$title,'tag'=>$tag,'deadline'=>$deadline,'description'=>$description,'apply_link'=>$apply_link,'official_link'=>$official,'image'=>$image,'vdlink'=>$video,'c_location'=>$c_location,'location'=>$location,'time'=>$time,'opp'=>$opp,'skill'=>$skill);
		  $this->db->trans_start();
		      $this->db->where('id',$decrypt_id);
	          $this->db->update('post',$array);
	          
	           if($oppcheck!=Null)
	           {
		           foreach ($oppcheck as  $value) 
		           { 
                        
		           	 $this->db->insert('post_opp',array('post_id'=>$decrypt_id,'opp_id'=>$value));
		           }
			   }
			   

           $this->db->trans_complete();
           if($this->db->trans_status()===FALSE)
           {
           	 $this->db->trans_rollback();
           }
           else
           {
           	 $this->db->trans_commit();
           	 return true;
           }

        }

   function editdraft_opp($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$opp,$skill,$oppcheck,$save)
          
        {
        	// var_dump($image);die();
           
		  $array=array('title'=>$title,'tag'=>$tag,'deadline'=>$deadline,'description'=>$description,'apply_link'=>$apply_link,'official_link'=>$official,'image'=>$image,'vdlink'=>$video,'c_location'=>$c_location,'location'=>$location,'opp'=>$opp,'skill'=>$skill,'save'=>$save);
		  $this->db->trans_start();
		      $this->db->where('id',$decrypt_id);
	          $this->db->update('post',$array);
	          
	           if($oppcheck!=Null)
	           {
		           foreach ($oppcheck as  $value) 
		           { 
                        
		           	 $this->db->insert('post_opp',array('post_id'=>$decrypt_id,'opp_id'=>$value));
		           }
			   }
			   

           $this->db->trans_complete();
           if($this->db->trans_status()===FALSE)
           {
           	 $this->db->trans_rollback();
           }
           else
           {
           	 $this->db->trans_commit();
           	 return true;
           }

        }

function delete_skill($decrypt_id)
{
	$this->db->where('post_id',$decrypt_id);
	$this->db->delete('post_skill');
	return true;
}
function editpost_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck)

        {
        	// var_dump($ti);die();
           
		  $array=array('title'=>$title,'tag'=>$tag,'deadline'=>$deadline,'description'=>$description,'apply_link'=>$apply_link,'official_link'=>$official,'image'=>$image,'vdlink'=>$video,'c_location'=>$c_location,'location'=>$location,'time'=>$time,'opp'=>$opp,'skill'=>$skill);
		   $this->db->trans_start();
		      $this->db->where('id',$decrypt_id);
	          $this->db->update('post',$array);
	          
	           if($skillcheck!=Null)
	           {
		           foreach ($skillcheck as  $value) 
		           { 

		           	 $this->db->insert('post_skill',array('post_id'=>$decrypt_id,'skill_id'=>$value));
		           }
			   }
			   

           $this->db->trans_complete();
           if($this->db->trans_status()===FALSE)
           {
           	 $this->db->trans_rollback();
           }
           else
           {
           	 $this->db->trans_commit();
           	 return true;
           }

        }
    function editdraft_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$c_location,$location,$time,$opp,$skill,$skillcheck,$save)

        {
        	// var_dump($deadline,$skillcheck);die();
           
		  $array=array('title'=>$title,'tag'=>$tag,'deadline'=>$deadline,'description'=>$description,'apply_link'=>$apply_link,'official_link'=>$official,'image'=>$image,'vdlink'=>$video,'c_location'=>$c_location,'location'=>$location,'time'=>$time,'opp'=>$opp,'skill'=>$skill,'save'=>$save);
		   $this->db->trans_start();
		      $this->db->where('id',$decrypt_id);
	          $this->db->update('post',$array);
	          
	           if($skillcheck!=Null)
	           {
		           foreach ($skillcheck as  $value) 
		           { 

		           	 $this->db->insert('post_skill',array('post_id'=>$decrypt_id,'skill_id'=>$value));
		           }
			   }
			   

           $this->db->trans_complete();
           if($this->db->trans_status()===FALSE)
           {
           	 $this->db->trans_rollback();
           }
           else
           {
           	 $this->db->trans_commit();
           	 return true;
           }

        }

//======TKH========post_delete
        function post_delete($id)
        {
        	$array=array('deleted'=>'1');
        	$this->db->where('id',$id);
        	$result=$this->db->update('post',$array);
            return $result;
        }

// ==============HTZN save & save_draft
		function draft($data)
		{
			// var_dump($data);die();
			$result = $this->db->insert('post',$data);
			return $result;
		}
		

		function get_draft()//GET Draft
		{
			$this->db->select('*,opportunity.type as opptype,skill.type as skilltype');
			$this->db->from('post');
			$this->db->join('opportunity','opportunity.opp_id=post.opp_id','left');
			$this->db->join('skill','skill.skill_id=post.skill_id','left');
			$this->db->order_by('post.id','desc');
			$this->db->where('post.deleted',0);
			$this->db->where('post.save',1);
			$data = $this->db->get();
			// var_dump($data->result_array());die();
			return $data->result_array();
		}
		function delete_draft($id)
		{
			$data=array('deleted'=>'1');
		    $this->db->where('id',$id);
		    $result=$this->db->update('post',$data);
		    return $result;
		}
		function save_savedraft($id)
		{
			$data=array('save'=>'0');
		    $this->db->where('id',$id);
		    $result=$this->db->update('post',$data);
		    return $result;
		}
		
		function post_draft($id)
		{
			$query=$this->db->get_where('post',array('id'=>$id));
			return $query->row();
		}
		// function draft_edit($data,$id){
		// 	$this->db->where('id',$id);
		// 	$result=$this->db->update('post',$data); //to check with values alreay added
		// 	return $result;
		// }

		//For Checkbox data by htzn
		function copp($decrypt_id)//GET OPP_ID For CHK data
		{
			// var_dump($decrypt_id);die();
			$this->db->select('opp_id');
			$this->db->from('post_opp');
			$this->db->where('post_id',$decrypt_id);
			$result = $this->db->get();
			return $result->result();
		}
		function cskill($decrypt_id)//GET SKILL_ID for CHK data
		{
			$this->db->select('skill_id');
			$this->db->from('post_skill');
			$this->db->where('post_id',$decrypt_id);
			$result = $this->db->get();
			return $result->result();
		}
		function clocation($decrypt_id)
		{
			$this->db->select('location');
			$this->db->from('post');
			$this->db->where('id',$decrypt_id);
			$result = $this->db->get();
			// var_dump($result->result_array());die();
			return $result->result_array();
		}
		function insert_opp_id($id,$oppcheck)//Post opp id insert
		{

			foreach ($oppcheck as $key) {
				$this->db->insert('post_opp',array('post_id'=>$id , 'opp_id'=>$key));
			}
			return true;

			
		}

		function insert_skill_id($id,$skillcheck)//Post opp id insert
		{

			foreach ($skillcheck as $key) {
				$this->db->insert('post_skill',array('post_id'=>$id , 'skill_id'=>$key));
			}
			return true;
		}
		function getfeature()
		{
			$this->db->select('*,opportunity.type as opptype,skill.type as skilltype');
			$this->db->from('post');
			$this->db->join('opportunity','opportunity.opp_id=post.opp_id','left');
			$this->db->join('skill','skill.skill_id=post.skill_id','left');
			$this->db->order_by('post.id','desc');
			$this->db->where('post.deleted','0');
			$this->db->where('post.feature',1);
			$this->db->where('post.save','0');
			$data=$this->db->get();
			return $data->result_array();
		}
		function edit_feature($opp,$skill,$tag,$deadline,$c_location,$location,$image,$video,$description,$title,$apply,$save,$decrypt_id,$feature,$oppcheck,$official)
		{
		  $array=array('opp'=>$opp,'skill'=>$skill,'tag'=>$tag,'deadline'=>$deadline,'c_location'=>$c_location,'location'=>$location,'image'=>$image,'vdlink'=>$video,'vdlink'=>$video,'description'=>$description,'title'=>$title,'apply_link'=>$apply,'save'=>$save,'feature'=>$feature,'official_link'=>$official);
		  $this->db->trans_start();
	      $this->db->where('id',$decrypt_id);
          $this->db->update('post',$array);
	          
	           if($oppcheck!=Null)
	           {
		           foreach ($oppcheck as  $value) 
		           { 
                        
		           	 $this->db->insert('post_opp',array('post_id'=>$decrypt_id,'opp_id'=>$value));
		           }
			   }
			   $this->db->trans_complete();
	           if($this->db->trans_status()===FALSE)
	           {
	           	 $this->db->trans_rollback();
	           }
	           else
	           {
	           	 $this->db->trans_commit();
	           	 return true;
	           }
		   
		}

		function get_image()
		{
			$this->db->select('image,id,official_link');
			$this->db->from('post');
			$this->db->where('feature',1);
			$res=$this->db->get();
			return $res->result_array();	
					
		}

		
	}
