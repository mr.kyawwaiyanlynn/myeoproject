<?php
/**
* 
*/
class Skill_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getall($id)
	{
		$this->db->select('*');
		$this->db->from('post');		
		$this->db->where('post.id',$id);
		// $this->db->where('save',0);
		$result=$this->db->get();
		// var_dump($result->result());die();
		return $result->row();
	}
	function get_type($id)
	{   
		// var_dump($id);die();
		$this->db->select('skill.type');
		$this->db->from('skill');		
		$this->db->join('post_skill','post_skill.skill_id=skill.skill_id');
		$this->db->join('post','post.id=post_skill.post_id');
		$this->db->where('post.id',$id);
		$result=$this->db->get();
		// var_dump($result->result_array());die();
		return $result->result_array();
	}
	function left_all()
	{
		$this->db->select('*');
		$this->db->from('skill');
		$result=$this->db->get();
		return $result->result();
	}

    function skilldetail1($skill_id,$limit=false,$offset=false,$order_by=false)
	{
		
		$this->db->select('*,post.id as id');
		$this->db->from('post');
		$this->db->join('post_skill','post_skill.post_id=post.id');
		// $this->db->join('skill','skill.skill_id=post_skill.skill_id');
		$this->db->where('post_skill.skill_id',$skill_id);
		$this->db->where('post.save',0);
		$this->db->where('post.deleted',0);
		$this->db->where('post.skill',1);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$skill_detail = $this->db->get();

		// var_dump($skill_detail->result());die();
		return $skill_detail->result();
	}

	function skilldetail($type,$limit=false,$offset=false,$order_by=false)
	{
		
		$this->db->select('*,post.id as id');
		$this->db->from('post');
		$this->db->join('post_skill','post_skill.post_id=post.id');
		$this->db->join('skill_de','skill_de.skill_de_id=post_skill.skill_id');
		$this->db->where('skill_de.type',$type);
		$this->db->where('post.save',0);
		$this->db->where('post.deleted',0);
		$this->db->where('post.skill',1);
		$this->db->limit($limit);
		$this->db->offset($offset);		
		$opp_detail = $this->db->get();
		return $opp_detail->result();
	}

	

	function check($search)
  	{
	  	$this->db->select('*');
	  	$this->db->from('skill_detail');
	  	$this->db->like('skill_title',$search);
	  	$data=$this->db->get();
	  	return $data->result();
 	 }
 	 function get_skill($limit=false,$offset=false,$order_by=false) 
	{       
		    $this->db->select('*');
			$this->db->from('skill_detail');	
			$this->db->limit($limit);
			$this->db->offset($offset);
			$this->db->order_by('skill_detail.sdetail_id',$order_by);
			$data=$this->db->get();
			return $data->result();
		
	}
	function count($id,$limit=false,$offset=false,$order_by=false)
	{

		$this->db->select('post.id');
		$this->db->from('post');
		$this->db->distinct('post.id');
		$this->db->where('post.opp',0);
		$this->db->where('post.deleted',0);
		$this->db->where('post.save',0);
		// var_dump($this->db->count_all_results());die();
		return $this->db->count_all_results();
	}
	   
	function skill($id,$limit=false,$offset=false,$order_by=false)
	{
		$this->db->select('post.id,post.vdlink,post.image,post.title,post.description,
			post.save,post.deleted,post.apply_link,post.deadline,post.skill,post.skill,post.tag,
			post.location');
		$this->db->from('post');
		// $this->db->join('post_skill','post.id=post_skill.post_id');		
		// $this->db->join('user_skill','post_skill.skill_id=user_skill.skill_id');
		// $this->db->where('user_skill.user_id',$id);		
		$this->db->where('post.opp',0);
		$this->db->where('post.deleted',0);
		$this->db->where('post.save',0);
		$this->db->order_by('id','desc');
		$this->db->distinct();
		$this->db->limit($limit);
		$this->db->offset($offset);
		$all = $this->db->get();
        // var_dump($all->result());die();
		return $all->result();
		

	} 
	function search($search,$limit=false,$offset=false,$order_by=false)
	{
        $this->db->select('*');
		$this->db->like('title',$search);
		$this->db->where('deleted','0');
		$this->db->where('save','0');
        $this->db->where('skill',1);
        $this->db->limit($limit);
		$this->db->offset($offset);
		$result=$this->db->get('post');
		return $result->result();
	}
	function searchcount($search)
	{
        $this->db->from('post');
        $this->db->like('title',$search);
		$this->db->where('save',0);
		$this->db->where('deleted',0);
		$this->db->where('skill',1);		
		return $this->db->count_all_results();
	}
}
?>