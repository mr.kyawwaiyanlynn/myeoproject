<?php
/**
*
*/
class User_model extends CI_Model
{
    private $email_code;

    function __construct()
    {
        parent:: __construct();
        $this->load->database();                            
        $this->load->library('email');

    }

    function check($email)
    {
        // var_dump($name,$email,$password);
        $this->db->select('*');
        $this->db->from('user');
        //$this->db->where('name', $name);
        $this->db->where('email', $email);
        //$this->db->where('password', $password);
        $data = $this->db->get();

        return $data->result();

    }

     function getuser($email)
    {
        // var_dump($email);die();
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $data = $this->db->get();
        // var_dump($data->row(),'aa');die();
        return $data->row_array();
    }

    // function getpwd($id,$current)
    // {
    //    $this->db->select('password');
    //    $this->db->from('user');
    //    $this->db->where('user_id',$id);
    //    $this->db->where('password',$current);
    //    $data=$this->db->get();
    //    var_dump($data->result());die();
    //    if($data)
    //    {
    //     return true;
    //    }
    //    else
    //    {
    //     return false;
    //    }
    // }


//Don't Touch
//#############################@authorWaiYan###########################################


    //Check Facebook User Function
    function checkfb($name, $email, $id)
    {
        //Facebook Login Emali is Null
        if($email!=null)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('name', $name);
            $this->db->where('email', $email);
            $this->db->where('facebook', $id);
            $data = $this->db->get();

        }else{

            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('name', $name);
            $this->db->where('facebook', $id);
            $data = $this->db->get();

        }

        return $data->result();
    }

    //Insert Facebook User
     function facebook_user($name, $email, $id)
    {
        //Facebook Login Email Null
        if($email!=null)
        {
            $array = array('name' => $name, 'facebook' => $id, 'email' => $email, 'role' => 'user');
        }else
        {
            $array = array('name' => $name, 'facebook' => $id,'role' => 'user');
        }
        $this->db->insert('user', $array);
        $insert_id = $this->db->insert_id();
        $sess_array = array('id'=>$insert_id,'username' => $name, 'email' => $email, 'role' => 'user', 'facebook' => $id,'linkedin'=>Null);
        $this->session->set_userdata('logged_in', $sess_array);
        $this->session->userdata('login_status');
        if($this->session->userdata('login_status')==0) {
            $status = array('status' => 'first');
            $this->session->set_userdata('login_status', 1);
        }

        return true;

    }

    //Get User By Email
     function getuserbyemail($email)
    {

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $data = $this->db->get();

        return $data->row_array();
    }

    //Get User By FacebookId
     function  getuserbyfacebookid($facebook_id)
     {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('facebook', $facebook_id);
        $data = $this->db->get();

        return $data->row_array();
     }

        function  getuserbylinkedinid($linkedin_id)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('linkedin', $linkedin_id);
            $data = $this->db->get();

            return $data->row_array();
        }

    //Get User By Id
      function  getuserbyid($facebook_id)
     {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $facebook_id);
        $data = $this->db->get();

        return $data->row_array();
     }

//##########################################################################################

    function signup($name, $email, $password)
    {
        //TzH for signup without Email verificaiton
        // $result=$this->db->insert('user', array('name' => $name, 'email' => $email, 'password' => $password, 'role' => 'user','status'=>0));
        $result=$this->db->insert('user', array('name' => $name, 'email' => $email, 'password' => $password, 'role' => 'user'));


    //return $result;//TzH
    }

    function login($email, $password)
    {
        $this->db->select('user.user_id,user.email,user.name,user.role,user.password');
        $this->db->from('user');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        // $this->db->limit(1);
        $data = $this->db->get();

        return $data->row_array();
    }

    // WY API Login

    function api_login($email,$fb_id,$img_url,$name)
    {

        $array = array('name' => $name, 'facebook' => $fb_id, 'email' => $email, 'picture' => $img_url, 'role' => 'user');
        return $this->db->insert('user', $array);

    }

    function getfbuser($email, $id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $this->db->where('facebook', $id);
        $data = $this->db->get();
        // var_dump($data->row(),'aa');die();
        return $data->row_array();
    }

    function changepwd($id, $new)
    {
        // var_dump($id,$new);die();
        $array = array('password' => $new);
        $this->db->where('user_id', $id);
        $this->db->update('user', $array);
        return true;
    }

    function get_pwd($user_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id',$user_id);
        $get_pwd = $this->db->get();
        // var_dump($get_pwd->row_array());die();
        return $get_pwd->row_array();
    }
    function getpwd($id,$current)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $id);
        $this->db->where('password', $current);
        $data = $this->db->get();
        return $data->row_array();
    }


   function checklinkedin($name, $id, $photourl)
   {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('name', $name);
        $this->db->where('linkedin', $id);
        $this->db->where('picture', $photourl);
        $data = $this->db->get();

        return $data->row_array();
    }


    function linkedin_user($name, $id, $photourl, $email)
    {
        $array = array('name' => $name, 'linkedin' => $id, 'picture' => $photourl, 'email' => $email);
        $this->db->insert('user', $array);
        $insert_id = $this->db->insert_id();
        $sess_array = array('id'=>$insert_id,'username' => $name, 'email' => $email, 'role' => 'user', 'facebook' => Null,'linkedin'=>$id);
        $this->session->set_userdata('logged_in', $sess_array);

        return true;

    }
    function getlinkedinuser($linkedin)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('linkedin',$linkedin);
        $data=$this->db->get();
        return $data->row_array();
    }

    function user_con($token)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('token',$token);
        $data=$this->db->get();
        return $data->row_array();
    }


    //send confirm mail
    function sendEmail($receiver,$name,$password)
    {

        $from = "waiyandev@gmail.com";    //senders email address
        $subject = 'Verify email address';  //email subject
        
        //sending confirmEmail($receiver) function calling link to the user, inside message body 
        $message = 'Dear User,<br><br> Please click on the below activation link to verify your email address<br><br>
        <a href=\'https://www.mymyeo.com/user/confirmEmail/' . md5($receiver) . '\'>http://localhost:8888/myeoproject/user/confirmEmail/' . md5($receiver) . '</a><br><br>Thanks';


        $array = array('name' => $name, 'email' => $receiver, 'password' => $password, 'token' => md5($receiver));
        $this->db->insert('user_confirm', $array);

        //config email settings
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.googlemail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "waiyandev@gmail.com";
        $config['smtp_pass'] = "testing1234";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->initialize($config);
        //send email
        $this->email->from($from);
        $this->email->to($receiver);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {

            return true;

        }else{

            var_dump("fail");
        }

        }

        //activate account
        function verifyEmail($hashcode,$log)
        {   
            // var_dump($hashcode);
            $this->db->insert('user', $log);
            $insert_id = $this->db->insert_id();
            $data = array('id'=>$insert_id,'email'=>$log['email'],'password'=>$log['password'],'name'=>$log['name'],'role'=>$log['role'],'status' => 1);
            $this->session->set_userdata('logged_in',$data);
            return true;
            //update status as 1 to make active user
        }


        //funtion to get email of user to send password
        function ForgotPassword($email)
        {
            $this->db->select('email');
            $this->db->from('user');
            $this->db->where('email', $email);
            $query = $this->db->get();
            return $query->row_array();
        }

        function sendpassword($data)
        {
            // $email = $data['email'];
            // var_dump($email);
            // die();
            $passwordplain = "";
            $six_digit_random_number = mt_rand(100000, 999999);
            $newpass['password'] = sha1($passwordplain);
            $this->db->where('email', $email);
            $this->db->update('user', $newpass);
            var_dump("send");
            // die();
            $from = "waiyandev@gmail.com";    //senders email address
            $subject = 'Verify email address';  //email subject

            //sending confirmEmail($receiver) function calling link to the user, inside message body
            $message = 'Dear User,<br><br> Myeo Verify Your Code.<br><br>
            <a >' . $six_digit_random_number . '</a><br><br>Thanks';


            //config email settings
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.googlemail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "waiyandev@gmail.com";
            $config['smtp_pass'] = "testing1234";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->initialize($config);
            //send email
              $this->email->from($from);
              $this->email->to($data);
              $this->email->subject($subject);
              $this->email->message($message);

            if ($this->email->send()) {


                echo "from: " . $from . "<br>";
                echo "protocol: " . $config['protocol'] . "<br>";
                echo "message: " . $message;
                $arr=array('digit_no'=>$six_digit_random_number);


                $this->db->where('email',$email);
                $this->db->update('user',$arr);

                return true;

            } else {
                echo "email send failed";
                return false;
            }

        }


        function digitcheck($digit)
        {

            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('digit_no',$digit);
            $data = $this->db->get();
            return $data->row();
        }
        function get_all($email)
        {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('email',$email);
            $data = $this->db->get();
            return $data->row_array();
        }
        function reset($pwd,$digit)
        {
            $array=array('password'=>$pwd);

            $this->db->where('digit_no',$digit);

            $this->db->update('user',$array);
            return true;
        }

        function sendDigit($digit_mail)
        {
            $email=$digit_mail['email'];
            // var_dump($email['email']);
            // die();

            $passwordplain = "";
            $six_digit_random_number = mt_rand(100000, 999999);
            $array = array('digit_no'=>$six_digit_random_number);

            $this->db->where('email', $email['email']);
            $this->db->update('user', $array);
            //var_dump("send");
            // die();
            $from = "waiyandev@gmail.com";    //senders email address
            $subject = 'Verify Digit';  //email subject

            //sending confirmEmail($receiver) function calling link to the user, inside message body
            $message = 'Dear User,<br><br> Verify Digit Code.<br><br>
            <a >' . $six_digit_random_number . '</a><br><br>Thanks';


            //config email settings
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.googlemail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "waiyandev@gmail.com";
            $config['smtp_pass'] = "testing1234";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->load->library('email', $config);

            $this->email->set_newline("\r\n");

            //$this->load->library('email', $config);
            $this->email->initialize($config);
            //send email
              $this->email->from($from);
              $this->email->to($email['email']);
              $this->email->subject($subject);
              $this->email->message($message);

            if ($this->email->send()) {

                return true;

            } else {
                echo "email send failed";
                return false;
            }
        }

    function check_email($email)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email',$email);
        $data = $this->db->get();
// var_dump($data->result());
      if($data->result() != Null)
      {
       return true;
      }
      else{
        return false;
      }
    }

}
