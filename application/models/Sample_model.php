<?php
 /**
 * 
 */
 class Sample_model extends CI_Model
 {
 	
 	function __construct()
 	{
 		parent::__construct();
		$this->load->database();
 	}
 	function count_all()
 	{
		$this->db->from('opportunity');	
		return $this->db->count_all_results();
    }
    
	
	
    function count_dopp($type,$limit=false,$offset=false,$order_by=false)
 	{
		// var_dump($type);die();
		$this->db->from('post');
		$this->db->join('post_opp','post_opp.post_id=post.id');	
		$this->db->join('opportunity','opportunity.opp_id=post_opp.opp_id');	
		$this->db->where('opportunity.type',$type);
		$this->db->where('post.opp',1);
		$this->db->where('post.deleted',0);
		$this->db->where('post.save',0);		
		// var_dump($this->db->count_all_results());die();
		return $this->db->count_all_results();
    }

     function count_sopp($search)
 	{
		$this->db->from('opp_detail');
		$this->db->like('opp_title',$search);
		return $this->db->count_all_results();
    }


	function count_skill()
 	{
		$this->db->from('skill_detail');	
		return $this->db->count_all_results();
    }
	
    function count_dskill($skill_id)
 	{
		// $this->db->select('post.id');
		$this->db->from('post');
		$this->db->join('post_skill','post_skill.post_id=post.id');		
		$this->db->where('post_skill.skill_id',$skill_id);
		$this->db->where('post.skill',1);
		$this->db->where('post.deleted',0);
		$this->db->where('post.save',0);
		// $this->db->from('user_opp');
		// $this->db->join('post_opp','post_opp.opp_id=user_opp.opp_id');
		// $this->db->join('')
		
		// var_dump($this->db->count_all_results());die();
		return $this->db->count_all_results();
    }
	
	function get_all($limit=false,$offset=false,$order_by=false)
	{
		    $this->db->select('*');
			$this->db->from('opp_detail');		
			$this->db->limit($limit);
			$this->db->offset($offset);
			$this->db->order_by('oppdetail_id',$order_by);
			$data=$this->db->get();
			return $data->result();
	}
	
    
 }
?>