<?php
/**
* 
*/
class Logout extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
        $this->load->model('Admin_model','',TRUE);
	}
	function index()
	{
		$logged_in=$this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		//var_dump($logged_in);die();
        $data['img'] = $this->Admin_model->get_image();
        //  var_dump($data['img']);

        if($data['img'])
        {
            $this->load->view('home/login_page',$data);
            $this->load->view('include/footer');
        }
        redirect('user');

	}
}
?>