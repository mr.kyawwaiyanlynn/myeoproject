<?php
  /**
  * 
  */
  class Admin extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('User_model','',TRUE);
      $this->load->library('form_validation');
      $this->load->model('Admin_model','',TRUE);
      $this->load->model('Custom_model','',TRUE);
      $this->load->model('Opportunity_model','',TRUE);
      $this->load->model('Profile_model','',TRUE);
      $this->load->model('Skill_model','',TRUE);
      $this->load->helper('form');
      $this->load->library('ckeditor');
      $this->load->library('ckfinder');
      $this->load->library('session');
      $this->load->helper('security');       
      $this->load->library('encrypt');
      $this->load->model('User_model');
    }
    function index()
    {
     $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
     if(isset($log))
     {
            // var_dump("Hello");die();
      if($log['role']=='admin')
      { 
              //  $data['name']   = $this->User_model->getuser($log['email']);
        if($log['email']!=Null)
        {
         $data['name']    = $this->User_model->getuserbyemail($log['email']);
       }
       else
       {
         $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
       }
       $data['all_opp']  = $this->Admin_model->opportunity();
       $data['all_skill']  = $this->Admin_model->skill();
            // var_dump($data['all_opp']);die();
       $this->load->view('include/header1');
       $this->load->view('admin/upload_view',$data);
       $this->load->view('admin/admin_nav',$data);

     }
     else
     {
      redirect('user/login');
    }

  }

}
//KADAY
function  post()  
{


  $log=$this->session->userdata('logged_in');
  if(isset($log))
  {
    if($log['role']=='admin')
    {
                    // var_dump('Hello');die();
                    //$data['name']       =   $this->User_model->getuser($log['email']);
      if($log['email']!=Null)
      {
        $data['name']    = $this->User_model->getuserbyemail($log['email']);
      }
      else
      {
        $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
      }
      $data['all_opp']    =   $this->Admin_model->opportunity();
      $data['all_skill']  =   $this->Admin_model->skill();

      $this->form_validation->set_rules('title','title','required');
      $this->form_validation->set_rules('description','description','required');

      if($this->form_validation->run()==FALSE)
      {

        redirect('admin');
      }
      else
      {


        $title= $this->input->post("title");
        $description = $this->input->post("description");
        $apply_link   = $this->input->post("apply");
        $official=$this->input->post("official");
        $deadline    = $this->input->post("date");
                            // $deadline2     = $this->input->post("date1");
        if($deadline==1)
        {
          $deadline=$this->input->post('deadline');
        }
        if($deadline==2)
        {
          $deadline=$this->input->post('deadline1');
        }
        $tag_input = $this->input->post('tags');
	if($tag_input!=Null)
         {
        $all_tags  = explode( ',' , $tag_input);                
        $tag  = serialize($all_tags);
       }
       else
       {
        $tag = "";
       }
        $c_location = $this->input->post('c_location');
        $loc          = $this->input->post('location');
        $time       = $this->input->post('time');
        if($loc!=NULL)
        {
          $location     = implode(',', $loc);
        }
        else
        {
          $location = " ";
        }
                            // var_dump($c_location);die();

                            // var_dump($title,$description,$apply_link,$official,$tag,$location,$deadline);die();
        $choiceid = $this->input->post("choice"); 
        $uploadchoiceid = $this->input->post('uploadchoice');
        $oppcheck     = $this->input->post('oppcheck');
        $skillcheck   = $this->input->post('skillcheck');
                         // var_dump($oppcheck);die();

        $btn=$this->input->post('submitform');
        if($btn=="post")
        {

          if($choiceid=="")
          {
            $this->session->set_flashdata('msg','You must choose opportunity or skill !!!');
            
            redirect('admin');
          }
          else if($choiceid=="1")
          {  
            $opp          =1;
            $skill        =0;

            if($uploadchoiceid=='1')
            {
             $image        = $this->img_upload2();
             if($image=="")
             {
              $this->session->set_flashdata('msg2','You must choose image or video !!!');

              redirect('admin');
            }
                               // var_dump($image);die();
            else
            {
             $video        ="";
             $res =$this->Admin_model->upload($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck);
             if($res)
             {
               redirect('admin/getpost');
             }
           }
         } 
         else
         {
           $video        = $this->input->post('videouploadchoice');
           if($video=="")
           {
            $this->session->set_flashdata('msg2','You must choose image or video !!!');
            
            redirect('admin');
          }
          else
          {
            $image="";
            $res =$this->Admin_model->upload($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck);
            if($res)
            {
             redirect('admin/getpost');
           }
         }
       } 

                        }//for $choiceid=='1'
                        else  //for $choiceid=='2'
                        {
                          $skill          =1;
                          $opp            =0;

                          if($uploadchoiceid==1)
                          {
                           $image        = $this->img_upload2();
                           if($image=="")
                           {
                            $this->session->set_flashdata('msg2','You must choose image or video !!!');

                            redirect('admin');
                          }
                          else
                          {
                           $video="";
                           $res =$this->Admin_model->upload($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck);
                           if($res)
                           {
                             redirect('admin/getpost');
                           }
                         }
                       } 
                       else
                       {

                         $video        = $this->input->post('videouploadchoice');
                         if($video=="")
                         {
                          $this->session->set_flashdata('msg2','You must choose image or video !!!');

                          redirect('admin');
                        }
                        else
                        {
                          $image="";
                          $res =$this->Admin_model->upload($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck);
                          if($res)
                          {
                           redirect('admin/getpost');
                         }
                       }
                     }
                        }//end for choiceid=2

                      }
                     else//btn==save as draft 
                     {
                         // var_dump($btn);die();
                      if($choiceid=="")
                      {
                        $this->session->set_flashdata('msg','You must choose opportunity or skill !!!');

                        redirect('admin');
                      }
                      else if($choiceid=="1")
                      {  
                        $opp          =1;
                        $skill        =0;
                        $save         =1;
                        if($uploadchoiceid=='1')
                        {
                         $image        = $this->img_upload2();
                         if($image=="")
                         {
                          $this->session->set_flashdata('msg2','You must choose image or video !!!');

                          redirect('admin');
                        }
                        else
                        {
                         $video        ="";
                         $res =$this->Admin_model->upload_draft($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck,$save);
                         if($res)
                         {
                           redirect('admin/get_savedraft');
                         }
                       }
                     }
                          // } 
                     else
                     {
                       $video        = $this->input->post('videouploadchoice');
                       if($video=="")
                       {
                        $this->session->set_flashdata('msg2','You must choose image or video !!!');

                        redirect('admin');
                      }
                      else
                      {
                        $image="";
                        $res =$this->Admin_model->upload_draft($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck,$save);
                        if($res)
                        {
                         redirect('admin/get_savedraft');
                       }
                     }
                   } 

                        }//for $choiceid=='1'
                        else  //for $choiceid=='2'
                        {
                          $skill          =1;
                          $opp            =0;
                          $save           =1;

                          if($uploadchoiceid==1)
                          {
                           $image        = $this->img_upload2();
                           if($image=="")
                           {
                            $this->session->set_flashdata('msg2','You must choose image or video !!!');

                            redirect('admin');
                          }
                          else
                          {
                           $video="";
                           $res =$this->Admin_model->upload_draft($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck,$save);
                           if($res)
                           {
                             redirect('admin/get_savedraft');
                           }
                         }
                       } 
                       else
                       {

                         $video        = $this->input->post('videouploadchoice');
                         if($video=="")
                         {
                          $this->session->set_flashdata('msg2','You must choose image or video !!!');

                          redirect('admin');
                        }
                        else
                        {
                          $image="";
                          $res =$this->Admin_model->upload_draft($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$skillcheck,$save);
                          if($res)
                          {
                           redirect('admin/get_savedraft');
                         }
                       }
                     }
                        }//end for choiceid=2

                     }//btn==save as draft
                    } //else   
                }//if admin
                else
                {
                 redirect('user');
               }
             }
             else
             {
              redirect('user');
            }
          }


///////////////////myatmonmonkyi/////////////////////
          function get_oppdetail()
          {  
            $log=$this->session->userdata('logged_in');
            if(isset($log))
            {
              if($log['role']=='admin')
              {
                //$data['name'] = $this->User_model->getuser($log['email']);
                if($log['email']!=Null)
                {
                 $data['name']    = $this->User_model->getuserbyemail($log['email']);
               }
               else
               {
                 $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
               }
               $data['query']  = $this->Admin_model->oppdetail_getall();
               $this->load->view('include/header1');
               $this->load->view('admin/getopportunity_view',$data);
               $this->load->view('admin/admin_nav',$data);
            // $this->load->view('include/footer'); 
             }
             else
             {
          //    var_dump('lll');die();
              redirect('user/login');
            }
            
          }

       /////END_OF_FIRST_IF
    }//////END_OF_upload_FUNCTION

    function edit_opportunity($oppdetail_id)
    {
      // var_dump($oppdetail_id);die();
      $origin_encrypt = str_replace("~","/",$oppdetail_id);
      $decrypt_id=$this->encrypt->decode($origin_encrypt);
      $data['location']   =  $this->Admin_model->clocation($decrypt_id);
      $deadline    = $this->input->post("date");


                            // $deadline2     = $this->input->post("date1");
      if($deadline==1)
      {
        $deadline=$this->input->post('deadline');
      }
      else if($deadline==2)
      {
        $deadline=$this->input->post('deadline2');
      }
      else
      {
       $deadline=  " ";
     }
     $loc          = $this->input->post('location');
     if($loc!=NULL)
     {
      $location     = implode(',', $loc);
    }
    else
    { 
      foreach ($data['location'] as$value)
      {                                                       
        $location = $value['location'];
      }

    }
    $official=$this->input->post('official');
    $tag_input  = $this->input->post('tags');
    $official=$this->input->post('official');
    $c_location = $this->input->post('c_location');
    $time       = $this->input->post('time');
    $all_tags   = explode( ',' , $tag_input);
    $tag    = serialize($all_tags);


    $log=$this->session->userdata('logged_in');

    if(isset($log))
    {
          // var_dump("Hello");die();
      if($log['role']=='admin')
      {
        $this->form_validation->set_rules('title','Title', 'trim|required');
        $this->form_validation->set_rules('description','Description','trim|required');
        $this->form_validation->set_rules('choice','Choice','required');
        //need or not for select
        $this->form_validation->set_rules('uploadchoice','UploadChoice','required');  
        //need or not for two upload choice

        if($this->form_validation->run()==FALSE){  
          $data['edit']   = $this->Admin_model->get_opp($decrypt_id);
          $data['location']   =  $this->Admin_model->clocation($decrypt_id);
         // $data['name']   = $this->User_model->getuser($log["email"]);
          if($log['email']!=Null)
          {
           $data['name']    = $this->User_model->getuserbyemail($log['email']);
         }
         else
         {
           $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
         } 
         $data['all_opp']  = $this->Admin_model->opportunity();
         $data['all_skill']  = $this->Admin_model->skill();
         $data['opp']    =   $this->Admin_model->copp($decrypt_id);
         $data['skill']    = $this->Admin_model->cskill($decrypt_id);
         $data['post']   = $this->Admin_model->post_edit($decrypt_id);
         $data['post1']    = $data['post']->tag;
          // var_dump($data['post']);die();
         $data['post1']    = unserialize($data['post1']);
          // var_dump($data['post1']);die();
         $this->load->view('include/header1');

         $this->load->view('admin/editopportunity_view',$data);
         $this->load->view('admin/admin_nav',$data);
          // $this->load->view('include/footer'); 
        }else{ //no filled condition for opportunity choice and two upload choiceid1

          $oppcheck     =   $this->input->post("oppcheck");
          $skillcheck     =   $this->input->post("skillcheck");
          $img1       =   $this->input->post("imageupload");
          $image      =   $this->img_upload2();
          $video      =   $this->input->post("videouploadchoice");
          $video1     =   $this->input->post("videoupload");
          $description  =   $this->input->post("description");
          $title=$this->input->post("title");
          $apply=$this->input->post("apply");
          $c_location = $this->input->post('c_location');
          $time       = $this->input->post('time');
          $uploadchoiceid   = $this->input->post("uploadchoice");

          $choiceid       = $this->input->post("choice");
          
          if($image==NULL)
          {
            $image=$img1;

          }
          else
          {
            $image=$this->img_upload2();
          }
          if($video==NULL)
          {   
            $video=$video1;
          }
          else
          {
            $video=$video;
          }
                // var_dump($tag,$opp_id ,$skill_id,$image,$video,$description,$title,$apply,$uploadchoiceid,$choiceid,$deadline);die();
        // var_dump($deadline);die();
          if($choiceid=="1")
          {
           $opp            =1;
           $skill          =0;
           if($uploadchoiceid=="1")
           {





                         // var_dump($apply,$official,$location,$deadline,$oppcheck);die();
            $delete=$this->Admin_model->delete_opp($decrypt_id);
            $delete=$this->Admin_model->delete_skill($decrypt_id);
            if($delete)
            {

              $res =$this->Admin_model->editpost_opp($decrypt_id,$title,$tag,$deadline,$description,$apply,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck);

              if($res)
              {
               redirect('admin/get_oppdetail');
             }
           }
          }//if $uploadchoice==1
          elseif($uploadchoiceid=="2")
          {





            $delete=$this->Admin_model->delete_opp($decrypt_id);
            $delete=$this->Admin_model->delete_skill($decrypt_id);                    if($delete){
              $result = $this->Admin_model->editpost_opp($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck);
              if($result)
              {
               redirect('admin/get_oppdetail');
             }
           }     


          }//else if Suploadchoice==2
        }
        else
        {
          $skill  = 1;
          $opp    = 0;
          if($uploadchoiceid=="1")
          {



            $delete=$this->Admin_model->delete_opp($decrypt_id);
            $delete=$this->Admin_model->delete_skill($decrypt_id);              if($delete){
              $result     = $this->Admin_model->editpost_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck);
              if($result)
              {
                redirect('admin/get_skilldetail');
              }
            }
          }//if $uploadchoice==1
          elseif($uploadchoiceid=="2")
          {
            $delete=$this->Admin_model->delete_opp($decrypt_id);
            $delete=$this->Admin_model->delete_skill($decrypt_id);              if($delete){
              $result     = $this->Admin_model->editpost_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck);
              if($result)
              {
                redirect('admin/get_skilldetail');
              }
            }
          }//else if $uploadchoice==2
        }
                // var_dump($opp_id,$skill_id,$image,$image1,$video,$video1,$description,$title,$subtitle,$uploadchoiceid);die;

      }//else

      }//second if
        }// if log

        else
        {
          redirect('user/login');
        }
        
   }//function
   function delete_opportunity($oppdetail_id)
   {
      // for decryption
    $origin_encrypt   = str_replace("~","/",$oppdetail_id);
    $decrypt_id     = $this->encrypt->decode($origin_encrypt);

    $log=$this->session->userdata('logged_in');
    if(isset($log))
    {
      if($log['role']=='admin')
      {
       $result=$this->Admin_model->opportunity_delete($decrypt_id);
       if($result)
       {
        redirect('admin/get_oppdetail');
      }
      else
      {
        echo "Fail Deleting Opportunity";
      }
    }
    else
    {
      redirect('user/login');
    }
  }     
}
function get_skilldetail()
{
  $log=$this->session->userdata('logged_in');
  if(isset($log))
  {
    if($log['role']=='admin')
    {
                //$data['name'] = $this->User_model->getuser($log["email"]);
      if($log['email']!=Null)
      {
       $data['name']    = $this->User_model->getuserbyemail($log['email']);
     }
     else
     {
       $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
     }
     $data['query']  = $this->Admin_model->skilldetail_getall();
     $this->load->view('include/header1');
     $this->load->view('admin/getskill_view',$data);
     $this->load->view('admin/admin_nav',$data);
              // $this->load->view('include/footer'); 
   }
   else
   {
    redirect('user/login');
  }
}
}

function edit_skill($sdetail_id)
{
  // for decryption
  $official=$this->input->post('official');
  $tag_input    = $this->input->post('tags');
  $all_tags     = explode( ',' , $tag_input);
                // var_dump($all_tags);die();
  $tag      = serialize($all_tags);
  $deadline    = $this->input->post("date");
                            // $deadline2     = $this->input->post("date1");
  if($deadline==1)
  {
    $deadline=$this->input->post('deadline');
  }
  else if($deadline==2)
  {
    $deadline=$this->input->post('deadline2');
  }
  else
  {
   $deadline=  " ";
 }
 $origin_encrypt = str_replace("~","/",$sdetail_id);
 $decrypt_id=$this->encrypt->decode($origin_encrypt);
  $data['location']   =  $this->Admin_model->clocation($decrypt_id);

 $log=$this->session->userdata('logged_in');
   // var_dump($log);die();
 if(isset($log))
 {
        // var_dump("Hello");die();
  if($log['role']=='admin')
  {
    $this->form_validation->set_rules('title','Title', 'trim|required');
      // $this->form_validation->set_rules('subtitle','Subtitle','trim|required');
    $this->form_validation->set_rules('description','Description','trim|required');
    $this->form_validation->set_rules('choice','Choice','required');
      //$this->form_validation->set_rules('skillchoice','SkillChoice','required'); 
      //need or not for select
    $this->form_validation->set_rules('uploadchoice','UploadChoice','required');  
      //need or not for two upload choice

    if($this->form_validation->run()==FALSE)
    {  
      $data['edit']   = $this->Admin_model->skilldetail_get($decrypt_id);
         //   $data['name']   = $this->User_model->getuser($log["email"]);
      if($log['email']!=Null)
      {
       $data['name']    = $this->User_model->getuserbyemail($log['email']);
     }
     else
     {
       $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
     }
     $data['location']   =  $this->Admin_model->clocation($decrypt_id);
     $data['all_opp']  = $this->Admin_model->opportunity();
     $data['all_skill']  = $this->Admin_model->skill();
     $data['skill']    =   $this->Admin_model->cskill($decrypt_id);
     $data['post']   = $this->Admin_model->post_edit($decrypt_id);
     $data['post1']    = $data['post']->tag;
     $data['post1']    = unserialize($data['post1']);
        // var_dump($data['edit']);die();
     $this->load->view('include/header1');

     $this->load->view('admin/editskill_view',$data);
     $this->load->view('admin/admin_nav',$data);
        // $this->load->view('include/footer'); 
   }
   else
      { //no filled condition for two upload choice 
        $oppcheck     =   $this->input->post("oppcheck");
        $skillcheck =   $this->input->post("skillcheck");
        $img1       =   $this->input->post("imageupload");
        $image      =   $this->img_upload2();
        $video      =   $this->input->post("videouploadchoice");
        $video1     =   $this->input->post("videoupload");
        $description  =   $this->input->post("description");
        $title=$this->input->post("title");
        $apply=$this->input->post("apply");
        $uploadchoiceid   = $this->input->post("uploadchoice");
        $c_location = $this->input->post("c_location");
        $choiceid       = $this->input->post("choice");
        $time       = $this->input->post('time');
        $loc          = $this->input->post('location');
        if($loc!=NULL)
        {
          $location     = implode(',', $loc);
        }
        else
        { 
          foreach ($data['location'] as $value)
          {                                                       
            $location = $value['location'];
          }

        }
        
        if($image==NULL)
        {
          $image=$img1;

        }
        else
        {
          $image=$this->img_upload2();
        }
        if($video==NULL)
        {   
          $video=$video1;
        }
        else
        {
          $video=$video;
        }
                // var_dump($tag,$opp_id ,$skill_id,$image,$video,$description,$title,$apply,$uploadchoiceid,$choiceid,$deadline);die();
        if($choiceid=="1")
        {
         $opp            =1;
         $skill          =0;
         if($uploadchoiceid=="1")
         {




          $delete=$this->Admin_model->delete_opp($decrypt_id);
          $delete=$this->Admin_model->delete_skill($decrypt_id);
          if($delete){
            $result = $this->Admin_model->editpost_opp($decrypt_id,$title,$tag,$deadline,$description,$apply,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck);
            if($result)
            {
             redirect('admin/get_oppdetail');
           }
         }     

          }//if $uploadchoice==1
          elseif($uploadchoiceid=="2")
          {

            $delete=$thiss->Admin_model->delete_opp($decrypt_id);
            $delete=$this->Admin_model->delete_skill($decrypt_id);
            if($delete){
              $result = $this->Admin_model->editpost_opp($decrypt_id,$title,$tag,$deadline,$description,$apply,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck);
              if($result)
              {
               redirect('admin/get_oppdetail');
             }
           }     
          }//else if Suploadchoice==2
        }
        else
        {
          $opp            =0;
          $skill          =1;
          if($uploadchoiceid=="1")
          {



            $delete=$this->Admin_model->delete_opp($decrypt_id);
            $delete=$this->Admin_model->delete_skill($decrypt_id);
            if($delete){
              $result     = $this->Admin_model->editpost_skill($decrypt_id,$title,$tag,$deadline,$description,$apply,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck);
              if($result)
              {
                redirect('admin/get_skilldetail');
              }
            }
          }//if $uploadchoice==1
          elseif($uploadchoiceid=="2")
          {

            $delete=$this->Admin_model->delete_opp($decrypt_id);
            $delete=$this->Admin_model->delete_skill($decrypt_id);
            if($delete){
              $result     = $this->Admin_model->editpost_skill($decrypt_id,$title,$tag,$deadline,$description,$apply,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck);
              if($result)
              {
                redirect('admin/get_skilldetail');
              }
            }
          }//else if $uploadchoice==2
        }
                // var_dump($opp_id,$skill_id,$image,$image1,$video,$video1,$description,$title,$subtitle,$uploadchoiceid);die;

      }//else

      }//second if
        }// if log

        else
        {
          redirect('user/login');
        }
        
   }//function
   function delete_skill($sdetail_id)
   {
             // for decryption
    $origin_encrypt   = str_replace("~","/",$sdetail_id);
    $decrypt_id     = $this->encrypt->decode($origin_encrypt);

    $log=$this->session->userdata('logged_in');
           // var_dump($decrypt_id);die();
    if(isset($log))
    {
            // var_dump("Hello");die();
      if($log['role']=='admin')
      {
        $result=$this->Admin_model->skill_delete($decrypt_id);
        if($result)
        {
              // echo "Successful Deleting Skill";
          redirect('admin/get_skilldetail');
        }else
        {
          echo "Fail Deleting Skill";
        }
      }
      else
      {
        redirect('user/login');
      }
    }
  }
    //ztd retrieve user list//
  function getall_users()
  {
    $log=$this->session->userdata('logged_in');

    if(isset($log))
    {

     if($log['role']=='admin')

     {
      $data['users']    = $this->Admin_model->getall_users();
      $data['user_count'] = $this->Admin_model->count();
      $data['skill']    = NULL;
           // $data['name']   = $this->User_model->getuser($log["email"]);
      if($log['email']!=Null)
      {
       $data['name']    = $this->User_model->getuserbyemail($log['email']);
     }
     else
     {
       $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
     }
     $data['oppquery'] = $this->Admin_model->getall_opp();
     $data['skillquery'] = $this->Admin_model->getall_skill();
     if($data)
     {
      $this->load->view('include/header1');
      $this->load->view('admin/user_view',$data);
      $this->load->view('admin/admin_nav',$data);
    }
  }
}
}
function search()
{ 

 $log=$this->session->userdata('logged_in');

 if(isset($log))
 {

   if($log['role']=='admin')

   {
    $opp        =   $this->input->post('opp');
    $skill        = $this->input->post('skill');

    $data['users']    = $this->Admin_model->search($opp,$skill);

    $data['user_count'] = count($data['users']);
    $data['oppquery'] = $this->Admin_model->getall_opp();
            //$data['name']   = $this->User_model->getuser($log["email"]);
    if($log['email']!=Null)
    {
     $data['name']    = $this->User_model->getuserbyemail($log['email']);
   }
   else
   {
     $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
   }
   $data['skillquery'] = $this->Admin_model->getall_skill();
   $data['opp']    =   $opp;
   $data['skill']    = $skill;
   $this->load->view('include/header1');
   $this->load->view('admin/user_view',$data);
   $this->load->view('admin/admin_nav',$data);

 }
}


}
function user_profile($id){
  $log=$this->session->userdata('logged_in');

  if(isset($log))
  {

   if($log['role']=='admin')

   {
    $origin_encrypt   = str_replace("~","/",$id);
    $decrypt_id     = $this->encrypt->decode($origin_encrypt);
    $data['user']   = $this->Admin_model->getuser_profile($decrypt_id);
        //$data['name']   = $this->User_model->getuser($log["email"]);
    if($log['email']!=Null)
    {
     $data['name']    = $this->User_model->getuserbyemail($log['email']);
   }
   else
   {
     $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
   }
   $data['opp']    = $this->Admin_model->getuser_opp($decrypt_id);
   $data['skill']    = $this->Admin_model->getuser_skill($decrypt_id);
   if($data['user'])
   {
    $this->load->view('include/header1');
    $this->load->view('admin/userprofile_view',$data);
    $this->load->view('admin/admin_nav',$data);
  }
}
}

}
function preview()
{
  $log=$this->session->userdata('logged_in');

  if(isset($log))
  {

   if($log['role']=='admin')

   {
         // $data['name']   = $this->User_model->getuser($log['email']);
     if($log['email']!=Null)
     {
       $data['name']    = $this->User_model->getuserbyemail($log['email']);
     }
     else
     {
       $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
     }
     $data['all_opp']  = $this->Admin_model->opportunity();
     $data['all_skill']  = $this->Admin_model->skill();

     $this->form_validation->set_rules('title','Title', 'trim|required');
     $this->form_validation->set_rules('description','Description','trim|required');
     $this->form_validation->set_rules('choice','Choice','required');
     $this->form_validation->set_rules('uploadchoice','UploadChoice','required');
     if($this->form_validation->run()==FALSE)
     {
      $this->load->view('include/header1');
      $this->load->view('admin/admin_nav',$data);
      $this->load->view('admin/upload_view');
        // $this->load->view('include/footer');   
    }else{

        $choiceid=$this->input->post("choice"); //////FIRST_RADIO_CHOICE
        if($choiceid=="1"){ 
          //var_dump($choiceid);
          //echo "Opp" +  $choiceid; 
          // $this->form_validation->set_rules('opportunitychoice','OpportunityChoice','required');
          if($this->form_validation->run()==FALSE){
            $this->load->view('include/header1');
            $this->load->view('admin/admin_nav',$data);
            $this->load->view('admin/upload_view',$data); //maketoremainfilleddata
            // $this->load->view('include/footer'); 
          }else{
            $uploadchoiceid=$this->input->post("uploadchoice"); //////SECOND_RADIO_CHOICE
            if($uploadchoiceid == "1"){  
              // $this->form_validation->set_rules('imageuploadchoice','ImageUploadChoice','required');
              if($this->form_validation->run()==FALSE){
                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$data);
                  // $this->load->view('include/footer');
              }else{

                $id       = $this->input->post("oppcheck");
                $image      = $this->input->post("imageuploadchoice");
                $description  = $this->input->post("description");
                $title      = $this->input->post("title");
                // $subtitle=$this->input->post("subtitle");
                $oppdata      = array($id,$image,$description,$title);

                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$data);
                // $this->load->view('include/footer');
              }
            }elseif($uploadchoiceid == "2"){
              $this->form_validation->set_rules('videouploadchoice','VideoUploadChoice','required');
              if($this->form_validation->run()==FALSE){

                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$data);
                // $this->load->view('include/footer');
              }else{

                $id         =   $this->input->post("oppcheck");
                $image        = $this->input->post("imageuploadchoice");
                $vdlink       = $this->input->post("videouploadchoice");
                $titl         = $this->input->post("title");
                // $subtitle=$this->input->post("subtitle");
                $skilldata      = array($id,$vdlink,$description,$title);
                
                $data['result']   = $this->Admin_model->return_opp($oppdata);
                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$data);
                // $this->load->view('include/footer');
              }
            }  //////END_OF_SECOND_RADIO_CHOICE
          }
        }else if($choiceid=="2"){ 
          //var_dump($choiceid);
          //echo "Skill"  + $choiceid;
          // $this->form_validation->set_rules('skillchoice','SkillChoice','required'); 
          if($this->form_validation->run()==FALSE){
            $this->load->view('include/header1');
            $this->load->view('admin/upload_view',$data); //maketoremainfilleddata
            // $this->load->view('include/footer'); 
          }else{
            $uploadchoiceid=$this->input->post("uploadchoice");
            if($uploadchoiceid == "1"){
              $this->form_validation->set_rules('imageuploadchoice','ImageUploadChoice','required');
              if($this->form_validation->run()==FALSE){

                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$data);
                // $this->load->view('include/footer');
              }else{

                $id       = $this->input->post("skillcheck");
                $image      = $this->input->post("imageuploadchoice");
                $description    = $this->input->post("description");
                $title      =   $this->input->post("title");
                // $subtitle=$this->input->post("subtitle");
                $skilldata=array('0'=>$id,'1'=>$image,'2'=>$description,'3'=>$title);
                
                
                // $data['result']=$this->Admin_model->return_skill($skilldata);
                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$skilldata);
                // $this->load->view('include/footer');
              }
            }elseif($uploadchoiceid=="2"){
              $this->form_validation->set_rules('videouploadchoice','VideoUploadChoice','required');
              if($this->form_validation->run()==FALSE){
                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$data);
                // $this->load->view('include/footer');
              }else{
                $id       = $this->input->post("skillcheck");
                $vdlink     = $this->input->post("videouploadchoice");
                $description  = $this->input->post("description");
                $title      = $this->input->post("title");
                // $subtitle=$this->input->post("subtitle");
                $skilldata=array('0'=>$id,'1'=>$vdlink,'2'=>$description,'3'=>$title);
                
                
                // $data['result']=$this->Admin_model->return_skill($skilldata);
                $this->load->view('include/header1');
                $this->load->view('admin/admin_nav',$data);
                $this->load->view('admin/upload_view',$skilldata);
                // $this->load->view('include/footer');
              }
            }
        }//////END_OF_FIRST_RADIO_CHOICE
      }
    }
  }
}
else
{
  redirect('user/login');
}

}
// }
  // ===========HTZN Save & Save Draft


 //==============TKH ===========for post list(upload content list)
function getpost()
{
    // $data['post']=$this->Admin_model->getpost()
 $log=$this->session->userdata('logged_in');
 if(isset($log))
 {
            // var_dump("Hello");die();
  if($log['role']=='admin')
  { 
                //$data['name']      = $this->User_model->getuser($log['email']);
    if($log['email']!=Null)
    {
     $data['name']    = $this->User_model->getuserbyemail($log['email']);
   }
   else
   {
     $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
   }
   $data['post']        = $this->Admin_model->getpost(); 
                // var_dump($data['post']);die();
   if($data['post']==Null)
   {
    $data['opptype']="";
                // var_dump($data['opptype']);die();
    $data['skilltype']="";
  }
  else
  {
    $count=count($data['post']);
    $i=0;
    foreach ($data['post'] as $value) 

    {
      $array[$i]=$value['id'];
      $i++;
    }
                        // var_dump($array);die();

    $data['opptype']=$this->Admin_model->opptype($count,$array);
                        // var_dump($data['opptype']);die();
    $data['skilltype']=$this->Admin_model->skilltype($count,$array);
  }
                // var_dump($data['post']);
  $data['all_opp']     = $this->Admin_model->opportunity();
  $data['all_skill']   = $this->Admin_model->skill();
  $data1['post']        = $this->Admin_model->getfeature();
  $data['fcount']      = count($data1['post']);
                        // var_dump($data['fcount']);die();

  $this->load->view('admin/postlist_view',$data);
  $this->load->view('include/header1');
  $this->load->view('admin/admin_nav',$data);
}

else
{
  redirect('user/login');
}
}
else
{
  redirect('user/login');
}
}

  //==============TKH ===========for post list(edit)
function post_edit($id)
{
    // var_dump($id);
  $official=$this->input->post('official');
  $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
  if(isset($log))
  {
        // var_dump("Hello");die();
    if($log['role']=='admin')
    {     


      $origin_encrypt=str_replace("~","/",$id);
      $decrypt_id=$this->encrypt->decode($origin_encrypt);
     // var_dump($origin_encrypt,$decrypt_id);die();
      //  $data['name']     = $this->User_model->getuser($log['email']);
      if($log['email']!=Null)
      {
       $data['name']    = $this->User_model->getuserbyemail($log['email']);
     }
     else
     {
       $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
     }
     $data['all_opp']    = $this->Admin_model->opportunity();
     $data['all_skill']  = $this->Admin_model->skill();
     $data['post']     = $this->Admin_model->post_edit($decrypt_id);
     $data['location']   =  $this->Admin_model->clocation($decrypt_id);         

     $data['opp']      = $this->Admin_model->copp($decrypt_id);
     $data['skill']    = $this->Admin_model->cskill($decrypt_id);
     $data['post1']    = $data['post']->tag;
     $data['post1']    = unserialize($data['post1']);
// var_dump($data['location']);die();
     $this->form_validation->set_rules('title','Title', 'trim|required');
     $this->form_validation->set_rules('description','Description','trim|required');
     if($this->form_validation->run()==FALSE)
     {
      $this->load->view('include/header1');
      $this->load->view('admin/postedit_view',$data);
      $this->load->view('admin/admin_nav',$data); 
        // $this->load->view('include/footer');
    }
    else
    {     
      $tag_input=$this->input->post('tags');
            // var_dump($tag_input);die();
      $all_tags = explode( ',' , $tag_input);
      $tag      =   serialize($all_tags);

      $loc          = $this->input->post('location');
      if($loc!=NULL)
      {
        $location     = implode(',', $loc);
      }
      else
      { 
        foreach ($data['location'] as $value)
        {                                                       
          $location = $value->location;
        }

      }

      $oppcheck       =   $this->input->post("oppcheck");
      $skillcheck     =   $this->input->post("skillcheck");
      $img1           =   $this->input->post("imageupload");
      $image          =   $this->img_upload2();
      $video          =   $this->input->post("videouploadchoice");
      $video1         =   $this->input->post("videoupload");
      $description    =   $this->input->post("description");
      $title          =   $this->input->post("title");
      $apply_link     =   $this->input->post("apply");
      $official       =   $this->input->post('official');
      $uploadchoiceid = $this->input->post("uploadchoice");
      $c_location     = $this->input->post("c_location");
      $time           = $this->input->post("time");
// var_dump($apply_link);die();
      $choiceid       = $this->input->post("choice");
      $deadline       = $this->input->post("date");
                            // $deadline2     = $this->input->post("date1");
      if($deadline==1)
      {
        $deadline   = $this->input->post('deadline');
      }
      else if($deadline==2)
      {
        $deadline   = $this->input->post('deadline2');
      }
      else
      {
        $deadline=  " ";
      }
           // var_dump($tag,$oppcheck ,$skillcheck,$img1,$image,$video,$video1,$description,$title,$apply,$uploadchoiceid,$choiceid,$deadline);die();
      if($image==NULL)
      {
        $image=$img1;

      }
      else
      {
        $image    = $this->img_upload2();
      }
      if($video==NULL)
      {   
        $video  = $video1;
      }
      else
      {
        $video=$video;
      }
                // var_dump($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$location,$oppcheck,$skillcheck,$choiceid,$uploadchoiceid);die();
      if($choiceid=="1")
      {  
        $opp          =1;
        $skill        =0;

        if($uploadchoiceid=='1')
        {

         $video        = "";
         $delete       = $this->Admin_model->delete_opp($decrypt_id);
         $delete       = $this->Admin_model->delete_skill($decrypt_id);
         if($delete)
         {
          $res =$this->Admin_model->editpost_opp($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck);

          if($res)
          {
           redirect('admin/getpost');
         }
       }
     } 
     else
     {
       $video        = $this->input->post('videouploadchoice');
       $image="";
       $delete=$this->Admin_model->delete_opp($decrypt_id);
       $delete=$this->Admin_model->delete_skill($decrypt_id);
       if($delete)
       {
        $res =$this->Admin_model->editpost_opp($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck);

        if($res)
        {
         redirect('admin/getpost');
       }
     }
   } 

                        }//for $choiceid=='1'
                        else  //for $choiceid=='2'
                        {
                          $skill          =1;
                          $opp            =0;

                          if($uploadchoiceid==1)
                          {
                           $image        = $this->img_upload2();
                           $video="";
                           $delete=$this->Admin_model->delete_opp($decrypt_id);
                           $delete=$this->Admin_model->delete_skill($decrypt_id);
                           if($delete)
                           {
                            $res =$this->Admin_model->editpost_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck);

                            if($res)
                            {
                             redirect('admin/getpost');
                           }
                         }
                       } 
                       else
                       {
                        $image="";
                        $video        = $this->input->post('videouploadchoice');
                        $delete=$this->Admin_model->delete_opp($decrypt_id);
                        $delete=$this->Admin_model->delete_skill($decrypt_id);
                        if($delete)
                        {
                          $res =$this->Admin_model->editpost_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck);

                          if($res)
                          {
                           redirect('admin/getpost');
                         }
                       }
                     }
                        }//end for choiceid=2
                      } 
    }//admin
    else
    {     
     redirect('user/login');
   }
   }// if log

   else
   {
    redirect('user/login');
  }

}//function

function post_delete($id)
{
  $origin_encrypt = str_replace("~","/",$id);
  $decrypt_id   = $this->encrypt->decode($origin_encrypt);

  $result=$this->Admin_model->post_delete($decrypt_id);
  if($result)
  {
    redirect('admin/getpost');
  }
  else
  {
    echo "Fail Editing Your Post";
  }
}


function get_savedraft()
{
  $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
  if(isset($log))
  {
            // var_dump("Hello");die();
    if($log['role']=='admin')
    { 
               // $data['name']     = $this->User_model->getuser($log['email']);
     if($log['email']!=Null)
     {
       $data['name']    = $this->User_model->getuserbyemail($log['email']);
     }
     else
     {
       $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
     }
     $data['query']    = $this->Admin_model->get_draft();
                         // var_dump($data['query']);die();
     if($data['query']==Null)
     {
       $data['opptype']="" ;
       $data['skilltype']="";
     }
     else
     {
      $count=count($data['query']);
      $i=0;
      foreach ($data['query'] as $value) 

      {
        $array[$i]=$value['id'];
        $i++;
      }
                     // if($array!= NULL){
                        // var_dump($array);die();

      $data['opptype']=$this->Admin_model->saveopptype($count,$array);
                        // var_dump($data['opptype']);die();
      $data['skilltype']=$this->Admin_model->saveskilltype($count,$array);
    }
       //         $data['all_opp']  = $this->Admin_model->opportunity();
            // $data['all_skill']   = $this->Admin_model->skill();
                // var_dump($data['query'],';;;;',$data['all_opp'],'all_opp',$data['all_skill'],'all_skill');die();

    $this->load->view('admin/savedraft_view',$data);
    $this->load->view('include/header1');
    $this->load->view('admin/admin_nav',$data);
  }

            // $this->load->view('include/footer');
            // }
  else
  {
    redirect('user/login');
  }

}
}


  function delete_draft($id)//HTZN
  {
    $origin_encrypt = str_replace("~","/",$id);
    $decrypt_id = $this->encrypt->decode($origin_encrypt);

    $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
    if(isset($log))
    {
            // var_dump("Hello");die();
      if($log['role']=='admin')
      {
        $result=$this->Admin_model->delete_draft($decrypt_id);
        if($result)
        {
              // echo "Successful Deleting Skill";
          redirect('admin/get_savedraft');
        }else
        {
          echo "Fail Deleting";
        }
      }
      else
      {
        redirect('user/login');
      }
    }
  }

  function save_savedraft($id)//HTZN
  {
    // var_dump($id);       
    $origin_encrypt = str_replace("~","/",$id);
    $decrypt_id = $this->encrypt->decode($origin_encrypt);

    $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
    if(isset($log))
    {
            // var_dump("Hello");die();
      if($log['role']=='admin')
      {
        $result=$this->Admin_model->save_savedraft($decrypt_id);
        if($result)
        {
              // echo "Successful Deleting Skill";
          redirect('admin/getpost');
        }else
        {
          echo "Fail Saving";
        }
      }
      else
      {
        redirect('user/login');
      }
    }
  }
  function edit_draft($id)
  {
    // var_dump($id);die();
    $official=$this->input->post('official');
    $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
    if(isset($log))
    {
        // var_dump("Hello");die();
      if($log['role']=='admin')
      {     


        $origin_encrypt=str_replace("~","/",$id);
        $decrypt_id=$this->encrypt->decode($origin_encrypt);
     // var_dump($decrypt_id);die();
      //  $data['name']     = $this->User_model->getuser($log['email']);
        if($log['email']!=Null)
        {
         $data['name']    = $this->User_model->getuserbyemail($log['email']);
       }
       else
       {
         $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
       }
       $data['all_opp']    = $this->Admin_model->opportunity();
       $data['all_skill']  = $this->Admin_model->skill();
       $data['post']     = $this->Admin_model->draft_edit($decrypt_id);
       $data['location']   =  $this->Admin_model->clocation($decrypt_id);         

       $data['opp']      = $this->Admin_model->copp($decrypt_id);
       $data['skill']    = $this->Admin_model->cskill($decrypt_id);
       $data['post1']    = $data['post']->tag;
       $data['post1']    = unserialize($data['post1']);
// var_dump($data['location']);die();
       $this->form_validation->set_rules('title','Title', 'trim|required');
       $this->form_validation->set_rules('description','Description','trim|required');
       if($this->form_validation->run()==FALSE)
       {
        $this->load->view('include/header1');
        $this->load->view('admin/editdraft_view',$data);
        $this->load->view('admin/admin_nav',$data);
        // $this->load->view('include/footer');
      }
      else
      {     
        $tag_input=$this->input->post('tags');
            // var_dump($tag_input);die();
        $all_tags = explode( ',' , $tag_input);
        $tag      =   serialize($all_tags);

        $loc          = $this->input->post('location');
        if($loc!=NULL)
        {
          $location     = implode(',', $loc);
        }
        else
        { 
          foreach ($data['location'] as$value)
          {                                                       
            $location = $value['location'];
          }

        }
        $save=1;
        $oppcheck     =   $this->input->post("oppcheck");
        $skillcheck     =   $this->input->post("skillcheck");
        $img1       =   $this->input->post("imageupload");
        $image      =   $this->img_upload2();
        $video      =   $this->input->post("videouploadchoice");
        $video1     =   $this->input->post("videoupload");
        $description  =   $this->input->post("description");
        $title=$this->input->post("title");
        $apply_link=$this->input->post("apply");
        $official=$this->input->post('official');
        $uploadchoiceid   = $this->input->post("uploadchoice");
        $c_location = $this->input->post("c_location");
        $time = $this->input->post("time");
        $choiceid       = $this->input->post("choice");
        $deadline    = $this->input->post("date");
                            // $deadline2     = $this->input->post("date1");
        if($deadline==1)
        {
          $deadline=$this->input->post('deadline');
        }
        else if($deadline==2)
        {
          $deadline=$this->input->post('deadline2');
        }
        else
        {
          $deadline=  " ";
        }
           // var_dump($tag,$oppcheck ,$skillcheck,$img1,$image,$video,$video1,$description,$title,$apply,$uploadchoiceid,$choiceid,$deadline);die();
          // var_dump($img1);die();
        if($image==NULL)
        {
          $image=$img1;

        }
        else
        {
          $image=$this->img_upload2();
        }
        if($video==NULL)
        {   
          $video=$video1;
        }
        else
        {
          $video=$video;
        }
                // var_dump($title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$location,$oppcheck,$skillcheck,$choiceid,$uploadchoiceid);die();
        if($choiceid=="1")
        {  
          $opp          =1;
          $skill        =0;

          if($uploadchoiceid=='1')
          {

           $video        ="";
           $delete=$this->Admin_model->delete_opp($decrypt_id);
           $delete=$this->Admin_model->delete_skill($decrypt_id);
           if($delete)
           {
            $res =$this->Admin_model->editdraft_opp($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$save);

            if($res)
            {
             redirect('admin/get_savedraft');
           }
         }
       } 
       else
       {
         $video        = $this->input->post('videouploadchoice');
         $image="";
         $delete=$this->Admin_model->delete_opp($decrypt_id);
         $delete=$this->Admin_model->delete_skill($decrypt_id);
         if($delete)
         {
          $res =$this->Admin_model->editdraft_opp($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$oppcheck,$save);

          if($res)
          {
           redirect('admin/get_savedraft');
         }
       }
     } 

                        }//for $choiceid=='1'
                        else  //for $choiceid=='2'
                        {
                          $skill          =1;
                          $opp            =0;

                          if($uploadchoiceid==1)
                          {
                               // $image        = $this->img_upload2();
                           $video="";
                           $delete=$this->Admin_model->delete_opp($decrypt_id);
                           $delete=$this->Admin_model->delete_skill($decrypt_id);
                           if($delete)
                           {
                            $res =$this->Admin_model->editdraft_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck,$save);

                            if($res)
                            {
                             redirect('admin/get_savedraft');
                           }
                         }
                       } 
                       else
                       {
                        $image="";
                        $video        = $this->input->post('videouploadchoice');
                        $delete=$this->Admin_model->delete_opp($decrypt_id);
                        $delete=$this->Admin_model->delete_skill($decrypt_id);
                        if($delete)
                        {
                          $res =$this->Admin_model->editdraft_skill($decrypt_id,$title,$tag,$deadline,$description,$apply_link,$official,$image,$video,$c_location,$location,$time,$opp,$skill,$skillcheck,$save);

                          if($res)
                          {
                           redirect('admin/get_savedraft');
                         }
                       }
                     }
                        }//end for choiceid=2
                      } 
    }//admin
    else
    {
     redirect('user/login');
   }
   }// if log

   else
   {
    redirect('user/login');
  }

   }//function

   function img_upload2()
   {
    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|PNG|JPG|JPEG|GIF';
    $config['max_size']             = 10000;

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('imageuploadchoice'))
    {
      $image  = $this->input->post('imageuploadchoice');
                        // var_dump($image);die();
      return $image;
                        // return $error;
    }
    else
    {
      $data = array('upload_data' => $this->upload->data());
      return $data['upload_data']['file_name'];
    }
  }
// zinthida
  function add_feature($id)
  {

    $origin_encrypt = str_replace("~","/",$id);
    $decrypt_id =$this->encrypt->decode($origin_encrypt);
    $log=$this->session->userdata('logged_in');

    if(isset($log))
    {

      if($log['role']=='admin')
      {

        $ans=$this->Admin_model->add_feature($decrypt_id);

        if($ans)
        {
          redirect('admin/getpost');
        }

      }
    }

  }  

  function remove_feature($id)
  {
    $origin_encrypt = str_replace("~","/",$id);
    $decrypt_id =$this->encrypt->decode($origin_encrypt);
    $log=$this->session->userdata('logged_in');

    if(isset($log))
    {

      if($log['role']=='admin')
      {

        $ans=$this->Admin_model->remove_feature($decrypt_id);

        if($ans)
        {
          redirect('admin/getfeature');
        }

      }
    }
  }

    // feature retrieve by zinthidar

  function getfeature()
  {

    $log=$this->session->userdata('logged_in');
    if(isset($log))
    {

      if($log['role']=='admin')
      { 
               // $data['name']      = $this->User_model->getuser($log['email']);
        if($log['email']!=Null)
        {
          $data['name']    = $this->User_model->getuserbyemail($log['email']);
        }
        else
        {
          $data['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
        }
        $data['post']        = $this->Admin_model->getfeature();
        $data['all_opp']     = $this->Admin_model->opportunity();
        $data['all_skill']   = $this->Admin_model->skill();
        $this->load->view('include/header1');

        $this->load->view('admin/featurelist_view',$data);
        $this->load->view('admin/admin_nav',$data);   
      }

      else
      {
        redirect('user/login');
      }
    }
    else
    {
      redirect('user/login');
    }
  }


    // end feature

  function feature_edit($id)
  {




    $log=$this->session->userdata('logged_in');

    if(isset($log))
    {

      if($log['role']=='admin')
      {     

        $official=$this->input->post('official');
        $tag_input=$this->input->post('tags');
        $all_tags = explode( ',' , $tag_input);
        $tag      =   serialize($all_tags);
        $origin_encrypt=str_replace("~","/",$id);
        $decrypt_id=$this->encrypt->decode($origin_encrypt);
        $data['location']   =  $this->Admin_model->clocation($decrypt_id);

        $loc          = $this->input->post('location');
        if($loc!=NULL)
        {
          $location     = implode(',', $loc);
        }
        else
        { 
          foreach ($data['location'] as $value)
          {                                                       
            $location = $value['location'];
          }

        }    
        $oppcheck    =   $this->input->post("oppcheck");

        $img1        =   $this->input->post("imageupload");
        $image       =   $this->img_upload2();
        $video       =   $this->input->post("videouploadchoice");
        $video1      =   $this->input->post("videoupload");
        $description =   $this->input->post("description");
        $title       =   $this->input->post("title");
        $apply       =   $this->input->post("apply");
        $c_location  =   $this->input->post("c_location");
        $feature=1;
        $save=0;
        $uploadchoiceid   = $this->input->post("uploadchoice");
        $deadline    = $this->input->post("date");

        if($deadline==1)
        {
          $deadline=$this->input->post('deadline');
        }
        else if($deadline==2)
        {
          $deadline=$this->input->post('deadline2');
        }
        else
        {
          $deadline=  " ";
        }

        if($image==NULL)
        {
          $image=$img1;

        }
        else
        {
          $image=$this->img_upload2();
        }
        if($video==NULL)
        {   
          $video=$video1;
        }
        else
        {
          $video=$video;
        }






                // $data['name']      = $this->User_model->getuser($log['email']);
        if($log['email']!=Null)
        {
         $result['name']    = $this->User_model->getuserbyemail($log['email']);
       }
       else
       {
         $result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
       }
       $data['all_opp']   = $this->Admin_model->opportunity();
       $data['all_skill'] = $this->Admin_model->skill();
       $data['post']      = $this->Admin_model->feature_edit($decrypt_id);

       $data['opp']       = $this->Admin_model->copp($decrypt_id);
       $data['skill']     = $this->Admin_model->cskill($decrypt_id);
       $data['post1']     = $data['post']->tag;
       $data['post1']     = unserialize($data['post1']);


       $this->form_validation->set_rules('title','Title', 'trim|required');
       $this->form_validation->set_rules('description','Description','trim|required');
       if($this->form_validation->run()==FALSE)
       {
        $this->load->view('include/header1');
        $this->load->view('admin/featureEdit_view',$data);
        $this->load->view('admin/admin_nav',$data);

      }
      else
      {     
                      // var_dump($image);die();
        $opp      = 1;
        $skill    = 0;
        if($uploadchoiceid=="1")
        {      
                                    // $image        = $this->img_upload2();
          $video        ="";
          $delete=$this->Admin_model->delete_opp($decrypt_id);
          $delete=$this->Admin_model->delete_skill($decrypt_id);
          if($delete){
            $result = $this->Admin_model->edit_feature($opp,$skill,$tag,$deadline,$c_location,$location,$image,$video,$description,$title,$apply,$save,$decrypt_id,$feature,$oppcheck,$official);
            if($result)
            {
             redirect('admin/getfeature');
           }
         }

                              }//if $uploadchoice==1
                              else if($uploadchoiceid=="2")
                              {
                                // $video        = $this->input->post('videouploadchoice');
                                $image        ="";
                                $delete=$this->Admin_model->delete_opp($decrypt_id);
                                $delete=$this->Admin_model->delete_skill($decrypt_id);
                                if($delete){
                                  $result = $this->Admin_model->edit_feature($opp,$skill,$tag,$deadline,$c_location,$location,$image,$video,$description,$title,$apply,$save,$decrypt_id,$feature,$oppcheck,$official);
                                  if($result)
                                  {
                                   redirect('admin/getfeature');
                                 }
                               }
                             }
                }//else

              }//second if
         }// if log

         else
         {
          redirect('user/login');
        }
        
   }//function


 }


