<?php
	/**
	* 
	*/
	class Feature extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('User_model','',TRUE);
			$this->load->model('Admin_model','',TRUE);
		    $this->load->library('form_validation');
		    $this->load->model('Feature_model','',TRUE);
		    $this->load->model('Custom_model','',TRUE);
		    $this->load->model('Opportunity_model','',TRUE);
		    $this->load->model('Profile_model','',TRUE);
		    $this->load->model('Skill_model','',TRUE);
		    $this->load->helper('form');
		    $this->load->library('ckeditor');
		    $this->load->library('ckfinder');
		    $this->load->library('session');
			$this->load->helper('security');		   
			$this->load->library('encrypt');
    		$this->load->model('User_model');
		}
		function index()
		{
			 
        }


		function add_feature()
		{  
            $log=$this->session->userdata('logged_in');
			    if(isset($log))
			    {
			      if($log['role']=='admin')
			      {
			   //    	     $data['name']		=	$this->User_model->getuser($log['email']);
			   //          $data['all_opp']	=	$this->Admin_model->opportunity();
						// $data['all_skill']	=	$this->Admin_model->skill();
    	 //    			$this->form_validation->set_rules('title','Title', 'trim|required');
						// $this->form_validation->set_rules('description','Description','trim|required');
						// $this->form_validation->set_rules('choice','Choice','required');
						// $this->form_validation->set_rules('uploadchoice','UploadChoice','required'); 
						// $this->form_validation->set_rules('submitform','Submit'); 
						
				  // if($this->form_validation->run()==FALSE)
				  // { 

      //                   $this->load->view('include/header');
						// $this->load->view('admin/admin_nav',$data);
						// $this->load->view('admin/upload_view',$data);
	                    
			   //    }
			   //    else
			   //    {
			            $tag_input 	=	$this->input->post('tags');
			            $all_tags 	= explode( ',' , $tag_input);
						$tag 		= serialize($all_tags);
						$description 	=	$this->input->post("description");
						$apply 			=	$this->input->post("apply");
	                    $title 			=	$this->input->post("title");
	                    $deadline 		=	$this->input->post("deadline");
	                    

	                    $choiceid = $this->input->post("choice");

						   if($choiceid=="1")
						{
						// $this->form_validation->set_rules('opportunitychoice','OpportunityChoice','required');
							if($this->form_validation->run()==FALSE)
							{ 
								$this->load->view('include/header');
								$this->load->view('admin/admin_nav',$data);
								$this->load->view('admin/upload_view',$data);
								
							}
							else
							{
								
							$uploadchoiceid = $this->input->post('uploadchoice');
							if($uploadchoiceid=="1")
							{  
								
								if($this->form_validation->run()==FALSE)
								{
									$this->load->view('include/header');
									$this->load->view('admin/admin_nav',$data);
									$this->load->view('admin/upload_view',$data);
									
								}
								else
								{
                                   
                                    
									$skill_id 	 = 0;
									$oppcheck 	 = 0;
 									
 									$oppcheck1 	 = $this->input->post('oppcheck1');
 									$opp 	 = $this->input->post('opp');
 									$opp_im = implode(',', $oppcheck1);
 									$feature=1;
									$save		 = 0;
									$tag 		 = $tag;
									$deadline 	 = $this->input->post("deadline");
									$image     	 = $this->img_upload2();
									// var_dump($opp_im);die();
								
									$res 	= 	$this->Feature_model->add_feature($title,$tag,$deadline,$description,$apply_link,$oppcheck,$opp_im,$opp,$skill_id,$image,$save,$feature);
									var_dump($res);die();
									

									if($res)
									{
										redirect('admin/get_featurelist');
									}
									else
										{
											echo "Fail";
											redirect('admin/savedraft');

										}
								
								}
							}//if($uploadchoiceid=='1')
							elseif($uploadchoiceid=='2')
							{ 
								$this->form_validation->set_rules('videouploadchoice','VideoUploadChoice','required');
								if($this->form_validation->run()==FALSE)
									{ 
										$this->load->view('include/header');
										$this->load->view('admin/admin_nav',$data);
										$this->load->view('admin/upload_view',$data);
										$this->load->view('include/footer');
									}
								else
									{
										$title 		 = $this->input->post("title");
										$description = $this->input->post("description");
										$apply_link	 = $this->input->post("apply");
										$oppcheck 	 = 0;
										// $oppcheck 	 = $this->input->post('oppcheck');
 										$oppcheck1 	 = $this->input->post('oppcheck1');
 										$opp 	 = $this->input->post('opp');
									    $feature =1;
 										$opp_im = implode(',', $oppcheck1);
 										// $opp 	 = $this->input->post('oppcheck1');
										$skill_id 	 = 0;
										$video 		 = $this->input->post('videouploadchoice');
										$tag         = $tag;
										$deadline    = $this->input->post("deadline");
										$save 		 = 0;
										// var_dump($opp_im);die();
										$res 	=  $this->Feature_model->add_feature1($title,$tag,$deadline,$description,$apply_link,$oppcheck,$opp_im,$opp,$skill_id,$video,$save,$feature);
										
										if($res)
										{
											redirect('admin/get_featurelist');
										}
										else{
											echo Fail;
											redirect('admin/savedraft');
										}
									}
							}//elseif($uploadchoiceid=='2')

							}//End of Uploadchoiceid1
						}//choiceid1
						elseif($choiceid=='2')
						{
							// $this->form_validation->set_rules('skillchoice','SkillChoice','required');
							if($this->form_validation->run()==FALSE)
							{

								$this->load->view('include/header');
								$this->load->view('admin/admin_nav',$data);
								$this->load->view('admin/upload_view',$data);
								// $this->load->view('include/footer');	
							}
							else
							{
								$uploadchoiceid 	 = $this->input->post('uploadchoice');
								if($uploadchoiceid=='1')
								{
										$title       = $this->input->post('title');
										$description = $this->input->post('description');
										$image       = $this->img_upload2();
										$oppcheck 	 = 0;
										$feature=1;
										$skill_id 	 = $this->input->post('skillcheck');
										$apply_link  = $this->input->post('apply');
										$tag 		 = $tag;
										$deadline 	 = $this->input->post("deadline");
										$save 		 = 0;
										$res   =  $this->Feature_model->add_feature($title,$tag,$deadline,$description,$apply_link,$oppcheck,$skill_id,$image,$save,$feature);
										if($res)
										{
											redirect('admin/get_featurelist');
										}
										else
											{
												echo Fail;
												redirect('admin/savedraft');

											}
									// }

								}//if($uploadchoiceid=='1')
								elseif($uploadchoiceid=='2')
								{
									$this->form_validation->set_rules('videouploadchoice','VideoUploadChoice','required');
									if($this->form_validation->run()==FALSE)
									{
										$this->load->view('include/header');
										$this->load->view('admin/admin_nav',$data);
										$this->load->view('admin/upload_view',$data);
										// $this->load->view('include/footer');	
									}
									else
									{
										$title 			= $this->input->post('title');
										$description 	= $this->input->post('description');
										$oppcheck 		= 0;
										$feature        = 1;
										$skill_id   	= $this->input->post('skillcheck');
										$apply_link 	= $this->input->post('apply');
										$video 			= $this->input->post('videouploadchoice');
										$tag 			= $tag;
										$deadline 		= $this->input->post("deadline");
										$save 			= 0;

										$res = $this->Feature_model->add_feature1($title,$tag,$deadline,$description,$apply_link,$oppcheck,$skill_id,$video,$save,$feature);
										if($res)
										{
											redirect('admin/get_featurelist');
										}
										else
											{
												echo Fail;
												redirect('admin/savedraft');

											}
									}
								}//elseif($uploadchoiceid=='2')
								else
								{
									redirect('admin');
								}
							}

						}//choiceid2
						else
							{
								redirect('admin');
							}
					       
			      }
			      
			    }
			}
		// }


	function getpost()
	{
		// $data['post']=$this->Admin_model->getpost()
       $log=$this->session->userdata('logged_in');
			    if(isset($log))
			    {
			      // var_dump("Hello");die();
			      if($log['role']=='admin')
			      { 
    	    			$data['name'] 	 	 = $this->User_model->getuser($log['email']);
    	    			$data['post'] 	  	 = $this->Admin_model->getpost();
    	    			// var_dump($data['post']);
			            $data['all_opp']     = $this->Admin_model->opportunity();
						$data['all_skill']   = $this->Admin_model->skill();
						$this->load->view('include/header');
						$this->load->view('admin/admin_nav',$data);
						$this->load->view('admin/postlist_view',$data);
					 }
			      
			      else
			      {
			        redirect('user/login');
			      }
			    }
			    else
			    {
			    	redirect('user/login');
			    }
	}

	//==============TKH ===========for post list(edit)
	function post_edit($id)
	{
		// var_dump($id);die();
		$log=$this->session->userdata('logged_in');
           // var_dump($log);die();
	    if(isset($log))
	    {
	      // var_dump("Hello");die();
	      if($log['role']=='admin')
	      {     
		

            $origin_encrypt=str_replace("~","/",$id);
	        $decrypt_id=$this->encrypt->decode($origin_encrypt);
		 // var_dump($decrypt_id);die();
		    $data['name']	  	= $this->User_model->getuser($log['email']);
            $data['all_opp']  	= $this->Admin_model->opportunity();
			$data['all_skill']	= $this->Admin_model->skill();
			$data['post']    	= $this->Admin_model->post_edit($decrypt_id);
			// var_dump($data['post']);die();
			$data['opp']     	= $this->Admin_model->copp($decrypt_id);
			$data['skill']   	= $this->Admin_model->cskill($decrypt_id);
			$data['post1']   	= $data['post']->tag;
		    $data['post1']   	= unserialize($data['post1']);

			$this->form_validation->set_rules('title','Title', 'trim|required');
			$this->form_validation->set_rules('description','Description','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$this->load->view('include/header');
				$this->load->view('admin/postedit_view',$data);
				$this->load->view('admin/admin_nav',$data);
				// $this->load->view('include/footer');
			}
			else
			{   	
			    $tag_input=$this->input->post('tags');
		        // var_dump($tag_input);die();
				$all_tags = explode( ',' , $tag_input);
				// $all_tags=array_chunk($tag_array, 3);
                // var_dump($all_tags);die();
				$tag 			= 	serialize($all_tags);	
                $opp_id 		= 	$this->input->post("oppcheck");
                $skill_id 		= 	$this->input->post("skillcheck");
				// $img=$this->input->post("imageuploadchoice");
				$img1 			= 	$this->input->post("imageupload");
				$image 			= 	$this->img_upload2();
				$video 			= 	$this->input->post("videouploadchoice");
				$video1 		= 	$this->input->post("videoupload");
				$description 	= 	$this->input->post("description");
                $title=$this->input->post("title");
                // $subtitle=$this->input->post("subtitle");
                $apply=$this->input->post("apply");

    			 $uploadchoiceid 	= $this->input->post("uploadchoice");
    			 $choiceid 			= $this->input->post("choice");
    			 $deadline       	= $this->input->post('deadline');
    			 // var_dump($uploadchoiceid);die();
    			 if($image==NULL)
				{
					$image=$img1;
				  // var_dump($opp_id,$opp_image,$opp_image1,$opp_description,$opp_title,$opp_subtitle);die();
				}
				else
				{
					$image=$this->img_upload2();
				}
				if($video==NULL)
				{		
					$video=$video1;
				}
				else
				{
					$video=$video;
				}

				if($choiceid=="1")
				{
						if($uploadchoiceid=="1")
					{
						$opp_id   		= $this->input->post("oppcheck");
                		$skill_id   	= 0;
						$image   		= $image;
						$video   		= " ";
						$description 	= $this->input->post("description");
                		$title    		= $this->input->post("title");
                		$apply    		= $this->input->post("apply");
                		$tag      		= $tag;
                		$deadline 		= $deadline;
                		$save     		= 0;

                		$res    = $this->Admin_model->edit_post($decrypt_id);
						$result = $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						if($result)
						{
							redirect('admin/getpost');
						}
						else
						{
							echo "Fail Editing Post";
						}
					}//if $uploadchoice==1
					elseif($uploadchoiceid=="2")
					{
						$skill_id 		= 0;
                		$opp_id 		= $this->input->post("oppcheck");
						$image 			= " " ;
						$video 			= $video;
						$description 	= $this->input->post("description");
                		$title 			= $this->input->post("title");
                		$apply 			= $this->input->post("apply");
                		$tag 			= $tag;
                		$deadline 		= $deadline;
                		$save 			= 0;

                		$res 	= $this->Admin_model->edit_post($decrypt_id);
						$result = $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						// var_dump($result);die();			
						if($result)
						{
							// echo "Successful Editing Opportunity";
							redirect('admin/getpost');
						}
						else
						{
							echo "Fail Editing Opportunity";
						}
					}//else if Suploadchoice==2
				}
				else
				{

						if($uploadchoiceid=="1")
					{
						
						$skill_id 		= $this->input->post("skillcheck");
						// var_dump($skill_id);die();
                		$opp_id 		= 0;
						$image 			= $image;
						$video 			= " ";
						$description 	= $this->input->post("description");
                		$title 			= $this->input->post("title");
                		$apply 			= $this->input->post("apply");
                		$save  			= 0;

                		$res 			= $this->Admin_model->edit_post($decrypt_id);
						$result			= $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						if($result)
						{
							redirect('admin/getpost');
						}
						else
						{
							echo "Fail Editing Post";
						}
					}//if $uploadchoice==1
					elseif($uploadchoiceid=="2")
					{
						$opp_id 	 = 0;
                		$skill_id    = $this->input->post("skillcheck");
						$image 	 	 = " ";
						$video 		 = $video;
						$description = $this->input->post("description");
                		$title 		 = $this->input->post("title");
                		$apply 		 = $this->input->post("apply");
                		$save 		 = 0;

                		$res 	= $this->Admin_model->edit_post($decrypt_id);
						$result = $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						// var_dump($result);die();			
						if($result)
						{
							redirect('admin/getpost');
						}
						else
						{
							echo "Fail Editing Opportunity";
						}
					}//else if Suploadchoice==2
				}
                // var_dump($opp_id,$skill_id,$image,$image1,$video,$video1,$description,$title,$subtitle,$uploadchoiceid);die;
					
			}//else
    			
			}//second if
	      }// if log
	  
	      else
	      {
	        redirect('user/login');
	      }
	      
	 }//function
	   function post_delete($id)
	   {
	   	    $origin_encrypt = str_replace("~","/",$id);
	        $decrypt_id 	= $this->encrypt->decode($origin_encrypt);

	        $result=$this->Admin_model->post_delete($decrypt_id);
            if($result)
            {
            	redirect('admin/getpost');
            }
            else
            {
            	echo "Fail Editing Your Post";
            }
	   }
	

	function get_savedraft()
	{
		$log=$this->session->userdata('logged_in');
           // var_dump($log);die();
			    if(isset($log))
			    {
			      // var_dump("Hello");die();
			      if($log['role']=='admin')
			      { 
    	    			$data['name'] 		= $this->User_model->getuser($log['email']);
    	    			$data['query'] 		= $this->Admin_model->get_draft();
    	    			$data['all_opp'] 	= $this->Admin_model->opportunity();
						$data['all_skill'] 	= $this->Admin_model->skill();
    	    			// var_dump($data['query'],';;;;',$data['all_opp'],'all_opp',$data['all_skill'],'all_skill');die();
						$this->load->view('include/header');
						$this->load->view('admin/admin_nav',$data);
						$this->load->view('admin/savedraft_view',$data);
						
						// $this->load->view('include/footer');
			      }
			      else
			      {
			        redirect('user/login');
			      }
			      
			    }
	}

	function delete_draft($id)//HTZN
	{
		$origin_encrypt = str_replace("~","/",$id);
		    $decrypt_id = $this->encrypt->decode($origin_encrypt);
		    
			  $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
			    if(isset($log))
			    {
			      // var_dump("Hello");die();
			      if($log['role']=='admin')
			      {
			            $result=$this->Admin_model->delete_draft($decrypt_id);
						if($result)
						{
							// echo "Successful Deleting Skill";
							redirect('admin/get_savedraft');
						}else
						{
							echo "Fail Deleting";
						}
			      }
			      else
			      {
			        redirect('user/login');
			      }
				}
	}

	function save_savedraft($id)//HTZN
	{
		// var_dump($id);				
		$origin_encrypt = str_replace("~","/",$id);
		    $decrypt_id = $this->encrypt->decode($origin_encrypt);
		    
			  $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
			    if(isset($log))
			    {
			      // var_dump("Hello");die();
			      if($log['role']=='admin')
			      {
			            $result=$this->Admin_model->save_savedraft($decrypt_id);
						if($result)
						{
							// echo "Successful Deleting Skill";
							redirect('admin/getpost');
						}else
						{
							echo "Fail Saving";
						}
			      }
			      else
			      {
			        redirect('user/login');
			      }
				}
	}
	function edit_draft($id)
	{

		$log=$this->session->userdata('logged_in');
           // var_dump($log);die();
	    if(isset($log))
	    {
	      // var_dump("Hello");die();
	      if($log['role']=='admin')
	      {     
		

            $origin_encrypt = str_replace("~","/",$id);
	        $decrypt_id=$this->encrypt->decode($origin_encrypt);
		 // var_dump($decrypt_id);die();
 			$data['name'] 	  	=  $this->User_model->getuser($log['email']);
            $data['all_opp']  	=  $this->Admin_model->opportunity();
			$data['all_skill'] 	=  $this->Admin_model->skill();
			$data['post'] 	  	=  $this->Admin_model->post_draft($decrypt_id);
			$data['opp'] 	  	=  $this->Admin_model->copp($decrypt_id);
			$data['skill'] 	  	=  $this->Admin_model->cskill($decrypt_id);
			$data['post1']	  	=  $data['post']->tag;
		    $data['post1'] 	  	=  unserialize($data['post1']);
            // var_dump($data['opp']);die();					
			$this->form_validation->set_rules('title','Title', 'trim|required');
			$this->form_validation->set_rules('description','Description','trim|required');
			$this->form_validation->set_rules('choice','Choice','required');
			$this->form_validation->set_rules('uploadchoice','UploadChoice','required');
			if($this->form_validation->run()==FALSE)
			{
				$this->load->view('include/header');
				$this->load->view('admin/editdraft_view',$data);
				$this->load->view('admin/admin_nav',$data);
				// $this->load->view('include/footer');	
			}
			else
			{   
			   $tag_input 	= $this->input->post('tags');
		        // var_dump($tag_input);die();
				$all_tags 	= explode( ',' , $tag_input);
				// $all_tags=array_chunk($tag_array, 3);
                // var_dump($all_tags);die();
				$tag 			= serialize($all_tags);			
                $opp_id 		= $this->input->post("oppcheck");
                $skill_id 		= $this->input->post("skillcheck");
				// $img=$this->input->post("imageuploadchoice");
				$img1 			= $this->input->post("imageupload");
				$image 			= $this->img_upload2();
				$video 			= $this->input->post("videouploadchoice");
				$video1 		= $this->input->post("videoupload");
				$description 	= $this->input->post("description");
                $title 			= $this->input->post("title");
                // $subtitle=$this->input->post("subtitle");
                $apply 			= $this->input->post("apply");
                $choiceid 		= $this->input->post('choice');
                $deadline 	 	= $this->input->post('deadline');
    			$uploadchoiceid = $this->input->post("uploadchoice");
    			 // var_dump($uploadchoiceid);die;
    			 if($image==NULL)
				{
					$image=$this->img_upload2();
				  // var_dump($opp_id,$opp_image,$opp_image1,$opp_description,$opp_title,$opp_subtitle);die();
				}
				else
				{
					$image=$img1;
				}
				if($video==NULL)
				{
					$video=$video1;
				}
				else
				{
					$video=$video;

				}
				if($choiceid=="1")
				{
						if($uploadchoiceid=="1")//img
					{
								
						$opp_id  	 = $this->input->post("oppcheck");
                		$skill_id 	 = 0;
						$image 		 = $image;
						$video 		 = " ";
						$description = $this->input->post("description");
                		$title 		 = $this->input->post("title");
                		$apply 		 = $this->input->post("apply");
                		$save 		 = 1;
                		$res 		 = $this->Admin_model->edit_post($decrypt_id);
						$result 	 = $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						if($result)
						{
							// echo "Successful Editing Opportunity";
							redirect('admin/get_savedraft');
						}
						else
						{
							echo "Fail Editing Post";
						}
					}//if $uploadchoice==1
					elseif($uploadchoiceid=="2")//vd
					{
						$skill_id 	 = 0;
                		$opp_id 	 = $this->input->post("oppcheck");
						$image  	 = "";
						$video 		 = $video;
						$description = $this->input->post("description");
                		$title 		 = $this->input->post("title");
                		$apply 		 = $this->input->post("apply");
                 		$save 		 = 1;

               			$res 		= $this->Admin_model->edit_post($decrypt_id);
               			// var_dump($opp_id,$skill_id,$image,$video,$description,$title,$apply,$decrypt_id);die();
						$result 	= $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						// var_dump($result);die();			
						if($result)
						{
							// echo "Successful Editing Opportunity";
							redirect('admin/get_savedraft');
						}
						else
						{
							echo "Fail Editing Opportunity";
						}
					}//else if Suploadchoice==2
				}
				else{
						if($uploadchoiceid=="1")//img
					{
						$skill_id 	 = $this->input->post("skillcheck");
                		$opp_id 	 = 0;
						$image 		 = $image;
						$video 		 = " ";
						$description = $this->input->post("description");
                		$title 		 = $this->input->post("title");
                		$apply 		 = $this->input->post("apply");
                		$save  		 = 1;

                		$res   	= $this->Admin_model->edit_post($decrypt_id);
						$result = $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						if($result)
						{
							// echo "Successful Editing Opportunity";
							redirect('admin/get_savedraft');
						}
						else
						{
							echo "Fail Editing Post";
						}
					}//if $uploadchoice==1
					elseif($uploadchoiceid=="2")//vd
					{
						$opp_id 	 = 0;
                		$skill_id 	 = $this->input->post("skillcheck");
						$image 		 = " ";
						$video 		 = $video;
						$description = $this->input->post("description");
                		$title 		 = $this->input->post("title");
                		$apply 		 = $this->input->post("apply");
                 		$save 		 = 1;

               			$res 	= $this->Admin_model->edit_post($decrypt_id);
						$result = $this->Admin_model->editpost($opp_id,$skill_id,$tag,$deadline,$image,$video,$description,$title,$apply,$save,$decrypt_id);
						// var_dump($result);die();			
						if($result)
						{
							// echo "Successful Editing Opportunity";
							redirect('admin/get_savedraft');
						}
						else
						{
							echo "Fail Editing Opportunity";
						}
					}//else if Suploadchoice==2
				}
                // var_dump($opp_id,$skill_id,$image,$image1,$video,$video1,$description,$title,$subtitle,$uploadchoiceid);die;
					
			}//else
    			
			}//second if
	      }// if log
	  
	      else
	      {
	        redirect('user/login');
	      }
	      
	 }//function

    	function img_upload2()
    {
      $config['upload_path']          = './uploads/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['max_size']             = 1000;

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('imageuploadchoice'))
                {
                        $image  = $this->input->post('imageuploadchoice');
                      	return $image;
                        // return $error;
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        return $data['upload_data']['file_name'];
                }
    }


}
	    
