<?php
/**
* 
*/
class Tag extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->model('User_model');
		$this->load->library('encrypt');
		$this->load->model('Opportunity_model');
		$this->load->model('Skill_model');
		$this->load->model('Custom_model');
		$this->load->model('Sample_model','',TRUE);
	}

	function index()
	{

		$log=$this->session->userdata('logged_in');
           // var_dump($log);die();
		if(isset($log))
		{
			if($log['role']!='admin')
			{   

				$tag =$this->input->post('tag');
				$all_tags  = explode( ',' , $tag);                
		        $tags  = serialize($all_tags);

		        // var_dump($tag,$tags);die();

		        // $data = $this->Custom_model
				if($tags!=Null)
			      	{
            // echo "HELLo";
			      	
                   	  $search 			= $this->input->post('tag');
                   	  // var_dump($search);die();
				      $pag 				= $this->config->item('pagination');   
				      $pag['base_url']  = base_url().'custom/search';
				      $data['search']	= Null;
				      $data['tag']		= Null;
				      // $pag['total_rows'] = $this->Sample_model->count_opp();
				      $page=$this->Custom_model->searchcount($search);
				      // $pag2=$this->Custom_model->count_skill($id);
				      $pag['total_rows']	= $page;
				      $data['result'] 	= $this->Custom_model->tagdata($tags,$pag['per_page'],$this->uri->segment(3),'desc');
				      
				      
				      $data['pag'] 		= $pag;
				     // $result['name']	= $this->User_model->getuser($log["email"]);
					 	if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
				      $this->load->view('include/header');
				      
				      $data1['opp']  	= $this->Opportunity_model->left_all();
				      $data1['skill']   = $this->Skill_model->left_all();
				      // var_dump($result['name']);die();
				      	$this->load->view('include/nav',$result);
						$this->load->view('home/custom_view',$data);
						$this->load->view('home/left_view',$data1);
						$this->load->view('include/footer2');   
					
			      }
			   //    else
			   //    { 
			      
      //                 $search1 			= $this->input->post('search1');
      //                 // var_dump($search1);die();

				  //     $pag 				= $this->config->item('pagination');   
				  //     $pag['base_url']  = base_url().'custom/search';
				  //     $data['search']	= Null;
				  //     $data['tag']		= Null;
				  //     // $pag['total_rows'] = $this->Sample_model->count_opp();
				  //     $page=$this->Custom_model->searchcount($search1);
				  //     // $pag2=$this->Custom_model->count_skill($id);
				  //     $pag['total_rows']	= $page;
				  //     // var_dump($pag);die();
				  //     // $data['result'] 	= $this->Custom_model->search_tag($search1,$pag['per_page'],$this->uri->segment(3),'desc');
				  //     // var_dump($data['result']);die();
				  //     // $real=unserialize($data['custom']);
				      
				      
				  //     $data['pag'] 		= $pag;
				  //     $result['name']	= $this->User_model->getuser($log["email"]);
				  //     $this->load->view('include/header');
				      
				  //     $data1['opp']  	= $this->Opportunity_model->left_all();
				  //     $data1['skill']   = $this->Skill_model->left_all();
				  //     // var_dump($data['result']);die();
						// $this->load->view('include/nav',$result);
						// $this->load->view('home/custom_view',$data);
						// $this->load->view('home/left_view',$data1);
						// $this->load->view('include/footer2');  
			   //    }  
			  }
			      else
			      {
			        redirect('user/login');
			      }
			      
 }
					} 

		}

	// }
// }
