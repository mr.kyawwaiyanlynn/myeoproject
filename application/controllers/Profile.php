<?php

/**
 *
 */
class Profile extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->helper('form');
        $this->load->model('Profile_model');
        $this->load->model('User_model');
        $this->load->model('Opportunity_model');
        $this->load->model('Skill_model');
        $this->load->model('Sample_model');
        $this->load->library('encrypt');
        $this->load->helper('file');

    }


    function index()
    {
        $log = $this->session->userdata('logged_in');
        if ($log['role'] != 'admin') {


            if ($log['email'] != Null) {
                $result['data'] = $this->User_model->getuserbyemail($log['email']);
            } else {
                $result['data'] = $this->User_model->getuserbyfacebookid($log['facebook']);
            }

            $user_id = $log['id'];
            $sess_array = array('id' => $result['data']['user_id'], 'username' => $result['data']['name'],
                'password' => $result['data']['password'], 'email' => $result['data']['email'], 'role' => $result['data']['role'], 'facebook' => Null, 'linkedin' => null);
            $this->session->set_userdata('logged_in', $sess_array);
            $id = $result['data']['user_id'];
            $result['opp'] = $this->Opportunity_model->left_all();
            $result['skill'] = $this->Skill_model->left_all();
            $result['edit'] = $this->Profile_model->cget($user_id);
            $result['edit1'] = $this->Profile_model->skill_chk2($user_id);
            $result['edit2'] = $this->Profile_model->opp_chk2($user_id);
            $this->form_validation->set_rules('education', 'Education', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');
            $this->form_validation->set_rules('sentence', 'Sentence', 'max_length[500]');
            if ($this->form_validation->run() == FALSE) {
                // $this->session->set_flashdata('opp','Require fields!!!');
                $this->load->view('include/header');
                $this->load->view('home/edit_profile', $result);
                $this->load->view('include/footer2');
            } else {
            }
        }
    }

    function create()
    {

        $log = $this->session->userdata('logged_in');
        if ($log['role'] != 'admin') {
            if ($log['email'] != Null) {
                $result['data'] = $this->User_model->getuserbyemail($log['email']);
            } else {
                $result['data'] = $this->User_model->getuserbyfacebookid($log['facebook']);
            }
            //$fb = $log["facebook"];
            $user_id = $log['id'];
            $sess_array = array('id' => $result['data']['user_id'], 'username' => $result['data']['name'],
                'password' => $result['data']['password'], 'email' => $result['data']['email'], 'role' => $result['data']['role'], 'facebook' => $result['data']['facebook'], 'linkedin' => null);
            $this->session->set_userdata('logged_in', $sess_array);
            $id = $result['data']['user_id'];
            $result['opp'] = $this->Opportunity_model->left_all();
            $result['skill'] = $this->Skill_model->left_all();
            $result['user'] = $this->Profile_model->cget($user_id);
            $result['edit1'] = $this->Profile_model->skill_chk2($user_id);
            $result['edit2'] = $this->Profile_model->opp_chk2($user_id);

            $this->form_validation->set_rules('education', 'Education', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');
            $this->form_validation->set_rules('sentence', 'Sentence', 'max_length[500]');


            if ($this->form_validation->run() == FALSE) {

                $this->load->view('include/header');
                $this->load->view('home/profile_page', $result);
                $this->load->view('include/footer2');
            } else {


//
                $name = $this->input->post('name');
                $education = $this->input->post('education');
                $location = $this->input->post('location');
                $sentence = $this->input->post('sentence');


                
               

                if ($education == NULL || $location == NULL) {
                    $this->session->set_flashdata('field', 'Require Fields!!!');
                    redirect('profile/create', 'refresh');
                }

                $oppcheck = $this->input->post('oppcheck');
                if ($oppcheck == NULL) {
                    $this->session->set_flashdata('opp', 'Require Opportunity!!!');
                    redirect('profile/create', 'refresh');
                }

                $skillcheck = $this->input->post('skillcheck');
                if ($skillcheck == NULL) {
                    $this->session->set_flashdata('skill', 'Require Skill!!!');
                    redirect('profile/create', 'refresh');
                }




                // if(!empty($_FILES['image']['tmp_name'])){

                //     $image = $this->img_upload();
                //     if ($image != NULL) {
                //         $image = $image['file_name'];
                //     } else {
                //         $this->session->set_flashdata('message_img', 'Filesize is 5MB Limited!');
                //         $image= "";
                //     }
                // }
                // else{
                //     $image= $this->input->post('h_image');
                // }
								

                $image= '1';
                if(!empty($_FILES['cv']['tmp_name'])){
                    $cv = $this->pdf_upload();
                    if ($cv != NULL) {
                        $cv = $cv['file_name'];
                    } else {
                        $this->session->set_flashdata('message_cv', 'Filesize is 5MB Limited!');
                        // $cv = $this->input->post("h_cv");
                        $cv = "";
                    }
                }
                else{
                    $cv= $this->input->post('h_cv');

                }


                if(!empty($_FILES['letter']['tmp_name'])){
                    $letter = $this->pdf_upload1();
                    if ($letter != NULL) {
                        $letter = $letter['file_name'];
                    } else {
                        $letter = $this->input->post("h_letter");
                    }
                }
                else{
                    $letter= $this->input->post('h_letter');

                }


//                 if(!empty($_FILES['video']['tmp_name'])){
//                     $video = $this->video_upload();
//                     if ($video != NULL) {
//                         $video = $video['file_name'];
//                     } else {
//                         $video = $this->input->post("h_video");
//                         $this->session->set_flashdata('message_video', 'Filesize is 50MB Limited!');
//                     }
//                 }
//                 else{
// //                    echo "in hidden";
//                     $video= $this->input->post('h_video');
//                     $this->session->set_flashdata('message_video', 'Filesize is 50MB Limited!');
//                 }
                $video = "";
                if($image!=null || $user_id!=null) {
                       
                        $result = $this->Profile_model->update($name, $location, $education, $sentence, $image, $cv, $letter, $video);
			 $de = $this->Profile_model->opp_de($user_id);
                         $de2 = $this->Profile_model->skill_de($user_id);
                        $check = $this->Profile_model->check($id, $oppcheck, $skillcheck);
               
                }else{
                    $check = false;
                }

                if ($result && $check) {
                    redirect('custom');
                } else {
//edit_profi
                    $this->load->view('include/header');
                    $this->load->view('home/profile_page', $result);
                    $this->load->view('include/footer2');
                }
            }
        }
    }

    function edit($user_id)
    {

        $origin_encrypt = str_replace("~", "/", $user_id);
        $decrypt_id = $this->encrypt->decode($origin_encrypt);
        $log = $this->session->userdata('logged_in');
        $result['opp'] = $this->Opportunity_model->left_all();
        $result['skill'] = $this->Skill_model->left_all();
        $result['edit'] = $this->Profile_model->get($decrypt_id);
        $result['edit1'] = $this->Profile_model->skill_chk($decrypt_id);
        $result['edit2'] = $this->Profile_model->opp_chk($decrypt_id);
	$result['user'] = $this->Profile_model->cget($user_id);

        if ($log['role'] != 'admin') {
            if ($log['email'] != Null) {
                $result['data'] = $this->User_model->getuserbyemail($log['email']);
            } else {
                $result['data'] = $this->User_model->getuserbyfacebookid($log['facebook']);
            }

            $user_id = $log['id'];
            // $origin_encrypt = str_replace("~","/",$user_id);
            // $decrypt_id     = $this->encrypt->decode($origin_encrypt);
            $sess_array = array('id' => $result['data']['user_id'], 'username' => $result['data']['name'],
                'password' => $result['data']['password'], 'email' => $result['data']['email'], 'role' => $result['data']['role'], 'facebook' => $log['facebook'], 'linkedin' => $log['linkedin']);
            $this->session->set_userdata('logged_in', $sess_array);
            $id = $result['data']['user_id'];
            $result['opp'] = $this->Opportunity_model->left_all();
            $result['skill'] = $this->Skill_model->left_all();
            $result['edit'] = $this->Profile_model->get($user_id);
            $result['edit1'] = $this->Profile_model->skill_chk2($user_id);
            $result['edit2'] = $this->Profile_model->opp_chk2($user_id);
	    $result['user'] = $this->Profile_model->cget($user_id);

            $this->form_validation->set_rules('education', 'Education', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');
            $this->form_validation->set_rules('sentence', 'Sentence', 'max_length[500]');


            if ($this->form_validation->run() == FALSE) {
                // $this->session->set_flashdata('field','Require fields!!!');
                $this->load->view('include/header');
                $this->load->view('home/edit_profile', $result);
                $this->load->view('include/footer2');
            } else {

                $name = $this->input->post('name');
                $education = $this->input->post('education');
                $location = $this->input->post('location');
                $sentence = $this->input->post('sentence');

                // if ($education==NULL || $location==NULL ) {
                //  $this->session->set_flashdata('field','Require Fields!!!');
                //  redirect('profile/create','refresh');
                //    }

                $oppcheck = $this->input->post('oppcheck');
                if ($oppcheck == NULL) {
                    $de = $this->Profile_model->opp_de($user_id);
                    $this->session->set_flashdata('opp', 'Require Opportunity!!!');

                    redirect('profile');
                }

                $skillcheck = $this->input->post('skillcheck');
                if ($skillcheck == NULL) {
                    $de = $this->Profile_model->skill_de($user_id);
                    $this->session->set_flashdata('skill', 'Require Skill!!!');
                    redirect('profile', 'refresh');
                }


                // if(!empty($_FILES['image']['tmp_name'])){

                //     $image = $this->img_upload();
                //     if ($image != NULL) {
                //         $image = $image['file_name'];
                //     }else {
                //         $image= $this->input->post('h_image');
                //     }
                // }
                // else{
                //     $image= $this->input->post('h_image');
                // }

                $image= '1';

                //if(!empty($_FILES['cv']['tmp_name'])){
                    //$cv = $this->pdf_upload();
                    //if ($cv != NULL) {
                      //  $cv = $cv['file_name'];
                    //} else {
                      //  $cv = $this->input->post("h_cv");
                    //}
                //}
                //else{
                  //  $cv= $this->input->post('h_cv');

                //}
		
		 if(!empty($_FILES['cv']['tmp_name'])){
                    $cv = $this->pdf_upload();
                    if ($cv != NULL) {
                        $cv = $cv['file_name'];
                    } else {
                        //$this->session->set_flashdata('message_cv', 'Filesize is 5MB Limited!');
                        $cv = $this->input->post("h_cv");
                       
                    }
                }
                else{
                    $cv= $this->input->post('h_cv');

                }


                if(!empty($_FILES['letter']['tmp_name'])){
                    $letter = $this->pdf_upload1();
                    if ($letter != NULL) {
                        $letter = $letter['file_name'];
                    } else {
                        $letter = $this->input->post("h_letter");
                    }
                }
                else{
//                    echo "in hidden";
                    $letter= $this->input->post('h_letter');

                }

                $video = " ";



//                 if(!empty($_FILES['video']['tmp_name'])){
//                     $video = $this->video_upload();

//                     if ($video != NULL) {
//                         $video = $video['file_name'];
//                     } else {
//                         $video = $this->input->post("h_video");
//                         $this->session->set_flashdata('message_video', 'Filesize is 50MB Limited!');
//                     }
//                 }
//                 else{
// //                    echo "in hidden";
//                     $video= $this->input->post('h_video');
//                     $this->session->set_flashdata('message_video', 'Filesize is 50MB Limited!');
//                 }



                 //var_dump($cv,$letter,$education,$sentence);
                //die();
                /******/
//                $video = $this->video_upload();
//                if ($video!=null) {
//                    $video = $video['file_name'];
//                } else {
////                    var_dump("video");
////                    die();
//                    $this->session->set_flashdata('message_video', 'Filesize is 50MB Limited!');
//                }

                if($image!=null || $user_id!=null ) {
                    $result = $this->Profile_model->update($name, $location, $education, $sentence, $image, $cv, $letter, $video);
		    $de = $this->Profile_model->opp_de($user_id);
                    $de2 = $this->Profile_model->skill_de($user_id);  
                  $check = $this->Profile_model->check($id, $oppcheck, $skillcheck);
                }else{
                    $check = false;
                }

                if ($result ) {
                    redirect('custom');

                } else {

                    $this->load->view('include/header');
                    $this->load->view('home/profile_page', $result);
                    $this->load->view('include/footer2');
                }
            }
        }
    }

    function img_upload()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNE';
        $config['max_size'] = 22430000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('image')) {
//            $image = $this->input->post('image');
            return $this->upload->data();
        } else {
            return null;
        }
    }


    function pdf_upload()
    {
        $config['upload_path'] = './pdf/';
        $config['allowed_types'] = 'pdf|PDF|docx|DOCX';
        $config['max_size'] = 8000;
        // $this->form_validation->set_rules('cv','Sentence','callback_file_check');

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('cv')) {
            return $this->upload->data();
        } else {
            return null;
        }
    }

    function pdf_upload1()
    {
        $config['upload_path'] = './pdf/';
        $config['allowed_types'] = 'pdf|PDF|docx|DOCX';
        $config['max_size'] = 10000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('letter')) {
            return $this->upload->data();
        } else {
            return null;
        }
    }

    function video_upload()
    {

        $configVideo['upload_path'] = './video/';
        $configVideo['max_size'] = 50000;
        $configVideo['allowed_types'] = 'mp4';
        $configVideo['overwrite'] = FALSE;
        $configVideo['remove_spaces'] = TRUE;

        $this->load->library('upload', $configVideo);
        $this->upload->initialize($configVideo);

        if ($this->upload->do_upload('video')) {
            return $this->upload->data();
        } else {
            return null;
        }
    }

    /*
      * file value and type check during validation
      */
    function file_check($str)
    {
        $allowed_mime_type_arr = array('application/pdf', 'application/PDF', 'application/docx', 'application/DOCX');
        $mime = get_mime_by_extension($_FILES['file']['type']);
        if (isset($_FILES['file']['type']) && $_FILES['file']['type'] != "") {
            if (in_array($mime, $allowed_mime_type_arr)) {
                return true;
            } else {
                $this->form_validation->set_message('file_check', 'Please select only pdf/PDF/DOCX/docx file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }


    function change($user_id)
    {

        $pwd = $this->input->post('password');
        $res = $this->Profile_model->change($user_id, $pwd);

    }

}



