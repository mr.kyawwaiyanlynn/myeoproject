<?php
/**
*
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('User_model','',TRUE);
    $this->load->library('form_validation');
    $this->load->model('Admin_model','',TRUE);
    $this->load->model('Custom_model','',TRUE);
    $this->load->model('Opportunity_model','',TRUE);
    $this->load->model('Profile_model','',TRUE);
    $this->load->model('Skill_model','',TRUE);
    $this->load->helper('form');
    $this->load->library('ckeditor');
    $this->load->library('ckfinder');
    $this->load->helper('security');
    $this->load->library('encrypt');
    $this->load->library('pagination');
    $this->load->model('Sample_model','',TRUE);
    $this->load->library('email');
  }
  function index()
  {
     $log=$this->session->userdata('logged_in');
//      $check_result = $this->User_model->checkfb('Wai Yan Lynn','','543632792682926');
//     var_dump($check_result[0]->user_id);
//         die();
        $user_id=$log['id'];
//        var_dump($user_id);
//        die();
    if(isset($log))
    {

      $loge=$this->session->userdata('confirm_logged_in');

        if($loge['status']=='sign')
        {
             $data['img'] = $this->Admin_model->get_image();

                if($data['img'])
                {
                   $this->load->view('home/login_page',$data);
                   $this->load->view('include/footer');
                }
        }else
        {

          if($log['role']=='admin')
          {
            redirect('admin');
          }
          else
          {
           
              if($this->session->userdata('login_status')==1) {

                 $result['user']  = $this->Profile_model->cget($user_id);
                 $result['edit1']  = $this->Profile_model->skill_chk2($user_id);
                 $result['edit2']  = $this->Profile_model->opp_chk2($user_id);     
                 //$this->session->set_userdata('login_status', 0);
                 // var_dump('1');die();
                 $post=$this->session->userdata('facebook_click_post');
                  if($post==null)
                  {
                     redirect('profile/create');
                  }else{
                      redirect('custom/alldetail/'.$post,'refresh');
                      $this->session->set_userdata('facebook_click_post',"");
                  }

              }else{
                 $post=$this->session->userdata('facebook_click_post');
                 if($post==null)
                  {
                     redirect('custom');
                  }else{
                      redirect('custom/alldetail/'.$post,'refresh');
                      $this->session->set_userdata('facebook_click_post',"");
                  }
              }

          }
        }


    }
    else
    {

       $data['img'] = $this->Admin_model->get_image();
     //  var_dump($data['img']);

                if($data['img'])
                {
                   $this->load->view('home/login_page',$data);
                   $this->load->view('include/footer');
                }

     }

  }

  function signup()
  {
      $this->form_validation->set_rules('name','Name','required');
      // $this->form_validation->set_rules('email','Email','trim|required|is_unique[user.email]|xss_clean|valid_email');
      // $this->form_validation->set_rules('password','Password','trim|required|min_length[8]|max_length[30]|xss_clean');
      // $this->form_validation->set_rules('cfmpassword','Confirm Password','trim|required|min_length[8]|max_length[30]|xss_clean');

      if($this->form_validation->run()==FALSE )
      {
          $this->session->set_flashdata('msg','Validation Failed');
          redirect('user');

      }
      else
      {

       $name=$this->input->post('name');
       $email=$this->input->post('email');
       $password=sha1($this->input->post('password'));
       $cfmpassword=sha1($this->input->post('cfmpassword'));


        if($password==$cfmpassword)
        {
           $result=$this->User_model->check($email);

           if($result!=NULL)
           {

            $this->session->set_flashdata('msg','Sign up with another Name or Email!!!');
            redirect('user');

           }
           else
           {
               $sess_array=array('username'=>$name,'password'=>$password, 'email'=>$email,'role'=>'user','status'=>'sign','linkedin'=>Null,'facebook'=>Null);
               $this->session->set_userdata('confirm_logged_in',$sess_array);
               $this->session->set_flashdata('message','Verify Your Email!!');
               //TzH for signup without Email verificaiton

              $success=$this->User_model->sendEmail($email,$name,$password);
              // $success=$this->User_model->signup($name,$email,$password);
              if($success)
              {
                 redirect('user');

               }else
               {
                 var_dump("HELLO");
               }

          }

        }
        else
        {
          $this->session->set_flashdata('msg','Your Password and Confirm Password does not match!!!');
          redirect('user');
        }

     }

}

function login()
  {

    $log=$this->session->userdata('logged_in');

    $id = $log['id'];

    if(isset($log))
    {
      if($log['role']=='admin')
      {
        redirect('admin');
      }
      else {

        redirect('custom');
      }

    }
 else
 {
      $this->form_validation->set_rules('email','Email','required');
      $this->form_validation->set_rules('password','Password','required');

      if($this->form_validation->run()==FALSE )
        {
          redirect('user');
        }
        else
        {
          $email=$this->input->post('email');
          $password=sha1($this->input->post('password'));
          $result=$this->User_model->login($email,$password);

          if($result==NULL)
          {

            $this->session->set_flashdata('msg1','Your Email or Password does not match!!!');
            redirect('user');
          }
          else if($result['role']=='admin')
          {
             $sess_array=array('id'=>$result['user_id'],'username'=>$result['name'],'email'=>$result['email'],'password'=>$password,'role'=>$result['role'],'facebook'=>Null,'linkedin'=>Null);
             $this->session->set_userdata('logged_in',$sess_array);

            $this->form_validation->set_rules('title','Title', 'trim|required');
            $this->form_validation->set_rules('subtitle','Subtitle','trim|required');
            $this->form_validation->set_rules('description','Description','trim|required');
            $this->form_validation->set_rules('choice','Choice','required');
            $this->form_validation->set_rules('uploadchoice','UploadChoice','required');
            if($this->form_validation->run()==FALSE)
            {

              redirect('admin');
            }
            else
            {
               redirect('admin');
            }
        }
          else
          {
              $sess_array=array('id'=>$result['user_id'],'username'=>$result['name'],'password'=>$result['password'],'email'=>$result['email'],'role' =>$result['role'],'facebook'=>Null,'linkedin'=>Null);
             $this->session->set_userdata('logged_in',$sess_array);

                $post=$this->session->userdata('facebook_click_post');
                  if($post==null)
                  {
                     redirect('profile/create');
                  }else{
                      redirect('custom/alldetail/'.$post,'refresh');
                      $this->session->set_userdata('facebook_click_post',"");
                  }

            //  redirect('profile/create');


          }
        }
    }

  }
 function changepwd()
 {
   // $origin_encrypt = str_replace("~", "/", $user_id);
   // $decrypt_id = $this->encrypt->decode($origin_encrypt);
  // var_dump('kkk');die();
   $log=$this->session->userdata('logged_in');
   $user_id = $log['id'];
   // var_dump($user_id);die();
   $this->form_validation->set_rules('current','Current Password','required');
   $this->form_validation->set_rules('new','New Password','required');
   $this->form_validation->set_rules('cfmnew','CfmNew','required');
   if($this->form_validation->run()==FALSE)
   {
    // var_dump($user_id);die();
      if($log['role']=="admin")
       {
        $data['pwd'] = $this->User_model->get_pwd($user_id);
        // var_dump($data['pwd']);die();
        $this->load->view('include/header');
        $this->load->view('admin/admin_pwd',$data);
       } 
       else
       {
         // redirect('custom');
        // var_dump('log');die();  
        $data['pwd'] = $this->User_model->get_pwd($user_id);
        // var_dump($data['pwd']);die();
        $this->load->view('include/header');
        // $this->load->view('include/nav',$data);
        $this->load->view('home/chg_pwd',$data);
       } 
   }
  else
   { 
// $current=$this->input->post('current');
    $current=sha1($this->input->post('current'));
    $new=sha1($this->input->post('new'));
    $cnew=sha1($this->input->post('cfmnew'));
    // var_dump($current,$new,$cnew,'..........');
    $check_current = $this->User_model->get_pwd($user_id);
    // var_dump($check_current);
      if($current==$check_current['password'])
      {
            if($new!=$cnew && $current!=$new)
            {
              // var_dump('diff');die();
                  if($log['role']=="admin")
                 {
                    $this->session->set_flashdata('msg1','Your new password and Confirm password do not match!!!'); 
                    redirect('User/changepwd');
                 } 
                 else
                 {
                    $this->session->set_flashdata('msg1','Your new password and Confirm password do not match!!!'); 
                    redirect('User/changepwd');
                 }
            }
            elseif($new==$cnew)
            {
                  // var_dump('ok');
                  if($log['role']=="admin")
                 {
                    $data=$this->User_model->changepwd($log['id'],$new);
                    if($data){
                    $this->session->set_flashdata('msg1','Change password is Successful!!');
                    redirect('admin');
                  }
                 } 
                 else
                 {
                     $data=$this->User_model->changepwd($log['id'],$new);
                     if($data){
                    $this->session->set_flashdata('msg1','Change password is Successful!!');
                    redirect('custom');
                  }
                 }
            }
            elseif($current==$new)
            {
              var_dump('expression');
              if($log['role']=="admin")
                 {
                    $this->session->set_flashdata('msg1','Your password and new password should different!!!'); 
                    redirect('User/changepwd');
                 } 
                 else
                 {
                  $this->session->set_flashdata('msg1','Your password and new password should different!!!'); 
                    redirect('User/changepwd');
                 }

            }
      }

      else
      {
         if($log['role']=="admin")
         {
          $this->session->set_flashdata('msg1','Wrong Current Password!!!'); 
          redirect('User/changepwd');
         } 
         else
         {
          $this->session->set_flashdata('msg1','Wrong Current Password!!!'); 
           redirect('User/changepwd');
         }
      }
    



}//Input box filled

}
 function confirmEmail($hashcode)
 {

     $log = $this->session->userdata('confirm_logged_in');
     // var_dump($log);die();
     $data = array('email' => $log['email'], 'password' => $log['password'], 'name' => $log['username'], 'facebook' => "", 'linkedin' => "", 'role' => $log['role'], 'status' => 1);
     $this->session->set_userdata('logged_in', $data);

             if ($data != null)
             {
                 if ($this->User_model->verifyEmail($hashcode, $data)) {
                     $this->session->set_flashdata('verify', '<div class="alert alert-success text-center">Email address is confirmed. Please login to the system</div>');
                     redirect('profile/create');
                 } else {
                     $this->session->set_flashdata('verify', '<div class="alert alert-danger text-center">Email address is not confirmed. Please try to re-register.</div>');
                     redirect('user');
                 }
             }else{

                 var_dump( $this->User_model->user_con($hashcode));
                 die();
                 if ($this->User_model->verifyEmail($hashcode, $data)) {
                     $this->session->set_flashdata('verify', '<div class="alert alert-success text-center">Email address is confirmed. Please login to the system</div>');
                     redirect('profile/create');
                 } else {
                     $this->session->set_flashdata('verify', '<div class="alert alert-danger text-center">Email address is not confirmed. Please try to re-register.</div>');
                     redirect('user');
                 }
             }
  }
 /*
Forgotpassword email sender:
No view
*/

    //WaiYan
     public function forgot_view()
     {


        $this->load->view('home/confirmdigit');
     }

     //WaiYan
     public function forgot()
     {
         $digit=$this->input->post('digit');
         var_dump($digit);
         $result=$this->User_model->digitcheck($digit);
         if($result)
         {

         }

     }


     public function ForgotPassword()
   {
         $email = $this->input->post('email');
         $findemail['email'] = $this->User_model->ForgotPassword($email);



         if($findemail['email'])
          {
            $sess_array=array('email'=>$email);
            // var_dump($sess_array);die();
             $this->session->set_userdata('log_in',$sess_array);
             $this->load->view('forgot_view',$findemail);
              $success=$this->User_model->sendDigit($findemail);
               if($success)
               {
                  // redirect('user');
               }

           }
        else
        {
          redirect('user');
        }
   }

   public function reset()
   {

       $this->form_validation->set_rules('new','New Password','required');
       $this->form_validation->set_rules('cfmnew','CfmNew','required');
       if($this->form_validation->run()==FALSE)
      {
         $this->load->view('reset_view');
      }
      else
      {
         $loged=$this->session->userdata('log_in');
             if($loged)
             {
                   $data['email']= $this->User_model->get_all($loged['email']);
                   $new=$this->input->post('new');
                   $cfmnew=$this->input->post('cfmnew');
                   $digit=$data['email']['digit_no'];
                   if($new==$cfmnew)
                   {
                     $result=$this->User_model->reset(sha1($new),$digit);
                     if($result)
                     {
                       redirect('user');
                     }

                   }
                   else
                   {
                      $this->load->view('reset_view');
                   }
             }

      }
   }


   public function checkdigit()
   {

     $this->form_validation->set_rules('digit','Digit','required');

      if($this->form_validation->run()==FALSE)
    {
       $loged=$this->session->userdata('log_in');
           if($loged)
           {
              $data['email']= $this->User_model->get_all($loged['email']);
              $this->load->view('forgot_view',$data);
           }

    }
    else
    {
     $digit=$this->input->post('digit');

     $data['data']=$this->User_model->digitcheck($digit);
     // var_dump($data['data']);die();
       if($data['data'])
       {
        $this->load->view('reset_view',$data);
       }
       else
         {
           $loged=$this->session->userdata('log_in');
           if($loged)
           {
              $data['email']= $this->User_model->get_all($loged['email']);
              $this->load->view('forgot_view',$data);
           }
         }
       }
    }

    function cc()
    {
      $email = 'suthinzar413@gmail.com';
      $cc = $this->User_model->check_email($email);
      var_dump($cc);
      if($cc == true)
      {
        var_dump('true');
      }
    }





}
