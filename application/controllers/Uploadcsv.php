<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadcsv extends CI_Controller {

public function __construct()
{
    parent::__construct();
    $this->load->helper('url');                    
    $this->load->model('Uploadcsv_model');
    $this->load->model('User_model');
}

public function index()
{
    $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
  if(isset($log))
  {
    // var_dump("Hello");die();
    if($log['role']=='admin')
  { 
    $data['name']=$this->User_model->getuser($log['email']);
    $this->data['view_data']= $this->Uploadcsv_model->view_data();
    $this->load->view('include/header');
    // $this->load->view('Admin/admin_nav',$data);
    $this->load->view('excelimport', $this->data, FALSE);
    $this->load->view('Admin/admin_nav',$data);
    // $this->load->view('include/footer');
  }
  else
  {
    redirect('user');
  }
}
}
public function importbulkemail(){
    $this->load->view('excelimport');
}

public function import(){
 if(isset($_POST["import"]))
  {
      $filename=$_FILES["file"]["tmp_name"];
      if($_FILES["file"]["size"] > 0)
        {
          $file = fopen($filename, "r");
           while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
           {
                  $data = array(
                      'title' => $importdata[0],
                      'sub_title' =>$importdata[1],
                      'description' => $importdata[2],
                      );
           $insert = $this->Uploadcsv_model->insertCSV($data);
           }                    
          fclose($file);
$this->session->set_flashdata('message', 'Data are imported successfully..');
redirect('uploadcsv/index');
        }else{
$this->session->set_flashdata('message', 'Something went wrong..');
redirect('uploadcsv/index');
    }
  }
}

}