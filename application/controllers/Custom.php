<?php
/**
* 
*/
class Custom extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper('security');
		$this->load->model('Opportunity_model');
		$this->load->model('Skill_model');
		$this->load->model('Custom_model');
		$this->load->model('Sample_model','',TRUE);
		$this->load->library('pagination');
		$this->load->model('User_model');
		$this->load->library('encrypt');
  		$this->load->model('Feed_model'); 
	}

	function index()
	{ 
		$log=$this->session->userdata('logged_in');
		// var_dump($log,$id,'log');die();
			    if(isset($log))
			    {
			    	// var_dump($log);
			      // var_dump("Hello c");die();
			      if($log['role']!='admin')
			      {
				      $pag 				= $this->config->item('pagination');   
				      $pag['base_url'] 	= base_url().'custom/index';
				      $data['search']	= Null;
				      $data['tag']		= Null;
				      // $pag['total_rows'] = $this->Sample_model->count_opp();
				      $page=$this->Custom_model->count();
				      // $pag2=$this->Custom_model->count_skill($id);
				      $pag['total_rows']	= $page;
				      // var_dump($pag['total_rows']);die();
				      $data['result'] 		= $this->Custom_model->custom($pag['per_page'],$this->uri->segment(3),'desc');
				     

				      
				      $data['pag'] 		= $pag;
				      
				      //$result['name']	= $this->User_model->getuser($log["email"]);
					 	 if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						if($log['facebook']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
						if($log['linkedin']!=Null)
						{
                         	 $result['name']    = $this->User_model->getuserbylinkedinid($log['linkedin']);
					 	}
                      
				      $this->load->view('include/header');
				      
				      $data1['opp']  	= $this->Opportunity_model->left_all();
				      $data1['skill']  	= $this->Skill_model->left_all();
						$this->load->view('include/nav',$result);
						$this->load->view('home/custom_view',$data);
						$this->load->view('home/left_view',$data1);
						$this->load->view('include/footer2');   

						// $this->load->view('include/footer');  
			      }
			      else
			      {
			        redirect('user/login');
			      }


			      
			    }     
        		
	}


	
	function linkedin()
	{ 
		$log=$this->session->userdata('logged_in');

		// var_dump($log,$id,'log');die();
			    if(isset($log))
			    {
			    	// var_dump($log);
			      // var_dump("Hello c");die();
			      if($log['role']!='admin')
			      {
				      $pag 				= $this->config->item('pagination');   
				      $pag['base_url'] 	= base_url().'custom/index';
				      $data['search']	= Null;
				      $data['tag']		= Null;
				      // $pag['total_rows'] = $this->Sample_model->count_opp();
				      $page=$this->Custom_model->count();
				      // $pag2=$this->Custom_model->count_skill($id);
				      $pag['total_rows']	= $page;
				      // var_dump($pag['total_rows']);die();
				      $data['result'] 		= $this->Custom_model->custom($pag['per_page'],$this->uri->segment(3),'desc');
				      // var_dump($data['result']);die();
				      // $real=unserialize($data['custom']);
				      
				      
				      $data['pag'] 		= $pag;
				      
				       $result['name']	= $this->User_model->getlinkedinuser($log["linkedin"]);
                      
				      $this->load->view('include/header');
				      
				      $data1['opp']  	= $this->Opportunity_model->left_all();
				      $data1['skill']  	= $this->Skill_model->left_all();
				      $this->load->view('include/nav',$result);						
				      $this->load->view('home/left_view',$data1);
					  $this->load->view('home/custom_view',$data);
					  $this->load->view('include/footer2');   
			      }
			      else
			      {
			        redirect('user/login');
			      }


			      
			    }     
        		
	}
	//   TKH =>for detail
	 function alldetail($id)
  {

  	    $origin_encrypt=str_replace("~","/",$id);
        $decrypt_id=$this->encrypt->decode($origin_encrypt);

         // var_dump($decrypt_id);die();
        $log=$this->session->userdata('logged_in');
          if(isset($log))
          {
            if($log['role']!='admin')
            {  
            	
				  		if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
						
                //$result['name']		= $this->User_model->getuser($log["email"]);
                $data['query'] 		= $this->Custom_model->getall($decrypt_id);
                $check=$this->Custom_model->check($decrypt_id);
                // var_dump($check->skill);die();
                if($check->skill=='0')
                {
                 $data['type']=$this->Opportunity_model->get_type($decrypt_id);
                }
                else
                {
                  $data['type']     =$this->Skill_model->get_type($decrypt_id);
                }
                // var_dump($data['type']);die();
                $data1['opp'] 		= $this->Opportunity_model->left_all();
		        $data1['skill']  	= $this->Skill_model->left_all();
		        
		        $data['post_id'] = $decrypt_id;

				// var_dump($data);
				// die();
				//var_dump($data1);
				$data["facebook_share"]=$id;

                $this->load->view('include/header');
				$this->load->view('include/nav',$result);
				$this->load->view('home/custom_detail',$data);
				$this->load->view('home/left_view',$data1);
				$this->load->view('include/footer2'); 

			
            }
             else
            {
              redirect('user/login');
            }
        }
        else
        {
        	redirect('user');
        }
  }

	function all($opp_id,$skill_id,$Search)
	{
			// for decryption
		$origin_encrypt 	= str_replace("~","/",$skill_id);
        $decrypt_id 		= $this->encrypt->decode($origin_encrypt);

        // for decryption
		$origin_encrypt1 	= str_replace("~","/",$opp_id);
        $decrypt_id1 		= $this->encrypt->decode($origin_encrypt1);

		$log=$this->session->userdata('logged_in');
               // var_dump($log);die();
			    if(isset($log))
			    {
			      // var_dump($log,"Hello");die();
			      if($log['role']!='admin')
			      {
				    $pag['base_url'] 	= base_url().'Custom/index';
					$data['search']		= Null;
					$pag['total_rows']  = $this->Sample_model->count_all();
				    // $this->db->order_by("opp_id",'asc');
			    	$data['oppo'] 		= $this->Sample_model->get_opp($pag['per_page'],$this->uri->segment(3),'desc');
			    	$data['skill'] 		= $this->Sample_model->get_skill($pag['per_page'],$this->uri->segment(3),'desc');
					$data['pag'] 		= $pag;
    				$result['name']		= $this->User_model->getuser($log["email"]);
					$this->load->view('include/header');
					$this->load->view('include/nav',$result);
					$data['opp_detail']  	= $this->Opportunity_model->oppdetail($decrypt_id1);
					$data['skill_detail']   = $this->Skill_model->skilldetail($decrypt_id);
					$data1['oppo']  		= $this->Opportunity_model->left_all();
					$data1['skill'] 		= $this->Skill_model->left_all();
					$this->load->view('home/left_view',$data1);
					$this->load->view('home/custom_view',$data);
					// var_dump($data['oppo'],$data['skill'],$data['skill_detail'],$data['opp_detail']);die();
					$this->load->view('include/footer2');
					// $this->load->view('include/footer1');
			      }
			      else
			      {
			        redirect('user/login');
			      }
			      
			    }	
		
	}

	function search()
	{
		
     $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
			    if(isset($log))
			    {
			    	// var_dump($log);
			      // var_dump("Hello c");die();
			      if($log['role']!='admin')
			      {   
			      	 // $this->form_validation->set_rules('search','Search','required');
			      // if($this->form_validation->run()==FALSE)
			      //  { 
				       // redirect('custom/search');
           //         }
           //         else
           //         { 
			      	$title=$this->input->post('search');
			      	$tag=$this->input->post('search1');
			      	// var_dump($a,$b);die();
                   $input =$this->input->post('submitform'); 
                   // var_dump($input);die();
			      	if($tag==Null)
			      	{
            // echo "HELLo";
			      	
                   	  $search 			= $this->input->post('search');
                   	  // var_dump($search);die();
				      $pag 				= $this->config->item('pagination');   
				      $pag['base_url']  = base_url().'custom/search';
				      $data['search']	= Null;
				      $data['tag']		= Null;
				      // $pag['total_rows'] = $this->Sample_model->count_opp();
				      $page=$this->Custom_model->searchcount($search);
				      // $pag2=$this->Custom_model->count_skill($id);
				      $pag['total_rows']	= $page;
				      // var_dump($pag);die();
				      $data['result'] 	= $this->Custom_model->searchdata($search,$pag['per_page'],$this->uri->segment(3),'desc');
				      // var_dump($data['result']);die();
				      // $real=unserialize($data['custom']);
				      
				      
				      $data['pag'] 		= $pag;
				     // $result['name']	= $this->User_model->getuser($log["email"]);
					 	if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
				      $this->load->view('include/header');
				      
				      $data1['opp']  	= $this->Opportunity_model->left_all();
				      $data1['skill']   = $this->Skill_model->left_all();
				      // var_dump($data['oppo']);die();
				      	$this->load->view('include/nav',$result);
						$this->load->view('home/custom_view',$data);
						$this->load->view('home/left_view',$data1);
						$this->load->view('include/footer2');   
					
			      }
			      else
			      { 
			      
                      $search1 			= $this->input->post('search1');
                      // var_dump($search1);die();

				      $pag 				= $this->config->item('pagination');   
				      $pag['base_url']  = base_url().'custom/search';
				      $data['search']	= Null;
				      $data['tag']		= Null;
				      // $pag['total_rows'] = $this->Sample_model->count_opp();
				      $page=$this->Custom_model->searchcount($search1);
				      // $pag2=$this->Custom_model->count_skill($id);
				      $pag['total_rows']	= $page;
				      // var_dump($pag);die();
				      $data['result'] 	= $this->Custom_model->search_tag($search1,$pag['per_page'],$this->uri->segment(3),'desc');
				      // var_dump($data['result']);die();
				      // $real=unserialize($data['custom']);
				      
				      
				      $data['pag'] 		= $pag;
				      $result['name']	= $this->User_model->getuser($log["email"]);
				      $this->load->view('include/header');
				      
				      $data1['opp']  	= $this->Opportunity_model->left_all();
				      $data1['skill']   = $this->Skill_model->left_all();
				      // var_dump($data['oppo']);die();
						$this->load->view('include/nav',$result);
						$this->load->view('home/custom_view',$data);
						$this->load->view('home/left_view',$data1);
						$this->load->view('include/footer2');  
			      }  
			  }
			      else
			      {
			        redirect('user/login');
			      }
			      
 }
 
}

 function feed(){
         $data['encoding'] = 'utf-8';
         $data['feed_name'] = 'www.mymyeo.com';
         $data['feed_url'] = '<?php base_url() ?>';
         $data['feed_description'] ='– Myanmar Youth Empowerment Opportunities';
         $data['page_description'] = 'Scholarships and other opportunities';
         $data['page_language'] = 'en-ca';
         $data['creator_email'] = 'suthinzar43@gmail.com';
         $query = $this->Feed_model->getFeedArticles();  
         $data['ARTICLE_DETAILS'] = null;
         // var_dump($data,$query);
  if($query != Null){
   $data['ARTICLE_DETAILS'] =  $query;
   // var_dump('dfewfaace',$data['ARTICLE_DETAILS']);die();
  }  
         header("Content-Type: application/rss+xml");
         $this->load->view('home/rss', $data);
    }
// HTZN
}
?>
