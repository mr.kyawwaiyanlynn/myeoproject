<?php
/**
* 
*/
class Skill extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->helper('security');
    $this->load->model('Opportunity_model');
    $this->load->model('Skill_model');
    $this->load->model('Sample_model');
    $this->load->library('pagination');
    $this->load->model('User_model');
    $this->load->model('Custom_model');
    $this->load->library('encrypt');
  }

  function index()
  {
     $log=$this->session->userdata('logged_in');
       $id = $log['id'];
          if(isset($log))
          {
            if($log['role']!='admin')
            {
        $pag = $this->config->item('pagination');   
          // $pag['base_url'] = base_url().'Skill/index';
        $pag['base_url']   = site_url('skill/index');
        $data['search']    = Null;
          // $pag['total_rows'] = $this->Sample_model->count_skill();
        $pag['total_rows'] = $this->Skill_model->count($id);
         // var_dump($pag['total_rows']);die();
          // $data['skill'] = $this->Skill_model->get_skill($pag['per_page'],$this->uri->segment(3),'desc');
        $data['skill']       = $this->Skill_model->skill($id,$pag['per_page'],$this->uri->segment(3),'desc');
        $data['pag']         = $pag;
       // $result['name']      = $this->User_model->getuser($log["email"]);
       	if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
        $this->load->view('include/header');
        $this->load->view('include/nav',$result);
        $data1['skill']      = $this->Skill_model->left_all();
        $this->load->view('home/skill_view',$data);
        $this->load->view('home/skill_left',$data1);
        $this->load->view('include/footer2');
      }
      else
            {
              redirect('user/login');
            }
            
          }
  }
  
  function skilldetail1($skill_id)
  {
    // for decryption
       $origin_encrypt=str_replace("~","/",$skill_id);
       $decrypt_id=$this->encrypt->decode($origin_encrypt);

   $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
          if(isset($log))
          {
            if($log['role']!='admin')
        {
            $result['name']    = $this->User_model->getuser($log["email"]);
            $pag               = $this->config->item('pagination');   
            $pag['base_url']   = base_url().'skill/index/'.$skill_id.'/';//TzH
            $data['search']    = Null;
            $data['tag']       = Null;
            $page=($this->uri->segment(4)) ? $this->uri->segment(4) : 0;//TzH
            $pag['cur_page']=$page;//TzH
            $pag['total_rows']   = $this->Sample_model->count_dskill( $decrypt_id);
            // var_dump($pag['total_rows']);die();
            $data1['skill']      = $this->Skill_model->skilldetail( $decrypt_id,$pag['per_page'],$this->uri->segment(3),'desc');
            $data1['pag']        = $pag;
          $this->load->view('include/header');
          $this->load->view('include/nav',$result);
          $data['skill']  = $this->Skill_model->left_all();
          $this->load->view('home/skill_view',$data1);
          $this->load->view('home/skill_left',$data);
          $this->load->view('include/footer2');
        }
     else
            {
              redirect('user/login');
            }
            
          }
  }

  function skilldetail($type)
  {  
   $log=$this->session->userdata('logged_in');
   if(isset($log))
   {
    if($log['role']!='admin')
    {
     // $result['name']   = $this->User_model->getuser($log["email"]);
     	if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}

      $pag              = $this->config->item('pagination');   
          $pag['base_url']  = base_url().'skill/skilldetail/'.$type.'/';//TzH
          $data['search']   = Null;
          $data['tag']      = Null;
          $page=($this->uri->segment(4)) ? $this->uri->segment(4) : 0;//TzH
          $pag['cur_page']=$page;//TzH
          $pag['total_rows']= $this->Sample_model->count_dskill($type);//TzH         
          $data1['skill']  = $this->Skill_model->left_all();
          $data['skill']= $this->Skill_model->skilldetail($type,$pag['per_page'],$this->uri->segment(4),'desc');  //TzH  
          $data['pag'] = $pag; 
          $this->load->view('include/header');
          $this->load->view('include/nav',$result);
          $this->load->view('home/skill_view',$data);
          $this->load->view('home/skill_left',$data1);
          $this->load->view('include/footer2');  
        }
        else
        {
          redirect('user/login');
        }
        
      }
    }
  
 //  function search()
 //  {
    
 //     $log=$this->session->userdata('logged_in');
 //           // var_dump($log);die();
 //          if(isset($log))
 //          {
            
 //            if($log['role']!='admin')
 //            {   
             
 //                      $search           = $this->input->post('search');
 //                      $pag              = $this->config->item('pagination');   
 //                      $pag['base_url']  = base_url().'skill/search';
 //                      $data['search']   = Null;
 //                      $data['tag']      = Null;                     
 //                      $page             = $this->Skill_model->searchcount($search);                     
 //                      $pag['total_rows']= $page;
                     
 //                      $data['skill']    = $this->Skill_model->search($search,$pag['per_page'],$this->uri->segment(3),'desc');
                      
 //                      $data['pag']    = $pag;
 //                      $result['name'] = $this->User_model->getuser($log["email"]);
 //                      $this->load->view('include/header');
 //                      $this->load->view('include/nav',$result);
 //                      $data1['opp']   = $this->Opportunity_model->left_all();
 //                      $data1['skill'] = $this->Skill_model->left_all();                  
 //                      $this->load->view('home/skill_view',$data);
 //                      $this->load->view('home/skill_left',$data1);
 //                      $this->load->view('include/footer2');   
          
 //        }
 //            else
 //            {
 //              redirect('user/login');
 //            }
 //          } 
 // }

 function search()
  {
    
     $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
          if(isset($log))
          {
            // var_dump($log);
            // var_dump("Hello c");die();
            if($log['role']!='admin')
            {   
               // $this->form_validation->set_rules('search','Search','required');
            // if($this->form_validation->run()==FALSE)
            //  { 
               // redirect('custom/search');
           //         }
           //         else
           //         { 
              $title=$this->input->post('search');
              $tag=$this->input->post('search1');
              // var_dump($a,$b);die();
                   $input =$this->input->post('submitform'); 
                   // var_dump($input);die();
              if($tag==Null)
              {
            // echo "HELLo";
              
                      $search       = $this->input->post('search');
                      // var_dump($search);die();
              $pag        = $this->config->item('pagination');   
              $pag['base_url']  = base_url().'skill/search';
              $data['search'] = Null;
              $data['tag']    = Null;
              // $pag['total_rows'] = $this->Sample_model->count_opp();
              $page=$this->Custom_model->searchcount($search);
              // $pag2=$this->Custom_model->count_skill($id);
              $pag['total_rows']  = $page;
              // var_dump($pag);die();
              $data['skill']   = $this->Skill_model->search($search,$pag['per_page'],$this->uri->segment(3),'desc');
              // var_dump($data['result']);die();
              // $real=unserialize($data['custom']);
              
              
              $data['pag']    = $pag;
             // $result['name'] = $this->User_model->getuser($log["email"]);
            if($log['email']!=Null)
            {
              $result['name']    = $this->User_model->getuserbyemail($log['email']);
            }
            else
            {
              $result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
            }
              $this->load->view('include/header');
              
              $data1['opp']   = $this->Opportunity_model->left_all();
              $data1['skill']   = $this->Skill_model->left_all();
              // var_dump($data['oppo']);die();
                $this->load->view('include/nav',$result);
            $this->load->view('home/skill_view',$data);
            $this->load->view('home/left_view',$data1);
            $this->load->view('include/footer2');   
          
            }
            else
            { 
            
                      $search1      = $this->input->post('search1');
                      // var_dump($search1);die();

              $pag        = $this->config->item('pagination');   
              $pag['base_url']  = base_url().'skill/search';
              $data['search'] = Null;
              $data['tag']    = Null;
              // $pag['total_rows'] = $this->Sample_model->count_opp();
              $page=$this->Custom_model->searchcount($search1);
              // $pag2=$this->Custom_model->count_skill($id);
              $pag['total_rows']  = $page;
              // var_dump($pag);die();
              $data['skill']   = $this->Skill_model->search($search1,$pag['per_page'],$this->uri->segment(3),'desc');
              // var_dump($data['result']);die();
              // $real=unserialize($data['custom']);
              
              
              $data['pag']    = $pag;
              $result['name'] = $this->User_model->getuser($log["email"]);
              $this->load->view('include/header');
              
              $data1['opp']   = $this->Opportunity_model->left_all();
              $data1['skill']   = $this->Skill_model->left_all();
              // var_dump($data['oppo']);die();
            $this->load->view('include/nav',$result);
            $this->load->view('home/custom_view',$data);
            $this->load->view('home/left_view',$data1);
            $this->load->view('include/footer2');  
            }  
        }
            else
            {
              redirect('user/login');
            }
            
 }
 
}

function alldetail($id)
  {  


        $origin_encrypt  = str_replace("~","/",$id);
        $decrypt_id     = $this->encrypt->decode($origin_encrypt);
        // var_dump($decrypt_id);die();
   $log=$this->session->userdata('logged_in');
          if(isset($log))
          {
            if($log['role']!='admin')
            {
                $result['name']   = $this->User_model->getuser($log["email"]);
                $data['query']    = $this->Skill_model->getall($decrypt_id);
                $data['type']     =$this->Skill_model->get_type($decrypt_id);
                // var_dump($data['query']);die();
                $data1['skill']   = $this->Skill_model->left_all();
                $this->load->view('include/header');
                $this->load->view('include/nav',$result);
                $this->load->view('home/skill_detail',$data);
                $this->load->view('home/skill_left',$data1);
                $this->load->view('include/footer2');
            }
             else
            {
              redirect('user/login');
            }
        }
  }
}
// <!--HTZN-->