<?php
	/**
	* 
	*/
	class Draft extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('User_model','',TRUE);
		    $this->load->library('form_validation');
		    $this->load->model('Admin_model','',TRUE);
		    $this->load->model('Custom_model','',TRUE);
		    $this->load->model('Opportunity_model','',TRUE);
		    $this->load->model('Profile_model','',TRUE);
		    $this->load->model('Skill_model','',TRUE);
		    $this->load->helper('form');
		    $this->load->library('ckeditor');
		    $this->load->library('ckfinder');
		    $this->load->library('session');
			$this->load->helper('security');		   
			$this->load->library('encrypt');
    		$this->load->model('User_model');
		}
		function index()
		{
			$this->load->view('admin/draft');
						
        }


function save_draft()
		{
			// var_dump("save_draft");
			$log=$this->session->userdata('logged_in');
           // var_dump($log);die();
			    if(isset($log))
			    {
			      // var_dump("Hello");
			      if($log['role']=='admin')
			{
			$data['name']=$this->User_model->getuser($log['email']);
            $data['all_opp']=$this->Admin_model->opportunity();
			$data['all_skill']=$this->Admin_model->skill();
			$this->form_validation->set_rules('title1','Title', 'trim|required');
			$this->form_validation->set_rules('subtitle1','Subtitle','trim|required');
			$this->form_validation->set_rules('description1','Description','trim|required');
			$this->form_validation->set_rules('choice1','Choice','required');
			$this->form_validation->set_rules('uploadchoice1','UploadChoice','required');
			if($this->form_validation->run()==FALSE)
			{
				// var_dump('hi');die();
				$this->load->view('include/header');
				$this->load->view('admin/upload_view',$data);
				$this->load->view('include/footer');
			}
				else{
// var_dump('ok');die();
				$choiceid=$this->input->post("choice1"); //////FIRST_RADIO_CHOICE
				if($choiceid=="1"){ 
					
					$this->form_validation->set_rules('opportunitychoice','OpportunityChoice','required');
					if($this->form_validation->run()==FALSE){
						$this->load->view('include/header');
						$this->load->view('admin/upload_view',$data); //maketoremainfilleddata
						$this->load->view('include/footer');
					}else{
						$uploadchoiceid=$this->input->post("uploadchoice1"); //////SECOND_RADIO_CHOICE
						if($uploadchoiceid == "1"){  
							$this->form_validation->set_rules('imageuploadchoice','ImageUploadChoice','required');
							if($this->form_validation->run()==FALSE){
								$this->load->view('include/header');
								$this->load->view('admin/upload_view',$data);
								$this->load->view('include/footer');
							}else{
								$data=array("opp_id"=>$this->input->post("opportunitychoice"),
											"image"=>$this->input->post("imageuploadchoice"),
											"description"=>$this->input->post("description"),
											"title"=>$this->input->post("title"),
											"sub_title"=>$this->input->post("subtitle"),
											"tag"=>$this->input->post("tags")
											
											
											);
								var_dump($data);die();
								$result=$this->Admin_model->upload($data);
								if($result){
									echo "Successful Uploading Opportunity";
									// redirect('admin/get_oppdetail');
								}else{
									echo "Fail Uploading Opportunity";
								}
							}
						}elseif($uploadchoiceid == "2"){
							$this->form_validation->set_rules('videouploadchoice','VideoUploadChoice','required');
							if($this->form_validation->run()==FALSE){
								$this->load->view('include/header');
								$this->load->view('admin/upload_view',$data);
								$this->load->view('include/footer');
							}else{
								$data=array("opp_id"=>$this->input->post("opportunitychoice"),
											"vdlink"=>$this->input->post("videouploadchoice"),
											"description"=>$this->input->post("description"),
											"title"=>$this->input->post("title"),
											"sub_title"=>$this->input->post("subtitle"),
											"tag"=>$this->input->post("tags")
											
											
											);
								var_dump($data);die();
								$result=$this->Admin_model->upload($data);
								if($result){
									echo "Successful Uploading Opportunity";
								}else{
									echo "Fail Uploading Opportunity";
								}
							}
						}  //////END_OF_SECOND_RADIO_CHOICE
					}
				}else if($choiceid=="2"){ 
					var_dump($choiceid);
					//echo "Skill"  + $choiceid;
					$this->form_validation->set_rules('skillchoice','SkillChoice','required');
					if($this->form_validation->run()==FALSE){
						$this->load->view('include/header');
						$this->load->view('admin/upload_view',$data); //maketoremainfilleddata
						$this->load->view('include/footer');
					}else{
						$uploadchoiceid=$this->input->post("uploadchoice");
						if($uploadchoiceid == "1"){
							$this->form_validation->set_rules('imageuploadchoice','ImageUploadChoice','required');
							if($this->form_validation->run()==FALSE){
								$this->load->view('include/header');
								$this->load->view('admin/upload_view',$data);
								$this->load->view('include/footer');
							}else{
								$data=array("skill_id"=>$this->input->post("skillchoice"),
											"image"=>$this->input->post("imageuploadchoice"),
											"description"=>$this->input->post("description"),
											"title"=>$this->input->post("title"),
											"sub_title"=>$this->input->post("subtitle"),
											"tag"=>$this->input->post("tags")
										
											
											);
								var_dump($data);die();
								$result=$this->Admin_model->upload($data);
								if($result){
									echo "Successful Uploading Skill";
									// redirect('admin/get_skilldetail');
								}else{
									echo "Fail Uploading Skill";
								}
							}
						}elseif($uploadchoiceid=="2"){
							$this->form_validation->set_rules('videouploadchoice','VideoUploadChoice','required');
							if($this->form_validation->run()==FALSE){
								$this->load->view('include/header');
								$this->load->view('admin/upload_view',$data);
								$this->load->view('include/footer');
							}else{
								$data=array("skill_id"=>$this->input->post("skillchoice"),
											"vdlink"=>$this->input->post("videouploadchoice"),
											"description"=>$this->input->post("description"),
											"title"=>$this->input->post("title"),
											"sub_title"=>$this->input->post("subtitle"),
											"tag"=>$this->input->post("tags")
											
											
											);
								var_dump($data);die();
								$result=$this->Admin_model->upload($data);
								if($result){
									echo "Successful Uploading Skill";
								}else{
									echo "Fail Uploading Skill";
								}
							}
						}
					}
				}//////END_OF_FIRST_RADIO_CHOICE
			}
			      }
			      else
			      {
			        redirect('user/login');
			      }
			      
			    }
			
		// }


	    }

}	 
?>