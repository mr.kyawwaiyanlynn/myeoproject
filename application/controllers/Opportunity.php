<?php
/**
* 
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Opportunity extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->helper('security');
    $this->load->model('Opportunity_model');
    $this->load->model('Skill_model');
    $this->load->model('Custom_model');
    $this->load->model('User_model');
    $this->load->library('encrypt');
    $this->load->model('Sample_model','',TRUE);
    $this->load->library('pagination');
    $this->load->model('Feed_model'); 
  }
//============TKH=================for custom page
  function index()
  {
    $log=$this->session->userdata('logged_in');
    $id = $log['id'];
    if(isset($log))
    {
      if($log['role']!='admin')
      {
              // var_dump('index');die();
        $pag = $this->config->item('pagination');   
        $pag['base_url']    = base_url().'opportunity/index';
        $data['search']     = Null ;
        $pag['total_rows']  = $this->Opportunity_model->count($id);
        $data['oppo']       = $this->Opportunity_model->opp($id,$pag['per_page'],$this->uri->segment(3),'desc');
        $data['pag']        = $pag;
 
          if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
        $this->load->view('include/header');
        $data1['opp']       = $this->Opportunity_model->left_all();
        $this->load->view('include/nav',$result); 
        $this->load->view('home/opportunity_view',$data);
        $this->load->view('home/opp_left',$data1);
        $this->load->view('include/footer2');
      }
      else
      {
        redirect('user/login');
      }
      
    }
  }

  function oppdetail($type)
  {  
// var_dump($type);
   $log=$this->session->userdata('logged_in');
   if(isset($log))
   {
    if($log['role']!='admin')
    {
     // $result['name']   = $this->User_model->getuser($log["email"]);
     	    if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
          $data['type']     = $type;
          // var_dump($data['type']);die();
          $pag              = $this->config->item('pagination');   
          $pag['base_url']  = base_url().'opportunity/oppdetail/'.$type.'/';//TzH
          $data['search']   = Null;
          $data['tag']      = Null;
          $page=($this->uri->segment(4)) ? $this->uri->segment(4) : 0;//TzH
          $pag['cur_page']=$page;//TzH
          $pag['total_rows']= $this->Sample_model->count_dopp($type);//TzH         
          $data1['opp']  = $this->Opportunity_model->left_all();
          $data['oppo']= $this->Opportunity_model->oppdetail($type,$pag['per_page'],$this->uri->segment(4),'desc');  //TzH  
          $data['pag'] = $pag; 
          $this->load->view('include/header');
          $this->load->view('include/nav',$result);
          $this->load->view('home/opportunity_view',$data);
          $this->load->view('home/opp_left',$data1);
          $this->load->view('include/footer2');  
//var_dump($data['oppo']);die();
        }
        else
        {
          redirect('user/login');
        }
        
      }
    }
  //   function search()
  //   {
      
  //    $log=$this->session->userdata('logged_in');
  //          // var_dump($log);die();
  //    if(isset($log))
  //    {
      
  //     if($log['role']!='admin')
  //     {   
       
  //       $search           = $this->input->post('search');
  //       $pag              = $this->config->item('pagination');   
  //       $pag['base_url']  = base_url().'opportunity/search';
  //       $data['search']   = Null;
  //       $data['tag']      = Null;
  //                     // $pag['total_rows'] = $this->Sample_model->count_opp();
  //       $page             = $this->Opportunity_model->searchcount($search);
  //                     // $pag2=$this->Custom_model->count_skill($id);
  //       $pag['total_rows']= $page;
  //                     // var_dump($pag);die();
  //       $data['oppo']     = $this->Opportunity_model->search($search,$pag['per_page'],$this->uri->segment(3),'desc');
  //                     // var_dump($data['result']);die();
  //                     // $real=unserialize($data['custom']);
        
        
  //       $data['pag'] = $pag;
  //       $result['name']=$this->User_model->getuser($log["email"]);
  //       $this->load->view('include/header');
        
  //       $data1['opp']     = $this->Opportunity_model->left_all();
  //       $data1['skill']   = $this->Skill_model->left_all();
  //             // var_dump($data['oppo']);die();
  //       $this->load->view('include/nav',$result);
  //       $this->load->view('home/opportunity_view',$data);
  //       $this->load->view('home/opp_left',$data1);
  //       $this->load->view('include/footer2');   
  //         // }
  //         //   }
  //         //   else
  //         //   {
  //         //     redirect('user/login');
  //         //   }
  //     }
  //     else
  //     {
  //       redirect('user');
  //     }
  //   }    
  // }

  function search()
  {
    
     $log=$this->session->userdata('logged_in');
           // var_dump($log);die();
          if(isset($log))
          {
            // var_dump($log);
            // var_dump("Hello c");die();
            if($log['role']!='admin')
            {   
               // $this->form_validation->set_rules('search','Search','required');
            // if($this->form_validation->run()==FALSE)
            //  { 
               // redirect('custom/search');
           //         }
           //         else
           //         { 
              $title=$this->input->post('search');
              $tag=$this->input->post('search1');
              // var_dump($a,$b);die();
                   $input =$this->input->post('submitform'); 
                   // var_dump($input);die();
              if($tag==Null)
              {
            // echo "HELLo";
              
                      $search       = $this->input->post('search');
                      // var_dump($search);die();
              $pag        = $this->config->item('pagination');   
              $pag['base_url']  = base_url().'opportunity/search';
              $data['search'] = Null;
              $data['tag']    = Null;
              // $pag['total_rows'] = $this->Sample_model->count_opp();
              $page=$this->Custom_model->searchcount($search);
              // $pag2=$this->Custom_model->count_skill($id);
              $pag['total_rows']  = $page;
              // var_dump($pag);die();
              $data['oppo']   = $this->Opportunity_model->search($search,$pag['per_page'],$this->uri->segment(3),'desc');
              // var_dump($data['result']);die();
              // $real=unserialize($data['custom']);
              
              
              $data['pag']    = $pag;
             // $result['name'] = $this->User_model->getuser($log["email"]);
            if($log['email']!=Null)
            {
              $result['name']    = $this->User_model->getuserbyemail($log['email']);
            }
            else
            {
              $result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
            }
              $this->load->view('include/header');
              
              $data1['opp']   = $this->Opportunity_model->left_all();
              $data1['skill']   = $this->Skill_model->left_all();
              // var_dump($data['oppo']);die();
                $this->load->view('include/nav',$result);
            $this->load->view('home/opportunity_view',$data);
            $this->load->view('home/left_view',$data1);
            $this->load->view('include/footer2');   
          
            }
            else
            { 
            
              $search1      = $this->input->post('search1');
                      // var_dump($search1);die();

              $pag        = $this->config->item('pagination');   
              $pag['base_url']  = base_url().'opportunity/search';
              $data['search'] = Null;
              $data['tag']    = Null;
              // $pag['total_rows'] = $this->Sample_model->count_opp();
              $page=$this->Custom_model->searchcount($search1);
              // $pag2=$this->Custom_model->count_skill($id);
              $pag['total_rows']  = $page;
              // var_dump($pag);die();
              $data['oppo']   = $this->Opportunity_model->search($search1,$pag['per_page'],$this->uri->segment(3),'desc');
              // var_dump($data['result']);die();
              // $real=unserialize($data['custom']);
              
              
              $data['pag']    = $pag;
              $result['name'] = $this->User_model->getuser($log["email"]);
              $this->load->view('include/header');
              
              $data1['opp']   = $this->Opportunity_model->left_all();
              $data1['skill']   = $this->Skill_model->left_all();
              // var_dump($data['oppo']);die();
            $this->load->view('include/nav',$result);
            $this->load->view('home/custom_view',$data);
            $this->load->view('home/left_view',$data1);
            $this->load->view('include/footer2');  
            }  
        }
            else
            {
              redirect('user/login');
            }
            
 }
 
}
//============TKH=================for detail
  function alldetail($id)
  {
  	
    $origin_encrypt  = str_replace("~","/",$id);
    $decrypt_id     = $this->encrypt->decode($origin_encrypt);
         // var_dump($decrypt_id);die();
    $log=$this->session->userdata('logged_in');
    if(isset($log))
    {
      if($log['role']!='admin')
      {
       // $result['name']   =$this->User_model->getuser($log["email"]);
       	  if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
        $data['query']    = $this->Opportunity_model->getall($decrypt_id);
                // var_dump($data['query']);die();
        $data['type']=$this->Opportunity_model->get_type($decrypt_id);
                // var_dump($data['type']);die();
        $data1['opp']     = $this->Opportunity_model->left_all();

        $data['post_id'] = $decrypt_id;

        $this->session->set_userdata('hello',1);

        $this->load->view('include/header');
        $this->load->view('include/nav',$result);
        $this->load->view('home/opp_detail',$data);
        $this->load->view('home/opp_left',$data1);
        $this->load->view('include/footer2');
      }
      else
      {
        redirect('user/login');
      }
    }else{
    	redirect('user');
    }
  }

  function location($value)
  {
    $log=$this->session->userdata('logged_in');
    // $id = $log['id'];
    if(isset($log))
    {
      if($log['role']!='admin')
      {
              // var_dump('index');die();
        $data['type'] = " ";
        $pag = $this->config->item('pagination');   
          $pag['base_url']    = base_url().'opportunity/location/'.$value.'/';//TzH
          $data['search']     = Null;
          $data['tag']    = Null;
          $page=($this->uri->segment(4)) ? $this->uri->segment(4) : 0;//TzH
          $pag['cur_page']=$page;//TzH
          $pag['total_rows']= $this->Opportunity_model->countloc($value);//TzH 
          $data['oppo']       = $this->Opportunity_model->location($value,$pag['per_page'],$this->uri->segment(4),'desc');//TzH
          $data['pag']        = $pag;
          //$result['name']     = $this->User_model->getuser($log["email"]);
          	if($log['email']!=Null)
						{
							$result['name']    = $this->User_model->getuserbyemail($log['email']);
						}
						else
						{
							$result['name']    = $this->User_model->getuserbyfacebookid($log['facebook']);
						}
          $this->load->view('include/header');
          $data1['opp']       = $this->Opportunity_model->left_all();
          $this->load->view('include/nav',$result); 
          $this->load->view('home/opportunity_view',$data);
          $this->load->view('home/opp_left',$data1);
          $this->load->view('include/footer2');       
          
        }
        else
        {
          redirect('user/login');
        }
        
      }
    }

    function feed(){

        
          
         $data['encoding'] = 'utf-8';
         $data['feed_name'] = '$type';
         $data['feed_url'] = '<?php base_url() ?>';
         $data['feed_description'] ='– Myanmar Youth Empowerment Opportunities';
         $data['page_description'] = 'Scholarships and other opportunities';
         $data['page_language'] = 'en-ca';
         $data['creator_email'] = 'suthinzar43@gmail.com';
         $data['query'] = $this->Feed_model->getFeedArticles();  
         // $data['ARTICLE_DETAILS'] = null;
         // var_dump($query['opp']);die();
  // if($query != Null){
  //  $data['ARTICLE_DETAILS'] =  $query;
  //  var_dump('dfewfaace',$data['ARTICLE_DETAILS']);die();
  // }  
         header("Content-Type: application/rss+xml");
         $this->load->view('home/rss', $data);
    }


  }
// <!--HTZN-->