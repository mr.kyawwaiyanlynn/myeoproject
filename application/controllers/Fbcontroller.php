<?php

class Fbcontroller extends CI_Controller {



    public function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('User_model');
        $this->load->model('Opportunity_model');
        $this->load->model('Skill_model');
        $this->load->model('Custom_model');
        $this->load->model('Sample_model','',TRUE);
        $this->load->library('pagination');
        $this->load->model('User_model');
        $this->load->library('encrypt');
    }

// Store user information and send to profile page (FACEBOOKLOGIN)
    public function facebooklogin() 
    {
        //Post Data to Facebook Login
        $name = $this->input->post('name');
        $email= $this->input->post('email');
        $id= $this->input->post('id');

        //Check Session
        $log=$this->session->userdata('logged_in');

        if($log)
        {
            //Redirect custom controller
           // redirect('custom');

        }
        else {

            //Check facebook user Has(or)NotHas
            $check_result = $this->User_model->checkfb($name, $email, $id);

            if (!$check_result) {
                //Insert Facebook User
                $result=$this->User_model->facebook_user($name, $email, $id);

                //Put into Session
                //Redirect profile/create controller
                if($result)
                {

                }else{

                    redirect('profile/create');

                }

            }else{

                //Put into Session
                $log=$this->session->userdata('logged_in');
                $sess_array = array('id'=>$check_result[0]->user_id,'username' => $name, 'email' => $email, 'role' => 'user', 'facebook' => $id ,'linkedin'=> Null);
                $this->session->set_userdata('logged_in', $sess_array);
                $this->session->userdata('login_status');
                if($this->session->userdata('login_status')==0) {
                    $status = array('status' => 'first');
                    $this->session->set_userdata('login_status', 1);
                }
                //Redirect custom controller
                //redirect('custom');
            }


        }

    }

 // Store user information and send to profile page (LINDINLOGIN)
    public function linkedinlogin() {

        //Post Data to Linkedin Login
        $name = $this->input->post('name');
        $photourl= $this->input->post('photourl');
        $id= $this->input->post('id');
        $email= $this->input->post('email');
        //Check Session
       // $log=$this->session->userdata('logged_in');
        $check_result = $this->User_model->checklinkedin($name, $id, $photourl);


            if ($check_result == null) {
                 //Put into Session
                $this->User_model->linkedin_user($name, $id, $photourl, $email);
               // redirect('custom/linkedin');

            }else{
                //Insert Linkedin User
                $log=$this->session->userdata('logged_in');
                $sess_array = array('id'=>$check_result[0]->user_id,'username' => $name, 'role' => 'user','email' =>$email,'linkedin'=>$id,'picture'=>$photourl,'facebook'=>Null);
                $this->session->set_userdata('logged_in', $sess_array);
                $this->session->userdata('login_status');
                if($this->session->userdata('login_status')==0) {
                    $status = array('status' => 'first');
                    $this->session->set_userdata('login_status', 1);
                }
            }

       
        
    }



// Logout from facebook
    public function logout() {


    }

}
?>