<?php

/**
 *
 */
class Detail extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

       
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('security');
        $this->load->model('Opportunity_model');
        $this->load->model('Skill_model');
        $this->load->model('Custom_model');
        $this->load->model('Sample_model', '', TRUE);
        $this->load->library('pagination');
        $this->load->model('User_model');
        $this->load->library('encrypt');
         $this->load->helper('url');
    }

    function detail($id)
    {

        $log=$this->session->userdata('logged_in');

        if($log)
        {
            $www=$this->session->userdata('hello');
             
                 if($www==1)
                 {
                    $data['query'] = $this->Custom_model->getall($id);
                    $check = $this->Custom_model->check($id);
                    // var_dump($check->skill);die();
                    if ($check->skill == '0') {

                        $data['type'] = $this->Opportunity_model->get_type($id);

                    } else {

                        $data['type'] = $this->Skill_model->get_type($id);

                    }

                    $data1['opp'] = $this->Opportunity_model->left_all();
                    $data1['skill'] = $this->Skill_model->left_all();
                    $data['post'] = $id;
                    $this->session->set_userdata('hello',0);
                    $this->load->view('include/header');
                    $this->load->view('home/detail_view', $data);
                    $this->load->view('include/footer2');
                    
              }else
              {
              
                  $encrypt_ids=$this->encrypt->encode($id);
                  $modify_encrypt=str_replace("/","~",$encrypt_ids);
                  $this->session->set_userdata('facebook_click_post',$modify_encrypt);
                  redirect('user'); 
              
             }


        }
        else{     
          
                  $encrypt_ids=$this->encrypt->encode($id);
                  $modify_encrypt=str_replace("/","~",$encrypt_ids);
                  $this->session->set_userdata('facebook_click_post',$modify_encrypt);
                  redirect('user');    
        }

}

function oppdetail($id)
    {

  
        $log=$this->session->userdata('logged_in');
       

        if($log!=null)
        {
            $www=$this->session->userdata('hello');
             
             if($www==1)
             {

                //$result['name']		= $this->User_model->getuser($log["email"]);
                $data['query'] = $this->Custom_model->getall($id);
                $check = $this->Custom_model->check($id);
                // var_dump($check->skill);die();
                if ($check->skill == '0') {

                    $data['type'] = $this->Opportunity_model->get_type($id);

                } else {

                    $data['type'] = $this->Skill_model->get_type($id);

                }

                $data1['opp'] = $this->Opportunity_model->left_all();
                $data1['skill'] = $this->Skill_model->left_all();

               // $this->session->set_userdata('hello',0);
                $this->load->view('include/header');
                $this->load->view('home/oppdetail_view', $data);
                $this->load->view('include/footer2');
          }else
          {
 
             // redirect('custom/alldetail/'.$encrypt_id,'refresh');
         }


    }{


         // redirect('user');
          $encrypt_ids=$this->encrypt->encode($id);
          $modify_encrypt=str_replace("/","~",$encrypt_ids);
          $this->session->set_userdata('facebook_click_post',$modify_encrypt);
          redirect('user');
    }


}

function skilldetail($id)
    {
         $encrypt_id  = $this->encrypt->encode($id);
         var_dump($encrypt_id);
  
        $log=$this->session->userdata('logged_in');

        if($log)
        {
            $www=$this->session->userdata('hello');
             
             if($www==1)
             {

                $decrypt_id = str_replace("~", "/", $id);

               
                //$result['name']		= $this->User_model->getuser($log["email"]);
                $data['query'] = $this->Custom_model->getall($id);
                $check = $this->Custom_model->check($id);
                // var_dump($check->skill);die();
                if ($check->skill == '0') {

                    $data['type'] = $this->Opportunity_model->get_type($id);

                } else {

                    $data['type'] = $this->Skill_model->get_type($id);

                }

                $data1['opp'] = $this->Opportunity_model->left_all();
                $data1['skill'] = $this->Skill_model->left_all();
               // $this->session->set_userdata('hello',0);
                $this->load->view('include/header');
                $this->load->view('home/skilldetail_view', $data);
                $this->load->view('include/footer2');
          }else
          {
                  //  var_dump($encrypt_id);
                // die();
                //$this->load->library('controllers/custom');
              redirect('custom/alldetail/'.$encrypt_id,'refresh');

              //$this->users->adddetail($encrypt_id);  
          
         }


    }{
         // redirect('user');
        var_dump("Here");
    }


}

}

?>
