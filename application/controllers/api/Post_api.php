<?php

require(APPPATH.'libraries/REST_Controller.php');

/**
* 
*/
class Post_api extends REST_Controller
{
  
    
    function __construct()
  {
    parent::__construct();
    $this->load->model('User_model');
    $this->load->library('form_validation');
    $this->load->model('Admin_model','',TRUE);
    $this->load->model('Api_model','',TRUE);
    $this->load->model('Custom_model','',TRUE);
    $this->load->model('Opportunity_model','',TRUE);
    $this->load->model('Profile_model','',TRUE);
    $this->load->model('Skill_model','',TRUE);
    $this->load->helper('form');
    $this->load->helper('security');
  }
  function data_post()
  {
     $data=$this->input->post('data');
           
     $data = $this->Api_model->getpost($data); 
    
    $this->response($data);
    
  }
  function profile_post()
  {
     $image=img_upload();
     $this->response($image);
  }
    function img_upload()
    {
      $config['upload_path']          = './uploads/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['max_size']             = 1000;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('image'))
                {
                      $image  = $this->input->post('image');
                      $this->respon($image);

                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $this->response($data['upload_data']['file_name']);
                        // $data = $this->Profile_model->img($this->upload->data('full_path'),$this->input->post());
                }
    }
    function pdf_upload()
    {
      $config['upload_path']          = './pdf/';
      $config['allowed_types']        = 'pdf|PDF';    
      $config['max_size']             = 1000;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('cv'))    
                {
                      $cv  = $this->input->post('cv');
                      return $cv;

                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        return $data['upload_data']['file_name'];
                        // $data = $this->Profile_model->img($this->upload->data('full_path'),$this->input->post());
                }
    }
   
    function pdf_upload1()
    {
      $config['upload_path']          = './pdf/';
      $config['allowed_types']        = 'pdf|PDF|docx|DOCX';
      $config['max_size']             = 1000;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);

      if ( ! $this->upload->do_upload('letter'))
                {
                   $letter  = $this->input->post('letter');
                      return $letter;
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        return $data['upload_data']['file_name'];
                }
    }

     function video_upload()
     {
    
        $configVideo['upload_path']   = './video/';
        $configVideo['max_size']      = 60000;
        $configVideo['allowed_types'] = 'mp4';
        $configVideo['overwrite']     = FALSE;
        $configVideo['remove_spaces'] = TRUE;

        $this->load->library('upload', $configVideo);
        $this->upload->initialize($configVideo);

        if(!$this->upload->do_upload('video')) 
        {
          $video  = $this->input->post('video');
                      return $video;
        }
        else
        {
          
               $data = array('upload_data' => $this->upload->data());
                        return $data['upload_data']['file_name'];
        }
  }

}
?>