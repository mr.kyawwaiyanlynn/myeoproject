<?php

require(APPPATH.'libraries/REST_Controller.php');

/**
* 
*/
class Api extends REST_Controller
{
	
    
    function __construct()
  {
    parent::__construct();
    $this->load->model('User_model');
    $this->load->library('form_validation');
    $this->load->model('Admin_model','',TRUE);
    $this->load->model('Custom_model','',TRUE);
    $this->load->model('Opportunity_model','',TRUE);
    $this->load->model('Profile_model','',TRUE);
    $this->load->model('Skill_model','',TRUE);
    $this->load->helper('form');
    $this->load->helper('security');
  }



//WY API For FacebookSignUp
function api_facebooksignup_post()
{

    $email=$this->input->post('email');
    $name=$this->input->post('name');
    $fb_id=$this->input->post('fb_id');
    $image_url=$this->input->post('img_url');

    //insert data 
    $result=$this->User_model->api_login($email,$fb_id,$image_url,$name);

    if($result==NULL)
    {
        // insert fail
        $this->response(array('error' => TRUE,'message'=>'invalid'));        
    }
    else
    {
        //insert success
        $this->response(array('error' => FALSE,'message'=>'valid'));          
    }

}


//WY API For SignUp
function api_signup_post()
{

    $this->form_validation->set_rules('name','Name','required');
    $this->form_validation->set_rules('email','Email','required'); 
    $this->form_validation->set_rules('password','Password','required'); 
    $this->form_validation->set_rules('cfmpassword','Confirm Password','required');

    if($this->form_validation->run()==FALSE )
      {

          //Validation Fail  
          $this->response(array('error' => TRUE,'message'=>'validation fail'));

      }
      else
      {

       $name=$this->input->post('name');
       $email=$this->input->post('email');
       $password=sha1($this->input->post('password'));
       $cfmpassword=sha1($this->input->post('cfmpassword'));

     
        if($password==$cfmpassword)
        {
           //Check User is Already Have 
           $result=$this->User_model->check($name,$email,$password);

           if($result!=NULL)
           {

            //Already Have Email  
             $this->response(array('error' => TRUE,'message'=>'validation fail'));

           }
           else
           {
            //New User
               $sess_array=array('username'=>$name,'password'=>$password,'email'=>$email,'role'=>'user');
               $this->session->set_userdata('confirm_logged_in',$sess_array);
               $this->session->set_flashdata('msg','Email Sent');
               $success=$this->User_model->sendEmail($email);
               if($success)
               {
                   //SignUp Success
                   $this->response(array('error' => FALSE,'message'=>'valid')); 
               }

            }

        }
        else
        {
             $this->response(array('error' => TRUE,'message'=>'wrong confirmed'));
        }

     }

}

//WY API For Login
function api_login_post()
{
    //Validation Username and Email
    $this->form_validation->set_rules('email','Email','required');
    $this->form_validation->set_rules('password','Password','required');

    if($this->form_validation->run()==FALSE )
    { 
      //Validation Fail  
      $this->response(array('error' => TRUE,'message'=>'validation fail'));
    }
    else
    { 

      $email=$this->input->post('email');
      $password=sha1($this->input->post('password')); // Password with --sha1--
      //Check Email and Password
      $result=$this->User_model->login($email,$password);          

        if($result==NULL)
        {
            //check fail
            $this->response(array('error' => TRUE,'message'=>'Your Email or Password does not match!!!'));
        }
        else 
        {
            //check success
            $sess_array=array('id'=>$result['user_id'],'username'=>$result['name'],'email'=>$result['email'],'role' =>$result['role']);
            $this->session->set_userdata('logged_in',$sess_array);
            $this->response(array('error' => FALSE,'message'=>'success','user_data'=>$result)); 
        }

    }

}





}
?>
