<div class="wrapper">

      <!-- Main Header -->
      <header class="main-header" style="position: fixed;width: 15%;float: left;">

        <!-- Logo -->
        <a href="<?= base_url();?>custom" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?=base_url();?>images/myeo_logo.png" width="60" height="50"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='nav-img '/></span>
        </a>

        <!-- Header Navbar -->
          
        <nav class="navbar navbar-static-top col-md-12 col-sm-11 col-xs-11" role="navigation">
          <!-- Sidebar toggle button-->

          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
<div>

          <!-- <a href="<?=base_url();?>custom" class="col-md-offset-1 col-md-2 col-sm-2 col-xs-2 nav-link" style="text-decoration: none;outline: none;">
            <label class="<?php echo activate_menu('custom'); ?>" >Home</label><!-- <span style="padding-left: 65px;color: white;margin-right:"></span> -->
          <a href="<?=base_url();?>opportunity" class="col-md-offset-2 col-sm-offset-2 col-xs-offsset-2 col-md-2 col-sm-1 col-xs-1 nav-link" style="text-decoration: none;outline: none;">
            <label class="<?php echo activate_menu('opportunity'); ?>" >Opportunity<span class="line">|</span></label></a>
          <a href="<?=base_url();?>skill" class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2 col-md-2 col-sm-1 col-xs-1 nav-link" style="text-decoration: none;outline: none;"><label class="<?php echo activate_menu('skill'); ?>">Skill</label></a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account Menu -->
              <li class="dropdown user user-menu" >
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                  <!-- The user image in the navbar-->
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xsa" style="margin-right: " ><img src="<?=base_url()?>images/pp.PNG" height="40px"></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                   <!--  <div class="pull-left">
                        SKILL
                    </div> -->

                      <?php 
                           $encrypt_ids=$this->encrypt->encode($name['user_id']);
                           $modify_encrypt=str_replace("/","~",$encrypt_ids);                           
                       ?>
                    <div class="pull-right"> 
                          <div style="background-color: #CCC;width:100%;">
                            <label style="padding-right:30px;font-family:awesome;" id="button"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $name['name'];?></label>
                           
                            <a href="<?=base_url();?>profile/edit/<?= $modify_encrypt?>" style="padding-right:60px;font-family:awesome;font-size: 15px;background-color: #004167"  class="button"> <span class="glyphicon glyphicon-edit"></span>&nbsp;Edit Profile</a>
                             <a href="<?=base_url();?>User/changepwd/<?= $modify_encrypt?>" style="padding-right:60px;font-family:awesome;font-size: 15px;background-color: #004167"  class="button"> <span class="glyphicon glyphicon-edit"></span>&nbsp;Change Password</a>
                              <!-- <label  data-toggle="modal"  class="button" data-target="#mymodal" > <span class="glyphicon glyphicon-edit"></span>&nbsp;Change Password</label> -->
                            <a href="<?=base_url();?>logout" style="padding-right:60px;font-family:awesome;font-size: 15px;background-color: #004167"  onclick="logout()" class="button"> <span class="glyphicon glyphicon-log-out"></span>&nbsp;Log Out</a>
                          </div>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>

      <?php if ($this->session->flashdata('msg_diff')) { ?>
                    <div class="alert alert-danger"><?= $this->session->flashdata('msg_diff') ?></div>
      <?php } ?>

    <div class="modal fade" id="mymodal">
        <div class="modal-dialog">

           

            <div class="modal-content">
                <div class="modal-header" style="background:#004167;color:#fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title">Change Your Password</h4></center>
                </div>
                <div class="modal-body">
                    <?php echo form_open(base_url().'user/changepwd', 'class="form-horizontal" method="post" ')?>

                    <div class="form-group">
                        <label class="control-label col-md-5">Current Password:</label>
                        <div class="col-md-5">
                            <input type="password" class="form-control" name="current" placeholder="Current password...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-5">New Password:</label>
                        <div class="col-md-5">
                            <input type="password" class="form-control" name="new" placeholder="New password...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-5">Confirm New Password:</label>
                        <div class="col-md-5">
                            <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Change</button>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
      <!-- Left side column. contains the logo and sidebar -->



     <script>

         function logout(){
             IN.User.logout(function() {

                 ////window.location = "<?=base_url();?>logout/logout"
                    window.location.reload()
             });
         }




     </script>