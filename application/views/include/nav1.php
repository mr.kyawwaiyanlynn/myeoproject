    <!-- HSU THINAR NYI -->
<!-- ===========PAGE HEADER=========== -->
<!-- ===========PAGE BODY=========== -->

<!--  <?php 
       $encrypt_ids=$this->encrypt->encode($user_id);
       $modify_encrypt=str_replace("/","~",$encrypt_ids);
   ?> -->

  <?php echo form_open_multipart('profile/edit/'.$modify_encrypt); ?>
   
   <div class="left_pp col-md-4" style="padding-top: 100px;">

    <?php if($this->session->flashdata('msg')){?>
     <center> <div class="alert alert-danger alert-dismissable" style="width:70%;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?=$this->session->flashdata('msg')?></div></center>
    
    <?php }?>
    <?php if($this->session->flashdata('msg1')){?>
          <center><div class="alert alert-success alert-dismissable" style="width:70%;" >
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?=$this->session->flashdata('msg1')?></div></center>
        
    <?php }?>


    <input type="file" id="browse" name="image" style="display: none" onchange="readURL(this);" accept="image/*">

        <?php if($edit['picture']==null){ ?>
            <img src="http://graph.facebook.com/<?= $edit['facebook'] ?>/picture" height="120px" style="min-width:70px" class="col-md-offset-8 col-md-4 thumbnail" id="fakeBrowse" onclick="HandleBrowseClick();"/  accept="image/*" onchange="showMyImage(this)" name="image">  
        <?php  }else{ ?>
            <img src="<?php echo base_url();?>uploads/<?= $edit['picture']?>" height="120px" style="min-width:70px" class="col-md-offset-8 col-md-4 thumbnail" id="fakeBrowse" onclick="HandleBrowseClick();"/  accept="image/*" onchange="showMyImage(this)" name="image">
        <?php } ?>


           <input type="text" name="image" class="form-control" value="<?= $edit->picture ?>" style="display: none">
           <a height="120px" style="min-width:70px" class="col-md-offset-8 col-md-4" id="fakeBrowse" onclick="HandleBrowseClick();"/  accept="image/*" onchange="showMyImage(this)" name="image">Edit Photo &nbsp;<span class="glyphicon glyphicon-camera"></span></a>

          <?php echo form_error('image', "<div class='error' style='color:red; font-size:10px;>*","</div>"); ?>
  </div>

  <div class=" col-md-8 form-group pp-right "><!-- Right -->

    <div class="input col-md-6 ">
      <label class="profile">Name :</label>
        <div class="input-placeholder"><input type="text" name="name" class="form-control" value="<?= $edit->name ?>">
          <div class="placeholder">
          </div>
        </div>

       <label class="profile">Education :</label>
        <div class="input-placeholder" ><input type="text" name="education" class="form-control" value="<?= $edit->education ?>" placeholder="Education">
          <div class="placeholder">
          </div>
        </div>
      
        <label class="profile">Location :</label>
        <div class="input-placeholder"><input type="text" name="location" class="form-control" value="<?= $edit->location?>" placeholder="location">
          <div class="placeholder">
          </div>
        </div>

        <label class="profile">A couple of sentences to introduce yourself...</label>
        <textarea name="sentence" class="form-control" rows="3" placeholder="A couple of sentences to introduce yourself..."><?=$edit->sentence?></textarea>

        <label class="col-md-offset-9"><font style="color:#9b9b9b">(100 words)</font></label><br>

        <div class="row pp-btn">
            <label class="col-md-4"><font style="color:#9b9b9b;">CV/Resume</font></label>
            <div class="col-md-offset-4">

            <!-- <iframe src="<?php echo base_url()?>pdf/<?= $edit->cv?>" style="width:600px; height:500px;" frameborder="0"></iframe> -->
            <input type="file" id="browse1" name="cv" style="display: none" onChange="Handlechange1();"/>
            <input type="button" value="Upload" id="fakeBrowse" onclick="HandleBrowseClick1();"/ style="background-color:#004167;color: white;border-radius: 7px"  onchange="showMyPdf(this)" accept="pdf/*" class="fileinput2 col-md-3" name="cv">
            <?php if($edit->cv == NULL){ ?>
            <label class="profile">Please upload CV</label> <?php }else{?>
            <label><?= $edit->cv?></label> <?php } ?>
            
            <input type="text" name="cv" class="form-control" value="<?= $edit->cv ?>" style="display: none">

            </div>
        </div>

        <div class="row pp-btn">
            <label class="col-md-4"><font style="color:#9b9b9b">Cover Letter</font></label>
            <div class="col-md-offset-4">
            <input type="file" id="browse2" name="letter" style="display:none;" onChange="Handlechange2();"/>
            <input type="button" value="Upload" id="fakeBrowse" onclick="HandleBrowseClick2();"/ style="background-color:#004167;color: white;border-radius:7px;" class=" col-md-3" name="letter">
             <?php if($edit->cover_letter == NULL){ ?>
            <!-- <img src="<?php echo base_url();?>images/60319-200.png" height="20px" width="20px"> -->
            <label class="profile">Please upload Cover Letter</label> <?php }else{?>
            <label><?= $edit->cover_letter?></label> <?php } ?>
           
            <input type="text" name="letter" class="form-control" value="<?= $edit->cover_letter ?>" style="display: none">

          </div>

        </div>

        <div class="row pp-btn">
            <label class="col-md-4"><font style="color:#9b9b9b">Video CV</font></label>
            <div class="col-md-offset-4">
            <div id="show">
            <!--  <video width="200" controls>
              <source src="" id="video_here">
            </video> -->

            <!-- <input type="file" name="file[]" class="file_multi_video" accept="video/*"> -->
            </div>
            <input type="file" id="browse3" name="video" class="file_multi_video" accept="video/*" style="display: none" onChange="Handlechange3();"/>
            <input type="button" value="Upload" id="fakeBrowse" onclick="HandleBrowseClick3();"/ style="background-color:#004167;color: white;border-radius: 7px" class=" col-md-3" name="video">
            <!-- <img src="<?php echo base_url();?>images/60319-200.png" height="20px" width="20px"> -->

             <?php if($edit->intro_video == NULL){ ?>
            <label class="profile">Please upload Video CV</label> <?php }else{?>
            <label><?= $edit->intro_video?></label> <?php } ?>
            <input type="text" name="video" class="form-control" value="<?= $edit->intro_video ?>" style="display: none">

            </div>
        </div><br>
       
       </div>

           <div class="row">
              <div class="col-md-3"></div>

    <div class="col-md-9 col-xs-12 col-sm-12">
      <!-- ===========OPPORTUNITIES=========== -->

        <div class="form-group oppo">
            <label><span style="color: #F00;">*</span>&nbsp;
            <font style="color:#4a4a4a;font-size: 15px">OPPORTUNITIES IN INTERESTED IN</font></label></p>

             <?php if($this->session->flashdata('msg1')){?>
                <div class="alert alert-danger"><?=$this->session->flashdata('msg1')?></div>
              <?php }?>

             <?php $count=1; foreach ($opp as $row) {;?>
             <font style="color:#9b9b9b">

              <div class="col-md-5 col-xs-6 col-sm-6" >
                <div class="row col-md-12 col-sm-12 col-xs-12">
                  <div class="checkbox">
                    <label>
                       <input type="checkbox" class="chk" id="<?=$row->opp_id?>" name="oppcheck[]" value="<?=$row->opp_id?>">
                       </input>
                      
                       <i class="helper"></i><font class="font"><?=$row->type?></font>
                    </label>
                  </div>
               </div>
            

              
            </div>

            </font>
            
         <?php $count++; };?>
           <p>&nbsp; </p> 
        </div><!-- End of Opportunitiew --> 
    </div>

         <div class="col-md-9 col-sm-12 col-xs-12">
      <!-- ===========SKILL=========== -->

        <div class="form-group oppo">
            <label style="padding-top: 20px;"><span style="color: #F00;">*</span>&nbsp;
            <font style="color:#4a4a4a;font-size: 15px;">SKILLS IN INTERESTED IN DEVELOPING</font></label></p>

             <?php if($this->session->flashdata('skill')){?>
                <div class="alert alert-danger"><?=$this->session->flashdata('skill')?></div>
              <?php }?>

              
           <?php $count=1; foreach ($skill as $row) {;?>
            <font style="color:#9b9b9b">

              <div class="col-md-5 col-xs-6 col-sm-6" >
                <div class="row col-md-12 col-sm-2 col-xs-2"">
                  <div class="checkbox col-xs-12 col-sm-12">
                    <label>
                       <input type="checkbox" id="chk" class="<?= $row->skill_id?>" name="skillcheck[]" value="<?=$row->skill_id?>">
                       <i class="helper"></i><font class="font"><?=$row->type?></font>
                    </label>
                  </div>

               </div>


               </div>
               
             </font>
             <?php $count++; };?> 

            <p>&nbsp; </p>
        </div><!-- End of Skill --> 
         </div>

   </div>
  
  <div class="col-md-offset-5 form-group">

     <a href="<?=base_url()?>profile/edit/<?=$modify_encrypt;?>" 
                        class="btn " name="Save" style="background-color: #004167;color: white;width: 90px">SAVE</a>
      <button type="submit" class="btn" style="background-color:#004167;color: white;width: 90px">Save</button>

    <a href="<?=base_url();?>custom" class="btn " style="background-color: #004167;color: white;width: 90px" >Cancel</a>
  </div>
 <br>   

  </div><!-- END OF RIGHT -->
</form>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#fakeBrowse').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
 </script>

 <script>
  $(document).ready(function(){
  $("#view").click(function(){
  $("#show").show();
  });
  
  });
  });
  </script>

<script type="text/javascript">
 $(document).on("change", ".file_multi_video", function(evt) {
  var $source = $('#video_here');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
});
</script>
                <script type="text/javascript">


                        var ex;
                        <?php foreach ($edit2 as $key ) { ?>

                              ex  = <?php echo json_encode($key->opp_id); ?> ;
                             // alert(ex);

                              if(ex == 1)
                              {
                                
                                 $( "#1").prop('checked', true);

                              }
                               if(ex == 2)
                              {
                                
                                 $( "#2").prop('checked', true);

                              }
                               if(ex == 3)
                              {
                                
                                 $( "#3").prop('checked', true);

                              }
                               if(ex == 4)
                              {
                                
                                 $( "#4").prop('checked', true);

                              }
                               if(ex == 5)
                              {
                                
                                 $( "#5").prop('checked', true);

                              }
                               if(ex == 6)
                              {
                                
                                 $( "#6").prop('checked', true);

                              }
                               if(ex == 7)
                              {
                                
                                 $( "#7").prop('checked', true);

                              }
                               if(ex == 8)
                              {
                                
                                 $( "#8").prop('checked', true);

                              }
                               if(ex == 9)
                              {
                                
                                 $( "#9").prop('checked', true);

                              } if(ex == 10)
                              {
                                
                                 $( "#10").prop('checked', true);

                              } if(ex == 11)
                              {
                                
                                 $( "#11").prop('checked', true);

                              } if(ex ==12)
                              {
                                
                                 $( "#12").prop('checked', true);

                              }

                             
                        <?php } ?>
                        
              </script>

  <script type="text/javascript">

              var ex1;
              <?php foreach ($edit1 as $skill ) { ?>

                    ex1  = <?php echo json_encode($skill->skill_id); ?> ;
                   // alert(ex);

                    if(ex1 == 1)
                    {
                      
                       $( ".1").prop('checked', true);

                    }
                     if(ex1 == 2)
                    {
                      
                       $( ".2").prop('checked', true);

                    }
                     if(ex1 == 3)
                    {
                      
                       $( ".3").prop('checked', true);

                    }
                     if(ex1 == 4)
                    {
                      
                       $( ".4").prop('checked', true);

                    }
                     if(ex1 == 5)
                    {
                      
                       $( ".5").prop('checked', true);

                    }
                     if(ex1 == 6)
                    {
                      
                       $( ".6").prop('checked', true);

                    }
                     if(ex1 == 7)
                    {
                      
                       $( ".7").prop('checked', true);

                    }
                     if(ex1 == 8)
                    {
                      
                       $( ".8").prop('checked', true);

                    }
                     if(ex1 == 9)
                    {
                      
                       $( ".9").prop('checked', true);

                    }
                     if(ex1 == 10)
                    {
                      
                       $( ".10").prop('checked', true);

                    }
                   
              <?php } ?>

    </script>




<div class="col-md-12" style="position:fixed;top: 0;width: 100%">
<div class="row pg_header"  style="background-color:#004167" >
<!-- <span class="label_tag"> -->
    <label class="col-md-3 col-sm-3 col-xs-3" id="navigations">
        <img src="<?=base_url();?>/images/myeo_logo.png"  width="71"  height="63" class='col-md-offset-2' />
    </label>

    <div class="col-md-offset-4" style="padding-top:15px" align="Right" >
      
    <!-- ==============PROFILE ICON=================== -->

      <ol class="dropdown nav navbar-nav navbar-right" style="margin-right: 25px;">
        <!-- <li class="dropdown"> --> 
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?=base_url();?>images/pp.png" width="35px"><b class="caret"></b></a>
          <br><br><ol class="dropdown-menu col-md-3 col-sm-3 col-xs-3 pull-right" style="background-color:white;">
                <div style="background-color: white;width:100%;">
                  <label style="padding-right:30px;" id="button" align="center"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $edit->name;?></label>
                  <label  data-toggle="modal"  class="button" data-target="#mymodal" > <span class="glyphicon glyphicon-edit"></span>&nbsp;Change Password</label>
                  <a href="<?=base_url();?>logout/logout" class="button" style="font-size:15px;padding-right:50px;"> <span class="glyphicon glyphicon-log-out"></span>&nbsp;Log Out</a>
                </div>
            </ol>
        <!-- </li> -->
      </ol>
            <!-- END OF PROFILE ICON -->

    </div>
</div><!-- PG_HEADER -->
</div>
<!-- Modal for change password -->
<div class="modal fade" id="mymodal">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center><h4 class="modal-title">Change Your Password</h4></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/changepwd', 'class="form-horizontal" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-5">Current Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="current" placeholder="Current password...">               
            </div>
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="new" placeholder="New password...">               
            </div>
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Confirm New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">               
            </div>
       </div>
       <div class="form-group">
           <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Change</button>
       </div>
     <?php echo form_close();?>
    </div>
</div>
</div> 
</div>
<!-- ==============End Modal================== -->


<!-- </div>