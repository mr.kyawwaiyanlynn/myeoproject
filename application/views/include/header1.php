<!DOCTYPE html>
<html>
<head>
 <style type="text/css">
 .button {
    display: inline-block;
    outline: none;
    cursor: pointer;
    border: solid 1px #4D56A1;
    background:  #004167;
    /*background-size:100;*/
    color: #fff;
    width: 100%;
    /*margin-bottom:30px;*/
    text-align: center;
    text-decoration: none;
    font: 14px/100% Arial, Helvetica, sans-serif;
    /*padding: .5em 2em .55em;*/
    padding-top: 15px;
    padding-bottom: 15px;
    /*padding-right: 10px;*/
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-border-radius: .5em; 
    -moz-border-radius: .5em;
    border-radius: .3em;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
}
.button:hover {
    background: #4D56A1;
    background: -webkit-gradient(linear, left top, left bottom, from(#4D56A1), to(#4D56A1));
    background: -moz-linear-gradient(top,  #4D56A1,  #4D56A1);
    filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#4D56A1', endColorstr='#4D56A1');
    color:white;
}
.button:active {
    position: relative;
    top: 1px;
}

#button{
  display: inline-block;
    outline: none;
    cursor: pointer;
    /*border: solid 1px #da7c0c;*/
    /*background:  #004167;*/
    /*background-size:100;*/
    /*color: #fff;*/
    width: 100%;
    /*margin-bottom:30px;*/
    text-align: left;
    padding-left: 10px;
    text-decoration: none;
    font: 14px/100% Arial, Helvetica, sans-serif;
    /*padding: .5em 2em .55em;*/
    /*padding-top: 20px;*/
    padding-bottom: 10px;
    /*padding-right: 10px;*/
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-border-radius: .5em; 
    -moz-border-radius: .5em;
    border-radius: .3em;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);

}
 </style>

  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
  <script type="text/javascript" src="http://www.techtricky.com/wp-content/jquery/jquery.jclock.js"></script>
  <script type="text/javascript">
    $(document).ready(
        function() {
           $("#zones").change(function(){
           if ($('#time-cont .time').length>0){ $('#time-cont .time').remove();}
     var offset = $(this).val();    
           if (offset == '') return;       
 
     $('#time-cont').append('<div class="time"></div>');
           
           var options = {
            format:'<span class=\"dt\">%A, %d %B %I:%M:%S %P</span>',
            timeNotation: '12h',
            am_pm: true,
            fontFamily: 'Verdana, Times New Roman',
            fontSize: '20px',
            foreground: 'black',
            background: 'yellow',
            utc:true,
            utc_offset: offset
          }
            
          $('#time-cont .time').jclock(options);
       });
     });
    </script>


  <!--  date picker link by ztd -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/datepicker.css">
  <script type="text/javascript" src="<?php echo base_url();?>template/js/datepicker.js"></script>
  <script type="text/javascript">
         $('#date').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
   
          });
  </script>
  <script type="text/javascript">
         $('#date1').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
   
          });
  </script>

  <!-- end here -->


  <link rel="icon" type="image/icon" href="<?=base_url();?>images/myeo.jpg">
  <title>MYEO</title>
    <meta name="viewport" content="width=device-width,initital-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.min.css">
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/3.2.1-jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery-2.2.1-jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>link/bootstrap-tokenfield.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>link/tokenfield-typeahead.min.css">
   <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.dataTables.min.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/> -->
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=awesome' >
   <!--dropdown-->
   <!-- <link href="<?=base_url();?>web/css/style.css" rel='stylesheet' type='text/css' />
   <link href="<?=base_url();?>web/css/font-awesome.css" rel="stylesheet"> -->

    
    <script src = "<?php echo base_url();?>jquery.com/jquery-1.10.2.js"></script>
    <script src = "<?php echo base_url();?>jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/ckfinder/ckfinder.js"></script>
     <script src="<?php echo base_url();?>template/js/jquery.localScroll.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>template/js/jquery.scrollTo.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>script.js"></script>
<!--for drop down -->
<!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>link/tagmanager.min.css">

  <script type="text/javascript" src="<?php echo base_url();?>link/tagmanager.min.js"></script>

<!-- fixed nav bar -->
    <script src="<?php echo base_url();?>link/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo base_url();?>link/typeahead.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/myeo.css">
    

<style>
 .active1{
 
        /*border-color: green;*/
        /*background-color: green;*/
        /*font-weight: lighter;*/
        /*text-decoration:underline;*/
        /*color: inherit;*/
        color:#85EC14;


}


a{
   text-decoration: none;
   color:white;
}



.mfont{

    text-shadow: 3px 3px  #DCDCDC;

}
.navbar {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9999;
  width: 100%;
  height: 50px;
  background-color: #00a087;

}
/* Primary Styles */
*, *::before, *::after {
   box-sizing: border-box;
}

em {
   font-style: normal;
}

a {
   text-decoration: none;
   color: inherit;
} 

/* Layout */
/*.s-layout {
   display: flex;
   width: 100%;
   min-height: 100vh;
}

.s-layout__content {
   display: flex;
   justify-content: center;
   align-items: center;
   flex: 1;
}*/

/* Sidebar */
.s-sidebar__trigger {
   /*z-index: 2;*/
   position: fixed;
   top: 65px;
   left: 0;
   width: 25%;
   height: 100em;
   background: #fff;
   padding-top: 6em;
}

.s-sidebar__trigger > i {
   
   margin: 0.1em 0 0 1em;
   color: #004167;
}

.s-sidebar__nav {
   position: fixed;
   top: 20;
   left: -15em;
   overflow: hidden;
   transition: all .3s ease-in;
   width: 15em;
   height: 100%;
   background: #fff;
   /*color: rgba(255, 255, 255, 0.7);*/
}

.active{
   /*position: fixed;
   top: 20;
   left: -15em;*/
   overflow: hidden;
   transition: all .3s ease-in;
   width: 15em;
   height: 100%;
   /*background: #243e56;*/
   /*color: rgba(255, 255, 255, 0.7);*/
}

.s-sidebar__nav:hover,
.s-sidebar__nav:focus,
.s-sidebar__trigger:focus + .s-sidebar__nav,
.s-sidebar__trigger:hover + .s-sidebar__nav {
   left: 0;
}

/* Mobile First */
@media (min-width: 42em) {
   .s-layout__content {
      margin-left: 4em;
   }
   
   /* Sidebar */
   .s-sidebar__trigger {
      width: 4em;
    padding-top: 6em;
   }
   
   .s-sidebar__nav {
      width: 4em;
      left: 0;
   }

   .s-layout__sidebar{
    padding-top: 3em;
   }
   
   /*.s-sidebar__nav:hover,
   .s-sidebar__nav:focus,
   .s-sidebar__trigger:hover + .s-sidebar__nav,
   .s-sidebar__trigger:focus + .s-sidebar__nav {
      width: 15em;
   }*/
}



@media (min-width: 68em) {
   .s-layout__content {
      margin-left: 15em;
   }
   
   /* Sidebar */
   .s-sidebar__trigger {
      display: none;
   }
   
   .s-sidebar__nav {
          width: 20.6em;
   }
   
   .s-sidebar__nav ul {
      top: 1.3em;
   }
}
#btnclass
{
  border: 1px solid transparent;
  outline:none; 
  width:50%;
  height: 40px;
  color:#fff;
  font-size: 16px;
  background: #004167;
  cursor: pointer;
  border-radius: 10px;
  text-decoration: none;
}
#navclass > a
{
   text-decoration: none;
   outline:none;
}
#navclass > a:active
{
  color:#13d70f;
}
</style>

<script>  
$( document ).ready(function() {
 $('#showopportunity').DataTable({
 
        });   
});
</script>

<script>
    $(document).ready(function(){
      $('#example').dataTables();
    });
</script>
  
<script type="text/javascript">
   $(function(){
      $("#btnclick").click(function(){
        $("divpopup").dialog({
          title : " Pop up",
          width : 200,
          height : 100,
          modal:true,
          buttons: {Close;
                function(){
                  $(this).dialog('close');
                }
            }
        });
      });
   })
</script>

<script type="text/javascript">
$(function(){
    $("#gotologin").click(function(){
     
    })
  function gotologin(){alert("Are You Sure To login!!!!")};
</script>

<!--for  nav active-->

<script>
  $(document).ready(function(){
        $("#focus").hover(function(){
        $(this).css("color","#85EC14")
      });
  }); 
</script>

 <!-- <script> 
  $(document).ready(function(){
     
        $("#btnbtn1").click(function(){
          $("#opp").animate({
              height: 'toggle'
          });
      });

           $("#btnbtn2").click(function(){
          $("#skill").animate({
              height: 'toggle'
          });
      });

        
  });
  </script>
 -->

<style>

body
{
  font-family:awesome;
}
.dropdown >.dropdown-menu> div >a
{
   text-decoration: none;
   outline:none;

}
.container-fluid{
  top: 0px;
  position: fixed;
  width: 100%;
  font-family:awesome;
}
/* The Modal (background) */
.modal1 {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal1-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal1-header {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}

.modal1-body {padding: 2px 16px;}

.modal1-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}

/*=============ANCHOR LINK STYLE==========*/
.row .nav a{
      text-decoration: none;
      font-size:20px;
      /*margin: 20px;*/
 } 

#whathere a:hover,#otherstuff a:hover{
      color:  #4a4a4a;
      /*text-decoration: none;*/
}

#whathere a ,#otherstuff a{
     color: #CCC;}

/*.row .nav a:visited{color: #85EC14;}*/

/*.row a:focus{color: #85EC14;
font-style: italic;
background-color: red;}*/

/*a:focus{
  font-style: italic;
  color: #85EC14;
  background-color: red;
}
*/
/*.aa a:hover,
.row .aa a:focus{
     color: #85EC14;
}*/
.row > .aa > a:hover,
.row > .aa > a:focus ,
.row > .aa > a:active {
  color: #85EC14;
  text-decoration: none;
  /*cursor: not-allowed;*/
}

.row .left a
{
  /*height: 800px;*/
  /*width: 20%;*/
  /*float: left;*/
}
.link:hover{color: #85EC14;}
/*=============END of ANCHOR LINK STYLE==========*/
.font{font-style:;}

/*=================FOOTER ====================*/

/*.page_footer{position: relative; 
             }*/

.page_footer .a a{
  font-weight: lighter;
  text-decoration: none;
  outline: none;
}

.right_title{
  position: fixed;
}
.a
{
  /*margin-right: 40px;*/

}
.page_footer{position:;
              bottom:0;

}

.pg_header{
  position: relative;
  top: 0;
  /*width: 100%;*/
}


.login
{
  background: url(images/myeo.jpg);
  background-size: 100%;
  background-origin: 0;
  max-height: 200px;
}

.container-fluid {
  position: fixed;
  top: 0;
  width: 100%;
  font-family:awesome;
}

.form-inline
{
  padding-right: 20px;
}


.row .right
{
    width: 78%;
    float: right;
    background-color:#F0f0f0;
}

.right
{
    width: 78%;
    float: right;
    background-color:#F0f0f0;
}



.input-placeholder {
        position: relative;
      }
      .input-placeholder input {
        padding: 2px;

      }
      .input-placeholder input:valid + .placeholder {
        display: none;
      }
      .placeholder {
        position: absolute;
        pointer-events: none;
        top: 2px;
        bottom: 2px;
        left: 6px;
        margin: auto;
        color: #ccc;
      }
      .placeholder span {
        color: red;
      }
      .oppo label{
        font-weight: lighter;
      }

      .section-cards .card a {
  opacity: .93;
}


/*.card p {
  min-height: 0;
}*/

/*.card {
  background: #fff;
  /*box-shadow: 0 32px 44px -24px rgba(0, 0, 0, 0.23), 0 20px 25px 0px rgba(0, 0, 0, 0.12), 0 15px 10px -10px rgba(0, 0, 0, 0.2);*/
  border-radius: 6px;
  margin-bottom: 30px;
  -webkit-transition: all 370ms cubic-bezier(0.34, 1.61, 0.7, 1);
  -moz-transition: all 370ms cubic-bezier(0.34, 1.61, 0.7, 1);
  -o-transition: all 370ms cubic-bezier(0.34, 1.61, 0.7, 1);
  -ms-transition: all 370ms cubic-bezier(0.34, 1.61, 0.7, 1);
  transition: all 370ms cubic-bezier(0.34, 1.61, 0.7, 1);
}*/
/*.card:not(.card-plain):not(.card-price):hover {
  -webkit-transform: scale(1.02);
  -moz-transform: scale(1.2);
  -o-transform: scale(1.05);
  -ms-transform: scale(1.03);
  transform: scale(1.3);
  box-shadow: 0 26px 50px -10px rgba(0, 0, 0, 0.38), 0 10px 20px 0px rgba(0, 0, 0, 0.2), 0 15px 10px -10px rgba(0, 0, 0, 0.2);
}*/

.aa{color: #000;}

/*Checkbox hide and chkbox color change by HTZN*/
.checkbox {
    /*margin: 50px auto;*/
    position: relative;
    display: block;
    width: 200px;
}

.chk{
    width: auto;
    opacity: 0.00000001;
    position: absolute;
    left: 0;
    margin-left: -20px;
}
#chk{
    width: auto;
    opacity: 0.00000001;
    position: absolute;
    left: 0;
    margin-left: -20px;
}
.helper {
    position: absolute;
    top: 2px;  /* box distance*/
    left: -3px;
    cursor: pointer;
   }
.helper:before {
    content: '';
    position: absolute;
    /*left: 0;*/
    /*top: 0;*/
    margin: 4px;
    width: 15px;
    height: 15px;
    /*transition: transform 0.28s ease;*/
    /*border-radius: 3px;*/
    border: 1px solid #9b9b9b;
}
.helper:after {
  content: '';
    display: block;
    width: 10px;
    height: 5px;
    border-bottom: 3px solid #7bbe72;
    border-left: 2px solid #7bbe72;
    -webkit-transform: rotate(-45deg) scale(0);
    -moz-transform: rotate(-45deg) scale(0);
    -ms-transform: rotate(-45deg) scale(0);
    transform: rotate(-45deg) scale(0);
    position: absolute;
    top: 10px;
    left: 6px;
}
.chk:checked ~ .helper::before {
    color: #7bbe72;
}

.chk:checked ~ .helper::after {
    -webkit-transform: rotate(-45deg) scale(1);
    -moz-transform: rotate(-45deg) scale(1);
    -ms-transform: rotate(-45deg) scale(1);
    transform: rotate(-45deg) scale(1);
}
#chk:checked ~ .helper::before {
    color: #7bbe72;
}

#chk:checked ~ .helper::after {
    -webkit-transform: rotate(-45deg) scale(1);
    -moz-transform: rotate(-45deg) scale(1);
    -ms-transform: rotate(-45deg) scale(1);
    transform: rotate(-45deg) scale(1);
}

.checkbox label {
    min-height: 2px;
    /*padding-left: 3px;*/
    margin-bottom: 0;
    /*font-weight: normal;*/
    /*cursor: pointer;*/
    vertical-align: sub;
}
/*input[type="checkbox"]:focus + label::before {*/
   /* outline: #000 auto 5px;
}*/


/*imagr Preview*/

#thumbnail img{
  width:150px;
  height:120px;
  margin:5px;
}
canvas{
  border:1px solid red;
}

</style>

<!--Opp skill drowdown By HTZN-->
<script type="text/javascript">

    function closeall() {
    var divs = document.getElementsByTagName('')
    var elements = divs.length
    for(var i = 0;i < elements;i++){
       var divStyle = divs.item(i)
       divStyle.style.display = 'none';
    }
    return;
}
function showOne(showme) {
    closeall()
    var element = document.getElementById(showme)
    element.style.display = 'block'
    return;
}
</script>



<!-- <script type="text/javascript">

    function closeOne() {
    var divs = document.getElementsByTagName('dd')
    var elements = divs.length
    for(var i = 0;i < elements;i++){
       var divStyle = divs.item(i)
       divStyle.style.display = 'none';
    }
    return;
}
function showadmin(show) {
    closeOne()
    var element = document.getElementById(show)
    element.style.display = 'block'
    return;
}
</script> -->


<!-- <script>
#home.onclick = function() {
  this.style.fontSize = '36px';
  this.style.color = 'red';
};
</script> -->

  <!-- <script> 
  $(document).ready(function(){
     
        $("#btnbtn1").click(function(){
          $("#opp").animate({
              height: 'toggle'
          });
      });

           $("#btnbtn2").click(function(){
          $("#skill").animate({
              height: 'toggle'
          });
      });

          
  });
  </script>
 -->

<!-- Profile Img browse hide by HTZN-->
<script language="JavaScript" type="text/javascript"> 

   function HandleBrowseClick() { 
   var fileinput = document.getElementById("browse");
   fileinput.click();
  }

   function Handlechange() {
    var fileinput = document.getElementById("browse");
    var textinput = document.getElementById("upload");
    textinput.value = fileinput.value; }
</script>
<!-- Profile CV browse hide by HTZN-->
<script language="JavaScript" type="text/javascript"> 

   function HandleBrowseClick1() { 
   var fileinput = document.getElementById("browse1");
   fileinput.click();
  }

   function Handlechange1() {
    var fileinput = document.getElementById("browse1");
    var textinput = document.getElementById("upload");
    textinput.value = fileinput.value; }
</script>
<!-- Profile Letter browse hide by HTZN-->
<script language="JavaScript" type="text/javascript"> 

   function HandleBrowseClick2() { 
   var fileinput = document.getElementById("browse2");
   fileinput.click();
  }

   function Handlechange2() {
    var fileinput = document.getElementById("browse2");
    var textinput = document.getElementById("upload");
    textinput.value = fileinput.value; }
</script>
<!-- Profile Video browse hide by HTZN-->
<script language="JavaScript" type="text/javascript"> 

   function HandleBrowseClick3() { 
   var fileinput = document.getElementById("browse3");
   fileinput.click();
  }

   function Handlechange3() {
    var fileinput = document.getElementById("browse3");
    var textinput = document.getElementById("upload");
    textinput.value = fileinput.value; }
</script>


<script type="text/javascript">
$('.palceholder').click(function() {
  $(this).siblings('input').focus();
});
$('.form-control').focus(function() {
  $(this).siblings('.palceholder').hide();
});
$('.form-control').blur(function() {
  var $this = $(this);
  if ($this.val().length == 0)
    $(this).siblings('.palceholder').show();
});
$('.form-control').blur();
</script> 
<!-- ==========Tag Input ============ -->


</head>
<body onload="closeall()">
<!--HTZN-->