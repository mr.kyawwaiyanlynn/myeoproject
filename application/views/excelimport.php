
<br><br>r><br><br>
<?php if($this->session->flashdata('message')){?>
          <div align="center" class="alert alert-success">      
            <?php echo $this->session->flashdata('message')?>
          </div>
        <?php } ?>

<br><br>

<div align="center">
  
<form action="<?php echo base_url(); ?>uploadcsv/import" 
method="post" name="upload_excel" enctype="multipart/form-data" class="form-inline">

<div class="row">

<input type="file" name="file" id="file" class="col-md-offset-4 col-md-3">
<div class="col-md-2">
<button type="submit" id="submit" name="import" class="form-control btn btn-primary">Import</button></div></div>
</form>
<br>
<br>
<div style="font-size:20px;">Your Posts</div>
<br><br>

<div style="width:80%;" align="center">
<table id="t01" class="table table-bordered table-striped">
  <tr class="info">
    <th>NO</th>
    <th>TITLE</th>
    <th>SUBTITLE</th>
    <th>DESCRIPTION</th>
  </tr>
<?php
if(isset($view_data) && is_array($view_data) && count($view_data)): $i=1;
foreach ($view_data as $key => $data) { 
?>
  <tr>
    <td><?php echo $i; ?></td>
    <td><?php echo $data['title'] ?></td>
    <td><?php echo $data['sub_title'] ?></td>
    <td><?php echo $data['description'] ?></td>
  </tr>
  <?php $i++; } endif; ?>
</table>
</div>
</div>
<br><br>
