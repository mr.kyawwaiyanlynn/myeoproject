<div class="col-md-8 col-md-offset-2 form-group" style="background-color:#fff;padding-top:55px;;padding-bottom:40px;"> 
   <center> <h1> Edit Your Post!!! </h1></center>
    
    <?php if($post != null) {?>
    <?php 
        foreach($post as $row) {
            $vdlink=$post->vdlink;
            $image=$post->image;
            $title=$post->title;
            // $subtitle=$post->sub_title;
            $description=$post->description;
            $choiceid=$post->id;
            $deadline=$post->deadline;
        }
    ?>
    <?php 
        $oppid = $this->uri->segment(3);
    ?>      
    <?php 
       $encrypt_ids=$this->encrypt->encode($choiceid);
       $modify_encrypt=str_replace("/","~",$encrypt_ids);
   ?>
    <?php $attribute=array('class'=>'form-horizontal','method'=>'post','id'=>'myForm');?>
    <?php echo form_open_multipart('admin/edit_draft/'.$modify_encrypt, $attribute);?> 
    <br/>
    <div class="col-md-10 col-md-offset-1">
        
        <div class="form-group">
            <label>TITLE: </label></br>
            <input type="text" name="title" class="form-control" value="<?=$post->title;?>"> 
        </div>
      
        <div class="form-group">
            <label>TAG:</label></br>
            <?php  if($post1 != Null )
                              {?>
                                     
                                      
                      <input type="text" name="tags" class="tokenfield-typeahead form-control" value="<?php foreach ($post1 as $key) { echo $key.","; }?>">

                 <?php } else {?>
             
                  <input type="text" name="tags" class="tokenfield-typeahead form-control" >
         <?php   }?>
        </div>
        <script type="text/javascript">
        var engine = new Bloodhound({
            local: [],
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });
    </script>


    <script type="text/javascript">

        engine.initialize();

        $('.tokenfield-typeahead').tokenfield({
            typeahead: [null, {source: engine.ttAdapter()}]
        });

        // function hellowaiyan() {
        //     // alert("here");
        //     alert((document.getElementById('tokenfield-typeahead')).value);
        // }


//

    </script>
        <div class="form-group">
            <label>Apply Link:</label></br>
            <input type="text" name="apply" class="form-control" value="<?=$post->apply_link;?>">
        </div>
        <div class="form-group">
            <label>Official Link</label></br>
            <input type="text" name="official" class="form-control" value="<?=$post->official_link;?>">
        </div>

        <div class="form-group">
          <label>Deadline :</label>&nbsp;&nbsp;

          <?php if($post->deadline!="As soon as possible" && $post->deadline!="Ongoing" && $post->deadline!="You can join anytime"){?>
          <input type="radio" name="date" value="1" id="deadline_date" <?php echo 'checked'?> onclick="showdeadline('date1')">&nbsp;<label>Date</label>
          <input type="radio" name="date" value="2" id="deadline_select" onclick="showdeadline('date2')">&nbsp;<label>Select</label>
          <?php }else{?>
            <input type="radio" name="date" value="1" id="deadline_date" onclick="showdeadline('date1')">&nbsp;<label>Date</label>
          <input type="radio" name="date" value="2" id="deadline_select" <?php echo 'checked'?> onclick="showdeadline('date2')">&nbsp;<label>Select</label>
          <?php }?>
        </div>
       <?php if($post->deadline!="As soon as possible" && $post->deadline!="Ongoing" && $post->deadline!="You can join anytime"){?>
        <div class="row">
          
          <div class="form-group" id="date1">
            <div class="col-md-5">
            <input type="date" class="form-control" id="date" name="deadline" value="<?=$post->deadline?>"> 
             <input type="hidden" name="deadline1" value="<?=$post->deadline?>">      
          </div>
          </div>

        </div>
 
        <div class="row">
          <ul class="form-group" id="date2" style="display: none;">
                          <div class="col-md-6 ">
                    <select name="deadline2" class="chosen-select form-control" >
                              
                              <option value="As soon as possible">As soon as possible</option>
                              <option value="Ongoing">Ongoing</option>
                              <option value="Ongoing">Open</option>
                              <option value="You can join anytime">You can join anytime</option>

          
                    </select>
                    
                </div>
          </ul>
        </div>
  <?php }else{ ?>
        <div class="row">
          
          <ul class="form-group" id="date1" style="display: none;">
            <div class="col-md-5">
            <input type="date" class="form-control" id="date" name="deadline" value="<?=$post->deadline?>"> 
             <input type="hidden" name="deadline1" value="<?=$post->deadline?>">      
          </div>
          </ul>

        </div>
 
        <div class="row">
          <div class="form-group" id="date2">
                <div class="col-md-5 col-md-offset-1">
                   
                      <select name="deadline2" class="chosen-select form-control" >
                              <?php $array=array("As soon as possible","Ongoing","You can join anytime");?>
                              <?php for ($i=0;$i<count($array);$i++) { 
                                  if ($array[$i] == $post->deadline){?>

                                      <option value="<?=$array[$i]?>" selected><?=$array[$i]?></option>
                                  <?php }else{?>
                                  
                                     <option value="<?=$array[$i]?>" ><?=$array[$i]?></option>
                                  <?php }}?>                              
                             
                    </select>
                  
                </div>
          </div>
        </div>
  <?php }?>

         <div class="form-group">
                <label>Location:</label>
                <input type="text" name="c_location" placeholder="Enter the location" class="form-control" value="<?=$post->c_location?>">
              </div>

         <div class="form-group">
                <label>Time:</label>
                <input type="text" name="time" placeholder="Enter the Time" class="form-control" value="<?=$post->time?>">
              </div>


       
        <div class="form-group">
            <label>DESCRIPTION:</label></br>
            <!-- <textarea name="description" rows="10" class="form-control" ><?=$description;?></textarea> -->
            <?php echo $this->ckeditor->editor('description',"$post->description")?>
        </div>
                  
<?php if($post->skill==0){?>

             <div class="form-group">
              <label class="col-md-5 control-label">CHOOSE TYPE: </label> 
              <div class="col-md-5">
               <!--  <form id="myForm">  -->   
                   <input type="radio" name="choice" value=1 id="oppchoice" <?php echo "checked" ?>> 
                  <label>Opportunity</label>
                  <input type="radio" name="choice" value=2 id="skillchoice" > <label>Skill</label>
             <!--  </form> -->
              </div>
          </div> 


         <div class="form-group" id="opp">      
            <label class="col-md-5  control-label">CHOOSE SUITABLE OPPORTUNITY:</label>
            <div class="col-md-7">
                  <label class=" col-md-7">Scholarship</label>
                  <div class="col-md-12">

                        <?php $count = 1;foreach ($all_opp as $row) {;?>
                        <?php if($row->opp_type=="Scholarship"){ ?>
                        <font style="color:">
                        <div class="col-md-6">
                          <div class="row col-md-9">

                            <div class="checkbox">
                              <label>
                               
                                <input type="checkbox" id="<?=$row->opp_id?>"   name="oppcheck[]" value="<?=$row->opp_id?>">
                                 
                                <font style="font-size:12px"><?=$row->type?></font>
                              </label>
                            </div>
                          </div>
                        </div>
                        </font>
                      <?php } ?>
                         <?php $count++;};?>
                         </div>
                  

                             
                  <label class=" col-md-7">Conference and Training </label>
                  <div class="col-md-12">

                        <?php $count = 1;foreach ($all_opp as $row) {;?>
                        <?php if($row->opp_type=="C & T"){ ?>
                        <div class="col-md-6">
                          <div class="row col-md-9">

                            <div class="checkbox">        
                              <label>
                                
                                <input type="checkbox" id="<?=$row->opp_id?>"   name="oppcheck[]" value="<?=$row->opp_id?>">
                                
                                <font style="font-size:12px"><?=$row->type?></font>
                              </label>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                         <?php $count++;};?>
                         </div>
                    
                  <label class=" col-md-7">Other Opportunities </label>
                  <div class="col-md-12">

                        <?php $count = 1;foreach ($all_opp as $row) {;?>
                        <?php if($row->opp_type=="Other"){ ?>
                        <font style="color:">
                        <div class="col-md-6">
                          <div class="row col-md-9">

                            <div class="checkbox">
                              <label>
                               
                                <input type="checkbox" id="<?=$row->opp_id?>"   name="oppcheck[]" value="<?=$row->opp_id?>">
                                 
                                <font style="font-size:12px"><?=$row->type?></font>
                              </label>
                            </div>
                          </div>
                        </div>
                        </font>
                      <?php } ?>
                         <?php $count++;};?>
                         </div>

               
            <div class="col-md-12">   
         <label>Accepted Location</label>
         <div class="col-md-12">
           <?php 
          
           $data=$location[0]['location'];
           $datas=explode(',',$data);
           $count=count($datas);
           if($data==" ")
           { ?>
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local">
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International">
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php }  else {?>            <!-- No location -->

         

         <?php if($count==1)
           {  
            if($datas[0]=="Local")
              { ?>
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local" checked>
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International">
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php } ?>

         <?php if($datas[0]=="International")
              { ?>
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local" >
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International" checked>
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php } ?>

          <?php } ?>            <!-- Choose one location -->


        <?php if($count==2)
           {  ?>
            
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local" checked>
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International" checked>
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php } ?>            <!-- Choose two location -->



          <?php } ?>       <!-- end of not null -->



        
               
          
                
          </div>
      </div>
    </div>
 </div>
      

        <ul class="form-group" id="skill" style="display: none;">      
            <label class="col-md-5  control-label">CHOOSE SUITABLE Skill:</label>
            <div class="col-md-7">
                 
             <?php $count=1; foreach ($all_skill as $row) {;?>
        
              <div class="col-md-5 col-sm-5 col-xs-5">

                <div class="row col-md-9 col-xs-9 col-sm-9">
                  <div class="checkbox">
                    <label>
                       <input type="checkbox" class="<?= $row->skill_id?>" name="skillcheck[]" value="<?=$row->skill_id?>">
                       <font style="font-size:13px"><?=$row->type?></font>
                    </label>
                  </div>

               </div>
               </div>
               <?php $count++; };?> 
         
                  </div>
              </ul>  
          </div>
      

       <?php }else{?>

        <div class="form-group">
            <label class="col-md-5 control-label">CHOOSE TYPE: </label>
            <div class="col-md-5">
                
                <input type="radio" name="choice" value=1 id="oppchoice"> <label > OPPORTUNITY </label> 
                <input type="radio" name="choice" value=2 id="skillchoice" <?php echo 'checked'?> > <label >SKILL</label>
                
            </div>
        </div>

         <ul class="form-group" id="opp" style="display: none;">
            <label class="col-md-5  control-label">CHOOSE SUTIBLE OPPORTUNITY:</label>
            <div class="col-md-7">
              
                 <label class=" col-md-7">Scholarship</label>
                  <div class="col-md-12">

                        <?php $count = 1;foreach ($all_opp as $row) {;?>
                        <?php if($row->opp_type=="Scholarship"){ ?>
                        <font style="color:">
                        <div class="col-md-6">
                          <div class="row col-md-9">

                            <div class="checkbox">
                              <label>
                               
                                <input type="checkbox" id="<?=$row->opp_id?>"   name="oppcheck[]" value="<?=$row->opp_id?>">
                                 
                                <font style="font-size:12px"><?=$row->type?></font>
                              </label>
                            </div>
                          </div>
                        </div>
                        </font>
                      <?php } ?>
                         <?php $count++;};?>
                         </div>
                  

                             
                  <label class=" col-md-7">Conference and Training </label>
                  <div class="col-md-12">

                        <?php $count = 1;foreach ($all_opp as $row) {;?>
                        <?php if($row->opp_type=="C & T"){ ?>
                        <div class="col-md-6">
                          <div class="row col-md-9">

                            <div class="checkbox">        
                              <label>
                                
                                <input type="checkbox" id="<?=$row->opp_id?>"   name="oppcheck[]" value="<?=$row->opp_id?>">
                                
                                <font style="font-size:12px"><?=$row->type?></font>
                              </label>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                         <?php $count++;};?>
                         </div>
                    
                  <label class=" col-md-7">Other Opportunities </label>
                  <div class="col-md-12">

                        <?php $count = 1;foreach ($all_opp as $row) {;?>
                        <?php if($row->opp_type=="Other"){ ?>
                        <font style="color:">
                        <div class="col-md-6">
                          <div class="row col-md-9">

                            <div class="checkbox">
                              <label>
                               
                                <input type="checkbox" id="<?=$row->opp_id?>"   name="oppcheck[]" value="<?=$row->opp_id?>">
                                 
                                <font style="font-size:12px"><?=$row->type?></font>
                              </label>
                            </div>
                          </div>
                        </div>
                        </font>
                      <?php } ?>
                         <?php $count++;};?>
                         </div>

                          <div class="col-md-12">   
         <label>Accepted Location</label>
         <div class="col-md-12">
           <?php 
          
           $data=$location[0]['location'];
           $datas=explode(',',$data);
           $count=count($datas);
           if($data==" ")
           { ?>
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local">
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International">
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php }  else {?>            <!-- No location -->

         

         <?php if($count==1)
           {  
            if($datas[0]=="Local")
              { ?>
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local" checked>
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International">
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php } ?>

         <?php if($datas[0]=="International")
              { ?>
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local" >
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International" checked>
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php } ?>

          <?php } ?>            <!-- Choose one location -->


        <?php if($count==2)
           {  ?>
            
               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                          <label>
                             <input type="checkbox" name="location[]" value="Local" checked>
                             <font style="font-size:13px">Local</font>
                          </label>
                       </div>
                   </div>
               </div>

               <div class="col-md-6">
                   <div class="row col-md-9">
                       <div class="checkbox">
                           <label>
                               <input type="checkbox" name="location[]" value="International" checked>
                               <font style="font-size:12px">International</font>
                          </label>
                       </div>
                    </div>         
               </div>

          <?php } ?>            <!-- Choose two location -->

          <?php } ?>       <!-- end of not null -->
                
          </div>

            </div>
         </ul>

        <div class="form-group" id="skill">
            <label class="col-md-5  control-label">CHOOSE SUTIBLE SKILL:</label>
            <div class="col-md-7">
              
             <?php $count=1; foreach ($all_skill as $row)  {;?>
               <div class="col-md-5 col-sm-5 col-xs-5">

                <div class="row col-md-9 col-xs-9 col-sm-9">
                  <div class="checkbox">
                    <label>
                       <input type="checkbox" class="<?= $row->skill_id?>" name="skillcheck[]" value="<?=$row->skill_id?>">
                       <font style="font-size:13px"><?=$row->type?></font>
                    </label>
                  </div> 

               </div>
               </div>
               <?php $count++; };?> 
            </div>
        </div>
        <?php }?>
<div class="row"></div>

        <?php if($post->image ==NULL){?>

        <div class="form-group">
            <label class="col-md-5 control-label"> CHOOSE TO UPLOAD PICTURE or VIDEO</label>
            <div class="col-md-5">
                <input type="radio" name="uploadchoice" value=1 id="imagechoice" onclick="showadmin('image')">
                <label>Image</label>
                <input type="radio" name="uploadchoice" value=2 id="videochoice" <?php echo 'checked'?> >
                <label>Video</label>
            </div>
        </div>

        <div class="form-group">
            <ul class="form-group" id="image" style="display: none;">
                <div class="col-md-offset-5">   
                    <div class=" fileinput">
                        <!-- <input type="file" name="imageuploadchoice"  accept="image/*" onchange="showMyImage(this)" value="<?=$image;?>"> -->
                        <input type="hidden" name="imageupload" value="<?=$image;?>">
                         <input type="file" accept="image/*"  onchange="showMyImage(this)" class="fileinput" name="imageuploadchoice" id="inputFile">
                          <img id="limage" style="width:40%; margin-top:10px;"  src=""/ name="imageuploadchoice">
                        <!-- <img src="" style="width:60%; margin-top:10px;"> -->
                    </div>
                </div>
            </ul>
        </div>

        <div class="form-group">
            <div class="form-group" id="video">
                <label class="col-md-5 control-label">Fill Video Link : </label>
                <div class="col-md-5">
                    <input type="text" name="videouploadchoice" placeholder="*paste_video_link" class="form-control" value="<?=$post->vdlink;?>">
                     <input type="hidden" name="videoupload" value="<?=$vdlink;?>">
                </div>
            </div>
        </div>
<?php } else{ ?>
         <div class="form-group">
            <label class="col-md-5 control-label"> CHOOSE TO UPLOAD PICTURE or VIDEO</label>
            <div class="col-md-5">
                <input type="radio" name="uploadchoice" value=1 id="imagechoice"  <?php echo 'checked'?> onclick="showadmin('image')">
                <label>Image</label>
                <input type="radio" name="uploadchoice" value=2 id="videochoice" onclick="showadmin('video')">
                <label>Video</label>
            </div>
        </div>

        <div class="form-group">
            <div class="form-group" id="image">
                <div class="col-md-offset-5">   
                    <div class=" fileinput">
                        <input type="file" name="imageuploadchoice"  accept="image/*" onchange="showMyImage(this)" value="<?=$image;?>">
                        <input type="hidden" name="imageupload" value="<?=$image;?>">
                      <img id="limage" style="width:40%; margin-top:10px;"  src="<?php echo base_url(); ?>uploads/<?=$post->image?>"/ name="imageuploadchoice">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <ul class="form-group" id="video" style="display: none;">
                <label class="col-md-5 control-label">Fill Vedio Link : </label>
                <div class="col-md-5">
                    <input type="text" name="videouploadchoice" placeholder="*paste_video_link" class="form-control">
                     <input type="hidden" name="videoupload" value="<?=$vdlink;?>">
                </div>
            </ul>
        </div>
        <?php } ?>
        <div class="col-md-offset-10">
        <button type="submit" value="Save" class="form-group btn" style="background-color:#004167;color: #fff" >Save</button>
        <br/> 
        </div>
    </div> <!-- END OF SECOND DIV -->
</form> 
    <?php }else{ echo "<h4 style='color:red'>There is no record!!</h4>";}?>
</div> <!-- END OF FIRST DIV -->

<script>
$(document).ready(function(){
    $("#oppchoice").click(function(){
        $("#skill").hide();
        $("#opp").show();
    });
    $("#skillchoice").click(function(){
        $("#skill").show();
        $("#opp").hide();
    });
});
</script>

<script>
$(document).ready(function(){
    $("#imagechoice").click(function(){
        $("#video").hide();
        $("#image").show();
    });
    $("#videochoice").click(function(){
        $("#video").show();
        $("#image").hide();
    });
});
</script> 

<script>
$(document).ready(function(){
    $("#deadline_date").click(function(){
        $("#date2").hide();
        $("#date1").show();
    });
    $("#deadline_select").click(function(){
        $("#date2").show();
        $("#date1").hide();
    });
});
</script>  
<script type="text/javascript">


                      var ex;
                      <?php foreach ($opp as $row ){ ?>

                            ex  = <?php echo json_encode($row->opp_id); ?> ;
                           // alert(ex);

                            if(ex == 1)
                            {
                              
                               $( "#1").prop('checked', true);

                            }
                             if(ex == 2)
                            {
                              
                               $( "#2").prop('checked', true);

                            }
                             if(ex == 3)
                            {
                              
                               $( "#3").prop('checked', true);

                            }
                             if(ex == 4)
                            {
                              
                               $( "#4").prop('checked', true);

                            }
                             if(ex == 5)
                            {
                              
                               $( "#5").prop('checked', true);

                            }
                             if(ex == 6)
                            {
                              
                               $( "#6").prop('checked', true);

                            }
                             if(ex == 7)
                            {
                              
                               $( "#7").prop('checked', true);

                            }
                             if(ex == 8)
                            {
                              
                               $( "#8").prop('checked', true);

                            }
                             if(ex == 9)
                            {
                              
                               $( "#9").prop('checked', true);

                            }
                             if(ex == 10)
                            {
                              
                               $( "#10").prop('checked', true);

                            }
                             if(ex == 11)
                            {
                              
                               $( "#11").prop('checked', true);

                            }
                             if(ex == 12)
                            {
                              
                               $( "#12").prop('checked', true);

                            }
                      <?php } ?>                
            </script>


<script type="text/javascript">


            var ex1;
            <?php foreach ($skill as $skill ) { ?>

                  ex1  = <?php echo json_encode($skill->skill_id); ?> ;
                 // alert(ex);

                  if(ex1 == 1)
                  {
                    
                     $( ".1").prop('checked', true);

                  }
                   if(ex1 == 2)
                  {
                    
                     $( ".2").prop('checked', true);

                  }
                   if(ex1 == 3)
                  {
                    
                     $( ".3").prop('checked', true);

                  }
                   if(ex1 == 4)
                  {
                    
                     $( ".4").prop('checked', true);

                  }
                   if(ex1 == 5)
                  {
                    
                     $( ".5").prop('checked', true);

                  }
                   if(ex1 == 6)
                  {
                    
                     $( ".6").prop('checked', true);

                  }
                   if(ex1 == 7)
                  {
                    
                     $( ".7").prop('checked', true);

                  }
                   if(ex1 == 8)
                  {
                    
                     $( ".8").prop('checked', true);

                  }
                   if(ex1 == 9)
                  {
                    
                     $( ".9").prop('checked', true);

                  }
                   if(ex1 == 10)
                  {
                    
                     $( ".10").prop('checked', true);

                  }                 
            <?php } ?>               
  </script>

  <script type="text/javascript">


                      var ex;
                      <?php foreach ($location as $row ){ ?>

                            ex  = <?php echo json_encode($row->location); ?> ;
                           // alert(ex);
<?php  var_dump($location);?>
                            if(ex == Local)
                            {
                              
                               $( "#aa").prop('checked', true);

                            }
                             if(ex == International)
                            {
                              
                               $( "#bb").prop('checked', true);

                            }
                                                     
                      <?php } ?>

                </script>

                <script>
function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("limage");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }
    </script>