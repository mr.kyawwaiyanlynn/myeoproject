
<!-- ===========BODY============ -->
<br><br><br>
<center><h2> Feature List </h2></center>
<center>
    <div class="container table-responsive" style="margin-top: 30px;">
        
        <table class="table table-bordered table-striped" id="showopportunity">
            <thead>
                <tr class="info">
                    <th>NO.</th>
                    <th>TITLE</th>
                    <th>Feature Image</th>
                    <!-- <th>Opportunity Title</th>
                    <th>Skill Title</th> -->
                    <th>Apply link</th>
                    <th>Operation</th>

                </tr>
            </thead>
            <tbody>
                <?php $count=1; foreach ($post as $row) {;?>
                <tr>                    
                    <td><?php echo $count;?></td>
                    <td><?=$row['title'];?></td>
                    
                    <td><center><img src="<?=base_url()?>uploads/<?=$row['image'];?>" style="width:80px;height:60px;"></center></td>
                    <!-- <td><?=$row['skill']?></td> -->
                    <td><?=$row['apply_link']?></td>

                      <!-- for encryption -->
                       <?php                        
                           $encrypt_ids=$this->encrypt->encode($row['id']);
                           $modify_encrypt=str_replace("/","~",$encrypt_ids);
                       ?>
                    <td>
                       <center> <a href="<?=base_url()?>Admin/feature_edit/<?=$modify_encrypt;?>" 
                        class="btn btn-warning " name="edit"><span class="glyphicon glyphicon-pencil"></span></a> 
                        <!-- <a href="<?=base_url()?>Admin/post_delete/<?=$modify_encrypt;?>"
                        class="btn btn-danger " name="delete" 
                        onclick="return confirm('Are you sure, do you want to delete post ?');"><span class="glyphicon glyphicon-trash"></span> </a> -->
                        <a href="<?=base_url()?>Admin/remove_feature/<?=$modify_encrypt;?>" 
                        class="btn btn-danger " name="remove"><span class="glyphicon glyphicon-cross"></span>Remove</a> </center>
                    </td>
                </tr>  
                <?php $count++; };?>

            </tbody>
        </table>
    </div>
</center><br><Br><br><br>

<script type="text/javascript">
    if(document).ready(function(){
        $('#showopportunity').DataTable();
    });
</script>
