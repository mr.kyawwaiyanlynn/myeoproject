<!-- <nav class="navbar navbar-default col-md-12" role="navigation" style="background-color: #004167"> -->


<div class="container-fluid  navbar-default " role="navigation" style="background-color:#004167;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-2">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>          
        <span class="icon-bar"></span>
      </button>

          <label class="col-md-3">
              <img src="<?=base_url();?>/images/myeo_logo.png" width="71" height="63" class="col-md-offset-1"/>
          </label>   
    </div>
        <div class="col-md-2"></div>
    <span class="label_tag"> 

    <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ol class="nav navbar-nav col-md-6 " style="padding-top: 20px">
          <a href="<?=base_url();?>Admin"><font style="color:#fff">Upload Contents</font></a> &nbsp;&nbsp;<font style="color:#13d70f;">|</font>&nbsp;&nbsp;
          <a href="<?=base_url();?>admin/getpost"><font style="color:#fff">Post list</font></a> &nbsp;&nbsp;<font style="color:#13d70f;">|</font>&nbsp;&nbsp;
               <a href="<?=base_url();?>admin/getfeature"><font style="color:#fff">Feature list</font></a> &nbsp;&nbsp;<font style="color:#13d70f;">|</font>&nbsp;&nbsp;
              <a href="<?=base_url();?>Admin/get_savedraft"><font style="color:#fff">Draft list</font></a> &nbsp;&nbsp;<font style="color:#13d70f;">|</font>&nbsp;&nbsp;
              <a href="<?=base_url();?>Admin/get_oppdetail"><font style="color:#fff">Opportunities</font></a> &nbsp;&nbsp;<font style="color:#13d70f;">|</font>&nbsp;&nbsp;
              <a href="<?=base_url();?>Admin/get_skilldetail"><font style="color:#fff">Skills</font></a> &nbsp;&nbsp;<font style="color:#13d70f;">|</font>&nbsp;&nbsp;
              <a href="<?=base_url();?>Admin/getall_users"><font style="color:#fff">Users</font></a>
        </ol>

         <?php 
                           $encrypt_ids=$this->encrypt->encode($name['user_id']);
                           $modify_encrypt=str_replace("/","~",$encrypt_ids);                           
                       ?>

          <ol class="dropdown pull-right" style="margin-right: 2px;margin-top:20px;"> 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?=base_url();?>images/pp.PNG" width="35px"><b class="caret"></b></a>
            <ol class="dropdown-menu" style="background-color:#CCC;font-family:awesome;">
               <div style="background-color: #CCC;width:100%;" >
                  &nbsp;<label style="padding-right:75px;cursor: pointer;" id="button"><span class="glyphicon glyphicon-user" style="cursor: pointer;font-family:awesome;"></span>&nbsp;<?php echo $name['name'];?></label>
                  <a href="<?=base_url();?>User/changepwd/<?= $modify_encrypt?>" style="padding-right:60px;font-family:awesome;font-size: 15px;background-color: #004167"  class="button"> <span class="glyphicon glyphicon-edit"></span>&nbsp;Change Password</a>
                  <!-- <label  data-toggle="modal" class="button" data-target="#mymodal" style="padding-right:5px;font-family:awesome;"><span class="glyphicon glyphicon-edit"></span>&nbsp;Change Password</label> -->
                  <label class="button" data-toggle="modal" data-target="#mymodal1" style="font-family:awesome;">&nbsp;<span class="glyphicon glyphicon-user"></span>&nbsp;Create New Member</label>
                <a href="<?=base_url();?>logout" class="button" style="padding-right:75px;font-family:awesome;"> <span class="glyphicon glyphicon-log-out">   </span>&nbsp;Log Out</a>
                </div>
            </ol>
          </ol> 
        <!-- </li> -->
       </div><!-- /.navbar-collapse -->
       
     </span>
  </div><!-- /.container-fluid -->

<!-- Modal for change password -->
<br>
<div class="modal fade" id="mymodal" style="margin-top:10px;font-family:awesome;">
  <br><br>
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center><h4 class="modal-title">Change Your Password</h4></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/changepwd', 'class="form-horizontal" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-5">Current Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="current" placeholder="Current password...">               
            </div>
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="new" placeholder="New password...">               
            </div>
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Confirm New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">               
            </div>
       </div>
       <div class="form-group">
           <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Change</button>
       </div>
     <?php echo form_close();?>
    </div>
    
</div>
    
</div> 
</div>
<!-- ==============create Modal================== --><!-- Modal for change password -->
<div class="modal fade" id="mymodal1" style="padding-top:60px;">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center><h4 class="modal-title">Create New Member</h4></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/addmember', 'class="form-horizontal" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-5">Name:</label>
            <div class="col-md-5">
               <input type="text" class="form-control" name="current" placeholder="Enter Name...">               
            </div>
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Email:</label>
            <div class="col-md-5">
               <input type="email" class="form-control" name="new" placeholder="Enter Email...">               
            </div>
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="new" placeholder="Enter password...">               
            </div>
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Confirm New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="cfmnew" placeholder="Confirm password...">               
            </div>
       </div>
       <div class="form-group">
           <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Create</button>
       </div>
     <?php echo form_close();?>
    </div>
    
</div>
    
</div> 
</div>
<!-- ==============End Modal==================