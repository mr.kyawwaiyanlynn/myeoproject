<html>
  <head>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
  <script type="text/javascript" src="http://www.techtricky.com/wp-content/jquery/jquery.jclock.js"></script>
  <script type="text/javascript">
    $(document).ready(
        function() {
           $("#zones").change(function(){
           if ($('#time-cont .time').length>0){ $('#time-cont .time').remove();}
	   var offset = $(this).val();    
           if (offset == '') return;       
 
	   $('#time-cont').append('<div class="time"></div>');
           
           var options = {
            format:'<span class=\"dt\">%A, %d %B %I:%M:%S %P</span>',
            timeNotation: '12h',
            am_pm: true,
            fontFamily: 'Verdana, Times New Roman',
            fontSize: '20px',
            foreground: 'black',
            background: 'yellow',
            utc:true,
            utc_offset: offset
          }
            
          $('#time-cont .time').jclock(options);
       });
     });
    </script>
   
   </head>
   <body>
   <select id="zones">
    <option value="">--Select--</option>
    <option value="10">Australia</option> // Australia UTC offset value is 10
    <option value="8">China</option>
    <option value="5.5">India</option>
    <option value="12">Newzealand</option>
    <option value="0">UK</option>
    <option value="-5">US EST</option>
 
  </select>
  <div id="time-cont" class="text">
  </div>
  </body>
</html>