
<!-- ==============End Modal================== -->
<div class="col-md-8 col-md-offset-2 form-group" style="background-color: #fff;padding-top:55px;padding-bottom:40px;"> 
    <center><h1> Edit Skill Content!!! </h1></center>
    <br/>
    <?php if($edit != null) {?>
    <?php 
        foreach($edit as $row) {
            $vdlink=$edit->vdlink;
            $image=$edit->image;
            $title=$edit->title;
            // $subtitle=$edit->sub_title;
            $description=$edit->description;
            $choiceid=$edit->id;
        }
    ?>
    <?php 
        $skillid = $this->uri->segment(3);
    ?>
     <?php 
       $encrypt_ids=$this->encrypt->encode($choiceid);
       $modify_encrypt=str_replace("/","~",$encrypt_ids);
   ?>
    <?php echo form_open_multipart(base_url().'admin/edit_skill/'.$modify_encrypt, 'class="form-horizontal" method="post" ')?> 
    <br/>
    <div class="col-md-10 col-md-offset-1">
        
        <div class="form-group">
            <label>TITLE: </label></br>
            <input type="text" name="title" class="form-control" value="<?=$title;?>"> 
        </div>
       <!--  <div class="form-group">
            <label>SUBTITLE:</label></br>
            <input type="text" name="subtitle" class="form-control" value="<?=$subtitle;?>">
        </div> -->
        <div class="form-group">
            <label>TAG:</label></br>
            <?php  if($post1 != Null )
                              {?>
                                     
                                      
                      <input type="text" name="tags" class="tokenfield-typeahead form-control" value="<?php foreach ($post1 as $key) { echo $key.","; }?>">
                            <?php}?>
                 <?php } else {?>
             
                  <input type="text" name="tags" class="tokenfield-typeahead form-control" >
         <?php   }?>
        </div>
        <script type="text/javascript">
        var engine = new Bloodhound({
            local: [],
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });
    </script>


    <script type="text/javascript">

        engine.initialize();

        $('.tokenfield-typeahead').tokenfield({
            typeahead: [null, {source: engine.ttAdapter()}]
        });

        // function hellowaiyan() {
        //     // alert("here");
        //     alert((document.getElementById('tokenfield-typeahead')).value);
        // }


//

    </script>
            <div class="form-group">
            <label>Apply Link:</label></br>
            <input type="text" name="apply" class="form-control" value="<?=$post->apply_link;?>">
        </div>
        <div class="form-group">
            <label>Official Link:</label></br>
            <input type="text" name="official" class="form-control" value="<?=$post->official_link;?>">
        </div>

        <div class="form-group">
          <label>Deadline :</label>&nbsp;&nbsp;

          <?php if($post->deadline!="As soon as possible" && $post->deadline!="Ongoing" && $post->deadline!="You can join anytime"){?>
          <input type="radio" name="date" value="1" id="deadline_date" <?php echo 'checked'?> onclick="showdeadline('date1')">&nbsp;<label>Date</label>
          <input type="radio" name="date" value="2" id="deadline_select" onclick="showdeadline('date2')">&nbsp;<label>Select</label>
          <?php }else{?>
            <input type="radio" name="date" value="1" id="deadline_date" onclick="showdeadline('date1')">&nbsp;<label>Date</label>
          <input type="radio" name="date" value="2" id="deadline_select" <?php echo 'checked'?> onclick="showdeadline('date2')">&nbsp;<label>Select</label>
          <?php }?>
        </div>
       <?php if($post->deadline!="As soon as possible" && $post->deadline!="Ongoing" && $post->deadline!="You can join anytime"){?>
        <div class="row">
          
          <div class="form-group" id="date1">
            <div class="col-md-5">
            <input type="date" class="form-control" id="date" name="deadline" value="<?=$post->deadline?>"> 
             <input type="hidden" name="deadline1" value="<?=$post->deadline?>">      
          </div>
          </div>

        </div>
 
        <div class="row">
          <ul class="form-group" id="date2" style="display: none;">
                          <div class="col-md-6 ">
                    <select name="deadline2" class="chosen-select form-control" >
                              
                              <option value="As soon as possible">As soon as possible</option>
                              <option value="Ongoing">Ongoing</option>
                              <option value="Ongoing">Open</option>
                              <option value="You can join anytime">You can join anytime</option>

          
                    </select>
                    
                </div>
          </ul>
        </div>
  <?php }else{ ?>
        <div class="row">
          
          <ul class="form-group" id="date1" style="display: none;">
            <div class="col-md-5">
            <input type="date" class="form-control" id="date" name="deadline" value="<?=$post->deadline?>"> 
             <input type="hidden" name="deadline1" value="<?=$post->deadline?>">      
          </div>
          </ul>

        </div>
 
        <div class="row">
          <div class="form-group" id="date2">
                <div class="col-md-5 col-md-offset-1">
                   
                      <select name="deadline2" class="chosen-select form-control" >
                              <?php $array=array("As soon as possible","Ongoing","You can join anytime");?>
                              <?php for ($i=0;$i<count($array);$i++) { 
                                  if ($array[$i] == $post->deadline){?>

                                      <option value="<?=$array[$i]?>" selected><?=$array[$i]?></option>
                                  <?php }else{?>
                                  
                                     <option value="<?=$array[$i]?>" ><?=$array[$i]?></option>
                                  <?php }}?>                              
                             
                    </select>
                  
                </div>
          </div>
        </div>
  <?php }?>


          <div class="form-group">
                <label>Location:</label>
                <input type="text" name="c_location" placeholder="Enter the location" class="form-control" value="<?= $post->c_location?>">
          </div>

           <div class="form-group">
                <label>Time:</label>
                <input type="text" name="time" placeholder="Enter the Time" class="form-control" value="<?= $post->time?>">
          </div>

        <div class="form-group">
            <label>DESCRIPTION:</label></br>
            <?php echo $this->ckeditor->editor('description',"$description")?>
        </div>

        <div class="form-group">
            <label class="col-md-5 control-label">CHOOSE TYPE: </label>
            <div class="col-md-5">
                <input type="radio" name="choice" value=1 disabled="disabled"> <label > OPPORTUNITY </label> 
                <input type="radio" name="choice" value=2 <?php echo 'checked'?>> <label >SKILL</label>
            </div>
        </div>
        <div class="form-group col-md-offset-1 " > <!-- id="skill" -->
            <label class="col-md-5  control-label">CHOOSE SUITABLE SKILL:</label>
            <div class="col-md-7">
             <?php $count=1; foreach ($all_skill as $row)  {;?>

              <div class="col-md-5">
                <div class="row col-md-9">
                  <div class="checkbox">
                    <label>
                       <input type="checkbox" class="<?= $row->skill_id?>" name="skillcheck[]" value="<?=$row->skill_id?>">
                       <font style="font-size:13px"><?=$row->type?></font>
                    </label>
                  </div>

               </div>
               </div>
               <?php $count++; };?> 
          
                  </div>
              </ul>  
          </div>
        
        <?php if($edit->image ==NULL) { ?>

        <div class="form-group">
            <label class="col-md-5 control-label"> CHOOSE TO UPLOAD PICTURE or VIDEO</label>
            <div class="col-md-5">
                <input type="radio" name="uploadchoice" value=1 id="imagechoice" onclick="showadmin('image')">
                <label>Image</label>
                <input type="radio" name="uploadchoice" value=2 id="videochoice" <?php echo 'checked'?> onclick="showadmin('video')">
                <label>Video</label>
            </div>
        </div>

        <div class="form-group">
            <ul class="form-group" id="image" style="display: none;">
                <div class="col-md-offset-5">   
                    <div class=" fileinput">
                        <input type="file" name="imageuploadchoice"  accept="image/*" onchange="showMyImage(this)" value="<?=$image;?>">
                        <input type="hidden" name="imageupload" value="<?=$image;?>">
                        <img src="" style="width:40%; margin-top:10px;" id="limage">
                    </div>
                </div>
            </ul>
        </div>

        <div class="form-group">
            <div class="form-group" id="video">
                <label class="col-md-5 control-label">Fill Video Link : </label>
                <div class="col-md-5">
                    <input type="text" name="videouploadchoice" placeholder="*paste_video_link" class="form-control" value="<?=$edit->vdlink;?>">
                     <input type="hidden" name="videoupload" value="<?=$vdlink;?>">
                </div>
            </div>
        </div>
<?php } else { ?>
         <div class="form-group">
            <label class="col-md-5 control-label"> CHOOSE TO UPLOAD PICTURE or VIDEO</label>
            <div class="col-md-5">
                <input type="radio" name="uploadchoice" value=1 id="imagechoice"  <?php echo 'checked'?> onclick="showadmin('image')">
                <label>Image</label>
                <input type="radio" name="uploadchoice" value=2 id="videochoice" onclick="showadmin('video')">
                <label>Video</label>
            </div>
        </div>

        <div class="form-group">
            <div class="form-group" id="image">
                <div class="col-md-offset-5">   
                    <div class=" fileinput">
                        <input type="file" name="imageuploadchoice"  accept="image/*" onchange="showMyImage(this)" value="<?=$edit->image;?>">
                        <input type="hidden" name="imageupload" value="<?=$image;?>">
                        <img src="<?php echo base_url(); ?>uploads/<?=$edit->image?>" style="width:40%; margin-top:10px;" id="limage">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <ul class="form-group" id="video" style="display: none;">
                <label class="col-md-5 control-label">Fill Vedio Link : </label>
                <div class="col-md-5">
                    <input type="text" name="videouploadchoice" placeholder="*paste_video_link" class="form-control">
                     <input type="hidden" name="videoupload" value="<?=$vdlink;?>">
                </div>
            </ul>
        </div>
         <?php } ?>
         <div class="col-md-offset-10">
        <input type="submit" value="Save" class="form-group btn btn-primary" id="btnclass" >
        <br/> 
    </div>
    </div>
  </div>

    </div> <!-- END OF SECOND DIV -->
</form> 
    <?php } ?>
<!--//        {-->
<!--//            echo "<h4 style='color:red'>There is no record!!</h4>";-->
<!--//        }?>-->
</div> <!-- END OF FIRST DIV -->

<script>
$(document).ready(function(){
    $("#imagechoice").click(function(){
        $("#video").hide();
        $("#image").show();
    });
    $("#videochoice").click(function(){
        $("#video").show();
        $("#image").hide();
    });
});
</script>  

<script>
$(document).ready(function(){
    $("#deadline_date").click(function(){
        $("#date2").hide();
        $("#date1").show();
    });
    $("#deadline_select").click(function(){
        $("#date2").show();
        $("#date1").hide();
    });
});
</script> 

<script type="text/javascript">


            var ex1;
            <?php foreach ($skill as $skill ) { ?>

                  ex1  = <?php echo json_encode($skill->skill_id); ?> ;
                 // alert(ex);

                  if(ex1 == 1)
                  {
                    
                     $( ".1").prop('checked', true);

                  }
                   if(ex1 == 2)
                  {
                    
                     $( ".2").prop('checked', true);

                  }
                   if(ex1 == 3)
                  {
                    
                     $( ".3").prop('checked', true);

                  }
                   if(ex1 == 4)
                  {
                    
                     $( ".4").prop('checked', true);

                  }
                   if(ex1 == 5)
                  {
                    
                     $( ".5").prop('checked', true);

                  }
                   if(ex1 == 6)
                  {
                    
                     $( ".6").prop('checked', true);

                  }
                   if(ex1 == 7)
                  {
                    
                     $( ".7").prop('checked', true);

                  }
                   if(ex1 == 8)
                  {
                    
                     $( ".8").prop('checked', true);

                  }
                   if(ex1 == 9)
                  {
                    
                     $( ".9").prop('checked', true);

                  }
                   if(ex1 == 10)
                  {
                    
                     $( ".10").prop('checked', true);

                  }

                 
            <?php } ?>

         
            
      


  </script>
  
 <script>
function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("limage");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }
    </script>
