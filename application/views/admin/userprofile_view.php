<!-- ==========PAGE HEADER============ -->


<!-- END OF PAGE HEADER -->
        <!-- ZTD /////////////////////////////user profile start /////////////////////////////////////////// -->
        <br><br><br><br>
        <div class="container">
            <div class="jumbotron col-md-offset-2 col-md-6 col-md-offset-5"  style="padding:10px;width:70%;height:40%;">
                <div class="container">
                    <div class="row">
                       <div class="col-md-3">
                       <img src="<?=base_url();?>uploads/<?=$user->picture;?>" style="width:80px;height:80px;border-radius:50px;">
                    </div>
                </div>
                <br><br>
                    <div class="row">
                        <div class="col-md-5">
                            
                            <div class="form-group">
                                <div class="row">
                                 <label class="control-label col-md-4">Name:</label>
                                 <div class="col-md-6">
                                 <?=$user->name;?></div>
                            </div></div>
                            
                            <div class="form-group">
                                <div class="row">

                                 <label class="control-label col-md-4">Email:</label>
                                 <div class="col-md-6">
                                 <?=$user->email;?></div>
                            </div></div>
                            
                            <div class="form-group">
                                <div class="row">

                                 <label class="control-label col-md-4">location:</label>
                                 <div class="col-md-6">
                                 <?=$user->location;?></div>
                            </div></div>
                            
                            <div class="form-group">
                                <div class="row">
                                 <label class="control-label col-md-4">Education:</label>
                                 <div class="col-md-6">
                                 <?=$user->education;?></div>
                            </div></div>


                        </div>
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <div class="row">
                                 <label class="control-label col-md-4">Sentence:</label>
                                 <div class="col-md-6">
                                 <?=$user->sentence;?></div>
                            </div>
                        </div>
                       
                            <div class="form-group">
                                 <div class="row">
                                 <label class="control-label col-md-4">CV:</label>
                                 <div class="col-md-6">
                                 <?=$user->cv;?>
                                 <?php if ($user->cv != Null) { ?>
                                <a href="<?= base_url()?>pdf/<?=$user->cv;?>" button class="btn btn-sm btn-info" >Read more</a>
                                  <?php } ?>
                                 </div>

                                
                            </div><br> 
                             
                            <div class="form-group">
                                <div class="row">
                                 <label class="control-label col-md-4">CoverLetter:</label>
                                 <div class="col-md-6">
                                 <?=$user->cover_letter;?> 
                                  <?php if ($user->cover_letter != Null) { ?>
                                <a href="<?= base_url()?>pdf/<?=$user->cover_letter;?>" button class="btn btn-sm btn-info" >Read more</a>
                                  <?php } ?></div>
                               

                            
                        </div></div>

                          
                            <div class="form-group">
                                <div class="row">
                                 <label class="control-label col-md-4">Intro Video:</label>
                                 <div class="col-md-6">
                                 <?=$user->intro_video;?></div>
                            </div></div>
                            
                            <div class="form-group">
                                <div class="row">
                                 <label class="control-label col-md-4">Opportunity:</label>
                                 <div class="col-md-8" style="word-break: break-all;">
                                 
                                 <?php $count=1;
                                 foreach ($opp as $row){
                                    $result=$row['type'].',';
                                    // print_r(implode(',', $result);
                                    print_r($result);
                                   
                                       }
                                      ?>
                                 
                            </div></div></div>
                            
                            <div class="form-group">
                                <div class="row">
                                 <label class="control-label col-md-4">Skill:</label>
                                 <div class="col-md-8" style="word-break: break-all;">
                                <?php 
                                 foreach ($skill as $row){
                                 print_r($row['type'].',');} ?>
                            </div></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

 <script type="text/javascript">
            function PreviewImage() {
                pdffile=document.getElementById("browse1").files[0];
                pdffile_url=URL.createObjectURL(pdffile);
                $('#viewer').attr('src',pdffile_url);
            }
    </script>

            
    
        <!-- ////////////////////////////////////////user profile end////////////////////////////////////// -->
