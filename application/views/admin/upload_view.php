  <script src="datepicker.min.js"></script>

  <br><br>
  <div class=" container1 col-md-8 col-md-offset-2 " style="padding-top:10px;" > 

    <?php if($this->session->flashdata('msg')){?>
    <center> <div class="alert alert-danger alert-dismissable" style="width:70%;margin-top:60px;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?=$this->session->flashdata('msg')?></div></center>

      <?php }?>
      <?php if($this->session->flashdata('msg1')){?>
      <center><div class="alert alert-success alert-dismissable" style="width:70%;margin-top:60px;" >
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?=$this->session->flashdata('msg1')?></div></center>

        <?php }?>
        <!-- f0f0f0 eceff1 efebe9 fafafa eeeeee e3f2fd bbdffb e0e0e0 EFEBE9 -->
        <?php echo form_open_multipart('Admin/post', 'class="form-horizontal" method="post"') ?>
        <br/>



        <div class="col-md-10 col-md-offset-1">
         <center><h1>Create Your Informations</h1></center>

         <div class="form-group">
          <label>TITLE: </label></br>
          <input type="text" name="title" placeholder="*Titles" class="form-control" id="title" >
        </div>

        <div class="form-group">
          <label>Add Tags:</label><br/>
          <div>
            <input type="text" name="tags" id="tokenfield-typeahead" value=""  class="form-control"/>
          </div>
        </div>

        <div class="form-group">
          <label>Apply Link:</label></br>
          <input type="text" name="apply" placeholder="apply Link..." class="form-control" id="apply">
        </div>

        <div class="form-group">
          <label>Official Link:</label></br>
          <input type="text" class="form-control" placeholder="official Link..." name="official">
        </div>

        <div class="form-group">
          <label>Deadline :</label>&nbsp;&nbsp;
          <input type="radio" name="date" value="1" id="deadline_date">&nbsp;<label>Date</label>
          <input type="radio" name="date" value="2" id="deadline_select">&nbsp;<label>Select</label>           
        </div>

        <div class="row">
          <ul class="form-group" id="date1" style="display: none;">
            <div class="col-md-6 col-md-offset-1">
              <input type="date" class="form-control" id="date" name="deadline">
            </div>
          </ul>
        </div>

        <div class="row">  
          <ul class="form-group" id="date2" style="display: none;">
            <div class="col-md-5">
              <select name="deadline1" class="chosen-select form-control" >

                <option value="As soon as possible">As soon as possible</option>
                <option value="Ongoing">Ongoing</option>
                <option value="Ongoing">Open</option>
                <option value="You can join anytime">You can join anytime</option>

              </select>
            </div>
          </ul>
        </div>

        <div class="form-group">
          <label>Location:</label>
          <input type="text" name="c_location" placeholder="Enter the location" class="form-control">
        </div>

        <div class="form-group">
          <label>Time:</label>
          <input type="text" name="time" placeholder="Enter Time" class="form-control">
              <input type="date" class="form-control" id="date" name="deadline">
               <select name="deadline1" class="chosen-select form-control" id="zones" >

               <!-- <option value="">--Select--</option> -->
    <option value="10">Australia</option> // Australia UTC offset value is 10
    <option value="8">China</option>
    <option value="5.5">India</option>
    <option value="12">Newzealand</option>
    <option value="0">UK</option>
    <option value="-5">US EST</option>
              </select>


        </div>
              <div id="time-cont"></div>

        <div class="form-group">
          <label>DESCRIPTION:</label></br>
          <?php echo $this->ckeditor->editor('description') ?>
        </div>


        <div class="form-group">
          <label class="col-md-5 control-label">CHOOSE TYPE: </label>
          <div class="col-md-5">
            <form id="myForm">
              <input type="radio" name="choice" value=1 id="oppchoice" >
              <label>Opportunity</label>
              <input type="radio" name="choice" value=2 id="skillchoice" > <label>Skill</label>
            </form>
            <?php if($this->session->flashdata('msg')){?>
            <div class="form-group" style="float-left;padding-top: 3px;margin-right:10px;margin-top:3px;color: red"><?=$this->session->flashdata('msg')?></div>              
            <?php }?>
          </div>
        </div>


        <div class="row">
          <ul class="form-group" id="opp" style="display: none;">
            <label class="col-md-5  control-label">CHOOSE SUITABLE OPPORTUNITY:</label>
            <div class="col-md-12">
              <label class="col-md-offset-5  col-md-7">Scholarship </label>
              <div class="col-md-offset-5 col-md-7">

                <?php $count = 1;foreach ($all_opp as $row) {;?>
                <?php if($row->opp_type=="Scholarship"){ ?>
                <font style="color:">
                  <div class="col-md-6">
                    <div class="row col-md-9">

                      <div class="checkbox">
                        <label>
                          <!-- <?php echo var_dump($all_opp); ?> -->
                          <!-- <input type="text" name="oppcheck_id" value="<?=$row->opp_id?>" hidden> -->
                          <input type="checkbox"   name="oppcheck[]" value="<?=$row->opp_id?>">
                          <!-- <input type="checkbox" class="oppCheckbox"  name="oppcheck[]" value="<?=$row->type?>"> -->
                          <!-- <input type="checkbox" class="oppCheckbox" value="<?=$row->type?>"> -->
                          <font style="font-size:12px"><?=$row->type?></font>
                        </label>
                      </div>
                    </div>
                  </div>
                </font>
                <?php } ?>
                <?php $count++;};?>
              </div>
            </div>


            <div class="col-md-12">
             <label class="col-md-offset-5  col-md-7">Coference and Training</label>
             <div class="col-md-offset-5 col-md-7">
              <?php $count = 1;foreach ($all_opp as $row) {;?>
              <?php if($row->opp_type=="C & T"){ ?>
              <font style="color:">
                <div class="col-md-6">
                  <div class="row col-md-9">

                    <div class="checkbox">
                      <label>
                        <!-- <?php echo var_dump($all_opp); ?> -->
                        <!-- <input type="text" name="oppcheck_id" value="<?=$row->opp_id?>" hidden> -->
                        <input type="checkbox"   name="oppcheck[]" value="<?=$row->opp_id?>">
                        <!-- <input type="checkbox" class="oppCheckbox"  name="oppcheck[]" value="<?=$row->type?>"> -->
                        <!-- <input type="checkbox" class="oppCheckbox" value="<?=$row->type?>"> -->
                        <font style="font-size:12px"><?=$row->type?></font>
                      </label>
                    </div>
                  </div>
                </div>
              </font>
              <?php } ?>
              <?php $count++;};?>
            </div>
          </div>

          <div class="col-md-12">
            <label class="col-md-offset-5  col-md-7">Other Opportunities </label>
            <div class="col-md-offset-5 col-md-7">

              <?php $count = 1;foreach ($all_opp as $row) {;?>
              <?php if($row->opp_type=="Other"){ ?>
              <font style="color:">
                <div class="col-md-6">
                  <div class="row col-md-9">

                    <div class="checkbox">
                      <label>
                        <!-- <?php echo var_dump($all_opp); ?> -->
                        <!-- <input type="text" name="oppcheck_id" value="<?=$row->opp_id?>" hidden> -->
                        <input type="checkbox"   name="oppcheck[]" value="<?=$row->opp_id?>">
                        <!-- <input type="checkbox" class="oppCheckbox"  name="oppcheck[]" value="<?=$row->type?>"> -->
                        <!-- <input type="checkbox" class="oppCheckbox" value="<?=$row->type?>"> -->
                        <font style="font-size:12px"><?=$row->type?></font>
                      </label>
                    </div>
                  </div>
                </div>
              </font>
              <?php } ?>
              <?php $count++;};?>
            </div>
          </div>


          <br><br>
          <div class="col-md-offset-5 col-md-7">
            <label>Accepted Location</label>
            <div class="col-md-12">
              <div class="col-md-6">
                <div class="row col-md-9">

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="location[]" value="Local">
                      <font style="font-size:12px">Local</font>
                    </label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row col-md-9">

                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="location[]" value="International">
                      <font style="font-size:12px">International</font>
                    </label>
                  </div>
                </div>
              </div>
            </ul>
          </div>

          <div class="form-group col-md-12" >
            <ul class="form-group" id="skill" style="display: none;">
              <label class="col-md-5  control-label">CHOOSE SUTIBLE SKILL:</label>
              <div class="col-md-7">
                <?php $count = 1;foreach ($all_skill as $row) {;?>
                <font style="color:">
                  <div class="col-md-6">
                    <div class="row col-md-9">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="skillcheck[]" value="<?=$row->skill_id?>">
                          <font style="font-size:12px"><?=$row->type?></font>
                        </label>
                      </div>
                    </div>
                  </div>
                </font>
                <?php $count++;}
                ;?>
              </div>

            </ul>
          </div>

          <div class="form-group">
            <label class="col-md-5 control-label"> CHOOSE TO UPLOAD PICTURE or VIDEO</label>
            <div class="col-md-5">
              <input type="radio" name="uploadchoice" value=1 id="imagechoice" >
              <label>Image</label>
              <input type="radio" name="uploadchoice" value=2 id="videochoice" >
              <label>Video</label>
            </div>      
          </div>

          <?php if($this->session->flashdata('msg2')){?>
          <div class="form-group" style="text-align: center;padding-top: 3px;margin-right:10px;margin-top:3px;color: red"><?=$this->session->flashdata('msg2')?></div>              
          <?php }?>
          <div class="form-group">
            <ul class="form-group" id="image" style="display: none;">
              <div class="col-md-offset-5">
                <div class=" fileinput">
                  <!-- <input type="file" name="imageuploadchoice"  accept="image/*" id="inputFile"> -->
                  <!-- <img src="" style="width:60%; margin-top:10px;" id="image_upload_perview"></label> -->
                  <input type="file" accept="image/*"  onchange="showMyImage(this)" class="fileinput" name="imageuploadchoice" id="inputFile">
                  <img id="limage" style="width:60%; margin-top:10px;"  src=""/>
                </div>
              </div>
            </ul>
          </div>
          <div class="form-group">
            <ul class="form-group" id="video" style="display: none;">
              <label class="col-md-5 control-label">Fill Video Link : </label>
              <div class="col-md-5">
                <input type="text" name="videouploadchoice" placeholder="*paste_video_link" class="form-control" id="vdo">
              </div>
            </ul>
          </div>

          <div class="col-md-12">

            <div data-large class="col-md-offset-5 col-md-1">
              <button type="submit" value="post" class="form-group btn" style="background-color: #004167;color: #fff" name="submitform">Post</button>
              <br/><br/><br/>
            </div>
            <div data-large class="col-md-2">
              <input type="button" value="Preview" class="btn" style="background-color: #004167;color: #fff"  data-toggle="modal" data-target="#previewmodal" onclick="fun()" >
              <br/><br/><br/>
            </div>
            <div data-large class="col-md-2">
              <!-- <a href="<?=base_url()?>admin/savedraft"  class="form-group btn" style="background-color: #004167;color: #fff" name="save">Save as Draft</a> -->
              <input type="submit" name="submitform" class="form-group btn" style="background-color: #004167;color: #fff" value="Save as Draft">
              <br/><br/><br/>
            </div>





          </div><!-- END OF COL_MD_7 -->

        </div>
      </div>


    </div>
  </ul>

</div>

</div>


</form>
</div>



<div class="container">
  <div class="modal fade" id="previewmodal" style="padding-top:60px;height:100%;box-shadow: 1px 1px 1px 2px;border-color: #9b9b9b">
    <div class="modal-dialog" style="width:70%;">
      <div class="modal-content" >
        <div class="modal-header" style="background:#9b9b9b;color:#00000;height:58px;">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <center><h4 class="modal-title" id="title1"></h4></center>
        </div>
        <div class="modal-body" style="min-height:400px">
          <div class="row">
            <div class="col-md-5">
              <img src="" style="width:350px;height:225px;padding:5px; " id="img1" class="img-thumbnail img-responsive">
            </div>
            <div class="form-group" align="left">
              Local ( Undergraduate ,   Graduate,   Ph.D,    )
            </div>
            <div class="col-md-4 jumbotron" style="width:50%;height:225px;background-color:#ffffff;margin-bottom:30px;">
              <div class="form-group">
                <label class="control-label">DEADLINE</label><br>
                <p id="deadline"> </p>  <!-- DeadLine -->
              </div>
              <div class="form-group">
                <label class="control-label">TIME LEFT</label>
                <br>
                <p id="timeleft"> </p>
              </div>
            </div>
          </div>  
          <div class="row"> 
           <!--  Preview Tags  -->
           <div class="col-md-4">                     
            <label style="font-weight: lighter;" class="label label-info" id="TagTag"></label>            
          </div>
        </div>
        <div class="row" style="padding-left:30px;padding-top:15px;" align="left">
          <div class="col-md-12" id="des1">
          </div>
        </div>
        <br>
        <br>
        <div  align="center" class="row">
          <a href="" class="btn btn-success">Apply this post</a>
          <a href="" class="btn btn-primary">Official link</a>
        </div>
        <div align="right">
          <div class="fb-share-button" data-href="http://www.mymyeo.com" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
        </div> 
      </div><!-- modal body -->
    </div><!-- content -->
  </div><!-- dialog -->
</div><!-- fade -->
</div>
<!-- /////////////////////////////////////preview modal box close /////////////////////////////////ztd/////////////  -->

<script>
  $(document).ready(function(){
    $("#oppchoice").click(function(){
      $("#skill").hide();
      $("#opp").show();
      $("#feature").show();
    });
    $("#skillchoice").click(function(){
      $("#skill").show();
      $("#opp").hide();
      $("#feature").hide();
    });
  });
</script>
<script>
  $(document).ready(function(){
    $("#imagechoice").click(function(){
      $("#video").hide();
      $("#image").show();
    });
    $("#videochoice").click(function(){
      $("#video").show();
      $("#image").hide();
    });
  });
</script>
<script>
  $(document).ready(function(){
    $("#deadline_date").click(function(){
      $("#date2").hide();
      $("#date1").show();
    });
    $("#deadline_select").click(function(){
      $("#date2").show();
      $("#date1").hide();
    });
  });
</script>
<script type="text/javascript">
  $(".tm-input").tagsManager();

</script>


<!-- /////////////////////////////////////preview modal box /////////////////////////////////ztd/////////////  -->

<script>
  function  save() {
    var inputs = document.getElementsByClassName( 'webcampics' ),
    names  = [].map.call(inputs, function( input ) {
      return input.value;
    }).join( '|' );
    alert(inputs);
  }
  $("#inputFile").change(function () {
    readURL(this);
  });
  var hello ;
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
              //  alert("here");
              reader.onload = function (e) {
                $('#img1').attr('src', e.target.result);
                hello = e.target.result;
              }
              reader.readAsDataURL(input.files[0]);
            }

          }
          function fun()
          {
            var timeleft="";var diff=0;var x=0;
            var title=document.getElementById("title").value;
            var tag=document.getElementById("tokenfield-typeahead").value;
            var deadline=$("#date").val();
            if(deadline=="As soon as possible" || deadline=="Ongoing" || deadline=="You can join anytime")
            {
              timeleft=" ";

            }
            else
            {
             var   exp_date=strtotime(deadline);
             var   current_date=date('Y-m-d');
             var  date=strtotime(current_date);
             if(date>exp_date)
             {
               diff=date-exp_date;
               x=abs(floor(diff/ (60 * 60 *24)));
               if(x>1)
               {
                                // echo " <font color='red'><b>Expired</b> </font> ". $x. " <i>days ago</i> <br><br>";
                                timeleft=+"Expired "+x+" days ago ";
                              }
                              else
                              {
                                  // echo " <font color='red'><b> Expired</b> </font>  ". $x. " <i>day ago </i><br><br>";
                                  timeleft=+"Expired "+x+" days ago ";
                                }

                              }
                              else
                              {
                                diff=date-exp_date;
                                x=abs(floor($diff/ (60 * 60 *24)));
                                if(x>1)
                                {
                                     // echo $x." days left <br><br>";
                                     timeleft=+x+"days left";
                                   }
                                   else{
                                     // echo $x." day left <br><br>";
                                     timeleft=+x+"days left";
                                   }

                                 }
                               }

                               var description = CKEDITOR.instances['description'].getData();
                               document.getElementById("deadline").innerHTML=deadline;
            // document.getElementById("deadline").innerHTML=deadline1;              
            document.getElementById("timeleft").innerHTML=timeleft;
            document.getElementById("title1").innerHTML=title;
            document.getElementById("img1").innerHTML=hello;
            document.getElementById("des1").innerHTML=description;
            var employeeArray = new Array();
            employeeArray = tag.split(",");

            for(var x=0;x<employeeArray.length;x++){
             document.getElementById("TagTag").appendChild(employeeArray[x]);
           }



           document.getElementById("dl").innerHTML="<input type=text id='" +dl + "' value='"+ deadline + "'>";

         }

       </script>
       <script language="JavaScript" type="text/javascript">
        function HandleBrowseClick2() {
          var fileinput = document.getElementById("dchoice");
          fileinput.click();
        }
        function Handlechange2() {
          var fileinput = document.getElementById("dchoice");
          var textinput = document.getElementById("choice");
          textinput.value = fileinput.value; 
        }
      </script>
      <script type="text/javascript">
        var engine = new Bloodhound({
          local: [],
          datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.value);
          },
          queryTokenizer: Bloodhound.tokenizers.whitespace
        });
      </script>
      <script type="text/javascript">
        engine.initialize();
        $('#tokenfield-typeahead').tokenfield({
          typeahead: [null, {source: engine.ttAdapter()}]
        });
      </script>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=430854080630004';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>


    <script>
      function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
          var file = files[i];
          var imageType = /image.*/;     
          if (!file.type.match(imageType)) {
            continue;
          }           
          var img=document.getElementById("limage");            
          img.file = file;    
          var reader = new FileReader();
          reader.onload = (function(aImg) { 
            return function(e) { 
              aImg.src = e.target.result; 
            }; 
          })(img);
          reader.readAsDataURL(file);
        }    
      }
    </script>

  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
  <script type="text/javascript" src="http://www.techtricky.com/wp-content/jquery/jquery.jclock.js"></script>
  