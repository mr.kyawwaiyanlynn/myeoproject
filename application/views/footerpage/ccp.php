<!DOCTYPE HTML>
<html>
    <head>
        <link rel="icon" type="image/icon" href="<?=base_url();?>images/myeo.jpg">
        <title>MYEO</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="<?=base_url()?>template/ccp/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.min.css">
        <!-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
        <!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/> -->
        <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=awesome' >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      
        <style type="text/css">
    
  
*,body
{
  font-family:'awesome';

}
.form-group button[type="submit"]
{
  border: 2px solid #fff;
  outline:none;
  font-size: 16px;
  color:#fff;
  background: transparent;
  cursor: pointer;
  border-radius: 10px;

}

.top
{
  position: fixed;
  top: 0;
  width: 100%;
}

.page_footer{
  position:relative;
            /*bottom:;*/
            width: 100% 

             }


.a 
{
  padding-right: 53px;
  margin-top: 5px;
  padding-left: 30px;
  font-weight: lighter;
}

@media only screen and (max-width: 768px) {
    
  .a 
    {
      padding-right: 1px;
      margin-top: 3px;
      padding-left: 0px;
    }
   
} 
.container input[type="text"],
.container input[type="email"],
.container input[type="password"]
{
  border:none;
  border-bottom: 1px solid #fff;
  background: transparent;
  outline: none;
  height: 30px;
  color: #fff;
  font-size: 16px;
}
.container button[type="submit"]
{
  border: 1px solid transparent;
  outline:none; 
  width:100%;
  height: 40px;
  color:#fff;
  font-size: 16px;
  background: #004167;
  cursor: pointer;
  border-radius: 10px;
  text-decoration: none;
}
.container button[type="submit"]:active
{
  /*background: #004167;*/
  color: #fff;
  text-decoration: none;
  text-indent: none;
  border: hidden;

}
.form-group button[type="submit"]
{
  border: 2px solid #fff;
  outline:none;
  font-size: 16px;
  color:#fff;
  background: transparent;
  cursor: pointer;
  border-radius: 10px;
}
/*.container button[type="submit"]:hover
{
  background: #004167;
  color: #fff;
}*/
.sign-up-with-email a 
{
  text-align: right;
}
.form-group button[type="submit"]
{
  border: 2px solid #fff;
  outline:none;
  font-size: 16px;
  color:#fff;
  background: transparent;
  cursor: pointer;
  border-radius: 10px;

}

body{
  width: 100%;
  font-family: Open Sans;
}

.page_footer, .pg_header{
  background-color: #004167;
}

.container-fluid{
  position: fixed;
  top: 0;
  width: 100%;
}
#img.source-image {
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
}
.a
{
  font-size: 13px;
}
.content
{
  font-size: 18px;
}

#header1 {
    background-attachment: scroll,scroll,fixed;
    /*background-image: url("images/received_732471613589905.jpeg"), url("images/received_732471613589905.jpeg"), url("../../images/received_732471613589905.jpeg");*/
    /*background-position: bottom center,top left,center center;*/
    /*background-repeat: repeat-x,repeat,no-repeat;*/
    /*background-size: 3200px 460px,auto,cover;*/
    color: rgba(255, 255, 255, 0.8);
    /*padding: 10em 0 20em 0;*/
    position: relative;
    text-align: center;
    margin-top:5px;
    padding-bottom: 0px;

  }
  #header1 .logo{
    text-align: center;
    width: 50px;
    height: 50px;
  }
  @media only screen and (max-width: 768px) {

    #header1 {
    background-attachment: scroll,scroll,fixed;
    /*background-image: url("images/received_732471613589905.jpeg"), url("images/received_732471613589905.jpeg"), url("../../images/received_732471613589905.jpeg");*/
    /*background-position: bottom center,top left,center center;*/
    /*background-repeat: repeat-x,repeat,no-repeat;*/
    /*background-size: 3200px 460px,auto,cover;*/
    color: rgba(255, 255, 255, 0.8);
    /*padding: 10em 0 20em 0;*/
    position: relative;
    text-align: center;
    margin-top:40px;
    padding-bottom: 0px;

  }
  
}

    </style>
    
   
    </head>
    <body>
   

   <!-- Header -->      
            <div id="header1">
                <!-- <span class="logo icon fa-paper-plane-o"></span> -->
                <img src="<?=base_url()?>images/myeo_logo.png" class="logo" >
                <h1 style="size:30px;">Campus Chapter Program</h1>
                <p><font >A student-led,youth empowerment initiative giving talented students <br> the skills to help themselves and their peer group</font></p>
            </div>

            <!-- Main -->
            <div id="main">
                 <header class="major container 75%">
                    <h2>With the support of MYEO , the skills to thrive in the workplace of tomorrow.</h2>
                    
                </header>

                <div class="container" >
                    <section class="feature left">
                        <a href="#" ><img src="<?=base_url();?>images/ccp1.jpg" alt="" class="image"></a>
                        <div class="content">
                            <h3>What is the Campus Chapter Program</h3>
                            <p style="text-align:justify;"><font >The Campus Chapter Program (CCP) was born out of meeting the needs of our community
                                of MYEOers. MYEO operated a series of workshops at universities across Yangon in June
                                2016 and again in January 2017.
                                Considering how to continue running such initiatives in a highly impactful but scalable
                                structure, the Campus Chapter Program was born. The CCP has been designed to be highly
                                scalable and implementable across campuses in Myanmar with the potential to truly expand
                                globally.</font></p>
                        </div>
                    </section><br>
                    <section class="feature right">
                        <a href="#" class="image "><img src="<?=base_url();?>images/ccp2.jpg" alt="" /></a>
                        <div class="content" style="padding-right:20px;">
                            <h3>Workshops</h3>
                            <p style="text-align:justify;"><font >The CCP is a student-led initiative operating through training and onboarding youth student
                                leaders on campuses across Myanmar, trained by the MYEO founding team to host monthly
                                professional skill development workshops focused on developing the professional skills (CV
                                writing, interview skills, critical thinking, communication, etc) of their student peer group.
                                MYEO Campus Chapter Directors work through a 4-week onboarding and training
                                curriculum delivered through online materials developed by MYEO. </font></p>
                                                        </div>
                    </section><br>
                    <section class="feature left">
                        <a href="#" class="image "><img src="<?=base_url();?>images/ccp3.jpg" alt="" /></a>
                        <div class="content" id="value">
                            <h3>Values</h3>
                            
                             <p style="font-size:18px;text-align:justify;">  Through this curriculum
                                we align youth leaders with MYEO’s values and stimulate their enthusiasm and motivation
                                developing the presentation, communication, time management and problem-solving skills
                                necessary to delivering valuable workshops to their student peers.</p>
                        </div>
                    </section>
                </div>

                <footer class="major container 75%">
                    <h3>Join Us</h3>
                    <p style="font-size:18px;text-align:justify;">If you read this far and are thinking ‘Hell yeah this sounds awesome! How do I get
                                        involved?’, send an email with a CV attached (max 2 pages) and an answer to the question
                                        ‘If you were to host a workshop on your campus, what one skill would you focus on
                                        developing through the workshop and why?’</p>
                    <ul class="actions">
                        <li><a href="http://www.gmail.com" class="button">SEND TO <label style="color: #fff;text-transform: lowercase;font-size: 14px">hello@mymyeo.com</label></a></li>
                    </ul>
                </footer>
          <!-- </div> -->


           <!-- ===========PAGE HEADER=========== -->

<div class="container-fluid  navbar-default " role="navigation" style="background-color:#004167;min-height: 70px">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-2">
      <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="margin-top: 19px">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='col-md-offset-1' style="margin-bottom: 10px;margin-top: 10px;" />
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse col-md-10 pull-right" id="bs-example-navbar-collapse-1">
      <span class="rspace col-md-12" style="padding-top:19px;" >        

            <?php $attribute=array("class"=> "form-inline");
              echo form_open('user/login',$attribute);?>
               <?php if($this->session->flashdata('msg1')){?>
                      <div class="form-group alert" style="float-left;height: 40px;padding-top: 10px;margin-right:10px;"><?=$this->session->flashdata('msg1')?></div>
                     <!-- <script type="text/javascript"> document.write("error");</script>  -->
              <?php }?>
              <div class="form-group">
              
              <input type="email"  class="form-control " name="email" placeholder="Email" required style="margin-left: 5px;background-color: #fff">
              </div>
              <div class="form-group">
              <input type="password" class="form-control " name="password" placeholder="Password"  required style="margin-left: 5px;background-color: #fff">
              </div>
              <div class="form-group">
              <input type="submit" class="form-control btn btn  " name="submit" value="LOG IN" 
                     style="color:white;background-color:transparent;border-color:white;height:30px;border-radius:10px;margin-left:10px;">
             
             </div>
             <div class="form-group">
             <label class="control-label" style="color:white;margin-left:10px;margin-right:20px;" data-toggle="modal" data-target="#myModal">Forgot Password?</label>  
             </div>           
             </form>           
     
      </span>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->

<!-- </nav> -->
<!-- Modal for change password -->
<div class="modal fade" id="mymodal" style="margin-top:50px;">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center><h4 class="modal-title">Change Your Password</h4></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/changepwd', 'class="form-horizontal" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-5">Current Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="current" placeholder="Current password...">            
            </div>
           
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="new" placeholder="New password...">               
            </div>
            
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Confirm New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">               
            </div>

       </div>
       <div class="form-group">
           <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Change</button>
       </div>
     <?php echo form_close();?>
     
    </div>
    
</div>

    
</div> 
</div>




<script  type="text/javascript" src="//platform.linkedin.com/in.js">

    function click() {

        alert("here");

        api_key: 868ezwj38m14cr
        authorize: true
        onLoad: onLoad

    }


 </script>

<script>



    function onLoad() {

        try {
            IN.User.logout();
        } catch (err) {
            console.log(err);
        }
        // setTimeout("goToHome()", 10000);
    }

//}
</script>
<!-- ==============End Modal================== -->
</div>