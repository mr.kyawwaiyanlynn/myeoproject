<html>
<head>
<link rel="icon" type="image/icon" href="<?=base_url();?>images/myeo.jpg">
<title>MYEO</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="<?=base_url()?>template/career/css/layout.css" rel="stylesheet" type="text/css" media="all">
<link href="<?=base_url()?>template/career/css/framework.css" rel="stylesheet" type="text/css" media="all">
<!-- <link href="<?=base_url()?>template/career/css/fontawesome-4.4.0.min.css" rel="stylesheet" type="text/css" media="all"> -->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.min.css">
 -->
<!-- JAVASCRIPTS -->
<script src="<?=base_url()?>template/career/js/jquery.min.js"></script>
<script src="<?=base_url()?>template/career/js/jquery.backtotop.js"></script>
<script src="<?=base_url()?>template/career/js/jquery.mobilemenu.js"></script>
<!-- IE9 Placeholder Support -->
<script src="<?=base_url()?>template/career/js/jquery.placeholder.min.js"></script>
<!-- / IE9 Placeholder Support -->
<!-- HEADER LINK //////////////////////////////////////////////////////////////////////////////////////////////////-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.min.css">
    <!-- coded by mk -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>index.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=awesome' >
    <!-- end of code by mk -->
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/bootstrap.min.js">
    </script>
    
<style type="text/css">
*{font-family: 'awesome';}
  * {box-sizing:border-box}

 /*Slideshow container */
/*.slideshow-container {
}*/

/*.mySlides {*/
    display: none;
}

/* Next & previous buttons */


/* Caption text */


/* Fading animation */
/*.container-fluid{
  position: fixed;
  top: 0;
  width: 100%;
}*/
#top
{

   padding-top: 0px;
}
.page_footer
{
  position: fixed;
  bottom: 0;
}
.headnav
{
  position: fixed;
  top: 0;
  width: 100%;
}

</style>
  
<script type="text/javascript">
$(document).ready(function() {
   $('#navigations').localScroll({duration:800});
});
</script>

<script type="text/javascript">


   $(function(){
      $("#btnclick").click(function(){
        $("divpopup").dialog({
          title : " Pop up",
          width : 200,
          height : 100,
          modal:true,
          buttons: {Close;
                function(){
                  $(this).dialog('close');
                }
            }
        });
      });
   })
</script>

<script type="text/javascript">
$(function(){
    $("#gotologin").click(function(){
     
    })
  function gotologin(){alert("Are You Sure To login!!!!")};
</script>

<script type="text/javascript">
$(document).ready(function() {
   $('#navigations').localScroll({duration:800});
});
</script>


<!--for  nav active-->









</head>
<!-- END HEADER LINK ///////////////////////////////////////////////////////////////////////////////////////////////-->
<!-- </head> -->


 <!--end of forgot model-->   



<body id="top" style="background-color:#222222;">

  <div class="wrapper bgded overlay" style="background-image:url('<?=base_url()?>images/background-myeo.png');
    -moz-background-size: cover;-webkit-background-size:cover;background-size:cover;min-height: 500px;
background-repeat: no-repeat;background-position:center;">
    <div id="pageintro" class="hoc clear"> 
           <article class="introtxt">
          <h2 class="heading" style="font-family:'awesome'">Career</h2>
          <p style="font-family: 'awesome';">Changing the future for millions of young people.<br/> Come join us.<br/>
          </p>
          <!-- <footer><a class="btn medium" href="#">Viverra egestas</a></footer> -->
    </article>
     <div class="clear"></div>
  </div>
</div>


<div class="wrapper row3">
  <main class="hoc container clear">
    <div class="row">
    <div class="container"  style="padding-top: 1px;padding-bottom: 40px;"> <!-- ztd -->
    <article>
      <header>
        <p class="nospace btmspace-10 font-xs uppercase"><font size="5px">Your career , your future</font></p>
        <h2 class="font-x2">WE-NEED-YOU</h2>
      </header>
      <p class="btmspace-30" style="text-align:justify">We’re looking for driven young leaders who want to shape their future with MYEO. If you
              have the drive, passion, ambition and capacity to learn to solve the challenges we are
              addressing then look no further.
              We’re always looking for the brightest talent the world has to offer to advance our mission.
              What you should know about us at MYEO before we go any further:<br><br>
              We don’t care where you’re from, but only where you’re going.
              We don’t care where you’re based, but only where your journey will take you.
              We don’t care whether you have a degree or not, we know talent when we see it.
              We don’t care how much experience you have, only how you learn from your past to shape
              your future.
              If you believe in our mission, vision, values and have the talent and drive to thrive with
              MYEO, we need to hear from you.&hellip;</p>
     <!--  <footer><a class="btn small" href="#">Read More &raquo;</a></footer> -->
    </article>
</div> <!--  ztd -->
</div>
        <div style="margin:0;">
      <ul class="nospace group services">
        <li class="one_half first btmspace-30">
          <article><a href="#"><i class="fa fa-database"></i></a>
            <h6 class="heading"><font color="#004167">Engineering Evangelist</font></h6>
            <p>We’re on the hunt for kick-ass web and mobile (android) developers to help build awesome
        products for our community of MYEOers. Years of experience nor degree needed, just a
        thirst for knowledge, hacking together awesome new features and getting creative!
        Send over your portfolio of amazing web and/or mobile products and CV to
        <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
       <!-- <footer><a href="#">View Details &raquo;</a></footer> -->
          </article>
        </li>
        <li class="one_half btmspace-30">
          <article><a href="#"><i class="fa fa-language"></i></a>
            <h6 class="heading"><font color="#004167">Dazzling Designer</font></h6>
            <p>We’ve got an awesome roadmap of new features and products for our MYEOer community
        and need awesome graphic design talent to bring this to life. We’re looking for the brightest
        creative talent to join MYEO in giving our community the user experience and products they
        deserve!
        Send over your portfolio of awesome creations and your CV to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
            <!-- <footer><a href="#">View Details &raquo;</a></footer> -->
          </article>
        </li>
        <li class="one_half first">
          <article><a href="#"><i class="fa fa-lastfm"></i></a>
            <h6 class="heading"><font color="#004167">Growth Guru</font></h6>
            <p>MYEO is only as valuable as the community of MYEOers using our platforms, content and
        finding new opportunities. We need an awesome growth team to ensure every young person
        across the world does not miss out on their next big opportunity and fulfill their potential.
        Send over your CV and an answer to the question “What marketing strategy would you
        develop and how would you execute it to grow the user base of MYEO?” to
        <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
                   <!--  <footer><a href="#">View Details &raquo;</a></footer> -->
          </article>
        </li>
        <li class="one_half btmspace-30 ">
           <article><a href="#"><i class="fa fa-lastfm"></i></a>
            <h6 class="heading"><font color="#004167">Content Crafter</font></h6>
                  <p>   MYEO thrives off the world-class professional skill development content hosted on our
            platforms. We need awesome content writers with a visionary outlook to address the skills
            gaps present in young people for today as well as the world of tomorrow.
            Send over your CV and a sample article written in the style you see across the MYEO
            platform focused on developing a skill of your choice to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
                       <!--  <footer><a href="#">View Details &raquo;</a></footer> -->
          </article>
        </li>

        <li class="one_half first">
          <article><a href="#"><i class="fa fa-lastfm"></i></a>
            <h6 class="heading"><font color="#004167">Veteran Videographer</font></h6>
            <p>The content world is shifting heavily into video. To keep crafting engaging content for our
        MYEOer community we need the most creative forward looking videography crew to bring
        our content to life. If you have the editorial, creativity and passion to craft amazing skill
        development video content for the MYEOer community then you’ve come to the right place.
        Send over your portfolio of amazing creations and a storyboard for a new video concept for
        our platform developing a skill of your choice to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
           <!--  <footer><a href="#">View Details &raquo;</a></footer> -->
          </article>
        </li>
        <li class="one_half btmspace-30">
          <article><a href="#"><i class="fa fa-sliders"></i></a>
            <h6 class="heading"><font color="#004167">Sassy Sales</font></h6>
            <p>We work with some of the biggest companies and organisations in the world to address their
        human capital needs and recruitment. We’re on the hunt for the best communicators,
        presenters and lead generators to scale the impact we are able to offer to our youth
        community and the world’s leading organisations.
        Send over your CV and a written concept note for a novel and creative way you would
        approach selling our talent subscription product to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
            <!-- <footer><a href="#">View Details &raquo;</a></footer> -->
          </article>
        </li>
        <li class="one_half first">
          <article><a href="#"><i class="fa fa-lastfm"></i></a>
            <h6 class="heading"><font color="#004167">Campus Chapter Champion</font></h6>
            <p>The Campus Chapter Program is integral to the development and impact MYEO is able to
        have with our MYEOer community. Involved in shaping, scaling and supporting the growth of
        our community through our physical Campus Chapter network, you will need exemplary
        communication, organisation and strategic skills as well as a vision for where you take this
        Program.
        Send over your CV and an answer to the question “How would you design the Campus
        Chapter Program to be as scalable and impactful as possible for our on the ground MYEOer
        community?” to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
           <!--  <footer><a href="#">View Details &raquo;</a></footer> -->
          </article>
        </li>
      </ul>
    </div>  <!-- end of two third -->
    <div class="clear"></div>
</main>
</div>  <!-- end of wrapper row3 -->
<div class="container-fluid  navbar-default headnav " role="navigation" style="background-color:#004167;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-1">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
          <a href="<?=base_url();?>"> <img src="<?=base_url();?>/images/myeo_logo.png"  width="10" height="10" class='col-md-offset-1' style="margin-left: 55px;margin-right:20px;margin-top:5px;width:40px;height: 40px;" /></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ol class="nav navbar-nav " style="display: inline;">
         <li style="margin-left: 80px"><a href="#" style="text-decoration:none;"><font style="color:#fff"><span style="text-align: right"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#fff"><span class="glyphicon glyphicon-copyright-mark"></span>&nbsp;2017</font></span></font></a></li>

        <li style="padding-left: 90px">
          <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">About</font></a>
        </li>

        <li style="margin-left: 90px"> 
                <a href="<?php echo base_url() ?>footer/career" style="text-decoration:none;"><font style="color:#fff">Career</font></a>
        </li>

        <li style="margin-left: 90px"> 
              <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">Compus Chapter Program </font></a>
        </li>

        <li style="margin-left: 90px"> 
              <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">User Aggrement</font></a>
        </li>

        <li style="margin-left: 90px"> 
                <a href="<?php echo base_url() ?>footer/pp" style="text-decoration:none;"><font style="color:#fff">Privacy Policy</font></a>
        </li>

      
       
   
      </ol>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->

<div class="container-fluid  navbar-default " role="navigation" style="background-color:#004167;">
    <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header col-md-2">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          <a href="<?=base_url();?>"><img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='col-md-offset-1' style="margin-bottom: 10px;margin-top: 10px;width: 60px;height: 50px;" /></a>
      </div>     


    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <div class="collapse navbar-collapse col-md-10 pull-right" id="bs-example-navbar-collapse-1">

      <span class="rspace col-md-12">        

      <?php $attribute=array("class"=> "form-inline");
        echo form_open('user/login',$attribute);?>
         <?php if($this->session->flashdata('msg1')){?>
                <div class="form-group alert" style="float-left;height: 40px;padding-top: 10px;margin-right:10px;margin-top: 20px;"><?=$this->session->flashdata('msg1')?></div>
               <!-- <script type="text/javascript"> document.write("error");</script>  -->
        <?php }?>
        <div class="form-group" style="padding-top:20px;">
        
        <input type="email"  class="form-control " name="email" placeholder="Emailaaaaaaaaaaaa" required>
        
        <input type="password" class="form-control " name="password" placeholder="Password" required>
        
        <input type="submit" class="btn btn " name="submit" value="LOG IN" 
               style="color:white;background-color:transparent;border-color:white;height:30px;border-radius:10px;margin-left:10px;">
       
       </div>
       <label  style="color:white;padding-top:20px;padding-left:25px;margin-right:20px;" data-toggle="modal" data-target="#myModal">Forgot Password?</label> 
       </form>
       
     
      </span>

  </div>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
<!-----------------------TKH forgot Model------------------->
<div class="modal " id="myModal">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <center><h3 class="modal-title"> Reset Your Password</h3></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/ForgotPassword', 'class="form-inline" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-4" style="margin-top:10px;">Email:</label>
            <div class="col-md-4">
               <input type="email" class="form-control" name="email" placeholder="Enter your Email..." required>            
            </div>
           
       </div>
       
       <div class="form-group">
        <div class="col-md-offset-5 col-md-4">
           <input type="submit" name="submit" class="form-control btn btn-success" value="Change">
         </div>
       </div>
     <?php echo form_close();?>
    
    </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>
    </div>
  </div>
</div>

 <!--end of forgot model-->   

  <div class="page_footer  navbar-default nav1 " role="navigation" style="background-color:#004167 ;width: 100%;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-1">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
          <img src="<?=base_url();?>/images/myeo_logo.png"  width="10" height="10" class='col-md-offset-1' style="margin-left: 40px;margin-right:20px;width:40px;height: 40px;" />
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <center>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ol class="nav navbar-nav " style="display: inline;">
         <li style="margin-left: 35px"><a href="#" style="text-decoration:none;"><font style="color:#fff"><span style="text-align: right"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#fff"><span class="glyphicon glyphicon-copyright-mark"></span>2017</font></span></font></a></li>

        <li style="margin-left: 40px">
          <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;padding-left: 40px;"><font style="color:#fff">About</font></a>
        </li>

        <li style="margin-left: 40px"> 
                <a href="<?php echo base_url() ?>footer/career" style="text-decoration:none;padding-left: 40px;"><font style="color:#fff">Career</font></a>
        </li>

        <li style="margin-left: 40px"> 
              <a href="<?php echo base_url() ?>footer/ccp" style="text-decoration:none;padding-left: 40px;"><font style="color:#fff">Compus Chapter Program </font></a>
        </li>

        <li style="margin-left: 40px"> 
              <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;padding-left: 40px;"><font style="color:#fff">User Aggrement</font></a>
        </li>

        <li style="margin-left: 40px"> 
                <a href="<?php echo base_url() ?>footer/pp" style="text-decoration:none;padding-left: 40px;"><font style="color:#fff">Privacy Policy</font></a>
        </li>

       
       
   
      </ol>

    </div><!-- /.navbar-collapse -->
  </center>
  </div><!-- /.container-fluid -->




  
</body>
</html>
