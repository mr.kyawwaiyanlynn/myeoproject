<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
  <head >
    <meta charset="UTF-8"/>
    <title>MYEO</title>
    <link rel="icon" href="../images/fav.png" type="image"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    

    <meta name="viewport" content="width=device-width,initital-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.min.css">
    
    <!-- ========About Pg========== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/icon" href="<?=base_url();?>images/myeo.jpg">
    <title>MYEO</title>
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/template/footer/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/template/footer/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/template/footer/css/style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=awesome' >
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.min.js"></script>

   
    <!-- =======================================================
      Theme Name: Medilab
      Theme URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
      Author: BootstrapMade.com
      Author URL: https://bootstrapmade.com
    ======================================================= -->

    <style>
   body 
{
  font-family:'awesome';

}
.container
{
  font-family:'awesome';
}

    .pg_header{
  position: fixed;
  top: 0;
  width: 100%;
}
    .nav1 {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9999;
  width: 100%;
  height: 70px;
  background-color: #00a087;


}
#aboutnav{
  padding-top: 3px;
}
.page_footer{
    position:relative; 
   /*height: 100px;*/
   

             }

.page_footer .a a{
  font-weight: lighter;
  text-decoration: none;
  outline: none;

}

.right_title{
  position: fixed;
}

.a 
{
  padding-right: 53px;
  margin-top: 5px;
  padding-left: 30px;
}

@media only screen and (max-width: 768px) {
    /* For mobile phones: */
   /* [class*="col-"] {
        width: 100%;
    }*/
  .a 
{
  padding-right: 10px;
  margin-top: 3px;
  padding-left: 0px;
}

}
.a_img 
{
  padding-right: 55px;
  margin-top: 5px;
  /*padding-left: 30px;*/
}

}
.page_footer1{position:fixed;
              bottom: 0;
              width: 100%;

}
.a_img
{
  /*margin-right: 65px;
  padding-left:30px;*/
  padding-right: 53px;
  margin-top: 5px;
  padding-left:30px;
}
.page_footer{

  
              bottom: 0;

}
*, *::before, *::after {
   box-sizing: border-box;
}

em {
   font-style: normal;
}

a {
   text-decoration: none;
   color: inherit;
} 


/* Layout */
/*.s-layout {
   display: flex;
   width: 100%;
   min-height: 100vh;
}

.s-layout__content {
   display: flex;
   justify-content: center;
   align-items: center;
   flex: 1;
}*/

/* Sidebar */
.s-sidebar__trigger {
   /*z-index: 2;*/
   position: fixed;
   top: 65px;
   left: 0;
   width: 25%;
   height: 100em;
   background: #192b3c;
    padding-top: 6em;
}

.s-sidebar__trigger > i {
   display: inline-block;
   margin: 1.5em 0 0 1.5em;
   color: #f07ab0;
}

.s-sidebar__nav {
   position: fixed;
   top: 20;
   left: -15em;
   overflow: hidden;
   transition: all .3s ease-in;
   width: 15em;
   height: 100%;
   background: #fff;
   /*color: rgba(255, 255, 255, 0.7);*/
}

.active{
   /*position: fixed;*/
   /*top: 20;*/
   /*left: -15em;*/
   overflow: hidden;
   transition: all .3s ease-in;
   width: 15em;
   height: 100%;
   /*background: #243e56;*/
   /*color: rgba(255, 255, 255, 0.7);*/
}

.s-sidebar__nav:hover,
.s-sidebar__nav:focus,
.s-sidebar__trigger:focus + .s-sidebar__nav,
.s-sidebar__trigger:hover + .s-sidebar__nav {
   left: 0;
}

/* Mobile First */
@media (min-width: 42em) {
   .s-layout__content {
      margin-left: 4em;
   }
   
   /* Sidebar */
   .s-sidebar__trigger {
      width: 4em;
    padding-top: 6em;
   }
   
   .s-sidebar__nav {
      width: 4em;
      left: 0;
   }

   .s-layout__sidebar{
    padding-top: 3em;
   }
   
   /*.s-sidebar__nav:hover,
   .s-sidebar__nav:focus,
   .s-sidebar__trigger:hover + .s-sidebar__nav,
   .s-sidebar__trigger:focus + .s-sidebar__nav {
      width: 15em;
   }*/
}

@media (min-width: 68em) {
   .s-layout__content {
      margin-left: 15em;
   }
   
   /* Sidebar */
   .s-sidebar__trigger {
      display: none
   }
   
   .s-sidebar__nav {
          width: 20em;
   }
   
   .s-sidebar__nav ul {
      top: 1.3em;
   }
}
#btnclass
{
  border: 1px solid transparent;
  outline:none; 
  width:50%;
  height: 40px;
  color:#fff;
  font-size: 16px;
  background: #004167;
  cursor: pointer;
  border-radius: 10px;
  text-decoration: none;
  font-family: awesome;
}
#navclass > a
{
   text-decoration: none;
   outline:none;
   font-family: awesome;
}
#navclass > a:active
{
  color:#13d70f;
}
*{
  font-family: 'awesome';
}
.modal
{
  padding-top:50px;
}

    </style>
</head>
 <!-- added header -->  
  <!-- ===========PAGE HEADER=========== -->

<div class="container-fluid  navbar-default " role="navigation" style="background-color:#004167;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-2">
      <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='col-md-offset-1' />
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse col-md-10 pull-right" id="bs-example-navbar-collapse-1">
      <span class="rspace col-md-12" style="padding-top: px">        

            <?php $attribute=array("class"=> "form-inline");
              echo form_open('user/login',$attribute);?>
               <?php if($this->session->flashdata('msg1')){?>
                      <div class="form-group alert" style="float-left;height: 40px;padding-top: 10px;margin-right:10px;"><?=$this->session->flashdata('msg1')?></div>
                     <!-- <script type="text/javascript"> document.write("error");</script>  -->
              <?php }?>
              <div class="form-group">
              
              <input type="email"  class="form-control " name="email" placeholder="Email" required style="margin-left: 5px;">
              </div>
              <div class="form-group">
              <input type="password" class="form-control " name="password" placeholder="Password"  required style="margin-left: 5px;">
              </div>
              <div class="form-group">
              <input type="submit" class="form-control btn btn  " name="submit" value="LOG IN" 
                     style="color:white;background-color:transparent;border-color:white;height:30px;border-radius:10px;margin-left:10px;">
             
             </div>
             <div class="form-group">
             <label class="control-label" style="color:white;margin-left:10px;margin-right:20px;" data-toggle="modal" data-target="#myModal">Forgot Password?</label>  
             </div>           
             </form>           
     
      </span>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
<!-- </nav> -->
<!-- Modal for change password -->
<div class="modal fade" id="mymodal" style="margin-top:30px;">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center><h4 class="modal-title">Change Your Password</h4></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/changepwd', 'class="form-horizontal" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-5">Current Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="current" placeholder="Current password...">            
            </div>
           
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="new" placeholder="New password...">               
            </div>
            
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Confirm New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">               
            </div>

       </div>
       <div class="form-group">
           <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Change</button>
       </div>
     <?php echo form_close();?>
     
    </div>
    
</div>
    
</div> 
</div>

<script  type="text/javascript" src="//platform.linkedin.com/in.js">

    function click() {

        alert("here");

        api_key: 868ezwj38m14cr
        authorize: true
        onLoad: onLoad
    }
 </script>

<script>
    function onLoad() {

        try {
            IN.User.logout();
        } catch (err) {
            console.log(err);
        }
        // setTimeout("goToHome()", 10000);
    }

</script>
<!-- ==============End Modal================== -->


<!-- kowaiyanstart -->

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<!--banner-->
<section id="banner" class="banner">
    <div class="bg-color">
        <!--<nav class="navbar navbar-default navbar-fixed-top">-->
        <div class="container">
            <div class="col-md-12">
                <!--<div class="navbar-header">-->
                <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">-->
                <!--<span class="icon-bar"></span>-->
                <!--<span class="icon-bar"></span>-->
                <!--<span class="icon-bar"></span>-->
                <!--</button>-->
                <!--<a class="navbar-brand" href="#"><img src="img/logo.png" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>-->
                <!--</div>-->
                <!--<div class="collapse navbar-collapse navbar-right" id="myNavbar">-->
                <!--<ul class="nav navbar-nav">-->
                <!--<li class="active"><a href="#banner">Home</a></li>-->
                <!--<li class=""><a href="#service">Services</a></li>-->
                <!--<li class=""><a href="#about">About</a></li>-->
                <!--<li class=""><a href="#testimonial">Testimonial</a></li>-->
                <!--<li class=""><a href="#contact">Contact</a></li>-->
                <!--</ul>-->
                <!--</div>-->
            </div>
        </div>
        <!--</nav>-->
        <div class="container" style="font-size: 16px;">
            <div class="row">
                <div class="banner-info" >
                    <div class="banner-text text-center">
                        <h2 style="font-size:50px;font-family: 'awesome">MYEO </h2><br>
                    </div>
                    <div class="banner-text text-center" >
                       
                        <p><h2 style="font-family: awesome;">Giving you the skills to acquire your next big opportunity.</h2></p>
                         <p><h3 style="font-family: awesome;"> We live in a world full of opportunity , but only if you have the necessary skills.</h3>
                        </p>
                        
                    </div>
                    <div class="overlay-detail text-center">
                        <a href="#service"><i class="fa fa-angle-down"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ banner-->
<!--service-->
<section id="service" class="section-padding">
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-push-6 right" style="padding-top:20px;font-size: 16px;">
                <h2 class="heading" style="font-family:awesome;">Our Story</h2>
                <p>MYEO is a youth development social enterprise based in Yangon, Myanmar, bridging this gap between the skills you need to acquire that next opportunity. 
                </p>

                <p>
                 Starting in 2014 as a university project, MYEO was created to bring much needed youth development resources empowering young people to overcome personal, educational and professional development challenges. In that time MYEO has developed into a significant community of users who we like to call MYEOers!
                </p>
                <p>
                  The MYEOer community is on a mission to provide a better future for young people across Myanmar and the world through access to resources, skills and opportunities. MYEO has developed to become a comprehensive education content platform encompassing both professional skill development content (communication, critical thinking, creativity, leadership) and new opportunities (internships, scholarships, jobs, international conferences).
                </p>
                <p>
                  MYEO is only as strong as our community of MYEOers! To keep up with what goes down at MYEO, check out our social media and give our content a share on Facebook and Instagram.
                </p>

            </div>

            <div class="col-md-6 col-md-pull-6 left">
                <div class="row image-grid">
                    <div style="margin-top:80px;margin-bottom:10px;" class="col-sm-12">
                        <!--520x252-->
                        <img  src="<?=base_url();?>images/received_732471100256623.jpeg" class="img-responsive" alt="Scott and Max">
                    </div>
                    <div style="margin-top: 380px">
                        <!--255x145-->
                        <div class="col-sm-6 left">
                            <img src="<?=base_url();?>images/received_732471613589905.jpeg" class="img-responsive hidden-sm hidden-xs" alt="Scott and Max">
                        </div>
                        <!--255x145-->
                        <div class="col-sm-6 right">
                            <img src="<?=base_url();?>images/received_732471710256562.jpeg" class="img-responsive hidden-sm hidden-xs" alt="Scott and Max">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--/ service-->
<!--cta-->
<section id="cta-1" class="section-padding">
    <div class="container">
        <center class="row">
            <center></center><h2  class="heading" style="font-family: awesome;">Our Team</h2></center>

        <div style="margin-top: 30px;" class="row">
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card team-member height hoverbio" style="font-size: 16px;">
                    <!--110x110-->
                    <img src="<?=base_url();?>images/Htet.JPG" style="width:150px;height:150px;" class="img-responsive" alt="Max">
                    <h3><font color="#004167">Htet</font></h3>
                    <h4 style="font-family: awesome;"> Founder, Head of Growth & Operations</h4>
                    <p class="bio" style="text-align:justify;">Htet founded MYEO as a university project whilst finishing her Bachelor degree at the University of Hong Kong in 2014. Htet founded MYEO having received a scholarship to study in Hong Kong after hearing about the opportunity from a friend and hoping MYEO could become that friend to millions around the world. In what time Htet has to spare away from MYEO she can be found jamming to techno music or reading up around the latest advancements in deep technologies but usually not at the same time ;)
                        </p>

                    <!--<div class="social-buttons">-->
                    <!--<a target="_blank" href="https://twitter.com/wholeearthweb" data-toggle="tooltip" title="Twitter" data-placement="bottom"><i class="fa fa-twitter" ariahidden></i></a>-->
                    <!--<a href="mailto:max@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>-->
                    <!--<a target="_blank" href="https://calendly.com/maxi/30min" data-toggle="tooltip" title="Meet for coffee ??" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>-->
                    <!--</div>-->
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card team-member height hoverbio">
                    <img src="<?=base_url();?>images/David.JPG" style="width:150px;height:150px;" class="img-responsive" alt="Anders">
                    <h3><font color="#004167">David</font></h3>
                    <h4 style="font-family: awesome;"> Co-founder, Head of Product & Strategy</h4>
                    <p class="bio" style="text-align:justify;font-family:'awesome';font-size: 16px;">David met Htet whilst whilst on an exchange program in Hong Kong and has worked with Htet on the journey to build MYEO ever since. Working in startups and technology with a focus on social impact since the age of 18, David combines his passion for youth development with his love of travel and high altitude mountaineering with an ultimate goal of scaling the ‘7 summits’.
                        </p>

                    <!--<div class="social-buttons">-->
                    <!--<a target="_blank" href="https://twitter.com/anderskravis" data-toggle="tooltip" title="Twitter" data-placement="bottom"><i class="fa fa-twitter" ariahidden></i></a>-->
                    <!--<a href="mailto:anders@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>-->
                    <!--<a target="_blank" href="https://calendly.com/andersfinimize" data-toggle="tooltip" title="Meet for coffee ??" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>-->
                    <!--</div>-->
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card team-member height hoverbio">
                    <img src="<?=base_url();?>images/EiSan.JPG" style="width:150px;height:150px;" class="img-responsive" alt="Max">
                    <h3><font color="#004167">Ei San</font></h3>
                   <h4 style="font-family: awesome;"> Content Creator</h4>
                    <p class="bio" style="text-align:justify;font-family: 'awesome';font-size: 16px;">Born and raised in a rural village on the Myanmar-Thai border, Ei San grasped an opportunity to pursue her studies at a college in Thailand to reach her dream of studying at an international university. Having previously worked as a translator, Ei San is now completing her studies also at the University of Hong Kong studying Journalism and Politics. Besides her passion for writing and investigative journalism, Ei San is also a pretty decent cook when she finds the time and energy!
                        </p>

                    <!--<div class="social-buttons">-->
                    <!--<a href="mailto:matt@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>-->
                    <!--<a target="_blank" href="http://calendly.com/matt-169" data-toggle="tooltip" title="Meet for coffee ??" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>          </div>-->
                </div>
            </div>
</div>
 <div style="margin-top: 30px;" class="row">
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card team-member height hoverbio">
                    <img src="<?=base_url();?>images/Dolphin.JPG" style="width:150px;height:150px;" class="img-responsive" alt="Scott">
                    <h3><font color="#004167">Dolphin</font></h3>
                    <h4 style="font-family: awesome;"> Content Creator</h4>
                    <p class="bio" style="text-align:justify;font-family: 'awesome';font-size: 16px;">Dolphin is a passionate youth leader having worked in social development since the age of 16 and now leads the content writer team at MYEO. Currently studying towards a degree in International Relations at the University of Yangon, she combines her passion for social entrepreneurship and youth empowerment with work in diplomacy on an international stage. Dolphin’s dream is to explore the world whilst helping others’ achieve their full potential.
                        </p>

                    <!--<div class="social-buttons">-->
                    <!--<a target="_blank" data-toggle="tooltip" title="Twitter" data-placement="bottom" href="https://twitter.com/scottytindle"><i class="fa fa-twitter" ariahidden></i></a>-->
                    <!--<a href="mailto:scott@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>-->
                    <!--</div>-->
                </div>
            </div>


            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card team-member height hoverbio">
                    <img src="<?=base_url();?>images/Yoon.JPG" style="width:150px;height:150px;" class="img-responsive" alt="Rehan">
                    <h3><font color="#004167" >Yoon</font></h3>
                    <h4 style="font-family: awesome;">Content Creator</h4>
                    <p class="bio" style="text-align:justify;font-family: 'awesome';font-size: 16px;">Yoon is a student majoring in International Relations at the University of Yangon having joined MYEO in 2016 after discovering and passionately believing in the mission of MYEO. Yoon has previously attended the Youth Model ASEAN Conference in Singapore, the South East Asia Youth Leadership Program in the USA and is the current President of the American Center Public Club in Yangon.
                        </p>
                    <!--<div class="social-buttons">-->
                    <!--<a href="mailto:jamie@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>-->
                    <!--<a target="_blank" href="https://calendly.com/jamiefinimize" data-toggle="tooltip" title="Meet for coffee ??" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>-->
                    <!--</div>-->
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card team-member height hoverbio">
                    <img src="<?=base_url();?>images/Hnin.JPG" style="width:150px;height:150px;" class="img-responsive" alt="Anna">
                    <h3><font color="#004167">Hnin</font></h3>
                    <h4 style="font-family: awesome;">Content Creator</h4>
                    <p class="bio" style="text-align:justify;font-family: 'awesome';font-size: 16px;">Hnin is young professional having worked across the nonprofit sector promoting vocational education through her work to address youth unemployment issues. Through her volunteer work at MYEO, she is keen to work with the youth to help build sustainable youth empowerment for themselves, overcoming various professional development challenges.
                        </p>
                    <!--<div class="social-buttons">-->
                    <!--<a href="mailto:anna@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>-->
                    <!--<a target="_blank" href="https://calendly.com/anna-51" data-toggle="tooltip" title="Meet for coffee ??" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>-->
                    <!--</div>-->
                </div>
            </div>

</div>



        </div>
    </div>
    </div>
</section>
<!--cta-->
<!--about-->
<!--<section id="about" class="section-padding">-->
<!--<div class="container">-->
<!--<div class="row">-->
<!--<div class="col-md-3 col-sm-4 col-xs-12">-->
<!--<div class="section-title">-->
<!--<h2 class="head-title lg-line">The Medilap <br>Ultimate Dream</h2>-->
<!--<hr class="botm-line">-->
<!--<p class="sec-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p>-->
<!--<a href="" style="color: #0cb8b6; padding-top:10px;">Know more..</a>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-9 col-sm-8 col-xs-12">-->
<!--<div style="visibility: visible;" class="col-sm-9 more-features-box">-->
<!--<div class="more-features-box-text">-->
<!--<div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>-->
<!--<div class="more-features-box-text-description">-->
<!--<h3>It's something important you want to know.</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud.</p>-->
<!--</div>-->
<!--</div>-->
<!--<div class="more-features-box-text">-->
<!--<div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>-->
<!--<div class="more-features-box-text-description">-->
<!--<h3>It's something important you want to know.</h3>-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud.</p>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</section>-->
<!--&lt;!&ndash;/ about&ndash;&gt;-->
<!--&lt;!&ndash;doctor team&ndash;&gt;-->
<!--<section id="doctor-team" class="section-padding">-->
<!--<div class="container">-->
<!--<div class="row">-->
<!--<div class="col-md-12">-->
<!--<h2 class="ser-title">Meet Our Doctors!</h2>-->
<!--<hr class="botm-line">-->
<!--</div>-->
<!--<div class="col-md-3 col-sm-3 col-xs-6">-->
<!--<div class="thumbnail">-->
<!--<img src="img/doctor1.jpg" alt="..." class="team-img">-->
<!--<div class="caption">-->
<!--<h3>Jessica Wally</h3>-->
<!--<p>Doctor</p>-->
<!--<ul class="list-inline">-->
<!--<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-3 col-sm-3 col-xs-6">-->
<!--<div class="thumbnail">-->
<!--<img src="img/doctor2.jpg" alt="..." class="team-img">-->
<!--<div class="caption">-->
<!--<h3>Iai Donas</h3>-->
<!--<p>Doctor</p>-->
<!--<ul class="list-inline">-->
<!--<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-3 col-sm-3 col-xs-6">-->
<!--<div class="thumbnail">-->
<!--<img src="img/doctor3.jpg" alt="..." class="team-img">-->
<!--<div class="caption">-->
<!--<h3>Amanda Denyl</h3>-->
<!--<p>Doctor</p>-->
<!--<ul class="list-inline">-->
<!--<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-3 col-sm-3 col-xs-6">-->
<!--<div class="thumbnail">-->
<!--<img src="img/doctor4.jpg" alt="..." class="team-img">-->
<!--<div class="caption">-->
<!--<h3>Jason Davis</h3>-->
<!--<p>Doctor</p>-->
<!--<ul class="list-inline">-->
<!--<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
<!--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</section>-->
<!--&lt;!&ndash;/ doctor team&ndash;&gt;-->
<!--&lt;!&ndash;testimonial&ndash;&gt;-->
<!--<section id="testimonial" class="section-padding">-->
<!--<div class="container">-->
<!--<div class="row">-->
<!--<div class="col-md-12">-->
<!--<h2 class="ser-title">see what patients are saying?</h2>-->
<!--<hr class="botm-line">-->
<!--</div>-->
<!--<div class="col-md-4 col-sm-4">-->
<!--<div class="testi-details">-->
<!--&lt;!&ndash; Paragraph &ndash;&gt;-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
<!--</div>-->
<!--<div class="testi-info">-->
<!--&lt;!&ndash; User Image &ndash;&gt;-->
<!--<a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>-->
<!--&lt;!&ndash; User Name &ndash;&gt;-->
<!--<h3>Alex<span>Texas</span></h3>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4 col-sm-4">-->
<!--<div class="testi-details">-->
<!--&lt;!&ndash; Paragraph &ndash;&gt;-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
<!--</div>-->
<!--<div class="testi-info">-->
<!--&lt;!&ndash; User Image &ndash;&gt;-->
<!--<a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>-->
<!--&lt;!&ndash; User Name &ndash;&gt;-->
<!--<h3>Alex<span>Texas</span></h3>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4 col-sm-4">-->
<!--<div class="testi-details">-->
<!--&lt;!&ndash; Paragraph &ndash;&gt;-->
<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
<!--</div>-->
<!--<div class="testi-info">-->
<!--&lt;!&ndash; User Image &ndash;&gt;-->
<!--<a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>-->
<!--&lt;!&ndash; User Name &ndash;&gt;-->
<!--<h3>Alex<span>Texas</span></h3>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</section>-->
<!--&lt;!&ndash;/ testimonial&ndash;&gt;-->
<!--&lt;!&ndash;cta 2&ndash;&gt;-->
<!--<section id="cta-2" class="section-padding">-->
<!--<div class="container">-->
<!--<div class=" row">-->
<!--<div class="col-md-2"></div>-->
<!--<div class="text-right-md col-md-4 col-sm-4">-->
<!--<h2 class="section-title white lg-line">« A few words<br> about us »</h2>-->
<!--</div>-->
<!--<div class="col-md-4 col-sm-5">-->
<!--Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a typek-->
<!--<p class="text-right text-primary"><i>— Medilap Healthcare</i></p>-->
<!--</div>-->
<!--<div class="col-md-2"></div>-->
<!--</div>-->
<!--</div>-->
<!--</section>-->
<!--&lt;!&ndash;cta&ndash;&gt;-->
<!--&lt;!&ndash;contact&ndash;&gt;-->
<!--<section id="contact" class="section-padding">-->
<!--<div class="container">-->
<!--<div class="row">-->
<!--<div class="col-md-12">-->
<!--<h2 class="ser-title">Contact us</h2>-->
<!--<hr class="botm-line">-->
<!--</div>-->
<!--<div class="col-md-4 col-sm-4">-->
<!--<h3>Contact Info</h3>-->
<!--<div class="space"></div>-->
<!--<p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i>321 Awesome Street<br> New York, NY 17022</p>-->
<!--<div class="space"></div>-->
<!--<p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i>info@companyname.com</p>-->
<!--<div class="space"></div>-->
<!--<p><i class="fa fa-phone fa-fw pull-left fa-2x"></i>+1 800 123 1234</p>-->
<!--</div>-->
<!--<div class="col-md-8 col-sm-8 marb20">-->
<!--<div class="contact-info">-->
<!--<h3 class="cnt-ttl">Having Any Query! Or Book an appointment</h3>-->
<!--<div class="space"></div>-->
<!--<div id="sendmessage">Your message has been sent. Thank you!</div>-->
<!--<div id="errormessage"></div>-->
<!--<form action="" method="post" role="form" class="contactForm">-->
<!--<div class="form-group">-->
<!--<input type="text" name="name" class="form-control br-radius-zero" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />-->
<!--<div class="validation"></div>-->
<!--</div>-->
<!--<div class="form-group">-->
<!--<input type="email" class="form-control br-radius-zero" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />-->
<!--<div class="validation"></div>-->
<!--</div>-->
<!--<div class="form-group">-->
<!--<input type="text" class="form-control br-radius-zero" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />-->
<!--<div class="validation"></div>-->
<!--</div>-->
<!--<div class="form-group">-->
<!--<textarea class="form-control br-radius-zero" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>-->
<!--<div class="validation"></div>-->
<!--</div>-->

<!--<div class="form-action">-->
<!--<button type="submit" class="btn btn-form">Send Message</button>-->
<!--</div>-->
<!--</form>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</section>-->
<!--&lt;!&ndash;/ contact&ndash;&gt;-->
<!--&lt;!&ndash;footer&ndash;&gt;-->
<!--<footer id="footer">-->
<!--<div class="top-footer">-->
<!--<div class="container">-->
<!--<div class="row">-->
<!--<div class="col-md-4 col-sm-4 marb20">-->
<!--<div class="ftr-tle">-->
<!--<h4 class="white no-padding">About Us</h4>-->
<!--</div>-->
<!--<div class="info-sec">-->
<!--<p>Praesent convallis tortor et enim laoreet, vel consectetur purus latoque penatibus et dis parturient.</p>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4 col-sm-4 marb20">-->
<!--<div class="ftr-tle">-->
<!--<h4 class="white no-padding">Quick Links</h4>-->
<!--</div>-->
<!--<div class="info-sec">-->
<!--<ul class="quick-info">-->
<!--<li><a href="index.html"><i class="fa fa-circle"></i>Home</a></li>-->
<!--<li><a href="#service"><i class="fa fa-circle"></i>Service</a></li>-->
<!--<li><a href="#contact"><i class="fa fa-circle"></i>Appointment</a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4 col-sm-4 marb20">-->
<!--<div class="ftr-tle">-->
<!--<h4 class="white no-padding">Follow us</h4>-->
<!--</div>-->
<!--<div class="info-sec">-->
<!--<ul class="social-icon">-->
<!--<li class="bglight-blue"><i class="fa fa-facebook"></i></li>-->
<!--<li class="bgred"><i class="fa fa-google-plus"></i></li>-->
<!--<li class="bgdark-blue"><i class="fa fa-linkedin"></i></li>-->
<!--<li class="bglight-blue"><i class="fa fa-twitter"></i></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="footer-line">-->
<!--<div class="container">-->
<!--<div class="row">-->
<!--<div class="col-md-12 text-center">-->
<!--© Copyright Medilab Theme. All Rights Reserved-->
<!--<div class="credits">-->
<!--&lt;!&ndash;-->
<!--All the links in the footer should remain intact.-->
<!--You can delete the links only if you purchased the pro version.-->
<!--Licensing information: https://bootstrapmade.com/license/-->
<!--Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Medilab-->
<!--&ndash;&gt;-->
<!--Designed by <a href="https://bootstrapmade.com/">BootstrapMade.com</a>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</footer>-->
<!--/ footer-->
<!-- ===========PAGE FOOTER=========== -->
 <!-----------------------TKH forgot Model------------------->
<div class="modal " id="myModal">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <center><h3 class="modal-title"> Reset Your Password</h3></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/ForgotPassword', 'class="form-inline" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-4" style="margin-top:10px;">Email:</label>
            <div class="col-md-4">
               <input type="email" class="form-control" name="email" placeholder="Enter your Email..." required>            
            </div>
           
       </div>
       
       <div class="form-group">
        <div class="col-md-offset-5 col-md-4">
           <input type="submit" name="submit" class="form-control btn btn-success" value="Change">
         </div>
       </div>
     <?php echo form_close();?>
    
    </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>
    </div>
  </div>
</div>

 <!--end of forgot model-->     

 <script src="<?php echo base_url();?>/template/footer/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>/template/footer/js/jquery.easing.min.js"></script>
<!-- <script src="<?php echo base_url();?>/template/footer/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url();?>/template/footer/js/custom.js"></script>
<script src="contactform/contactform.js"></script>


 

</body></html>
