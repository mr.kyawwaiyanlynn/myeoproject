
<!DOCTYPE html>
<html class="" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us | Finimize</title>
    <!--
    / _(_)_ __ (_)_ __ ___ (_)_______
    | |_| | '_ \| | '_ ` _ \| |_  / _ \
    |  _| | | | | | | | | | | |/ /  __/
    |_| |_|_| |_|_|_| |_| |_|_/___\___|

    Oh hello there, it looks like you're into tech. Lucky for you we're hiring! Check out finimize.com/jobs
    PS: We value diversity - tabs and spaces are both considered with equal opportunity.
  ``-->
    <!-- SEO Info -->
    <meta name="description" content="Finimize: We&#39;re making finance accessible for our generation"/>
    <meta name="robots" content="noodp"/>
    <link rel="canonical" href="https://www.finimize.com/about" />
    <link rel="canonical" href="http://finimize.com/about" />
    <link rel="canonical" href="http://www.finimize.com/about" />
    <!-- other meta -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#00b2ff">
    <meta name="theme-color" content="#ffffff">
    <!-- Social Info -->
    <!-- Facebook -->
    <meta property="og:locale" content="en_GB" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Finimize: About Us" />
    <meta property="og:description" content="Finimize: We&#39;re making finance accessible for our generation" />
    <meta property="og:url" content="http://www.finimize.com/about" />
    <meta property="og:site_name" content="Finimize" />
    <meta property="og:image" content="http://finimize.com/img/Finimize_ShareImage.jpg">
    <meta property="og:image:type" content="image/png">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Finimize: We&#39;re making finance accessible for our generation" />
    <meta name="twitter:title" content="Finimize: About Us" />
    <meta name="twitter:site" content="@finimize" />
    <meta name="twitter:creator" content="@finimize" />
    <meta name="twitter:image" content="http://finimize.com/img/Finimize_ShareImage.jpg">
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Finimize">
    <meta itemprop="description" content="Finimize: We&#39;re making finance accessible for our generation">
    <meta itemprop="image" content="http://finimize.com/img/Finimize_ShareImage.jpg">
    <!-- End SEO / Social Info -->
    <!-- End Initial Scripts -->
    <!-- Finimize Stylesheets -->
    <link rel="stylesheet" href="/css/styles.min.css">
    <!-- End Stylesheets -->
    <!-- Fonts & Icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
    <!-- End Fonts & Icons -->



    <!--[if lt IE 9]>
    <script src="bower_components/html5shiv/dist/html5shiv.js"></script>
    <![endif]-->

    <!-- Initial Load Scrips -->
    <!-- jQuery -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Finimize JavaScript -->
    <script src="/js/fmz_js.js"></script>


    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script src="/js/cookie.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body id="about">


<nav class="navbar navbar-default navbar-fixed-top navbar-custom" role="navigation">
  <div class="container">

  <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
            <i class="fa fa-bars"></i>
        </button>
        <!--Finimize Logo-->
        <a class="navbar-brand" href="/">
          <div>
            <svg class="finimizelogo" id="finimizelogo" data-name="finimizelogo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3728.24 835.75"><title>Finimize</title><path d="M438.1,513.95H323.37a1.47,1.47,0,0,0-1.47,1.47V937.16a1.47,1.47,0,0,1-1.47,1.47H222.82a1.47,1.47,0,0,1-1.47-1.47V515.42a1.47,1.47,0,0,0-1.47-1.47H122.26a1.47,1.47,0,0,1-1.47-1.47V433.05a1.47,1.47,0,0,1,1.47-1.47h97.61a1.47,1.47,0,0,0,1.47-1.47V325.68q0-44.93,10.16-82.9T264.67,177q23-27.8,58.83-43.86t87.18-16q35.51,0,61.87,7.15a1.47,1.47,0,0,1,1.07,1.63l-11.37,83.71a1.47,1.47,0,0,1-1.83,1.22q-11-2.92-21.92-4.91a151.1,151.1,0,0,0-26.74-2.14q-28.88,0-46.53,10.16a71.74,71.74,0,0,0-27.28,27.28,112.09,112.09,0,0,0-12.84,39,313.88,313.88,0,0,0-3.21,45.46V430.11a1.47,1.47,0,0,0,1.47,1.47H438.1a1.47,1.47,0,0,1,1.47,1.47v79.43A1.47,1.47,0,0,1,438.1,513.95Z" transform="translate(-120.79 -117.08)"/><path d="M678,268.36q0,27.83-19.64,46t-47.22,18.19q-27.58,0-46.68-18.72t-19.1-45.46q0-27.8,19.1-46.53t46.68-18.72q27.58,0,47.22,18.72T678,268.36Z" transform="translate(-120.79 -117.08)"/><rect x="440.59" y="314.5" width="100.55" height="507.05" rx="1.47" ry="1.47"/><path d="M891.23,431.58a1.47,1.47,0,0,1,1.47,1.31q2,18.76,3.07,42,1,23,1.07,38.66A1.46,1.46,0,0,0,898.3,515h0.51a2,2,0,0,0,1.77-1.11A148.14,148.14,0,0,1,926.25,477a189.73,189.73,0,0,1,38-31,187.48,187.48,0,0,1,47.07-20.86,190.39,190.39,0,0,1,53.49-7.49q49.19,0,84.51,16.58t58.84,44.39q23.52,27.83,34.77,65.25t11.23,80.23v313a1.47,1.47,0,0,1-1.47,1.47H1155a1.47,1.47,0,0,1-1.47-1.47V657.29a345,345,0,0,0-4.81-58.84q-4.81-27.8-17.65-49.21A95.76,95.76,0,0,0,1095.8,515q-22.46-12.84-57.77-12.84-59.92,0-97.88,45.46t-38,121.41v268.1a1.47,1.47,0,0,1-1.47,1.47H803.1a1.47,1.47,0,0,1-1.47-1.47V540.69q0-20.31-1.07-52.42-1-31.07-3.07-55.11a1.46,1.46,0,0,1,1.47-1.58h92.28Z" transform="translate(-120.79 -117.08)"/><path d="M1496.55,268.36q0,27.83-19.64,46t-47.22,18.19q-27.58,0-46.68-18.72t-19.1-45.46q0-27.8,19.1-46.53t46.68-18.72q27.58,0,47.22,18.72T1496.55,268.36Z" transform="translate(-120.79 -117.08)"/><rect x="1259.16" y="314.5" width="100.55" height="507.05" rx="1.47" ry="1.47"/><path d="M1773.86,446q42.25-28.33,100-28.35,51.35,0,92.53,23.53,40.11,22.94,63.5,75.32a1.47,1.47,0,0,0,2.64.1q25.51-47.24,70.25-72.74,46-26.2,100.56-26.21,49.19,0,84.51,16.58t58.84,44.39q23.52,27.83,34.77,65.25t11.23,80.23v313a1.47,1.47,0,0,1-1.47,1.47h-97.61a1.47,1.47,0,0,1-1.47-1.47V657.29a311.76,311.76,0,0,0-5.35-58.84q-5.36-27.8-18.18-49.21A98.44,98.44,0,0,0,2234.38,515q-21.41-12.84-54.56-12.84-34.25,0-58.84,14.44a122.22,122.22,0,0,0-40.11,37.44,158,158,0,0,0-22.46,51.88,250,250,0,0,0-7,58.84V937.16a1.47,1.47,0,0,1-1.47,1.47h-97.61a1.47,1.47,0,0,1-1.47-1.47V637a257.77,257.77,0,0,0-5.35-54.56q-5.36-24.6-18.18-42.26a90.31,90.31,0,0,0-33.16-27.81q-20.33-10.15-50.28-10.16-55.63,0-92.53,45.46t-36.91,121.41v268.1a1.47,1.47,0,0,1-1.47,1.47h-97.61a1.47,1.47,0,0,1-1.47-1.47V540.69q0-20.31-1.07-52.42-1-31.07-3.07-55.11a1.46,1.46,0,0,1,1.47-1.58h92.28a1.47,1.47,0,0,1,1.47,1.31q2,18.76,3.07,42,1,23,1.07,38.66a1.46,1.46,0,0,0,1.47,1.46h0.51a2,2,0,0,0,1.77-1.11Q1732.19,474,1773.86,446Z" transform="translate(-120.79 -117.08)"/><path d="M2623.32,268.36q0,27.83-19.64,46t-47.22,18.19q-27.58,0-46.68-18.72t-19.1-45.46q0-27.8,19.1-46.53t46.68-18.72q27.58,0,47.22,18.72T2623.32,268.36Z" transform="translate(-120.79 -117.08)"/><rect x="2385.93" y="314.5" width="100.56" height="507.05" rx="1.47" ry="1.47"/><path d="M3125.25,514.36L2838.83,851.7a1.47,1.47,0,0,0,1.12,2.42H3135.9a1.47,1.47,0,0,1,1.47,1.47v81.56a1.47,1.47,0,0,1-1.47,1.47H2714.16a1.47,1.47,0,0,1-1.47-1.47V856.8a1.47,1.47,0,0,1,.34-0.95l284.32-339.48a1.47,1.47,0,0,0-1.13-2.42H2728.07a1.47,1.47,0,0,1-1.47-1.47V433.05a1.47,1.47,0,0,1,1.47-1.47h396.06a1.47,1.47,0,0,1,1.47,1.47v80.36A1.47,1.47,0,0,1,3125.25,514.36Z" transform="translate(-120.79 -117.08)"/><path d="M3675,678.79v17a138.2,138.2,0,0,1-.91,15.68,1.47,1.47,0,0,1-1.46,1.3H3275.33a1.47,1.47,0,0,0-1.47,1.53,148.26,148.26,0,0,0,14.27,58,156.64,156.64,0,0,0,35.56,48.5,166.74,166.74,0,0,0,113.57,43.73q51,0,88.1-22.29,36.38-21.83,57.5-52.83a1.45,1.45,0,0,1,2.11-.32l67.71,54.38a1.47,1.47,0,0,1,.25,2q-40.07,52.62-94.13,77.62-54.68,25.25-121.54,25.26-57.32,0-106.14-19.11t-83.85-53.6q-35-34.48-55.19-83.32T3171.91,684.1q0-58.36,19.67-107.74T3246.36,492q35.1-35,83-54.66t103.17-19.64q55.3,0,100.52,18t76.59,52Q3641,521.71,3658,570T3675,678.79Zm-100.31-38.61a1.48,1.48,0,0,0,1.48-1.54A191.33,191.33,0,0,0,3566.1,584a129.06,129.06,0,0,0-26.74-45.46q-17.65-19.25-43.86-30.49T3434,496.83a162.52,162.52,0,0,0-60.44,11.23,156.91,156.91,0,0,0-49.21,30.49A160.55,160.55,0,0,0,3290.11,584a144.65,144.65,0,0,0-15.39,54.56,1.48,1.48,0,0,0,1.48,1.6h298.52Z" transform="translate(-120.79 -117.08)"/><path d="M3849,888.65q0,27.83-19.64,46t-47.22,18.19q-27.58,0-46.68-18.72t-19.1-45.46q0-27.8,19.1-46.53t46.68-18.72q27.58,0,47.22,18.72T3849,888.65Z" transform="translate(-120.79 -117.08)"/>
            </svg>
            <img class="finimizelogo-s" src="/img/finbills.png" width="56" alt="Finbills">
          </div>
        </a>
      </div> <!-- End Navbar Header -->

     <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav right-links">
                    <li class="mylifelink">
                        <a class="main-link" href="/mylife"><i class="fa fa-bolt mylifenavdot "></i> MyLife</a>
                    </li>
                    <li class="">
                        <a class="main-link" href="/wp">NEWS</a>
                    </li>
                      <li class="">
                        <a class="main-link" href="/about">About</a>
                    </li>
                    <li class="hidden-md hidden-sm">
                        <a class="main-link" href="http://insider.finimize.com/join">Insiders</a>
                    </li>
                    <li class="hidden-xs">  <a href="#" class="navsignup" data-toggle="modal" data-target="#signupmodal">Sign up</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<img src="/img/aboutusbg.jpg" style="display:none;">

<section class="mission">
  <div class="container">
    <h2 class="heading">We're making finance accessible for our generation.</h2>
    <p>Our generation enjoys a high degree of education and is lucky to be fairly well off. But there’s an elephant in the room that finally needs to be addressed: we’re quietly struggling with financial literacy.</p>
    <a href="#" data-toggle="modal" data-target="#manifestoModal">Read our manifesto 🙌</a>
  </div>

  <div id="angle">
    <svg viewBox="0 0 796 116" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <polygon id="Rectangle" points="0 0 398 58 796 0 796 116 0 116"></polygon>
    </svg>
  </div>
</section>

<section class="story">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-push-6 right">
        <h2 class="heading">Our Story</h2>
        <p>We’re building Finimize to solve a massive problem: we want to help people get their personal finances in order – not just savings, not just investments. But everything. Your whole financial life.</p>

        <p>It all started when we had some savings ourselves and couldn’t figure out what to do with them. Everyone tells you to budget and save. But no one tells you what to do after that.</p>

        <p>No one explained to you why interest rates matter, how you’re supposed to “diversify” your finances, or what an “emergency fund” is.</p>

        <p>So we decided we would start with the basics, and launched a daily email to explain these concepts in simple terms as they coincide with financial news.</p>

        <p>Since then we have expanded our team, built <a href="/mylife">Finimize MyLife</a> (your free personal financial guide) and grown our community to over 100,000 Finimizers!</p>

        <p>We want to become the top of mind brand for finance – similar to how Amazon is the top of mind for shopping.</p>

        <p>Let's get there together.&nbsp;💸</p>

      </div>

      <div class="col-md-6 col-md-pull-6 left">
        <div class="row image-grid">
          <div class="col-sm-12">
            <img src="./img/story-1.jpg" class="img-responsive" alt="Scott and Max">
          </div>
          <div class="col-sm-6 left">
            <img src="./img/story-2.jpg" class="img-responsive hidden-sm hidden-xs" alt="Scott and Max">
          </div>
          <div class="col-sm-6 right">
            <img src="./img/story-3.jpg" class="img-responsive hidden-sm hidden-xs" alt="Scott and Max">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="team">
  <div class="container">
    <h2 class="heading">Our Team</h2>

    <div class="row">
      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height hoverbio">
          <img src="./img/FinimizeTeam_Max.png" class="img-responsive" alt="Max">
          <h3>Max</h3>
          <h4>✌️ Founder + CEO</h4>
          <p class="bio">Before Finimize, Max co-founded an ecommerce startup with 200 employees and sold it in 2015. Now he spends his days leading our strategy and basically doing every job you can think of. He also mentors at Techstars and Startup Bootcamp and was named one of Forbes "30 under 30" for 2016.</p>

          <div class="social-buttons">
            <a target="_blank" href="https://twitter.com/wholeearthweb" data-toggle="tooltip" title="Twitter" data-placement="bottom"><i class="fa fa-twitter" ariahidden></i></a>
            <a href="mailto:max@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
            <a target="_blank" href="https://calendly.com/maxi/30min" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height hoverbio">
          <img src="./img/FinimizeTeam_Anders.png" class="img-responsive" alt="Anders">
          <h3>Anders</h3>
          <h4>⚡️ Head of Product</h4>
          <p class="bio">Originally a Finimize user, Anders has designed FinTech products for Fortune 500 clients. He oversees our design and can be found jumping between Sketch, code, and the mug of coffee on his&nbsp;desk.</p>

          <div class="social-buttons">
            <a target="_blank" href="https://twitter.com/anderskravis" data-toggle="tooltip" title="Twitter" data-placement="bottom"><i class="fa fa-twitter" ariahidden></i></a>
            <a href="mailto:anders@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
            <a target="_blank" href="https://calendly.com/andersfinimize" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height hoverbio">
          <img src="./img/FinimizeTeam_Matt.png" class="img-responsive" alt="Max">
          <h3>Matt</h3>
          <h4>💻 Tech Lead</h4>
          <p class="bio">Matt previously worked at IBM iX before joining Finimize. He's in charge of pushing code and oversees all our tech. He's the only one of us with an actual British accent – and the only one with a patent under&nbsp;his belt.</p>

          <div class="social-buttons">
            <a href="mailto:matt@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
            <a target="_blank" href="http://calendly.com/matt-169" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height hoverbio">
          <img src="./img/FinimizeTeam_Scott.png" class="img-responsive" alt="Scott">
          <h3>Scott</h3>
          <h4>✏️ Editorial</h4>
          <p class="bio">Originally a university friend of Max, Scott is a CFA &reg; charterholder with 7 years of experience at Barclays Capital under his belt.</p>

          <div class="social-buttons">
            <a target="_blank" data-toggle="tooltip" title="Twitter" data-placement="bottom" href="https://twitter.com/scottytindle"><i class="fa fa-twitter" ariahidden></i></a>
            <a href="mailto:scott@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
        </div>
      </div>
    </div>


      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height hoverbio">
          <img src="./img/FinimizeTeam_Jamie.png" class="img-responsive" alt="Rehan">
          <h3>Jamie</h3>
          <h4>📡 Editor + Comms</h4>
          <p class="bio">A proud son of Scotland, Jamie heroically sets aside his dislike of American orthography to lead on editing Finimize’s output each day. He’s also the first point of contact for any media enquiries/free beer giveaways.</p>

          <div class="social-buttons">
            <a href="mailto:jamie@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
            <a target="_blank" href="https://calendly.com/jamiefinimize" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height hoverbio">
          <img src="./img/FinimizeTeam_Anna.png" class="img-responsive" alt="Anna">
          <h3>Anna</h3>
          <h4>👋 Community</h4>
          <p class="bio">Originally from rural Wisconsin, Anna oversees community and user engagement. You can find her on Intercom and Slack, analyzing surveys and planning the next awesome event.</p>

          <div class="social-buttons">
            <a href="mailto:anna@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
            <a target="_blank" href="https://calendly.com/anna-51" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>
          </div>
        </div>
      </div>


      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height"/>
          <img src="./img/FinimizeTeam_Michael.png" class="img-responsive" alt="Michael">
          <h3>Michael</h3>
          <h4>👾 Full-Stack Engineer</h4>
          <div class="social-buttons">
						<a target="_blank" href="https://twitter.com/michaelgunnulf" data-toggle="tooltip" title="Twitter" data-placement="bottom"><i class="fa fa-twitter" ariahidden></i></a>
            <a href="mailto:michael@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
						<a target="_blank" href="https://calendly.com/finimize-michael" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height"/>
          <img src="./img/FinimizeTeam_Florian.png" class="img-responsive" alt="Florian">
          <h3>Florian</h3>
          <h4>👾 FULL-STACK ENGINEER</h4>
          <div class="social-buttons">
						<a href="mailto:florian@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
						<a target="_blank" href="https://calendly.com/fmoga" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>
				 </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member height"/>
          <img src="./img/FinimizeTeam_Jack.png" class="img-responsive" alt="Jack">
          <h3>Jack</h3>
          <h4>🦋 UX / UI Designer</h4>
          <div class="social-buttons">
            <a href="mailto:jack@finimize.com" data-toggle="tooltip" title="Email" data-placement="bottom"><i class="fa fa-paper-plane" ariahidden></i></a>
            <a target="_blank" href="https://calendly.com/jacktw" data-toggle="tooltip" title="Meet for coffee ☕️" data-placement="bottom"><i class="fa fa-coffee" ariahidden></i></a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member" style="min-height:100px">
          <img src="./img/FinimizeTeam_Andrew.png" class="img-responsive" alt="Andrew">
          <h3>Andrew</h3>
          <h4>🌱 Growth</h4>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member" style="min-height:100px">
          <img src="./img/FinimizeTeam_Hayley.png" class="img-responsive" alt="Simon">
          <h3>Hayley</h3>
          <h4>✍️ Editor</h4>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-4">
        <div class="card team-member" style="min-height:100px">
          <img src="./img/FinimizeTeam_Alex.png" class="img-responsive" alt="Alex">
          <h3>Alex</h3>
          <h4>✍️ Writer</h4>
        </div>
      </div>

    </div>
  </div>

</section>

<section class="investors">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-4">
            <h2 class="heading">Our Investors</h2>
            <p>We’re backed by a team with a track record of building successful companies and a wealth of knowledge in the FinTech sector. They’re also pretty great people.</p>
          </div>

          <div class="col-sm-6 col-md-6 col-lg-4">
            <a class="card-link" href="http://www.passioncapital.com/" target="_blank">
              <div class="card investor">
                <div class="card-left">
                  <img src="./img/passion.png" width="120" alt="Passion Capital">
                </div>

                <div class="card-body">
                  <h3>Passion Capital</h3>
                  <p class="bio">Passion Capital is a London-based early stage venture capital firm started by Eileen Burbidge, Stefan Glänzer and Robert Dighero. They have invested in many creative, product-driven startups and are regarded as one of the leading VCs in Europe.</p>
                </div>
              </div>
            </a>
          </div>

          <div class="col-sm-6 col-md-6 col-lg-4">
            <a class="card-link" href="https://twitter.com/jbthewealthman" target="_blank">
              <div class="card investor">
                <div class="card-left">
                  <img src="./img/jason.jpg" width="120" alt="Jason Butler">
                </div>

                <div class="card-body">
                  <h3>Jason Butler</h3>
                  <p class="bio">Jason is a Chartered and Certified Financial Planner and author of <i>The Financial Times Guide to Wealth Management</i>. He’s been voted one of the top 100 financial advisors in the UK.</p>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="community">
  <div class="container">
    <h2 class="heading">Our Community</h2>
    <p>Our mission is too big for us to be able to achieve it ourselves. Our community is at the center of what we’re doing. They’re global, they’re powerful, and they’re ready for some change.</p>
    <div class="map-container">
      <img src="./img/map.png" class="img-responsive map" alt="World Map">
      <img src="./img/avatars/user-1-europe.png" width="55" style="top: 28%; left: 43%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-2-safrica.png" width="50" style="top: 72%; left: 53%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-3-europe.png" width="55" style="top: 31%; left: 52%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-4-europe.png" width="50" style="top: 52%; left: 77%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-5-na.png" width="55" style="top: 10%; left: 14%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-6-cali.png" width="60" style="top: 27%; left: 12%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-7-europe.png" width="55" style="top: 10%; left: 63%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-8-na.png" width="55" style="top: 19%; left: 29%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-9-europe.png" width="55" style="top: 11%; left: 47%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-10-usa.png" width="55" style="top: 36%; left: 20%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-11-sa.png" width="55" style="top: 51%; left: 27%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-12-usa.png" width="55" style="top: 20%; left: 20%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-13-europe.png" width="55" style="top: 20%; left: 56%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-14-lagos.png" width="55" style="top: 47%; left: 45%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-15-usa.png" width="55" style="top: 13%; left: 5%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-16-australia.png" width="45" style="top: 75%; left: 85%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-17-europe.png" width="55" style="top: 23%; left: 85%;" class="avatar" alt="Avatar">
      <img src="./img/avatars/user-18-europe.png" width="45" style="top: 40%; left: 60%;" class="avatar" alt="Avatar">
    </div>
  </div>
</section>

<section class="location">
  <div class="container">
    <img src="./img/london-icons.svg" alt="London Icons">
    <p><span>Want to say hey?</span> We're located at 144a Clerkenwell Road, London, UK.</p>
  </div>
</section>

<!-- Modal -->
<div class="modal fade" id="manifestoModal" tabindex="-1" role="dialog" aria-labelledby="manifestoModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="manifestoModalLabel">Our Manifesto</h4>
      </div>
      <div class="modal-body">
        <p>We’re not here to bash banks or to stick it to the man.</p>
        <p>We just want to solve a problem that we ourselves encountered.</p>
        <p>We want to help people get their personal finances in order.</p>
        <p>Not just savings, not just investments. But everything. Your whole financial life.</p>
        <p>That’s why we’re providing free, fully digital financial plans. Something that usually would cost you at least a few grand.</p>
        <p>We fully realize that financial planning is an immensely dry and boring subject.</p>
        <p>That’s why we want to take out as much friction as possible for you. Cut to the chase while maintaining the substance.</p>
        <p>We don’t want you to follow our guidance blindly. So we do our best to explain to you *why* it makes sense to set your financial life up in a certain way. But ultimately we let you decide what you want to do with your money. We’re just here to help.</p>
        <p>Let’s get this done together. We have a magnificent opportunity to improve financial literacy.</p>
        <p>We’re already more than 100,000 Finimizers and growing rapidly.</p>
        <p>Here’s to our journey of making finance more accessible. 🚀</p>

        <img width="200" src="img/TeamSignatures.jpg" class="img-responsive signatures" alt="Signatures">
      </div>
    </div>
  </div>
</div>

<!-- //// FOOTER //// -->
<!--  =========  SIGN UP MODAL  =========  -->
  <div class="modal fade" id="signupmodal" tabindex="-1" role="dialog" aria-labelledby="signupmodal">
  <div class="modal-dialog" role="document">
  <!-- Modal Content -->
        <div class="modal-content animated SlideInDown" id="fmz-signup">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Sign Up For Finimize</h4>
        </div>
        <!-- Modal Body -->
        <div class="modal-body">
          <!-- SIGN UP FORM -->
          <form action="/inc/mailchimp.php" method="post" data-parsley-validate data-parsley-errors-messages-disabled="true" id="signupmodal_form">
					<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <label><span class="sr-only">Email</span></label>
                <input name="email" data-parsley-type="email" data-parsley-type-message="hmm... that doesn't look like a valid email" data-parsley-required-message="We need an email to reach you at"  data-parsley-maxlength="42" id="signupform_email" type="email" class="signupform_email" required placeholder="Your email"/>
                </div>
					</div>
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <label><span class="sr-only">First Name</span></label>
              <input required type="text" data-parsley-required-message="What should we call you?" data-parsley-maxlength="42" name="fname" placeholder="First name" class=" fname" id="signupform_fname"/>
            </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
                <label><span class="sr-only">Last Name</span></label>
            <input required type="text" class="" data-parsley-required-message="What people called you in Gym class" data-parsley-maxlength="42" name="lname" placeholder="Last name" />
          </div>
        </div>
  <div class="row">
    <div class="col-xs-12 col-md-12">

            <p class="editions"> <span id="edition-description">The Global Edition sends out at 12 am GMT and covers a global perspective.</span><a target="_blank" alt="More info on editions" href="/wp/editions" class="information"><i class="fa fa-question-circle"></i></a></p>
            <select id="edition-select" name="edition" class="form-control" placeholder="Select Your Edition" />
              <option value="Global Edition">🗺 Global Edition</option>
              <option value="American Edition">🌎 American Edition</option>
              <option value="European Edition">🌍 European / UK Edition</option>
            </select>

    </div>
  </div>
  <input type="hidden" name="refer" value="finimize.com/about" />


        <div class="modal-footer">
          <button class="submit btn btn-primary btn-block" type="submit"/>Sign Up 😃</button>
            </form>
            <!-- END SIGN UP FORM -->

          <p class="terms">By signing up you agree to our <a target="_blank" href="/wp/terms/"> terms & conditions</a>. </p>
        </div>
      </div> <!-- End Modal Body -->
    </div> <!-- End Modal Content -->
  </div>
  </div>
<!--  =========  END SIGN UP MODAL =========  -->

<!-- Parsley JS -->
<script src="/bower_components/parsleyjs/dist/parsley.min.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        // instanciate parsley and set the container
        // as the element title without a wrapper
        $("#signupmodal_form").parsley({
            errorsContainer: function (ParsleyField) {
                return ParsleyField.$element.attr("title");
            },
            errorsWrapper: false
        });

        // when there is an error, display the tooltip with the error message
        $.listen('parsley:field:error', function(fieldInstance) {
            var messages = ParsleyUI.getErrorsMessages(fieldInstance);
            fieldInstance.$element.tooltip('destroy');
            fieldInstance.$element.tooltip({
                animation: false,
                container: 'body',
                placement: 'bottom',
                title: messages
            });
        });

        // destroy tooltip when field is valid
        $.listen('parsley:field:success', function(fieldInstance) {
            fieldInstance.$element.tooltip('destroy');
        });
    });

        $(document).on("change", "#edition-select", function() {
          var edition = $("#edition-select").val();
          var send = "at 12 am GMT"
          if(edition !== "Global Edition") {
            send = "in the morning"
          };
           $("#edition-description").text('The ' + edition + ' sends out ' + send + ' and covers a global perspective.');
      });
</script>
<footer class="main-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="list-inline footerlinks">
          <li class="finicon"><a href="/"><img class="" src="/img/fin_icon_black.png"></a></li>
          <li><a href="https://medium.com/finimize">Blog</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="http://insider.finimize.com/join">Insiders</a></li>
          <li><a href="mailto:help@finimize.com">Help</a></li>
          <li style="position:relative;">
            <a href="/jobs">Jobs
              <div class="yourluckyday show-for-large show-for-xlarge" style="">
                <i class="fa fa-pied-piper-alt" aria-hidden="true"></i> Hiring
              </div>
            </a>
          </li>
          <li class="appiconholder">
            <a href="/app/ios" onClick="ga('send', 'event', { eventCategory: 'button', eventAction: 'click', eventLabel: 'get the app', eventValue: 1});"><img class="appbutton" src="/img/gettheappbutton.png"></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>
<!-- Subfooter -->
<div class="subfooter">
  <div class="container">
    <div class="row">
      <div class="col-md-6 legal">
        <span class="copyright">Copyright &copy; Finimize 2017</span>
        <br> <span class="sosume"> All content provided by the Finimize Ltd. is for informational and educational purposes only and is not meant to represent trade or investment recommendations. (Our lawyers make us say this)</span>
      </div>
      <div class="col-md-6">
        <ul class="list-inline social-buttons ">
          <li><a href="https://twitter.com/finimize" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="http://snapchat.com/add/finimize" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
          <li><a href="https://www.facebook.com/finimize/" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://www.linkedin.com/company/finimize/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        </ul>
        <ul class="list-inline quicklinks">
          <li><a href="/wp/privacy-policy/">Privacy Policy</a></li>
          <li><a href="/wp/terms/">Terms of Use</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- //// End FOOTER //// -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    // $('.height').matchHeight();
    $('.image-grid img').matchHeight();
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"87ac11f528","applicationID":"79388827","transactionName":"NQdaMEtSVhVYU0FQXwxNbRZQHFkEVkVBF0AKEg==","queueTime":0,"applicationTime":1,"atts":"GUBZRgNIRRs=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>
