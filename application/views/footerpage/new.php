<html>
<head>
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/icon" href="<?=base_url();?>images/myeo.jpg">
    <title>MYEO</title>
    <link rel="stylesheet" href="<?php base_url() ?>footerpage/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=awesome' >
    <!-- end of code by mk -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>

  * 
  {
    box-sizing:border-box;
    font-family: 'awesome';

  }
/*body {
  margin: 0;
  
  height: 100vh;
  display: -webkit-flex;*/
  /*display: flex;*/
  /*-webkit-align-items: center; */
  /*align-items: center;*/
/*}*/
.container1{
  /*width: 600px;*/
  /*height: 340px;*/
  /*margin: 0 auto; */
  /*margin:10px;*/
  /*position: relative;*/
  -webkit-perspective: 1000;
  -moz-perspective: 1000;
  perspective: 1000;
  -moz-transform: perspective(1400px);
  -ms-transform: perspective(1400px);
  -webkit-transform-style: preserve-3d;
  -moz-transform-style: preserve-3d; 
  transform-style: preserve-3d;
  -webkit-perspective-origin: right;
  -moz-perspective-origin: right;
  perspective-origin: right;
}
.card {
  width: 400px;
  height: 340px;
  box-shadow: 0 27px 55px 0 rgba(0, 0, 0, .7), 0 17px 17px 0 rgba(0, 0, 0, .5);
  position: relative; 
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -ms-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transform-origin: 100% 0%;
  -moz-transform-origin: 100% 0%;
  -ms-transform-origin: 100% 0%;
  transform-origin: 100% 0%;
  -webkit-transform-style: preserve-3d;
  -moz-transform-style: preserve-3d; 
  transform-style: preserve-3d;
  transition: .8s ease-in-out;
}

/*.front, .back {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: white;
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility: hidden;
  backface-visibility: hidden;
}
.front {
  display:-webkit-flex;
  display: flex;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-align-items: center;
  align-items: center;
  z-index: 2;
  -webkit-transform: rotateY(0deg);
  -moz-transform: rotateY(0deg);
  -ms-transform: rotateY(0deg);
  transform: rotateY(0deg);
}
.back {
  -webkit-transform: rotateY(-180deg);
  -moz-transform: rotateY(-180deg);
  -ms-transform: rotateY(-180deg);
  transform: rotateY(-180deg);
  font-family: 'Arimo', sans-serif;
}*/
.container1:hover .card {
  -webkit-transform: rotateY(180deg) translateX(100%);
  -moz-transform: rotateY(180deg) translateX(100%);
  -ms-transform: rotateY(180deg) translateX(100%);
  transform: rotateY(180deg) translateX(100%);
  cursor: pointer;
}
/*ul {
  margin: 0;
  width: 100%;
  list-style: none;
  position: absolute;
  bottom: 30px;
  left: 0;
  padding: 0 1%;
}
ul:after {
  content: '';
  display: table;
  clear: both;
}

h1 span {
  color: #4E203C;
  display: block;
  font-size: .45em;
  letter-spacing: 3px;
}
h1 i {
  font-style: normal;
  text-transform: none;
  font-family: 'Playfair Display', serif;
}*/
.card-front .layer h1 {
  font-size: 32px;
  font-weight: 400;
  letter-spacing: -2px;
  margin: 0 auto;
  padding: 6px 18px 4px;
  text-align: center;
}
.card-front .layer .corner {
  -webkit-backface-visibility: hidden;
          backface-visibility: hidden;
  border-right: 2px solid #d4cd96;
  border-top: 2px solid #d4cd96;
  height: 12px;
  position: absolute;
  width: 12px;
}
.card-front .layer .corner:nth-of-type(1) {
  right: 0;
  top: 0;
}
.card-front .layer .corner:nth-of-type(2) {
  left: 0;
  top: 0;
  -webkit-transform: rotateZ(-90deg);
          transform: rotateZ(-90deg);
}
.card-front .layer .corner:nth-of-type(3) {
  bottom: 0;
  left: 0;
  -webkit-transform: rotateZ(180deg);
          transform: rotateZ(180deg);
}
.card-front .layer .corner:nth-of-type(4) {
  bottom: 0;
  right: 0;
  -webkit-transform: rotateZ(90deg);
          transform: rotateZ(90deg);
}
.card-back .layer h2, .card-back .layer h3, .card-back .layer h4 {
  font-weight: 400;
  margin: 2px 0;
}
.card-back .layer h2 {
  font-size: 18px;
  font-weight: 500;
  text-transform: uppercase;
}
.card-back .layer h3 {
  font-size: 16px;
}
.card-back .layer h4 {
  font-size: 16px;
  font-style: italic;
}

.card-front .layer {
  -webkit-backface-visibility: hidden;
          backface-visibility: hidden;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  height: 100%;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  position: relative;
  width: 100%;
  /*background: #fff;*/
}
.card-front .layer h1 {
  font-size: 32px;
  font-weight: 400;
  letter-spacing: -2px;
  margin: 0 auto;
  padding: 6px 18px 4px;
  text-align: center;
  color:#fff;
}
.card-front,
.card-back {
  -webkit-backface-visibility: hidden;
          backface-visibility: hidden;
  background: repeating-linear-gradient(135deg, rgba(0, 0, 0, 0.3), transparent 1px, rgba(0, 0, 0, 0.3) 2px);
  background-size: 3px 3px;
  background-color: #302f34;
  border-radius: 1px;
  -webkit-box-shadow: 0px -6px 8px 0px rgba(0, 0, 0, 0.1), 0px 6px 8px 0px rgba(0, 0, 0, 0.1), 6px 0px 8px 0px rgba(0, 0, 0, 0.1), -6px 0px 8px 0px rgba(0, 0, 0, 0.1);
          box-shadow: 0px -6px 8px 0px rgba(0, 0, 0, 0.1), 0px 6px 8px 0px rgba(0, 0, 0, 0.1), 6px 0px 8px 0px rgba(0, 0, 0, 0.1), -6px 0px 8px 0px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  height: 100%;
  left: 0;
  padding: 5%;
  position: absolute;
  top: 0;
  width: 100%;

}

.card-back {
  -webkit-transform: rotateY(180deg);
          transform: rotateY(180deg);
}
.card-back .layer {
  /*background: repeating-linear-gradient(135deg, rgba(76, 71, 31, 0.3), transparent 1px, rgba(76, 71, 31, 0.3) 2px);*/
  background-size: 3px 3px;
  background-color: #fff;
  color: #302f34;
  height: 100%;
  position: relative;
  width: 100%;
}
.card-back .layer:after {
  background: linear-gradient(135deg, rgba(0, 0, 0, 0.2), transparent);
  content: '';
  display: block;
  height: 100%;
  left: 0;
  position: absolute;
  top: 0;
  width: 100%;
}
.card-back .layer .top,
.card-back .layer .bottom {
  left: 8%;
  position: absolute;
  z-index: 1;
}
.card-back .layer .top {
  top: 6%;
}
.card-back .layer .bottom {
  bottom: 6%;
}
.card-back .layer h2, .card-back .layer h3, .card-back .layer h4 {
  font-weight: 400;
  margin: 2px 0;
}
.card-back .layer h2 {
  font-size: 18px;
  font-weight: 500;
  text-transform: uppercase;
}
.card-back .layer h3 {
  font-size: 16px;
}
.card-back .layer h4 {
  font-size: 16px;
  font-style: italic;
}

.page_footer{
  position:;
            bottom:;
            width: 100% 
             }


.a 
{
  padding-right: 53px;
  margin-top: 5px;
  padding-left: 30px;
  font-weight: lighter;
}

.card-row{
  margin-left: 73px;
}

.font{
  font-size: 14px;
}
@media only screen and (max-width: 768px) {
    /* For mobile phones: */
   /* [class*="col-"] {
        width: 100%;
    }*/
  .a 
    {
      padding-right: 9px;
      margin-top: 3px;
      padding-left: 0px;
    }
  .card-row{
    margin-left: -30px;
    /*width: 40%;*/
  }
  .card{
    width: 350px;
  }
  .font{
   font-size: 13px;
   
}
  /*.container1{
    width: 400px;
    margin: 0px;
  }*/

}

.modal
{
  /*padding-top: 60px;*/
}


</style>

  </head>
  <body>

    <!-- added header -->
   <!-- ===========PAGE HEADER=========== -->

<div class="container-fluid  navbar-default " role="navigation" style="background-color:#004167;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-2">
      <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="margin-top: 19px">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='col-md-offset-1' style="margin-bottom: 10px;margin-top: 10px;" />
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse col-md-10 pull-right" id="bs-example-navbar-collapse-1">
      <span class="rspace col-md-12" style="padding-top:19px;" >        

            <?php $attribute=array("class"=> "form-inline");
              echo form_open('user/login',$attribute);?>
               <?php if($this->session->flashdata('msg1')){?>
                      <div class="form-group alert" style="float-left;height: 40px;padding-top: 10px;margin-right:10px;"><?=$this->session->flashdata('msg1')?></div>
                     <!-- <script type="text/javascript"> document.write("error");</script>  -->
              <?php }?>
              <div class="form-group">
              
              <input type="email"  class="form-control " name="email" placeholder="Email" required style="margin-left: 5px;">
              </div>
              <div class="form-group">
              <input type="password" class="form-control " name="password" placeholder="Password"  required style="margin-left: 5px;">
              </div>
              <div class="form-group">
              <input type="submit" class="form-control btn btn  " name="submit" value="LOG IN" 
                     style="color:white;background-color:transparent;border-color:white;height:30px;border-radius:10px;margin-left:10px;">
             
             </div>
             <div class="form-group">
             <label class="control-label" style="color:white;margin-left:10px;margin-right:20px;" data-toggle="modal" data-target="#myModal">Forgot Password?</label>  
             </div>           
             </form>           
     
      </span>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->

<!-- </nav> -->
<!-- Modal for change password -->
<div class="modal fade" id="mymodal" style="margin-top:50px;">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center><h4 class="modal-title">Change Your Password</h4></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/changepwd', 'class="form-horizontal" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-5">Current Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="current" placeholder="Current password...">            
            </div>
           
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="new" placeholder="New password...">               
            </div>
            
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Confirm New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">               
            </div>

       </div>
       <div class="form-group">
           <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Change</button>
       </div>
     <?php echo form_close();?>
     
    </div>
    
</div>

    
</div> 
</div>




<script  type="text/javascript" src="//platform.linkedin.com/in.js">

    function click() {

        alert("here");

        api_key: 868ezwj38m14cr
        authorize: true
        onLoad: onLoad

    }


 </script>

<script>



    function onLoad() {

        try {
            IN.User.logout();
        } catch (err) {
            console.log(err);
        }
        // setTimeout("goToHome()", 10000);
    }

//}



</script>
<!-- ==============End Modal================== -->

<br><br><br>
<main class="container">
  <center><h1>Career</h1>
  <p><h3>Changing the future for millions of young people.<br/> Come join us.</h3><br/><hr></center>
    <div class="container">
    <div class="row col" style="font-size: 18px;">
      <!-- <div class="col-md-6"> -->
    We’re looking for driven young leaders who want to shape their future with MYEO. If you
have the drive, passion, ambition and capacity to learn to solve the challenges we are
addressing then look no further.
We’re always looking for the brightest talent the world has to offer to advance our mission.
What you should know about us at MYEO before we go any further:
<!-- </div> -->
</div>
<div class="row" style="font-size: 18px;padding-top: 10px">
<div class="col-md-6">
  <ul>
<li>We don’t care where you’re from, but only where you’re going.</li>
<li>We don’t care where you’re based, but only where your journey will take you.</li>
</ul>
</div>
<div class="col-md-6">
  <ul>
<li>We don’t care whether you have a degree or not, we know talent when we see it.</li>
<li>We don’t care how much experience you have, only how you learn from your past to shape
your future.</li>
</ul>
</div>
</div>
<div class="row" style="font-size: 18px;">
  If you believe in our mission, vision, values and have the talent and drive to thrive with
MYEO, we need to hear from you.<hr>
  </div>
</div>
    <!-- test -->

     <div class="row card-row">
      <div class="col-md-6 col-sm-12 col-xs-12 card1">
    <div class="container1">
  <div class="card">
        <div class='card-front'>
      <div class='layer'>
        <h1 style="">Engineering Evangelist</h1>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
      </div>
    </div>
     <div class='card-back'>
      <div class='layer'>
        <div class='top'>
          <h2>Engineering Evangelist</h2>
        </div>
        <div class='bottom'>
           <p>We’re on the hunt for kick-ass web and mobile (android) developers to help build awesome
                  products for our community of MYEOers. Years of experience nor degree needed, just a
                  thirst for knowledge, hacking together awesome new features and getting creative!
                  Send over your portfolio of amazing web and/or mobile products and CV to
                  <a href="http://www.gmail.com">jointhefamily@mymyeo.com</a>&hellip;</p>
        </div>
      </div>
    </div>
  <!-- </div> -->
    </div>
  </div>
</div> <!-- endof col-md-6 -->
      <div class="col-md-6  col-sm-12 col-xs-12">
    <div class="container1">
  <div class="card">
        <div class='card-front'>
      <div class='layer'>
        <h1 style="">Dazzling Designer</h1>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
      </div>
    </div>
     <div class='card-back'>
      <div class='layer'>
        <div class='top'>
          <h2>Dazzling Designer</h2>
        </div>
        <div class='bottom'>
            <p>We’ve got an awesome roadmap of new features and products for our MYEOer community
        and need awesome graphic design talent to bring this to life. We’re looking for the brightest
        creative talent to join MYEO in giving our community the user experience and products they
        deserve!
        Send over your portfolio of awesome creations and your CV to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
        </div>
      </div>
    </div>
  <!-- </div> -->
    </div>
  </div>
</div> <!-- endof col-md-6 -->
</div> <!-- endof row -->
<br><br>
<div class="row card-row">
      <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="container1">
  <div class="card">
        <div class='card-front'>
      <div class='layer'>
        <h1 style="">Growth Guru</h1>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
      </div>
    </div>
     <div class='card-back'>
      <div class='layer'>
        <div class='top'>
          <h2>Growth Guru</h2>
        </div>
        <div class='bottom font'>
              <p>MYEO is only as valuable as the community of MYEOers using our platforms, content and
        finding new opportunities. We need an awesome growth team to ensure every young person
        across the world does not miss out on their next big opportunity and fulfill their potential.
        Send over your CV and an answer to the question “What marketing strategy would you
        develop and how would you execute it to grow the user base of MYEO?” to
        <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
        </div>
      </div>
    </div>
  <!-- </div> -->
    </div>
  </div>
</div> <!-- endof col-md-6 -->
 <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="container1">
  <div class="card">
        <div class='card-front'>
      <div class='layer'>
        <h1 style="">Content Crafter</h1>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
      </div>
    </div>
     <div class='card-back'>
      <div class='layer'>
        <div class='top'>
          <h2>Content Crafter</h2>
        </div>
        <div class='bottom'>
             <p>MYEO thrives off the world-class professional skill development content hosted on our
platforms. We need awesome content writers with a visionary outlook to address the skills
gaps present in young people for today as well as the world of tomorrow.
Send over your CV and a sample article written in the style you see across the MYEO
platform focused on developing a skill of your choice to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
        </div>
      </div>
    </div>
  <!-- </div> -->
    </div>
  </div>
</div> <!-- endof col-md-6 -->
</div> <!-- endof row -->
<br><br><br><br>

    <div class="row card-row">
      <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="container1">
  <div class="card">
        <div class='card-front'>
      <div class='layer'>
        <h1 style="">Veteran Videographer</h1>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
      </div>
    </div>
     <div class='card-back'>
      <div class='layer'>
        <div class='top'>
          <h2>Veteran Videographer</h2>
        </div>
        <div class='bottom font'>
             <p>The content world is shifting heavily into video. To keep crafting engaging content for our
        MYEOer community we need the most creative forward looking videography crew to bring
        our content to life. If you have the editorial, creativity and passion to craft amazing skill
        development video content for the MYEOer community then you’ve come to the right place.
        Send over your portfolio of amazing creations and a storyboard for a new video concept for
        our platform developing a skill of your choice to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
        </div>
      </div>
    </div>
  <!-- </div> -->
    </div>
  </div>
</div> <!-- endof col-md-6 -->
      <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="container1">
  <div class="card">
        <div class='card-front'>
      <div class='layer'>
        <h1 style="">Sassy Sales</h1>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
      </div>
    </div>
     <div class='card-back'>
      <div class='layer'>
        <div class='top'>
          <h2>Sassy Sales</h2>
        </div>
        <div class='bottom font'>
                <p>We work with some of the biggest companies and organisations in the world to address their
        human capital needs and recruitment. We’re on the hunt for the best communicators,
        presenters and lead generators to scale the impact we are able to offer to our youth
        community and the world’s leading organisations.
        Send over your CV and a written concept note for a novel and creative way you would
        approach selling our talent subscription product to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
        </div>
      </div>
    </div>
  <!-- </div> -->
    </div>
  </div>
</div> <!-- endof col-md-6 -->
</div> <!-- endof row -->
<br><br><br><br>
    <div class="row card-row">
      <div class="col-md-6 col-sm-12 col-xs-12">
    <div class="container1">
  <div class="card">
        <div class='card-front'>
      <div class='layer'>
        <h1 style="">Campus Chapter Champion</h1>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
        <div class='corner'></div>
      </div>
    </div>
     <div class='card-back'>
      <div class='layer'>
        <div class='top'>
          <h2>Campus Chapter Champion</h2>
        </div>
        <div class='bottom font'>
             <p>The Campus Chapter Program is integral to the development and impact MYEO is able to
        have with our MYEOer community. Involved in shaping, scaling and supporting the growth of
        our community through our physical Campus Chapter network, you will need exemplary
        communication, organisation and strategic skills as well as a vision for where you take this
        Program.
        Send over your CV and an answer to the question “How would you design the Campus
        Chapter Program to be as scalable and impactful as possible for our on the ground MYEOer
        community?” to <a href="">jointhefamily@mymyeo.com</a>&hellip;</p>
        </div>
      </div>
    </div>
  <!-- </div> -->
    </div>
  </div>
</div> <!-- endof col-md-6 -->
</div><!--  end of row -->
<br><br><br><br>
    <!-- end test -->



           

        </div>

      </div>

  




</main>


</div>
<!-----------------------TKH forgot Model------------------->
<div class="modal " id="myModal">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <center><h3 class="modal-title"> Reset Your Password</h3></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/ForgotPassword', 'class="form-inline" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-4" style="margin-top:10px;">Email:</label>
            <div class="col-md-4">
               <input type="email" class="form-control" name="email" placeholder="Enter your Email..." required>            
            </div>
           
       </div>
       
       <div class="form-group">
        <div class="col-md-offset-5 col-md-4">
           <input type="submit" name="submit" class="form-control btn btn-success" value="Change">
         </div>
       </div>
     <?php echo form_close();?>
    
    </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
      </div>
    </div>
  </div>
</div>

 <!--end of forgot model-->   
