      <aside class="main-sidebar" >

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <!-- <img src="<?=base_url();?>images/pp.PNG" class="img-circle" alt="User Image" /> -->
            </div>
            <div class="pull-left info">
              
              <!-- Status -->
              <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
          </div>


          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
           
            <!-- Optionally, you can add icons to the links -->
            <li ><a href="<?=base_url();?>custom" ><span class="font-color">HOME</span></a></li>    
          
            <li class="treeview">
                <a href="<?=base_url();?>opportunity"> <span class="font-color">Scholarships</span> 
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 <a href="<?php echo base_url();?>Opportunity/oppdetail/Undergraduate"  class="mfont">
                        <font class="mfont">Undergraduate</font><input type="text" name="feed" value="Undergraduate" style="display: none;"> 
</a><Br>
                      <a href="<?php echo base_url();?>Opportunity/oppdetail/Graduate"  class="mfont">
                        <font class="mfont">Graduate</font></a><Br>
                      <a href="<?php echo base_url();?>Opportunity/oppdetail/Ph.D" class="mfont">
                        <font class="mfont">Ph.D</font></a><Br>
              </ul>
            </li>

             <li class="treeview">
                <a href="<?=base_url();?>skill"><span class="font-color">Conferences & Training</span> 
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <a href="<?php echo base_url();?>opportunity/oppdetail/Conferences" class="mfont">
                                                     <font class="mfont" value="Conferences">Conferences</font></a><Br>    
                                                    <a href="<?php echo base_url();?>opportunity/oppdetail/Training"  class="mfont">
                                                     <font class="mfont">Training</font></a><Br>  
              </ul>
            </li>

             <li class="treeview">
                <a href="<?=base_url();?>skill"><span class="font-color">Other Opportunities</span> 
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <a href="<?php echo base_url();?>opportunity/oppdetail/Internships" class="mfont">
                                                 <font class="mfont">Internships</font></a><Br>          
                                                <a href="<?php echo base_url();?>opportunity/oppdetail/ExchangePrograms"  class="mfont">
                                                 <font class="mfont">Exchange Programs</font></a><Br>          
                                                  <a href="<?php echo base_url();?>opportunity/oppdetail/Competitions"  class="mfont">
                                                 <font class="mfont">Competitions</font></a><Br>          
                                                <a href="<?php echo base_url();?>opportunity/oppdetail/VolunteeringOpportunities"  class="mfont">
                                                 <font class="mfont">Volunteering Opportunities</font></a><Br>  
                                                 <!-- <a href="<?php echo base_url();?>opportunity/oppdetail/Opportunities"  class="mfont">
                                                 <font class="mfont">Opportunities</font></a><Br>         -->
                                                  <a href="<?php echo base_url();?>opportunity/oppdetail/Fellowships"  class="mfont">
                                                 <font class="mfont">Fellowships</font></a><Br>          
              </ul>
            </li>

             <li class="treeview">
                <a href="#">
                <span class="font-color">LOCATION</span> 
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <a href="<?php echo base_url();?>Opportunity/location/Local"  class="mfont">
                                                  <font class="mfont">Local</font></a><Br>          
                                                <a href="<?php echo base_url();?>Opportunity/location/International" class="mfont">
                                                  <font class="mfont">International</font></a><Br>          
              </ul>
            </li>
            
           <!--  <li class="treeview">
                <a href="#">
                <i class="fa fa-cogs"></i> <span>Settings</span> 
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li ><a href="<?=base_url();?>category"><i class="fa fa-bullseye"></i></i><span>Category</span></a></li>
                <li ><a href="<?=base_url();?>language"><i class="fa fa-language"></i><span>Language</span></a></li> 
                <li ><a href="<?=base_url();?>profile"><i class="fa fa-user"></i><span>Profile</span></a></li> 
              </ul>
            </li>
           -->
          
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>