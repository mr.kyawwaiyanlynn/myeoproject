 <html xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
  <link rel="icon" type="image/icon" href="<?=base_url();?>images/myeo.jpg">
  <title>MYEO(Myanmar Youth Empowerment Opportunities)</title>
    <meta name="viewport" content="width=device-width,initital-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/bootstrap.min.css">
    <!-- coded by mk -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>index.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/>
    <!-- end of code by mk -->
    <script type="text/javascript" src="<?php echo base_url();?>template/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>template/js/bootstrap.min.js">
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>template/css/index-login-page.css">
    <script src ="<?php echo base_url();?>jquery.com/jquery-1.10.2.js"></script>
    <script src ="<?php echo base_url();?>jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>template/js-king/jquery.js"></script> 
    <script src="<?php echo base_url();?>template/js-king/jquery.glide.js"></script>
    <script src="<?php echo base_url();?>template/js-king/jquery.localScroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>template/js-king/jquery.scrollTo.min.js" type="text/javascript"></script> 
    <script src="<?php echo base_url();?>template/js-king/wow.min.js" type="text/javascript"></script>  
<style type="text/css">
  * {box-sizing:border-box}

 /*Slideshow container */
/*.slideshow-container {
}*/

/*.mySlides {*/
    display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: white; 
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
}
  
/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor:pointer;
  height: 13px;
  width: 13px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}
</style>
  
<script type="text/javascript">
$(document).ready(function() {
   $('#navigations').localScroll({duration:800});
});
</script>

<script type="text/javascript">


   $(function(){
      $("#btnclick").click(function(){
        $("divpopup").dialog({
          title : " Pop up",
          width : 200,
          height : 100,
          modal:true,
          buttons: {Close;
                function(){
                  $(this).dialog('close');
                }
            }
        });
      });
   })
</script>

<script type="text/javascript">
$(function(){
    $("#gotologin").click(function(){
     
    })
  function gotologin(){alert("Are You Sure To login!!!!")};
</script>

<script type="text/javascript">
$(document).ready(function() {
   $('#navigations').localScroll({duration:800});
});
</script>


<!--for  nav active-->


<style>
/* The Modal (background) */
.modal1 {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal1-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal1-header {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}

.modal1-body {padding: 2px 16px;}

.modal1-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}

/*=============ANCHOR LINK STYLE==========*/
.a:hover{text-decoration: none;}
.a:active{color: green;}
/*=============END of ANCHOR LINK STYLE==========*/


/*=================FOOTER ====================*/
.pg_header{
  position: fixed;
  top: 0;
  width: 100%;
}
.page_footer{
  position:fixed;
            bottom: 0;
            width: 100% 
             }

.right_title{
  position: fixed;
}
.a 
{
  padding-right: 53px;
  margin-top: 5px;
}
}
.page_footer1{position:fixed;
              bottom: 0;
              width: 100%;

}
.background
{
  /*background: url(images/myeo-background2.jpg);*/
  background: url(images/background-myeo.png);
  -moz-background-size: cover;
  -webkit-background-size:cover;
  background-size:cover;
  min-height: 190px;
  background-repeat: no-repeat;
  background-position: center;
}
.rspace
{
  /*text-align: right;*/
}

.container
{
  height: 450px;
  font-family:'Open Sans',sans-serif;

}

.container input[type="text"],
.container input[type="email"],
.container input[type="password"]
{
  border:none;
  border-bottom: 1px solid #fff;
  background: transparent;
  outline: none;
  height: 30px;
  color: #fff;
  font-size: 16px;
}
.container button[type="submit"]
{
  border: 1px solid transparent;
  outline:none; 
  width:100%;
  height: 40px;
  color:#fff;
  font-size: 16px;
  background: #004167;
  cursor: pointer;
  border-radius: 10px;
  text-decoration: none;
}
.container button[type="submit"]:active
{
  /*background: #004167;*/
  color: #fff;
  text-decoration: none;
  text-indent: none;
  border: hidden;

}
  button::-moz-focus-inner{border:0;}
.sign-up-with-email a 
{
  text-align: right;
}
.form-group button[typr="submit"]
{
  border: 2px solid #fff;
  outline:none;
  font-size: 16px;
  color:#fff;
  background: transparent;
  cursor: pointer;
  border-radius: 10px;
}
/*.container button[type="submit"]:hover
{
  background: #004167;
  color: #fff;
}*/
.sign-up-with-email a 
{
  text-align: right;
}
.form-group button[type="submit"]
{
  border: 2px solid #fff;
  outline:none;
  font-size: 16px;
  color:#fff;
  background: transparent;
  cursor: pointer;
  border-radius: 10px;

}

body{
  width: 100%;
  font-family: Open Sans;
}

.page_footer, .pg_header{
  background-color: #004167;
}

.container-fluid{
  position: fixed;
  top: 0;
  width: 100%;
}
.alert 
{
  background-color: red;
  color:white;
}

</style>






</head>
<body >
	<br><br><br><br>
 <div class="container-fluid  navbar-default " role="navigation" style="background-color:#004167;" >
    <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header col-md-2">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          <img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='col-md-offset-1' style="margin-bottom: 10px;margin-top: 10px;" />
      </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse col-md-10 pull-right" id="bs-example-navbar-collapse-1">
<!-- 
      <span class="rspace col-md-offset-5 col-md-7"> -->
      <?php $attribute=array("class"=> "form-inline");
        echo form_open('user/login',$attribute);?>
        
        <div class="form-group" style="padding-top:20px;">
        
        <input type="email"  class="form-control " name="email" placeholder="Email" required>
        
        <input type="password" class="form-control " name="password" placeholder="Password" required>
        
        <input type="submit" class="btn btn " name="submit" value="LOG IN" 
               style="color:white;background-color:transparent;border-color:white;height:30px;border-radius:10px;margin-left:10px;">
        
       </div>
       <label  style="color:white;padding-top:15px;padding-left:25px;" data-toggle="modal" data-target="#myModal">forgot password?</label>
       </form>
       
     </div></div></div>
     <!--  </span> -->

  </div>

  </div>

<!-- kowaiyan start -->
<br><br>
<center>
	<div class="jumbotron" style="height:400px;width:400px;align:center;">
       <?php echo form_open(base_url().'user/forgot', 'class="form-horizontal" method="post" ')?>
           <p class="h5 text-center mb-4">Comfirm your email</p><br><br>
              <div class="container">
                 <div class="form-group">
    	             <!-- <div class="row"> -->
                        
                       <label class="control-label">Enter Code Number</label>
                       
                       
                       <input type="text" name="digit" class="form-control" placeholder="# # # # # #">
                   
                      <!-- </div> -->
                 </div>
<br><br>
          
             <input type="submit" class="btn btn-primary" value="comfirm" style="padding-left:100px;padding-right:100px;">
   
</div>
</form></div>

</center>
<!-- Form login -->
  <div class="page_footer  navbar-default " role="navigation" style="background-color:#004167 ">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-1">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
          <img src="<?=base_url();?>/images/myeo_logo.png"  width="40" height="40" class='col-md-offset-1' style="margin-bottom: 5px;margin-top: 5px;margin-left: 40px;margin-right:20px;" />
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ol class="nav navbar-nav " style="display: inline;">
         <li style="margin-left: 35px"><a href="#" style="text-decoration:none;"><font style="color:#fff"><span style="text-align: right"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#fff"><span class="glyphicon glyphicon-copyright-mark"></span>2017</font></span></font></a></li>

        <li style="margin-left: 40px">
          <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">About</font></a>
        </li>

        <li style="margin-left: 40px"> 
                <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">Career</font></a>
        </li>

        <li style="margin-left: 40px"> 
              <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">Compus Chapter Program </font></a>
        </li>

        <li style="margin-left: 40px"> 
              <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">User Aggrement</font></a>
        </li>

        <li style="margin-left: 40px"> 
                <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">Cookie Policy</font></a>
        </li>

        <li style="margin-left: 40px;margin-right:50px"> 
                <a href="<?php echo base_url() ?>footer/about" style="text-decoration:none;"><font style="color:#fff">Copyright Policy</font></a>
        </li>
       
   
      </ol>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</body>
</html>
