

<div class="wrapper">

      <!-- Main Header -->
      <header class="main-header" style="position: fixed;width: 15%;float: left;">

        <!-- Logo -->
        <a href="<?= base_url();?>custom" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?=base_url();?>images/myeo_logo.png" width="60" height="50"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='nav-img '/></span>
        </a>

        <!-- Header Navbar -->
          
        <nav class="navbar navbar-static-top col-md-12 col-sm-11 col-xs-11" role="navigation">
          <!-- Sidebar toggle button-->

          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
<div>

          <!-- <a href="<?=base_url();?>custom" class="col-md-offset-1 col-md-2 col-sm-2 col-xs-2 nav-link" style="text-decoration: none;outline: none;">
            <label class="<?php echo activate_menu('custom'); ?>" >Home</label><!-- <span style="padding-left: 65px;color: white;margin-right:"></span> -->
            <a href="<?=base_url();?>custom" class=" col-md-2 col-sm-1 col-xs-1 nav-link" style="text-decoration: none;outline: none;">
            <label class="<?php echo activate_menu('home'); ?>" >Home<span class="line">|</span></label></a>
          <a href="<?=base_url();?>opportunity" class="col-md-offset-2 col-sm-offset-2 col-xs-offsset-2 col-md-2 col-sm-1 col-xs-1 nav-link" style="text-decoration: none;outline: none;">
            <label class="<?php echo activate_menu('opportunity'); ?>" >Opportunity<span class="line">|</span></label></a>
          <a href="<?=base_url();?>skill" class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2 col-md-2 col-sm-1 col-xs-1 nav-link" style="text-decoration: none;outline: none;"><label class="<?php echo activate_menu('skill'); ?>">Skill</label></a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account Menu -->
             

            </ul>
          </div>
        </nav>
      </header>

      <?php if ($this->session->flashdata('msg_diff')) { ?>
                    <div class="alert alert-danger"><?= $this->session->flashdata('msg_diff') ?></div>
      <?php } ?>

    
      <!-- Left side column. contains the logo and sidebar -->



     <script>

         function logout(){
             IN.User.logout(function() {

                 ////window.location = "<?=base_url();?>logout/logout"
                    window.location.reload()
             });
         }




     </script>



  <?php 
  
                $encrypt_ids=$this->encrypt->encode($pwd["user_id"]);
                $modify_encrypt=str_replace("/","~",$encrypt_ids);  
                // var_dump($pwd);die();                         
  ?>
<?php echo form_open_multipart('user/changepwd/' . $modify_encrypt); ?>

<div style="padding-top: 100px">

    <div class="modal-dialog">
        <div class="modal-content">

              <?php if ($this->session->flashdata('msg1')) { ?>
                    <div class="alert alert-danger"><?= $this->session->flashdata('msg1') ?></div>
            <?php } ?>

            <div class="modal-header" style="background:#004167;color:#fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title">Change Your Password</h4></center>
            </div>
            <div class="modal-body">
                <?php echo form_open(base_url() . 'user/changepwd', 'class="form-horizontal" method="post" ') ?>

                <div class="form-group">
                    <label class="control-label col-md-5">Current Password:</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control" name="current" placeholder="Current password...">
                    </div>
                </div>
                <br><br>
                <div class="form-group">
                    <label class="control-label col-md-5">New Password:</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control" name="new" placeholder="New password...">
                    </div>
                </div><br><br>
                <div class="form-group">
                    <label class="control-label col-md-5">Confirm New Password:</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">
                    </div>
                </div><br><br>
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-success col-md-offset-7 ">Change</button>
                    <!-- <a href="<?= base_url()?>user/changepwd/<?=$modify_encrypt ?>" class="btn">Change</a> -->

                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>



