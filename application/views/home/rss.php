<?php 
echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";

?>
<rss version="2.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:admin="http://webns.net/mvcb/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"    
    >
    <channel>
    <title><?php echo $feed_name; ?><?php echo $feed_description; ?></title>
    <link><?php echo $feed_url; ?> </link>
    <description><?php echo $page_description; ?></description>
    <dc:language><?php echo $page_language; ?></dc:language>
    <dc:creator><?php echo $creator_email; ?></dc:creator>
    <dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>
    <admin:generatorAgent rdf:resource="http://www.codeigniter.com/" />
   <?php
 if(is_array($ARTICLE_DETAILS) && count($ARTICLE_DETAILS) ) {     
  foreach($ARTICLE_DETAILS as $loop){ 
  ?>    
        <item>
          <title><?php echo xml_convert($loop->title); ?></title>
          <link><?php echo site_url('' . $loop->ARTICLE_URL) ?></link>
          <guid><?php echo site_url('' . $loop->ARTICLE_URL) ?></guid>
          <description><?php echo xml_convert($loop->ARTICLE_DESCR); ?></description>      
        </item>        
   <?php } } ?>    
    </channel>
</rss>