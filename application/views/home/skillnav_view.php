<!-- <nav class="navbar navbar-default col-md-12" role="navigation" style="background-color: #004167"> -->
  <div class="container-fluid  navbar-default " role="navigation" style="background-color:#004167;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-md-2">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <img src="<?=base_url();?>/images/myeo_logo.png"  width="60"  height="50" class='col-md-offset-1' style="margin-top:10px;"/>
    </div>
   <div class="col-md-2"></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ol class="nav navbar-nav col-md-6 ">
        <li class="col-md-3"><a href="<?=base_url();?>custom" id="nav_a" style="padding-left:55px;font-size: 20px;text-decoration: none;outline:none;color: white" >Home<span style="padding-left: 65px;color: white;margin-right:">|</span></a></li>
        <li class="col-md-4"> <a href="<?=base_url();?>opportunity" id="nav_a" style="padding-right:30px;padding-left: 55px;outline:none;font-size: 20px;text-decoration:none;color: white">Opportunities<span style="padding-left: 35px;color: white">|</span></a></li>
         <li class="col-md-4"><a href="<?=base_url();?>skill" id="nav_a" style="padding-left:55px;font-size: 20px;text-decoration: none;outline:none;color: #85EC14" tabindex="1">Skills</a></li>
       
      </ol>

      <ol class="dropdown nav navbar-nav navbar-right" style="margin-right: 0px;margin-top: 15px">
        <!-- <li class="dropdown"> --> 
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?=base_url();?>images/pp.png" width="35px"><b class="caret"></b></a>
          <br><br><ol class="dropdown-menu" style="background-color:#CCC">
                <div style="background-color: #CCC;width:100%;">
                  <label style="padding-right:30px;" id="button"><span class="glyphicon glyphicon-user"></span>&nbsp;<?php echo $name['name'];?></label>
                 <a href="<?=base_url();?>profile/edit/<?= $name['user_id']?>" style="padding-right:45px;text-decoration:none;" class="button"><span class="glyphicon glyphicon-edit"></span>&nbsp;Edit Profile</a><br/> 
                  <label  data-toggle="modal"  class="button" data-target="#mymodal" > <span class="glyphicon glyphicon-edit"></span>&nbsp;Change Password</label>
                  <a href="<?=base_url();?>logout/logout" style="padding-right:60px;"  class="button"> <span class="glyphicon glyphicon-log-out"></span>&nbsp;Log Out</a>
                </div>
            </ol>
        <!-- </li> -->
      </ol>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
<!-- </nav> -->
<!-- Modal for change password -->
<div class="modal fade" id="mymodal">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header" style="background:#004167;color:#fff;">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center><h4 class="modal-title">Change Your Password</h4></center>
    </div>
    <div class="modal-body">
     <?php echo form_open(base_url().'user/changepwd', 'class="form-horizontal" method="post" ')?>

       <div class="form-group">
           <label class="control-label col-md-5">Current Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="current" placeholder="Current password...">            
            </div>
           
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="new" placeholder="New password...">               
            </div>
            
       </div>
       <div class="form-group">
           <label class="control-label col-md-5">Confirm New Password:</label>
            <div class="col-md-5">
               <input type="password" class="form-control" name="cfmnew" placeholder="Retype new password...">               
            </div>

       </div>
       <div class="form-group">
           <button type="submit" name="submit" class="btn btn-success col-md-offset-6 ">Change</button>
       </div>
     <?php echo form_close();?>
     <?php echo $this->session->flashdata('msg');?>
    </div>
    
</div>
    
</div> 
</div>
<!-- ==============End Modal================== -->